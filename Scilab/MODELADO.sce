/*=====================================================================*/
/*                        MODELO DEL MOTOR                             */
/*=====================================================================*/

close
clear

pi = 3.14159265359;

/*================== MODELO TEÓRICO MATEMÁTICO ========================*/

//Parámetros característicos
Ke = 0.0035;                      //Constante eléctrica          [Vs/rad]
R = 0.8;                          //Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       //Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       //Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      //Momento de inercia           [Nms^2]

tau_m = (J*3*R)/(Ke*Kt);
tau_e = L/(3*R);

