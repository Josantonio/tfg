close
close
close
close
close
j=1;

/*================== MODELO TEÓRICO MATEMÁTICO ========================*/

//Parámetros característicos
Ke = 0.00335;                     //Constante eléctrica          [V/RPM]
Ke_r = 0.00335*60/(2*%pi);        //Constante eléctrica          [Vs/rad]
R = 0.8;                          //Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       //Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       //Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      //Momento de inercia           [Nms^2]
v_app = 6;

tau_m = (J*3*R)/(Ke_r*Kt);
tau_e = L/(3*R);

//Función de transferencis
num = poly([1/(Ke*tau_e*tau_m)], 's', 'coeff');
den = poly([1/(tau_e*tau_m) 1/tau_e 1], 's', 'coeff');
p = roots(den);                    //Obtenemos polos del denominador
G = num/den

t_max =0.2;
t=[0:0.0001:t_max]';

plzr(G)
a = get("current_axes");
a.font_size = 4;
a.thickness = 1;
a.background = color("white");
f = gcf();
xlabel('Eje Real', "fontsize", 4, "color", "black");
ylabel('Eje Imaginario', "fontsize", 4, "color", "black");
title('Polos de G(s)', "fontsize", 5, "color", "black");



/*======================== FILTRO PASO BAJAS ==========================*/
/*=====================================================================*/
fc = 1;                                   //Frecuencia de corte
wc = 2*%pi*fc;
num = poly(1,'s','coeff');
den = poly([1 1/wc], 's', 'coeff');
LPF = num/den;
Ts = 20e-3;

//Discretización del filtro
LPF_d = ss2tf(dscr(tf2ss(LPF),Ts));         //Aproximación 'zoh'
//LPF_d = ss2tf(cls2dls(tf2ss(LPF),Ts));    //Aproximación 'tustin'


/*===================== Efectos de Ts y Wc ============================*/

ncolor = ["red" "blue" "orange" "green" "yellow" "purple" "black" "gray" "brown"]
                          
fc = 5:50:500;                          //Para varias fc
wc = 2*%pi*fc;

figure(j)
j = j+1;
Ts=[50e-6:1e-6:10e-3]';

for i=1:length(wc)-1
    plot2d(Ts, exp(-2*%pi*wc(i)*Ts),style=[color(ncolor(i))]);xgrid(2);
    str(i,:) = 'fc = ' + string(fc(i)) + ' Hz'; 
    e=gce();
    e.children(1).thickness=1.5; 
end
a = get("current_axes");
a.font_size = 4;
a.thickness = 1;
a.background = color("white");
ylabel("a", "font_size", 4);
xlabel("Ts [s]", "font_size", 4)
//title("Influencia de Ts y Wc sobre a","font_size", 4);
gcf().background = color("white");
legend(str);




/*==== Representación de la respuesta en frecuencia en función de a =====*/
i=1;                                        //índice
th = %pi*(-1:0.001:1)';                     //Vector de frecuencias

for a=0.1:0.1:0.9
    H(i,:) = syslin('d',(1-a)*%z/(%z-a)); //Filtro discreto
    //H(i,:) = syslin('d',(%z)/(4*%z-3));
    comp(i,:) = horner(H(i,:), exp(%i*th)); //Evaluamos en e^(j*w)
    
    //Para la leyenda
    str(i,:) = 'a = ' + string(a);
    
    i = i + 1;                              //Incrementamos contador
end


figure(j)
j = j+1;

//MAGNITUD
subplot(211)
for k=1:i-1
  plot2d(th, abs(comp(k,:)), rect=[-%pi,0,%pi,1], style=[color(ncolor(k))]);xgrid(2); 
  e=gce();
  e.children(1).thickness=1.5; 
end
a = get("current_axes");
a.font_size = 4;
a.thickness = 1;
a.background = color("white");
ylabel("Magnitud [dB]", "font_size", 4);
xlabel("Frecuencia [rad/sample]", "font_size", 4)
title("Filtro paso baja","font_size", 4);
gcf().background = color("white");

//FASE
subplot(212)
for k=1:i-1
    plot2d(th, atan(imag(comp(k,:)),real(comp(k,:)))*360/%pi, rect=[-%pi,-180,%pi,180], style=[color(ncolor(k))]);
    e=gce();
    e.children(1).thickness=1.5;
end
a = get("current_axes");
a.font_size = 4;
a.thickness = 1;
a.background = color("white");
xgrid(2);
ylabel("Fase [º]","font_size", 4);
xlabel("Frecuencia [rad/sample]", "font_size", 4)
title("");
legend(str);






/*================ RESPUESTA AL ESCALÓN de 6V ================================*/
gs1 = csim('step',t,G*v_app);

figure(j)
j = j+1;               
plot(t,gs1,'r', "thickness",1.5);
xgrid;
k = get("current_axes");
k.font_size = 4;
k.thickness = 1;
k.background = color("white");
f = gcf();
f.background = color("white");
legend('Modelo G(s)')
xlabel('Tiempo [s]', "fontsize", 4, "color", "black");
ylabel('Velocidad [rpm]', "fontsize", 4, "color", "black");
title("Respuesta a un escalón de 6V", "fontsize", 5, "color", "black");
