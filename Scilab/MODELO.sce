/*=====================================================================*/
/*                        MODELO DEL MOTOR                             */
/*=====================================================================*/

close
close
close
close
close
clear
j = 1;


/*================== MODELO TEÓRICO MATEMÁTICO ========================*/

//Parámetros característicos
Ke = 0.00335;                     //Constante eléctrica          [V/RPM]
Ke_r = 0.00335*60/(2*%pi);        //Constante eléctrica          [Vs/rad]
R = 0.8;                          //Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       //Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       //Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      //Momento de inercia           [Nms^2]

tau_m = (J*3*R)/(Ke_r*Kt);
tau_e = L/(3*R);

//Función de transferencis
num = poly([1/(Ke*tau_e*tau_m)], 's', 'coeff');
den = poly([1/(tau_e*tau_m) 1/tau_e 1], 's', 'coeff');

Gm = num/den

t_max = 2;                    //Para la representación, eje x [s]



/*===================== DATOS EXPERIMENTALES ==========================*/
/*=====================================================================*/
cd D:\Documentos\UNIVERSIDAD\TFG\Scilab\
data = fscanfMat('respuesta6V_escalonT1m_Fc5.txt')

//Definición de índices
c_wreal = 1;
c_tiempo = 7;

[f,c] = size(data);

for i=1:f-1
    data(i,c+1) = data(i+1,c_tiempo)-data(i,c_tiempo);
end
c_diff = c+1;


data(1,c+2) = 0;
for i=2:f-1
    data(i,c+2) = data(i-1,c+2)+data(i,c_diff);
end
c_tiempo_real = c+2;

//Eliminamos última fila
data(f,:) = [];


//Cogemos hasta t máx para la representación
for i=1:f
    if data(i,c_tiempo_real)>t_max
        break;
    end
end

data = data(1:i-1,:);

[f,c] = size(data);                 //Actualizamos coeficientes



/*======================== FILTRO PASO BAJAS ==========================*/
/*=====================================================================*/
fc = 5;                                   //Frecuencia de corte
wc = 2*%pi*fc;
num = poly(1,'s','coeff');
den = poly([1 1/wc], 's', 'coeff');
LPF = num/den;
Ts = 20e-3;

//Discretización del filtro
LPF_d = ss2tf(dscr(tf2ss(LPF),Ts));         //Aproximación 'zoh'
//LPF_d = ss2tf(cls2dls(tf2ss(LPF),Ts));    //Aproximación 'tustin'


/*===================== Efectos de Ts y Wc ============================*/
//a = exp(-wc*Ts)

ncolor = ["red" "blue" "orange" "green" "yellow" "purple" "black" "gray" "brown"]
                          
fc = 5:50:500;                          //Para varias fc
wc = 2*%pi*fc;

figure(j)
j = j+1;
Ts=[50e-6:100e-6:10e-3]';

for i=1:length(wc)-1
    plot2d(Ts, exp(-2*%pi*wc(i)*Ts),style=[color(ncolor(i))]);xgrid(2);
    str(i,:) = 'fc = ' + string(fc(i)) + ' Hz'; 
    e=gce();
    e.children(1).thickness=1.5; 
end
ylabel("a", "font_size", 4);
xlabel("Ts [s]", "font_size", 4)
title("Influencia de Ts en a","font_size", 4);
gcf().background = color("white");
legend(str);




/*==== Representación de la respuesta en frecuencia en función de a =====*/
i=1;                                        //índice
th = %pi*(-1:0.001:1)';                     //Vector de frecuencias

for a=0.1:0.1:0.9
    H(i,:) = syslin('d',(1-a)*%z/(%z-a)); //Filtro discreto
    //H(i,:) = syslin('d',(%z)/(4*%z-3));
    comp(i,:) = horner(H(i,:), exp(%i*th)); //Evaluamos en e^(j*w)
    
    //Para la leyenda
    str(i,:) = 'a = ' + string(a);
    
    i = i + 1;                              //Incrementamos contador
end


figure(j)
j = j+1;

//MAGNITUD
subplot(211)
for k=1:i-1
  plot2d(th, abs(comp(k,:)), rect=[-%pi,0,%pi,1], style=[color(ncolor(k))]);xgrid(2); 
  e=gce();
  e.children(1).thickness=1.5; 
end

ylabel("Magnitud [dB]", "font_size", 4);
xlabel("Frecuencia [rad/sample]", "font_size", 4)
title("Filtro paso baja","font_size", 4);
gcf().background = color("white");

//FASE
subplot(212)
for k=1:i-1
    plot2d(th, atan(imag(comp(k,:)),real(comp(k,:)))*360/%pi, rect=[-%pi,-180,%pi,180], style=[color(ncolor(k))]);
    e=gce();
    e.children(1).thickness=1.5;
end
xgrid(2);
ylabel("Fase [º]","font_size", 4);
xlabel("Frecuencia [rad/sample]", "font_size", 4)
title("");
legend(str);









/*=========================== GRÁFICAS ================================*/
/*=====================================================================*/
Ts = 1e-3
t = 0:1e-4:t_max;                               //Vector del tiempo
td = 0:Ts:t_max;                                //Vector tiempo discreto

v_app = 5;                                      //Voltaje aplicado

//wc = 2*%pi*5;
//a = exp(-2*wc*Ts);
//F_d = syslin('d',(1-a)/(1-a*%z^-1));
Y_d = ss2tf(dscr(tf2ss(LPF*Gm),0.020));         //Aproximación 'zoh'

//Y_d = F_d*LPF_d;


/*=================== POLOS Y CEROS DE Y(z) ===========================*/
figure(j)
j = j+1;
plzr(Y_d)
a = get("current_axes");
a.font_size = 4;
a.thickness = 1;
a.background = color("white");
a.x_label.font_size = 4;
a.y_label.font_size = 4;
a.title.font_size = 5;
f = gcf();
f.background = color("white");



/*================ RESPUESTA AL ESCALÓN ================================*/
gs1 = csim('step',t,Gm*LPF*v_app); 
gs1_d = flts(ones(1,length(td))*v_app, Y_d);

figure(j)
j = j+1;               
plot(t,gs1,'r', "thickness",1.5);
plot2d2(td, gs1_d,style=[color("green")]);
e=gce();
e.children(1).thickness=1.5;
plot(data(:,c_tiempo_real),data(:,c_wreal),"thickness",1.5);
xgrid;
a = get("current_axes");
a.font_size = 4;
a.thickness = 1;
a.background = color("white");
f = gcf();
f.background = color("white");
legend('Modelo','H(z)','Real', "fontsize", 4)
xlabel('Tiempo [s]', "fontsize", 4, "color", "black");
ylabel('Velocidad [rpm]', "fontsize", 4, "color", "black");
title("Respuesta a un escalón de 6V", "fontsize", 5, "color", "black");



//Lugar de las raíces del sistema
figure(j)
j = j+1;
evans(Gm)
