close
j=1;

/*================== MODELO TEÓRICO MATEMÁTICO ========================*/

//Parámetros característicos
Ke = 0.00335;                     //Constante eléctrica          [V/RPM]
Ke_r = 0.00335*60/(2*%pi);        //Constante eléctrica          [Vs/rad]
R = 0.8;                          //Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       //Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       //Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      //Momento de inercia           [Nms^2]
v_app = 5;

tau_m = (J*3*R)/(Ke_r*Kt);
tau_e = L/(3*R);

//Función de transferencis
num = poly([1/(Ke*tau_e*tau_m)], 's', 'coeff');
den = poly([1/(tau_e*tau_m) 1/tau_e 1], 's', 'coeff');
p = roots(den);                    //Obtenemos polos del denominador
K = num;
A = [1 1 1;p(1)+p(2) p(2) p(1);p(1)*p(2) 0 0];
B = [0;0;K];
X = inv(A)*B;


Gm = num/den

t_max =2;
t=[0:0.001:t_max]';

/*==================== RUIDO DE ALTA FRECUENCIA ========================*/
Fns = 20e3;                         //Frecuencia del ruido
A = 1;                            //Amplitud del ruido
noise = A*sin(2*%pi*Fns*t);



/*====================== FILTRO DIGITAL ================================*/
fc = 5;
Ts = 51e-3;

////z = poly(0, "z");
a = exp(-2*%pi*fc*Ts);
//a = 0.7;
NUM = %z;
DEN = 4*%z-3;


/*========================= Filtro FIR =================================*/
h = ffilt('lp', 10, 1, 500);

[hzm,fr]=frmag(h, 10);
figure(j)
j = j +1;
plot(fr,hzm)


/*===================== DATOS EXPERIMENTALES ==========================*/
/*=====================================================================*/
cd D:\Documentos\UNIVERSIDAD\TFG\Scilab\
data = fscanfMat('respuesta6V_escalonT51m_5.txt')

//Definición de índices
c_wreal = 1;
c_tiempo = 7;

[f,c] = size(data);

for i=1:f-1
    data(i,c+1) = data(i+1,c_tiempo)-data(i,c_tiempo);
end
c_diff = c+1;


data(1,c+2) = 0;
for i=2:f-1
    data(i,c+2) = data(i-1,c+2)+data(i,c_diff);
end
c_tiempo_real = c+2;

//Eliminamos última fila
data(f,:) = [];


//Cogemos hasta t máx para la representación
for i=1:f
    if data(i,c_tiempo_real)>t_max
        break;
    end
end

data = data(1:i-1,:);

[f,c] = size(data);                 //Actualizamos coeficientes




/*================ RESPUESTA AL ESCALÓN ================================*/
gs1 = csim('step',t,Gm*v_app);
//y = X(1) + X(2)*exp(p(1)*t) + X(3)*exp(p(2)*t);
res = filter(NUM, DEN, gs1);

figure(j)
j = j+1;               
plot(t,gs1,'r', "thickness",1.5);
plot(t,res, 'b', "thickness", 1.5);
plot(data(:,c_tiempo_real),data(:,c_wreal),'g',"thickness",1.5);
xgrid;
k = get("current_axes");
k.font_size = 4;
k.thickness = 1;
k.background = color("white");
f = gcf();
f.background = color("white");
legend('Modelo Gm(s)', 'Gm(s) filtrada', 'Real')
xlabel('Tiempo [s]', "fontsize", 4, "color", "black");
ylabel('Velocidad [rpm]', "fontsize", 4, "color", "black");
title("Respuesta a un escalón de 1V", "fontsize", 5, "color", "black");
