close all
graphics_toolkit fltk
pkg load control


%Filtro paso baja
fc = 500;             %Frecuencia de corte en Hz
wc = 2*pi*fc;         %Frecuencia en rad/s
wsignal_max = 266;
i = 1;

%En el espacio continuo de laplace
num = 1;
den =[1/wc 1];
LPF = tf(num,den)



%%%%%% BODE %%%%%
figure(i)
i++;
f = logspace(1, 5, 200);            % 200 pt f vector from 100 Hz (10^2) to 10kHz (10^5)
[mag, phase] = bode(LPF,2*pi*f);    % Use bode to determine mag and phase
subplot(2,1,1);
semilogx(f, 20*log10(abs(mag)));
grid on
title 'Respuesta en frecuencia del filtro'
ylabel 'Magnitud dB'
subplot(2,1,2);
semilogx(f, phase);
grid on
xlabel 'Frecuencia Hz'
ylabel 'Fase Grados'


%%%% DISCRETIZACIÓN %%%%

Ts = 1e-5;                            %Período de muestreo

LPFd = c2d(LPF,Ts,'zoh')              %Discretización con la aproximación zoh
LPFd_tustin = c2d(LPF,Ts,'tustin')    %Discretización con la aproximación 'tustin'

%%%% RESPUESTA AL IMPULSO %%%%
t = [0:1e-5:0.003];                   %Vector de tiempo para la representación
t1 = [0:Ts:0.003];
figure(i)
i++;
step(LPF, t, 'g');hold on; step(LPFd, t1, 'r');hold on; step(LPFd_tustin,t1); hold off;
h = legend ({"Continua", "zoh", "tustin"});
legend (h, "location", "northeastoutside");
set (h, "fontsize", 20);