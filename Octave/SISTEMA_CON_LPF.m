close all
clear all
pkg load control
pkg load symbolic
j =1;

%===============================================================%
%         CONTROLADOR PI POR ASIGNACI�N DE POLOS                %
%===============================================================%


%OBTENCI�N DE LOS PAR�METROS DEL SISTEMA

%Abrimos  la respuesta al escal�n 0.5u(t) del sistema
DATA = importdata('D:/Documentos/UNIVERSIDAD/TFG/Octave/respuesta6V_escalon.txt');

nfilas = rows(DATA); %N�mero de filas de A

%Editar dependiendo del fichero
i_wreal = 1;
i_wref = 2;
i_time = 8;
i_lapso = columns(DATA)+1;
i_real_t = columns(DATA)+2;

%Calculamos diferencia de tiempo entre sample y sample
for i=1:nfilas-1
  DATA(i,i_lapso) = DATA(i+1,i_time) - DATA(i,i_time);
endfor

%Calculamos el nuevo eje del tiempo, con origen en t=0
 DATA(1,i_real_t) = 0;
 
 for i=1:nfilas-1
    DATA(i+1,i_real_t) = DATA(i,i_real_t) + DATA(i,i_lapso);
 endfor
 
 %Obtenemos la ganancia
 Kp = 4*max(DATA(:,i_wreal));
 
 %Valor en el que alcanza ts
 H_ts = 0.98*0.5*Kp;
 
 for i=1:nfilas-1
   if (DATA(i,i_wreal) > H_ts)   %Hemos encontrado el valor
     Ts = DATA(i, i_real_t)
     break                      %Asignamos Ts y salimos
   endif
 endfor

%FILTRO PASO BAJAS
fc = 300;             %Frecuencia de corte en Hz
wc = 2*pi*fc;         %Frecuencia en rad/s

%En el espacio continuo de laplace
num = 1;
den =[1/wc 1];
LPF = tf(num,den)

%Bode del filtro
figure(j)
j++;
f = logspace(1, 5, 200);            % 200 pt f vector from 100 Hz (10^2) to 10kHz (10^5)
[mag, phase] = bode(LPF,2*pi*f);    % Use bode to determine mag and phase
subplot(2,1,1);
semilogx(f, 20*log10(abs(mag)));
grid on
title 'Respuesta en frecuencia del filtro'
ylabel 'Magnitud dB'
subplot(2,1,2);
semilogx(f, phase);
grid on
xlabel 'Frecuencia Hz'
ylabel 'Fase Grados'

%Calculamos tau
tau = -Ts/(log(0.02))


%Sistema de 1er orden en el espacio continuo:
num = Kp;
den =[tau 1];
G = tf(num,den)

%Respuesta temporal a 0.5u(t)
t = DATA(:,i_real_t);
%G_t = 0.5*Kp*(1-1*exp(-1*t/tau));
%H = G*LPF

%REPRESENTAMOS RESPUESTA AL ESCAL�N de 0.5u(t)
figure(j)
j++;
%Real
plot(t, DATA(:,i_wreal),'r',"marker", '*');
grid on
hold on;

%G(s) sin LPF
%plot(t,G_t);
step(0.5*G, 'b', t(nfilas),"marker", 'x');
hold off;


%G(s) + LPF(s)
%step(0.5*H, 'g', t(nfilas),"marker", 'O');
%hold on;
legend('Real', 'G(s)');


%DISE�O DEL CONTROLADOR

Tss = 0.75*(tau*4);       %Reducci�n del 50% en Ts
Mp = 0.001;                 % 2% de Mp permitido

xi = sqrt((log(Mp/100))^2/(pi^2+(log(Mp/100))^2)) %Factor de amortiguamiento

%Criterio del 2%
wn = 4/(xi*Tss)

Pd = [1 2*xi*wn wn^2]

a1 = Pd(2);
a2 = Pd(3);

%Obtenemos par�metros del PI
Kc = (a1*tau-1)/Kp
tau_i = (Kc*Kp)/(a2*tau)

C = tf(Kc*[tau_i 1],[tau_i 0])

%LAZO CERRADO
H = tf((C*G)/(1+C*G))
H_c = minreal(H)

figure(j)
j++;
%Model
step(0.5*G, 'b',t(nfilas));
grid on
hold on;

%Con PI
step(0.5*Kp*H_c, 'r',t(nfilas));
legend('Modelo', 'Con PI');
hold off;

figure(j)
j++;
pzmap(H_c)

figure(j)
j++;
pzmap(G)


%Discretizamos el controlador PI

%Ancho de banda de G(s)
BW = 1/tau
fs = 40*BW      %La frecuencia de muestreo tiene que ser al menos 30 veces el ancho de banda
T = 1/fs        %Per�odo de muestreo

%PI discreto por aproximaci�n 'zoh'
PI_d = c2d(C, T, 'zoh')

figure(j)
j++;
step(C);
hold on;


%td =[0:T:t(nfilas)];
%step(PI_d);

legend('PI continuo', 'PI discreto');
