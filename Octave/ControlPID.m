close all
clear all
pkg load control
pkg load symbolic
j =1;

%===============================================================%
%                     CONTROLADOR PID                           %
%===============================================================%


%===============================================================%
%                        TE�RICO                                %
%===============================================================%

Ke = 0.00335;               % [Vs/rad]  Constante el�ctrica
Ke_r = 0.00335*60/(2*pi);        %Constante el�ctrica          [Vs/rad]
R = 0.8;                                % [ohm]     Resistencia bobinados
L = 1.2e-3;                             % [H]       Inductancia bobinados
Vn = 24-0.2*2;                                % [V]       Tensi�n nominal
Kt = 5.81*0.27801385176568*0.0254;      % [Nm/A]    Constante de torque
J = 6.8e-4*0.27801385176568*0.0254;     % [Nms^2]   Inercia del rotor

tau_m = J*3*R/(Ke_r*Kt);
tau_e = L/(3*R);

%Sistema de la planta
Gm = tf([1/Ke*Vn],[tau_m*tau_e tau_m 1])




%===============================================================%
%                     LOW PASS FILTER                           %
%===============================================================%

fc = 5;             %Frecuencia de corte en Hz
wc = 2*pi*fc;         %Frecuencia en rad/s
Ts = 1e-3

%En el espacio continuo de laplace
num = 1;
den =[1/wc 1];
LPF = tf(num,den)

%Discretizaci�n del filtro por aproximaci�n 'zoh'
LPF_d = c2d(LPF, Ts, 'zoh')        %Per�odo de muestreo 51 us

%RESPUESTA AL IMPULSO
figure(j)
j++;
step(LPF)
hold on
step(LPF_d, 'r')
legend('LPF(s)', 'LPF(z)');

%===============================================================%
%                     EXPERIMENTAL                              %
%===============================================================%

%OBTENCI�N DE LOS PAR�METROS DEL SISTEMA

%Abrimos  la respuesta al escal�n 0.5u(t) del sistema
DATA = importdata('D:/Documentos/UNIVERSIDAD/TFG/Scilab/respuesta6V_escalonT1m_Fc5.txt');

nfilas = rows(DATA); %N�mero de filas de A

%Editar dependiendo del fichero
i_wreal = 1;
i_wref = 2;
i_time = 7;
i_lapso = columns(DATA)+1;
i_real_t = columns(DATA)+2;

%Calculamos diferencia de tiempo entre sample y sample
for i=1:nfilas-1
  DATA(i,i_lapso) = DATA(i+1,i_time) - DATA(i,i_time);
endfor

%Calculamos el nuevo eje del tiempo, con origen en t=0
 DATA(1,i_real_t) = 0;
 
 for i=1:nfilas-1
    DATA(i+1,i_real_t) = DATA(i,i_real_t) + DATA(i,i_lapso);
 endfor
 

%===============================================================%
%           REPRESENTACI�N RESPUESTA A ESCAL�N 0.5*u(t)         %
%===============================================================%
figure(j)
j++;
t = DATA(:,i_real_t);

%REAL
plot(t, DATA(:,i_wreal),'r',"marker", '*');
grid on
hold on;


%MODELO
Y_d = c2d(LPF*Gm, Ts, 'zoh') 
step(Y_d*0.25, 'b', t(nfilas),"marker", 'x');
Max_m = max(step(0.25*Gm*60/(2*pi), t(nfilas)))
hold off;
%legend('Real', 'Modelo');



