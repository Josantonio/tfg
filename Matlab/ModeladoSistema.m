close all
clear
j=1;

%================== MODELO TE�RICO MATEM�TICO ========================%

%Par�metros caracter�sticos
Ke = 0.00335;                     %Constante el�ctrica          [V/RPM]
Ke_r = 0.00335*60/(2*pi);         %Constante el�ctrica          [Vs/rad]
R = 0.8;                          %Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       %Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       %Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      %Momento de inercia           [Nms^2]
Vdd = 24;                         %Tensi�n de alimentaci�n      [V]

tau_m = (J*3*R)/(Ke_r*Kt);
tau_e = L/(3*R);

wn = 1/sqrt(tau_e*tau_m);
ep = 1/2*sqrt(tau_m/tau_e);

%Funci�n de transferencia W(S)/V(S) [RPM]/[V]
num = [1/(Ke*tau_e*tau_m)];
den = [1 1/tau_e 1/(tau_e*tau_m)];
Gm = tf(num,den)

%Funci�n de transferencia con duty W(S)/d(s) [RPM]/[duty %]
Gm_dc = tf(num*Vdd,den)

sigma = roots(den);
a = den(2);
b = den(3);
K = num;

%VECTOR DE TIEMPOS
t_max = 1;
t=[0:0.00001:t_max];

%Discretizaci�n
Ts = 20e-3;
fc = 1;
wc = 2*pi*fc;
t_d=0:Ts:t_max;

%================== PASA BAJA IMPLEMENTADO ============================%
wc = 2*pi*fc;
LPF = tf([wc],[1 wc]);

LPF_d = c2d(LPF, Ts, 'zoh')

[H_LPF,W] = freqz(LPF_d.Numerator{:,:},LPF_d.Denominator{:,:},10000);

f = W/(2*pi);
F = f/Ts;

figure(j)
j = j + 1;
set(gcf,'Name','Filtro LPF');
subplot(2,1,1)
plot(F,20*log10(abs(H_LPF)))
ylabel('Magnitud [dB]')
grid on;
subplot(2,1,2)
plot(F,unwrap(angle(H_LPF)))
ylabel('Fase [rad]')
xlabel('Frecuencia [Hz]')
title('Respuesta en frecuencia del filtro LPF')
grid on;

%================= UBICACI�N POLOS DEL SISTEMA CON FILTRO  ===============%
figure(j)
j = j+1;
pzmap(Gm*LPF)
title('Polos de Gm(s)*LPF(s)')




%===================== DATOS EXPERIMENTALES ==========================*/
%=====================================================================*/
cd D:\Documentos\UNIVERSIDAD\TFG\Scilab\
data = importdata('respuesta6V_20m_1H.txt');

%Definici�n de �ndices
c_wreal = 1;
c_tiempo = 7;

[f,c] = size(data);

for i=1:f-1
    data(i,c+1) = data(i+1,c_tiempo)-data(i,c_tiempo);
end
c_diff = c+1;


data(1,c+2) = 0;
for i=2:f-1
    data(i,c+2) = data(i-1,c+2)+data(i,c_diff);
end
c_tiempo_real = c+2;

%Eliminamos �ltima fila
data(f,:) = [];


%Cogemos hasta t m�x para la representaci�n
for i=1:f
    if data(i,c_tiempo_real)>t_max
        break;
    end
end

data = data(1:i-1,:);

[f,c] = size(data);                 %Actualizamos coeficientes


%=============== TIEMPO DE ESTABLECIMIENTO =============================%

% REAL
K_real = max(data(:,c_wreal));

for i=1:f
    if data(i,c_wreal) >= 0.98*K_real
        ts_real = data(i,c_tiempo_real);
        break
    end  
end


%TE�RICO
%Considerando que wc es el polo dominante
ts_teorico = log(0.02)/(-wc);

%Sin aprox
y = step(t,Gm*LPF);
K = max(y);
for i=1:length(t)
    if y(i) >= 0.98*K
        ts = t(i);
        break
    end  
end

%======================== ZIEGLER NICHOLS ==============================%
%Aproximamos el sistema a uno de primer orden
% wc es el polo dominante, podemos decir que:
fa = 1;                   %Factor ajuste ganancia
Kp = Vdd/Ke*fa; 
P = tf(Kp,[1/wc 1]);
PID_T = 20e-3;


%Buscamos el Retardo
for i=1:f
    if data(i,c_wreal)>0
        theta = data(i,c_tiempo_real)
        break;
    end
end
tau = 1/wc;

%El factor de controlabilidad tiene que estar entre 0.1 <= L/tau <= 0.3 
factor_contr = theta/tau;

theta = theta + PID_T/2

Kp_Z = 1.2*tau/(Kp*theta);
ti_Z = 2*theta;
td_Z = 0.5*theta;

Kp_Z = Kp_Z;
td_Z = td_Z;
ti_Z = ti_Z;

r = 0.1;
s = tf('s')
%PID_Ziegler = tf([Kp_Z*td_Z*ti_Z*(r+1) Kp_Z*ti_Z+r*Kp_Z*td_Z Kp_Z],[r*td_Z*ti_Z ti_Z 0]);
PID_Ziegler = Kp_Z*(1+1/(ti_Z*s)+td_Z*s)
H_Ziegler = minreal((PID_Ziegler*P)/(1+PID_Ziegler*P))

figure(j)
j = j+1;
pzmap(H_Ziegler)
title('Polos y ceros de H ziegler')


%================= PI POR ASIGNACI�N DE POLOS ==========================%
tss = 0.7*ts_teorico;      %Queremos reducir ts un 30%
Mp = 0.5;                    %Sobredisparo m�ximo del 5%

ep = sqrt((log(Mp/100))^2/(pi^2+(log(Mp/100))^2));
wn = 4/(tss*ep);

pol_des = [1 2*ep*wn wn^2];
p_ds = roots(pol_des);

beta1 = pol_des(2);
beta2 = pol_des(3);

kc_PI = (beta1*tau-1)/Kp;
ti_PI = (kc_PI*Kp)/(beta2*tau);

PI = tf([kc_PI*ti_PI kc_PI],[ti_PI 0]);
H_PI = minreal(PI*P/(1+PI*P));

figure(j)
j = j+1;
pzmap(H_PI)
title('Polos y ceros de H por PI')

%========================== DISE�O PID =================================%
%====================== ASIGNACI�N DE POLOS ============================%

%================ APROXIMACI�N G(s) ==============================%

% Consideramos p y sigma2 dominantes, sigma1 alejado del eje imaginario
num = Vdd/Ke*sigma(2)*(-wc)*fa;
den = poly([sigma(2) -wc]);
G = tf(num, den);
figure(j)
j = j+1;
pzmap(G)
title('Ubicaci�n polos de G(s)')

Gain = num;
a = den(2);
b = den(3);

%============================== DISE�O ===================================%
tss = 0.7*ts_teorico;      %Queremos reducir ts un 20%
Mp = 5;                    %Sobredisparo m�ximo del 10%

%Par�metros del sistema de segundo orden deseado
ep_d = sqrt((log(Mp/100))^2/(pi^2+(log(Mp/100))^2));
%wn_d = 4/(ep_d*tss);
wn_d = -log(0.02*sqrt(1-ep_d^2))/(ep_d*tss)

%Polinomio de segundo orden deseado
Pol_d = [1 2*ep_d*wn_d wn_d^2];
pd = roots(Pol_d);

%Ponemos un polo real alejado
p1 = real(pd(1))*50;

%Obtenemos el nuevo polinomio de grado 3 deseado:
deseado = poly([pd;p1]);

alpha1 =  deseado(2);
alpha2 =  deseado(3);
alpha3 =  deseado(4);

%Par�metros del PID
kc = (alpha2 - b)/Gain;
ti = Gain*kc/alpha3;
td = (alpha1-a)/(Gain*kc);

%FT del controlador
alpha = 1;            %Filtro paso bajo en el derivativo
d2 = alpha*kc*ti*td+kc*ti*td;
d1 = kc*ti+alpha*kc*td;
d0 = kc;

C = tf([d2 d1 d0],[alpha*td*ti ti 0])
PID = tf([td*ti kc*ti 1],[ti 0])

%LAZO CERRADO
%H = minreal(C*G/(1+C*G));
H = minreal(PID*G/(1+PID*G));

%POLOS DEL SISTEMA REALIMENTADO
figure(j)
j = j+1;
pzmap(H)
title('Polos de H(s) por ubicaci�n de polos')

%================ RESPUESTA AL ESCAL�N ================================%
Gm_d = c2d(Gm, Ts, 'zoh');
y_d = Gm_d*LPF_d;


v_app = 5.6;
rpm = v_app*Kp;

figure(j)
j = j+1; 
step(t,Gm*v_app,'r');                                   %Modelo matem�tico Gm(s)
hold on;
step(t,Gm*v_app*LPF,'y');                               %Modelo y filtro Gm(s) + LPF(s) = Y(s)
step(t_d,y_d*v_app,'b');                                %Discretizado Gm(z) + LPF (z) = Y(z)
plot(data(:,c_tiempo_real),data(:,c_wreal), 'g');
step(t, G*0.25, 'black');                              %Aproximaci�n segundo orden G(s) omitiendo sigma1
step(t, P*0.25, '--');                                  % P(s) modelo
legend('Modelo Gm(s)', 'Gm(s) con filtro', 'Gm(z) con filtro', 'Real', 'G(s) aprox', 'P(s) aprox')
ylabel('Velocidad [rpm]');
grid on;



%==================== ENTRADA DUTY CYCLE =================================%
duty = 0.25;
wref = duty*Kp;


%====================== ZIEGLER NICHOLS ==================================%
figure(j)
j = j+1;
step(t,Gm_dc*duty,'r');                                 %Modelo matem�tico
hold on;
plot(data(:,c_tiempo_real),data(:,c_wreal), 'g');       %Experimental
step(t, P*duty, '--');                                  %Aproximaci�n primer orden P(s)
step(t, H_Ziegler*wref, 'm');                           %Realimentado con el controlador Z-N
legend('Modelo Gm(s)','Real', 'Modelo P(s)', 'H(s) Z-N')
ylabel('Velocidad [rpm]');
title('Respuesta a un duty del 25% M�todo: Ziegler-Nichols');
grid on;

%========================= PID ubicaci�n polos ===========================%
figure(j)
j = j+1;
step(t,Gm_dc*duty,'r');                                 %Modelo matem�tico
hold on;
plot(data(:,c_tiempo_real),data(:,c_wreal), 'g');       %Experimental
step(t, G*0.25, 'black');                               %Aproximaci�n segundo orden G(s) omitiendo sigma1
step(t, H*wref, 'm');
legend('Modelo Gm(s)','Real', 'G(s) aprox', 'H(s) PID ubicacion');
ylabel('Velocidad [rpm]');
title('Respuesta a un duty del 25% M�todo: Ubicaci�n de polos');
grid on;


%===================== PI ubicaci�n polos ================================%
figure(j)
j = j+1;
step(t,Gm_dc*duty,'r');                                 %Modelo matem�tico
hold on;
plot(data(:,c_tiempo_real),data(:,c_wreal), 'g');       %Experimental
step(t, G*0.25, 'black');                               %Aproximaci�n segundo orden G(s) omitiendo sigma1
step(t, H_PI*wref, 'm');
legend('Modelo Gm(s)','Real', 'G(s) aprox', 'H(s) PI ubicacion');
ylabel('Velocidad [rpm]');
title('Respuesta a un duty del 25% M�todo: Ubicaci�n de polos');
grid on;