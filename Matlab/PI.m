close all
clear
j=1;

%================== MODELO TE�RICO MATEM�TICO ========================%

%Par�metros caracter�sticos
Ke = 0.00335;                     %Constante el�ctrica          [V/RPM]
Ke_r = 0.00335*60/(2*pi);         %Constante el�ctrica          [Vs/rad]
R = 0.8;                          %Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       %Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       %Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      %Momento de inercia           [Nms^2]
Vdd = 24;                         %Tensi�n de alimentaci�n      [V]

tau_m = (J*3*R)/(Ke_r*Kt);
tau_e = L/(3*R);

wn = 1/sqrt(tau_e*tau_m);
ep = 1/2*sqrt(tau_m/tau_e);

%Funci�n de transferencia W(S)/V(S) [RPM]/[V]
num = [1/(Ke*tau_e*tau_m)];
den = [1 1/tau_e 1/(tau_e*tau_m)];
Gm = tf(num,den)

figure(j)
j = j+1;
pzmap(Gm)
grid on
title('Polos de G(s)')


sigma = roots(den);

%VECTOR DE TIEMPOS
t_max = 1;
t=[0:0.00001:t_max];

%Discretizaci�n
Ts = 10e-3;
fc = 1;
wc = 2*pi*fc;
t_d=0:Ts:t_max;


%================== PASA BAJA IMPLEMENTADO ============================%
wc = 2*pi*fc;
LPF = tf([wc],[1 wc]);

LPF_d = c2d(LPF, Ts, 'zoh')


%================= UBICACI�N POLOS DEL SISTEMA CON FILTRO  ===============%
figure(j)
j = j+1;
pzmap(Gm*LPF)
title('Polos de Gm(s)*LPF(s)')



%===================== DATOS EXPERIMENTALES ==========================*/
%=====================================================================*/
cd D:\Documentos\UNIVERSIDAD\TFG\Scilab\
data = importdata('respuesta12V_10m_1H.txt');

%Definici�n de �ndices
c_wreal = 1;
c_tiempo = 7;

[f,c] = size(data);

for i=1:f-1
    data(i,c+1) = data(i+1,c_tiempo)-data(i,c_tiempo);
end
c_diff = c+1;


data(1,c+2) = 0;
for i=2:f-1
    data(i,c+2) = data(i-1,c+2)+data(i,c_diff);
end
c_tiempo_real = c+2;

%Eliminamos �ltima fila
data(f,:) = [];


%Cogemos hasta t m�x para la representaci�n
for i=1:f
    if data(i,c_tiempo_real)>t_max
        break;
    end
end

data = data(1:i-1,:);

[f,c] = size(data);                 %Actualizamos coeficientes

K = max(data(:,c_wreal));
Ke_real = 10/K;


%Normalizamos resultado a entrada escal�n unitario
for i=1:f
    data(i,c_wreal) = data(i,c_wreal)/K;
end


%=============== TIEMPO DE ESTABLECIMIENTO =============================%

%Criterio
cr = 0.05;

% REAL
for i=1:f
    if data(i,c_wreal) >= 1-cr
        ts_real = data(i,c_tiempo_real);
        break
    end  
end


%TE�RICO
%Considerando que wc es el polo dominante
ts_teorico = -log(cr)/wc;

%Sin aprox
y = step(t,Gm*LPF);
K = max(y);
for i=1:length(t)
    if y(i) >= (1-cr)*K
        ts = t(i);
        break
    end  
end


%================ APROXIMACI�N A P DOMINANTE ============================%
p = wc;
P = tf(1/Ke_real*p, [1 p]);


%======================= CONTROLADOR PI ================================%

%Par�metros dise�o
Mp = 0.0001;
tss = 10e-3;

ep_d = sqrt((log(Mp/100))^2/(pi^2+(log(Mp/100))^2));
wn_d = -log(cr*sqrt(1-ep_d^2))/(tss*ep_d);

Ps = [1 2*ep_d*wn_d wn_d^2];

Ki = Ps(3)*Ke_real/p
Kp = (Ps(2)-p)*Ke_real/p

C = tf([Kp Ki],[1 0]);


%Sistema realimentado
H = minreal(C*P/(1+C*P));
%Ubicaci�n polos
figure(j)
j = j+1;
pzmap(H)
title('Polos de H(s)')


%=============== APROXIMACI�N A SEGUNDO ORDEN ===========================%
sig = abs(sigma(2));

P2 = tf([1/Ke*sig*p],[1 sig+p sig*p]);


beta = (sig+p)/(ep_d*wn_d-2)
Kp_2 = (wn_d^2*(1+2*beta*ep_d^2)-sig*p)/(1/Ke*sig*p)
Ki_2 = (beta*ep_d*wn_d^3)/(1/Ke*sig*p)

%================ RESPUESTA AL ESCAL�N ================================%
Gm_d = c2d(Gm, Ts, 'zoh');
y_d = Gm_d*LPF_d;


figure(j)
j = j+1; 
step(t,Gm*Ke,'r');                                   %Modelo matem�tico Gm(s)
hold on;
step(t,Gm*LPF*Ke,'y');                               %Modelo y filtro Gm(s) + LPF(s) = Y(s)
step(t_d,y_d*Ke,'b');                                %Discretizado Gm(z) + LPF (z) = Y(z)
plot(data(:,c_tiempo_real),data(:,c_wreal), 'g');    %Real
step(t, P*Ke, '--');                                 %Aproximaci�n a polo dominante
step(t, P2*Ke, 'k');                                      %Aproximaci�n con dos polos
step(t, H, 'm');                                     %Sistema con PI
legend('Modelo Gm(s)', 'Gm(s) con filtro', 'Gm(z) con filtro', 'Real', 'P(s)', 'P2(s)', 'H(s), PI')
ylabel('Velocidad [rpm]');
grid on;


%=================== RESPUESTA DEL CONTROLADOR PI ========================%
figure(j)
j = j+1; 
step(C);
