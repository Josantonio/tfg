clc
clear all
close all


%Par�metros del motor BLY172S-24V-4000 08/14
Ke = 0.00335;                   %Constante Back-EMF [V/rpm]
L = (1.2e-3)*3/2;                   %Inductacia en cada devanado [H]
R = (0.8)*3/2;                      %Resistencia en cada devanado[Ohm]
Kt = 5.81/141.61193227806;      %Constante de torque [Nm/A]
J_m = 0.00068/141.61193227806;  %Momento de inercia del motor [Kg*m^2]
Bm = 0;
tau_e = L/(3*R);                %Constante el�ctrica [s]
tau_m = (3*R*J_m)/(Kt*Ke);      %Constante mec�nica [s]
Vdd = 24;                       %Tensi�n de alimentaci�n [V]


%Filtro
fc = 500;
wc = 2*pi*fc;

%Funci�n de transferencia en lazo abierto
G = tf([1/Ke*Vdd],[tau_m*tau_e tau_m 1])   %G(s) = W(s)/d(s) Vel ang / duty

%Normalizada
H = tf([Vdd/(Ke*tau_m*tau_e)],[1 1/tau_e 1/(tau_m*tau_e)])

%Obtiene numerador y denominador de la FT
[num,den] = tfdata(H, 'v')

%Nombramos los t�rminos
K = num(3);
a = den(2);
b = den(3);

%Representaci�n de los ceros y polos de la ft en lazo abierto
figure(1)
pzmap(num,den);
title ('Ceros y polos FT lazo abierto');
grid on

%Respuesta al escal�n unitario
figure(2)
step(H,0.3)
title ('Respuesta a una entrada escalon de duty = 100%');
xlabel ('tiempo(s)');
ylabel ('vel (rpm)');
grid on

%Diagrama de Bode
figure(3)
bode(H)
grid on

% PAR�METROS DISE�O %

%Tss = 50e-3;     % 50ms tiempo establecimiento
Mp = 10;         % 10% sobredisparo

ep = sqrt((log(Mp/100))^2/(pi^2+(log(Mp/100))^2))   %Epsilon, factor de amortiguamiento
tau = 1/(abs(max(roots(den))))                      %Toma el valor del polo dominante, constante del sistema
Tss = (tau*4)*0.30                                  %Queremos que sea un 70% m�s r�pido
wn = 3/(ep*Tss)                                     %Frecuencia natural


p_d = [-ep*wn+wn*sqrt(1-ep^2)*i, -ep*wn-wn*sqrt(1-ep^2)*i]    %Polos conjugados del nuevo sistema deseado
p3 = real(p_d(1))*10                                          %Polo no dominante, alejado del origen
p_deseado = [p_d p3]                                         %A�adimos el tercer polo calculado
pol_ds = poly(p_deseado)                                     %Creamos el polinomio

alpha = 0.0;        %Par�metro para el filtro paso baja

%C�LCULO DEL CONTROLADOR
Kc = (alpha-b)/K
ti = (K*Kc)/pol_ds(4)
td = (pol_ds(2)-a)/(K*Kc)

% Control PID %
Kp = 15.61;
Ti = 4.84e-3;
Td = 1.21e-3;
H = tf([Kp*Td*Ti Ti*Kp Kp],[Ti 0]);

G_control = H*G/(H*G+1);
figure(3)
step(G_control,0.3)
title ('Respuesta a una entrada escalon de 1V lazo cerrado');
xlabel ('tiempo(s)');
ylabel ('vel (rpm)');
grid on

figura(4)
bode(G_control)