close
j=1;

%================== MODELO TE�RICO MATEM�TICO ========================%

%Par�metros caracter�sticos
Ke = 0.00335;                     %Constante el�ctrica          [V/RPM]
Ke_r = 0.00335*60/(2*pi);         %Constante el�ctrica          [Vs/rad]
R = 0.8;                          %Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       %Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       %Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      %Momento de inercia           [Nms^2]

tau_m = (J*3*R)/(Ke_r*Kt);
tau_e = L/(3*R);
wn = 1/sqrt(tau_m*tau_e);
xi = 0.5*sqrt(tau_m/tau_e);

p1 = -xi*wn+wn*sqrt(xi^2-1);
p2 = -xi*wn-wn*sqrt(xi^2-1);

%Funci�n de transferencis
num = [1/(Ke*tau_e*tau_m)];
den = [1 1/tau_e 1/(tau_e*tau_m)];
p = roots(den);                    %Obtenemos polos del denominador
K = num;
A = [1 1 1; -p1-p2 -p2 -p1; p1*p2 0 0];
%A = [1 1 1;-p(1)-p(2) -p(2) -p(1);-p(1)*p(2) 0 0];
B = [0;0;K];
X = inv(A)*B;


Gm = tf(num,den)

t_max = 15;
t=[0:0.001:t_max];

%==================== RUIDO DE ALTA FRECUENCIA ========================%
Fns = 20e3;                         %Frecuencia del ruido
An = 1;                              %Amplitud del ruido
noise = An*sin(2*pi*Fns*t);


%=================== FILTRO FIR =======================================%
Fc = 0.2;
Fs = 10;
wc = 2*pi*Fc/Fs;
L = 100;  %2*L es el orden del filtro
[B,A] = fir1(2*L, wc/pi, 'low');


%Con freqz obtenemos cada una de las funciones de transferencia de cada
%filtro, evaluadas para unos determinados puntos de frecuencia, el
%resultado son n�meros complejos
puntos = 10000; %N de puntos, valores que se eval�an de frecuencia entre 0 y pi
[H,W] = freqz(B,A,puntos); %Freqz nos da valores de W entre 0 y pi, no normalizados

f = W/(2*pi);
F = Fs*f;


figure(j)
j = j + 1;
set(gcf,'Name','Filtro');
subplot(2,1,1)
plot(F,20*log10(abs(H)))
ylabel('Magnitud [dB]')
subplot(2,1,2)
plot(F,unwrap(angle(H)))
ylabel('Fase [rad]')
xlabel('Frecuencia [Hz]')
title('Respuesta en frecuencia del filtro FIR')

figure(j)
j = j + 1;
freqz(B,A,10000)

%Fixed point
B_f = round(B*2^12);
writematrix(B_f, 'D:\Documentos\UNIVERSIDAD\TFG\Matlab\coeficientes.txt');



%=========================== FILTRO IIR ================================%
N = 5;
[B_IIR,A_IIR]=butter(N,wc/pi,'low');
H_IIR = freqz(B_IIR,A_IIR,puntos);

figure(j)
j = j + 1;
set(gcf,'Name','Filtro IIR');
subplot(2,1,1)
plot(F,20*log10(abs(H_IIR)))
ylabel('Magnitud [dB]')
subplot(2,1,2)
plot(F,unwrap(angle(H_IIR)))
ylabel('Fase [rad]')
xlabel('Frecuencia [Hz]')
title('Respuesta en frecuencia del filtro IIR')


%================== PASA BAJA IMPLEMENTADO ============================%
wc = 2*pi*0.1;
LPF = tf([1],[1/wc 1]);

%[B_LP,A_LP] = impinvar(num,den,Fs);
LPF_d = c2d(LPF, 1/Fs, 'zoh')

H_LPF = freqz(LPF_d.Numerator{:,:},LPF_d.Denominator{:,:},puntos);

figure(j)
j = j + 1;
set(gcf,'Name','Filtro LPF');
subplot(2,1,1)
plot(F,20*log10(abs(H_LPF)))
ylabel('Magnitud [dB]')
subplot(2,1,2)
plot(F,unwrap(angle(H_LPF)))
ylabel('Fase [rad]')
xlabel('Frecuencia [Hz]')
title('Respuesta en frecuencia del filtro LPF')


%===================== DATOS EXPERIMENTALES ==========================*/
%=====================================================================*/
cd D:\Documentos\UNIVERSIDAD\TFG\Scilab\
data = importdata('respuesta6V_100m_02H.txt');

%Definici�n de �ndices
c_wreal = 1;
c_tiempo = 7;

[f,c] = size(data);

for i=1:f-1
    data(i,c+1) = data(i+1,c_tiempo)-data(i,c_tiempo);
end
c_diff = c+1;


data(1,c+2) = 0;
for i=2:f-1
    data(i,c+2) = data(i-1,c+2)+data(i,c_diff);
end
c_tiempo_real = c+2;

%Eliminamos �ltima fila
data(f,:) = [];


%Cogemos hasta t m�x para la representaci�n
for i=1:f
    if data(i,c_tiempo_real)>t_max
        break;
    end
end

data = data(1:i-1,:);

[f,c] = size(data);                 %Actualizamos coeficientes


%================ RESPUESTA AL ESCAL�N ================================%
y = X(1) + X(2)*exp(p1*t) + X(3)*exp(p2*t) + noise;
Gm_d = c2d(Gm, 1/Fs, 'zoh');
y_d = Gm_d*LPF_d;
td=0:1/Fs:t_max;

v_app = 5;

figure(j)
j = j+1; 
step(t,Gm*v_app,'r');
legend('Modelo Gm(s)')
hold on;
step(td,y_d*v_app,'b');
plot(data(:,c_tiempo_real),data(:,c_wreal), 'g');
legend('Modelo Gm(s)', 'Gm(z) con filtro', 'Real')
ylabel('Velocidad [rpm]');