close all
clear
j = 1;


%================== MODELO MATEM�TICO DEL MOTOR ========================%

%Par�metros caracter�sticos
Ke = 0.00335;                     %Constante el�ctrica          [V/RPM]
Ke_r = 0.00335*60/(2*pi);         %Constante el�ctrica          [Vs/rad]
R = 0.8;                          %Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       %Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       %Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      %Momento de inercia           [Nms^2]
Vdd = 24;                         %Tensi�n de alimentaci�n      [V]

tau_m = (J*3*R)/(Ke_r*Kt);        %Constante de tiempo mec�nica     [s]
tau_e = L/(3*R);                  %Constante de tiempo el�ctrica    [s]

wn = 1/sqrt(tau_e*tau_m);         %Frecuencia natural               [rad/s]
xi = 1/2*sqrt(tau_m/tau_e);       %Factor de amortiguamiento      

%Funci�n de transferencia W(S)/V(S) [RPM]/[V]
num = 1/(Ke*tau_e*tau_m);
den = [1 1/tau_e 1/(tau_e*tau_m)];
G = tf(num,den);

sigma = roots(den);               %Polos del sistema
ts_aprox = -log(0.05)/abs(sigma(2));

%Ubicaci�n de los polos de la ft de la planta
figure(j)
j = j+1;
pzmap(G)
grid on
title 'Polos de G(s)'


%VECTOR DE TIEMPOS PARA LA REPRESENTACI�N
t_max = 0.3;                %M�x valor de t en la gr�fica
t= 0:0.00001:t_max;
cr = 0.05;                  %Criterio para tiempo de establecimiento


%===================== FILTRO PASO-BAJA  ==============================%

Ts = 200e-6;                %Per�odo de muestreo [s]
fc = 10;                    %Frecuencia de corte [Hz]
wc = 2*pi*fc;               % [rad/s]
t_d=0:Ts:t_max;             %Vector de tiempos discreto

LPF = tf(wc,[1 wc]);      %Funci�n de transferencia del filtro

LPF_d = c2d(LPF, Ts, 'zoh'); %Discretizaci�n por el m�todo del retenedor de orden cero


%================= UBICACI�N POLOS DEL SISTEMA CON FILTRO  ===============%
figure(j)
j = j+1;
pzmap(G*LPF)
title('Polos de G(s)*LPF(s)')


figure(j)
j = j+1;
rlocus(G*LPF);

%================= wn VS ts ==============================================%

%Sistema subamortiguado
xi = 0.91028;
ts = 1e-3:1e-5:1;         % ts posible desde 1ms a 1s

wn = -log(cr*sqrt(1-xi^2))./(xi*ts);

figure(j)
j = j+1;
loglog(ts, wn);
xlabel('Tiempo de establecimiento t_s [s]');
ylabel('\omega_n necesaria [rad/s]');
title('\omega_n en funci�n de t_s requerido, con \xi = 0.91');
grid on


%================ REPRESENTACI�N RESPUESTA ESCAL�N =======================%
v_app = 12;

%Discretizaci�n
G_d = c2d(G, Ts, 'zoh');
y_d = G_d*LPF_d;


figure(j)
j = j+1;
step(t, G*v_app, 'r');
hold on
step(t, G*LPF*v_app, '--');
step(t_d, y_d*v_app, 'g');
grid on;
legend('G(s)', 'G(s)*LPF(s)', 'G(z)*LPF(z)');
title('Respuesta a escal�n de 12V');
ylabel('Velocidad \omega [rpm]');


%======== TIEMPO DE ESTABLECIMIENTO ========%
y = step(t, G*LPF*v_app, '--');
 
for i=1:length(t)
    if y(i) >= (1-cr)*max(y)
        ts_teorico_LPF = t(i);
        break
    end  
end

%Iterando
ts_teorico_LPF_i = 0.1;             %Valor de partida
sig = abs(sigma(2));
for i=1:100
   ts_teorico_LPF_i = -log((wc*exp(-sig*ts_teorico_LPF_i)+cr*(sig-wc))/sig)/wc;
end




%====================== PI por ubicaci�n de polos ========================%

%============================================ CASO B aprox 0
Mp = 0.1;                                                   %Sobredisparo m�ximo deseado
xi = sqrt((log(Mp/100))^2/(pi^2+(log(Mp/100))^2));          %Valor de xi para Mp deseado
wn_lim = (sig+wc)/(2*xi) - 0.5;                           %Valor l�mite de wn, se resta 1 para ser inferior                            

beta = (sig+wc)/(xi*wn_lim)-2;

p = beta*xi*wn_lim;
Kp = (wn_lim^2*(2*beta*xi^2+1)-sig*wc)/(1/Ke*sig*wc);
Ki = (wn_lim^3*beta*xi)/(1/Ke*sig*wc);

%Funci�n de transferencia del controlador PI
PI = tf([Kp Ki],[1 0]);
G1 = tf(1/Ke*sig*wc,poly([-sig -wc]));

%Sistema realimentado
H = minreal((PI*G*LPF)/(1+PI*G*LPF));

%Ubicaci�n polos y ceros
figure(j)
j = j+1;
pzmap(H)
title('Polos y ceros de H(s), \beta \approx 0')

t_max = 5;                %M�x valor de t en la gr�fica
t=0:0.00001:t_max;


figure(j)
j = j+1;
step(t, G*LPF*v_app)
w_ref = max(step(t, G*LPF*v_app));
hold on
step(t, H*w_ref)
legend('Respuesta en lazo abierto', 'Respuesta en lazo cerrado')
title('Respuesta a escal�n de 12V');
ylabel('Velocidad \omega [rpm]');
grid on;

%Nuevo tiempo de estableicmiento, mucho m�s lento
y = step(t, H*w_ref);
K = max(y);
for i=1:length(t)
    if y(i) >= (1-cr)*K
        ts_PI_sub = t(i);
        break
    end  
end

%=================================================== CASO B = 1
xi = 0.889;                            %Valor del facor de amortiguamiento deseado
beta = 1;                               %Los tres polos alineados sobre el eje real
wn = (sig+wc)/((beta+2)*xi);            %Valor de wn                            

%Constantes del controlador
Kp = (wn^2*(2*beta*xi^2+1)-sig*wc)/(1/Ke*sig*wc);
Ki = (wn^3*beta*xi)/(1/Ke*sig*wc);


%Funci�n de transferencia del controlador PI
PI = tf([Kp Ki],[1 0]);

%Sistema realimentado
H = minreal((PI*G*LPF)/(1+PI*G*LPF));

%Ubicaci�n polos y ceros
figure(j)
j = j+1;
pzmap(H)
title('Polos y ceros de H(s), \beta = 1')


t_max = 0.2;                %M�x valor de t en la gr�fica
t=0:1e-6:t_max;


figure(j)
j = j+1;
step(t, G*LPF*v_app)
w_ref = max(step(t, G*LPF*v_app));
hold on
step(t, H*w_ref)
legend('G(s)*LPF(s)', 'H(s) con PI')
title('Respuesta a escal�n de 12V');
ylabel('Velocidad \omega [rpm]');
grid on;

%Sobredisparo
y = step(t, H*w_ref);
Mp = max(y);                    %Valor sobredisparo
Mp_perc = (Mp-K)*100/K;      %Valor en porcentaje

%Buscamos tp
for i=1:length(t)
    if y(i) == Mp
        tp_PI = t(i);
        n = i;
        break
    end  
end

%Nuevo tiempo de estableicmiento
for i=1:length(t)
    if y(i) >= (1-cr)*K
        ts_PI_b1 = t(i);
        break
    end  
end



%=============================================== CASO B > 1
xi = 0.2;              %Fijamos dos valores para xi y beta, que son orientativos, los polos no ser�n exactos
beta = 2;

%Caculo wn
wn = (sig+wc)/(xi*(beta+2));

%Constantes del controlador
Kp = (wn^2*(2*beta*xi^2+1)-sig*wc)/(1/Ke*sig*wc);
Ki = (wn^3*beta*xi)/(1/Ke*sig*wc);


%Tiempo de establecimiento y sobredisparo te�ricos
Mp = 100*exp(-xi/(sqrt(1-xi^2))*pi);
ts = -log(0.05*sqrt(1-xi^2))/(xi*wn);

%Funci�n de transferencia del controlador PI
PI = tf([Kp Ki],[1 0]);

%Sistema realimentado
H = minreal((PI*G*LPF)/(1+PI*G*LPF));

%Ubicaci�n polos y ceros
figure(j)
j = j+1;
pzmap(H)
title('Polos y ceros de H(s), \beta > 1')


t_max = 0.2;                %M�x valor de t en la gr�fica
t=0:1e-6:t_max;


figure(j)
j = j+1;
step(t, G*LPF*v_app)
w_ref = max(step(t, G*LPF*v_app));
hold on
step(t, H*w_ref)
legend('G(s)*LPF(s)', 'H(s) con PI')
title('Respuesta a escal�n de 12V');
ylabel('Velocidad \omega [rpm]');
grid on;

%Sobredisparo
y = step(t, H*w_ref);
Mp = max(y);                    %Valor sobredisparo
Mp_perc = (Mp-K)*100/K;      %Valor en porcentaje

%Buscamos tp
for i=1:length(t)
    if y(i) == Mp
        tp_PI = t(i);
        n = i;
        break
    end  
end

% 1.05*K;           %Valor superior para buscar ts
% 0.95*K;           %Valor inferior para buscar ts


%========================= PID por ubicaci�n de polos ====================%

%Establecemos par�metros deseados
Mp_d = 0.001;                             %Sobredisparo deseado
ts_d = 0.99*ts_teorico_LPF;                %Tiempo de establecimiento deseado


%Ubicaci�n deseada de los polos complejos conjugados
xi = sqrt((log(Mp_d))^2/(pi^2+(log(Mp_d))^2))
wn = -log(0.05*sqrt(1-xi^2))/(xi*ts_d)

%Polo simple alejado de los anteriores
p = 15*xi*wn

%Par�metros del controlador PID
Kd = (2*xi*wn+p-(sig+wc))/(1/Ke*sig*wc)         %Derivativo
Kp = (wn*(1+2*xi*p)-sig*wc)/(1/Ke*sig*wc)       %Proporcional
Ki = (p*wn)/(1/Ke*sig*wc);

%Funci�n de transferencia del controlador PI
PID = tf([Kd Kp Ki],[1 0])

%Sistema realimentado
H = minreal((PID*G*LPF)/(1+PID*G*LPF));

%Ubicaci�n polos y ceros
figure(j)
j = j+1;
pzmap(H)
title('PID: Polos y ceros de H(s)')


t_max = 0.2;                %M�x valor de t en la gr�fica
t=0:1e-6:t_max;


figure(j)
j = j+1;
step(t, G*LPF*v_app)
w_ref = max(step(t, G*LPF*v_app));
hold on
step(t, H*w_ref)
legend('G(s)*LPF(s)', 'H(s) con PID')
title 'Respuesta a escal�n de 12V'
ylabel 'Velocidad \omega [rpm]'
grid on
