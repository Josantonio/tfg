function [x,ts_real, c_tiempo_real,c_wreal,K_real] = ProcData(x, tmax)

%Definici�n de �ndices
c_wreal = 1;          %Columna d�nde se encuentra el valor de la velocidad
c_tiempo = 8;

[f,c] = size(x);   %Valores m�ximos de los �ndices

%Intervalos de tiempo
for i=1:f-1
    x(i,c+1) = x(i+1,c_tiempo)-x(i,c_tiempo);
end
c_diff = c+1;

%Columna con tiempo real
x(1,c+2) = 0;
for i=2:f-1
    x(i,c+2) = x(i-1,c+2)+x(i,c_diff);
end
c_tiempo_real = c+2;

%Eliminamos �ltima fila
x(f,:) = [];


%Cogemos hasta t m�x para la representaci�n
for i=1:f
    if x(i,c_tiempo_real)>tmax
        break;
    end
end
x = x(1:i-1,:);

[f,c] = size(x);                 %Actualizamos coeficientes

%=============== TIEMPO DE ESTABLECIMIENTO =============================%
cr = 0.05;                  %Criterio para tiempo de establecimiento

% REAL
K_real = max(x(:,c_wreal));

for i=1:f
    if x(i,c_wreal) >= (1-cr)*K_real
        ts_real = x(i,c_tiempo_real);
        break
    end  
end

%Calcular el retardo introducido (tiempo que tarda en responder)
for i=1:f
    if x(i,c_wreal) > 0
        break
    end  
end
%Lo eliminamos del tiempo de establecimiento
ts_real = ts_real - x(i,c_tiempo_real);


end