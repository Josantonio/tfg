close all
clear all
j = 1;

%===============================================================%
%         CONTROLADOR PI POR ASIGNACI�N DE POLOS                %
%===============================================================%


%OBTENCI�N DE LOS PAR�METROS DEL SISTEMA

%Abrimos  la respuesta al escal�n 0.5u(t) del sistema
DATA = importdata('D:/Documentos/UNIVERSIDAD/TFG/Octave/respuesta_escalon05_contiempo.txt');

nfilas = 437; %N�mero de filas de A

%Editar dependiendo del fichero
i_wreal = 1;
i_wref = 2;
i_time = 8;
i_lapso = 11;
i_real_t = 12;

%Calculamos diferencia de tiempo entre sample y sample
for i=1:nfilas-1
  DATA(i,i_lapso) = DATA(i+1,i_time) - DATA(i,i_time);
end

%Calculamos el nuevo eje del tiempo, con origen en t=0
 DATA(1,i_real_t) = 0;
 
 for i=1:nfilas-1
    DATA(i+1,i_real_t) = DATA(i,i_real_t) + DATA(i,i_lapso);
 end
 
 %Obtenemos la ganancia
 Kp = 2*max(DATA(:,i_wreal));
 
 %Valor en el que alcanza ts
 H_ts = 0.98*0.5*Kp;
 
 for i=1:nfilas-1
   if (DATA(i,i_wreal) > H_ts)   %Hemos encontrado el valor
     Ts = DATA(i, i_real_t)
     break                      %Asignamos Ts y salimos
   end
 end

%FILTRO PASO BAJAS
fc = 500;             %Frecuencia de corte en Hz
wc = 2*pi*fc;         %Frecuencia en rad/s

%En el espacio continuo de laplace
num = 1;
den =[1/wc 1];
LPF = tf(num,den)


%Calculamos tau
tau = -Ts/(log(0.02))


%Sistema de 1er orden en el espacio continuo:
num = Kp;
den =[tau 1];
G = tf(num,den)

%Respuesta temporal a 0.5u(t)
t = DATA(:,i_real_t);
%G_t = 0.5*Kp*(1-1*exp(-1*t/tau));
%H = G*LPF

%REPRESENTAMOS RESPUESTA AL ESCAL�N de 0.5u(t)
figure(j)
j=j+1;
%Real
plot(t, DATA(:,i_wreal),'r');
grid on
hold on;

%G(s) sin LPF
%plot(t,G_t);
step(0.5*G, 'b', t(nfilas));
hold off;


%G(s) + LPF(s)
%step(0.5*H, 'g', t(nfilas),"marker", 'O');
%hold on;
legend('Real', 'G(s)');


%DISE�O DEL CONTROLADOR

Tss = 0.5*(tau*4);       %Reducci�n del 50% en Ts
Mp = 0.1;                 % 2% de Mp permitido

xi = sqrt((log(Mp/100))^2/(pi^2+(log(Mp/100))^2)) %Factor de amortiguamiento

%Criterio del 2%
wn = 4/(xi*Tss)

Pd = [1 2*xi*wn wn^2]

a1 = Pd(2);
a2 = Pd(3);

%Obtenemos par�metros del PI
Kc = (a1*tau-1)/Kp
tau_i = (Kc*Kp)/(a2*tau)

C = tf(Kc*[tau_i 1],[tau_i 0])

%LAZO CERRADO
H = tf((C*G)/(1+C*G))
H_c = minreal(H)

figure(j)
j=j+1;
%Model
%step(0.5*G, 'b',t(nfilas));
%grid on
%hold on;

%Con PI
step(0.5*H_c, 'r',t(nfilas));
legend('Modelo', 'Con PI');
hold off;

figure(j)
j=j+1;
pzmap(H_c)

figure(j)
j=j+1;
pzmap(G)