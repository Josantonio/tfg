/*==============================================================================================
 *                                           PID.h                                               
 *
 * + Constantes y variables necesarias para el control PID.
 * + Definición del tipo estructura para almacenar los parámetros PID
 * + Funciones para asignar esos parámetros y para realizar el control.
 *
===============================================================================================*/



#pragma once                        //Indica al precompilador que sólo lo incluya una vez

#include <cytypes.h> 

#define PWM_PERIOD (uint8)(51)       //Período del bloque PWM en us
#define PID_MAX   (float)(24000)      //Máximo valor del compare para el PWM
#define PID_MIN   (float)(0)      //Máximo valor del compare para el PWM


/**** ESTRUCTURA PARA LOS PARÁMETROS DEL PID ****/
typedef struct 
{
    float Kp;   //Constante proporcional Kp = 1.2*T/L
    float Ki;   //Parámetro integral ki = 2*L
    float Kd;   //Parámetro derivativo Kd = 0.5*L
    float Tn;   //Tiempo de muestreo del controlador en ms
}my_struct_PID;

my_struct_PID PID;            //Declaramos variable PID, tipo estructura anteriormente definida


/*** VARIABLES EXTERNAS ***/
extern uint8 duty;
extern int16 e_k[3];
extern float PID_result[2];
extern uint8* pPID_result;
extern int16* p_ek; 


/**** DECLARACIÓN FUNCIONES ****/
void asign_param_PID(float Kp, float Ki, float Kd, float Tn);
void controlPID(void);

