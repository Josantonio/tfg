close all
clear
j = 1;

%================== MODELO MATEMÁTICO DEL MOTOR ========================%

%Parámetros característicos
Ke = 0.00335;                     %Constante eléctrica          [V/RPM]
Ke_r = 0.00335*60/(2*pi);         %Constante eléctrica          [Vs/rad]
R = 0.8;                          %Resistencia bobinados L-L    [Ohm]
L = 1.2e-3;                       %Inductancia L-L              [H]
Kt = 5.81*0.2780139*0.0254;       %Constante de torque          [Nm/A]
J = 6.8e-4*0.2780139*0.0254;      %Momento de inercia           [Nms^2]
Vdd = 24;                         %Tensión de alimentación      [V]

tau_m = (J*3*R)/(Ke_r*Kt);        %Constante de tiempo mecánica     [s]
tau_e = L/(3*R);                  %Constante de tiempo eléctrica    [s]    

%Función de transferencia W(S)/V(S) [RPM]/[V]
num = 1/(Ke*tau_e*tau_m);
den = [1 1/tau_e 1/(tau_e*tau_m)];
G = tf(num,den);


sigma = roots(den);               %Polos del sistema
sig = abs(sigma(2));                   %Polo dominante

%VECTOR DE TIEMPOS
t_max = 0.3;
t=0:1e-6:t_max;

cr = 0.05;                  %Criterio para tiempo de establecimiento


%========================= PASO BAJA  ===================================%
Ts = 200e-6;                %Período de muestreo [s]
fc = 10;                    %Frecuencia de corte [Hz]
wc = 2*pi*fc;               %Frecuencia de corte [rad/s]
t_d=0:Ts:t_max;             %Vector de tiempos discreto

LPF = tf(wc,[1 wc]);      %Función de transferencia del filtro

LPF_d = c2d(LPF, Ts, 'zoh'); %Discretización del filtro




%===================== DATOS EXPERIMENTALES ============================%
%=======================================================================%
data = importdata('respuesta12V2.txt'); %Importamos el archivo
[data,ts_real,c_tiempo_real,c_wreal,K_real] = ProcData(data,t_max);



%==================== MODELO APROXIMADO ==================================%
Ga = tf(1/Ke*sig*wc,poly([-sig -wc]));


%====================== RESPUESTA AL ESCALÓN =============================%

G_d = c2d(G, Ts, 'zoh');   %Discretización del modelo
y_d = G_d*LPF_d;           %Aplicación del filtro

v_app = 12;              %Voltaje aplicado, 0.4V perdidos en transistores

figure(j)
j = j+1; 
step(t,G*v_app,'r');                        %Modelo matemático G(s)
hold on;
step(t_d,y_d*v_app,'b');                    %Discretizado G(z) + LPF(z) 
plot(data(:,c_tiempo_real),data(:,c_wreal), 'g'); %Respuesta real
legend('Modelo G(s)', 'G(z) con filtro', 'Real')
ylabel('Velocidad [rpm]');
title('Respuesta a escalón de 12V');
grid on;



%================ AJUSTE EN LA GANANCIA =================================%
Ke_real = v_app/K_real;                 %Valor de Ke real
Gr = tf(1/Ke_real*sig*wc, poly([-sig -wc]));

figure(j)
j = j+1; 
step(t,Ga*v_app,'r');                        %Modelo matemático G(s)
hold on;
step(t_d,y_d*v_app,'b');                    %Discretizado G(z) + LPF(z)
plot(data(:,c_tiempo_real),data(:,c_wreal), 'g'); %Respuesta real
step(t ,Gr*v_app,'--');                      %Ajuste en la ganancia
legend('Modelo Ga(s)', 'G(z) con filtro', 'Real', 'Ajuste en la ganancia')
ylabel('Velocidad [rpm]');
title('Respuesta a escalón de 12V');
grid on;


%=============== PI ubicación de polos ==================================%
Mp = 0.01;                                        %Sobredisparo máximo deseado
xi = sqrt((log(Mp/100))^2/(pi^2+(log(Mp/100))^2));  %Valor de xi para Mp deseado
ts_d = 0.85*ts_real;           %Tiempo de establecimiento deseado
wn = -log(cr*sqrt(1-xi^2))/(xi*ts_d);         %Valor límite de wn, se resta 1 para ser inferior 

beta = (sig+wc)/(xi*wn)-2;

%Constantes del controlador
Kp = (wn^2*(2*beta*xi^2+1)-sig*wc)/(1/Ke_real*sig*wc);
Ki = (wn^3*beta*xi)/(1/Ke_real*sig*wc);

%Función de transferencia del controlador PI
PI = tf([Kp Ki],[1 0]);

%Funcion en tiempo discreto
Tn = 2e-3;                      %Período de muestreo del PI
z = tf('z',Tn) 
PId = Kp + Ki*Tn/2*(z+1)/(z-1);

%=============== RESPUESTA LAZO CERRADO =================================%

%Sistema realimentado
H1 = minreal((PI*Gr*LPF)/(1+PI*Gr*LPF));
Gr_d = c2d(Gr, Tn, 'zoh');   %Discretización del modelo
H1_d = (Gr_d*PId)/(1+Gr_d*PId);          %Aplicación del filtro

%Ubicación polos y ceros
figure(j)
j = j+1;
pzmap(H1)
title('Polos y ceros de H_1(s), \beta \approx 1')



%=================== PID Ziegler y Nichols ==============================%

Kcr = Ke_real*(sig+wc);
Tcr = (2*pi)/(sqrt(sig*wc));

Kp2 = Kcr/5;
Ki2 = (2/5)*Kcr/Tcr;
Kd2 = (Kcr*Tcr)/15;

%Función de transferencia del controlador PID
PID = tf([Kd2 Kp2 Ki2],[1 0]);

%=============== RESPUESTA LAZO CERRADO =================================%

%Sistema realimentado
H2 = minreal((PID*Gr*LPF)/(1+PID*Gr*LPF));

%Ubicación polos y ceros
figure(j)
j = j+1;
pzmap(H2)
title('Polos y ceros de H_2(s), PID con Ziegler y Nichols')


%===================== DATOS EXPERIMENTALES ============================%
%=======================================================================%
%VECTOR DE TIEMPOS
t_max = 0.5;
t=0:1e-6:t_max;

data1 = importdata('respuesta12VPI.txt'); %Importamos el archivo
[data1,ts_realPI,c_tiempo_realPI,c_wrealPI,K_realPI] = ProcData(data1,t_max);



%================= RESPUESTA A ESCALÓN EN LAZO CERRADO ===================%

w_ref = K_realPI;
t_d=0:Tn:t_max;             %Vector de tiempos discreto

tit = strcat('Controlador PI con \omega_{ref} = ', num2str(K_realPI));

figure(j)
j = j+1;
hold on
step(t, H1*w_ref)                                  %H(s)
step(t_d, H1_d*w_ref)                              %H(z)
plot(data1(:,c_tiempo_realPI),data1(:,c_wrealPI), 'g');   %Respuesta real
legend('H(s)', 'H(z)', 'Real con PI')
title(tit);
ylabel('Velocidad \omega [rpm]');
grid on;
