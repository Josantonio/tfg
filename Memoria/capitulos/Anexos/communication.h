/*==============================================================================================
 *                                  COMMUNICATION.h                                              
 *
 * Archivo de cabecera, definición de constantes, variable y funciones relacionadas con el
 * archivo de programa communication.c 
 *
===============================================================================================*/


#pragma once                    //Indica al precompilador que sólo lo incluya una vez


/*** FUNCIONES ***/
void INIT_HW_COMM (void);       //Inicializar hardware para la comunicación
void BCP_trans (void);          //Creación del array y transmisión por vía serial
void update_LED_status(void);