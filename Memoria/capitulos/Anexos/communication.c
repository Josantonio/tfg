/*==============================================================================================
 *                                  COMMUNICATION.C                                                
 *
 * + Cambio del estado del sistema a partir de pulsar un botón, para arrancar o parar el motor
 * + Comunicación entre la PSoC y el PC, y monitorización de variables de interés 
 * + Indicación del estado del sistema mediante un LED
 * + Función para la inicialización del hardware relacionado con los bloques de communicación   
 *
===============================================================================================*/



/*** ARCHIVOS DE CABECERA ***/
#include <project.h>
#include "stdio.h" 
#include "math.h"
#include "motor.h"
#include "communication.h"
#include "protection.h"
#include "PID.h"


static uint8 bcpTxbuffer[32]; //Array de 32 elementos, cada uno de 8 bits. Buffer para transmitir datos
//Se declara static para que no se borre y de tiempo a transmitir



/************************************ BOTÓN SW2 ************************************************/
CY_ISR(RUN_STOP){
    
    /* Al pulsarlo se cambia el estado del sistema, siempre y cuando no haya errores */
    if(!SYSTEM_STATE.error){// No se puede poner en marcha el motor hasta que no haya errores.
        
        SYSTEM_STATE.run = !SYSTEM_STATE.run; 
     } 
    //La detección de un error parará el motor hasta que este no desaparezca.    
}



/***************************** HARDWARE INITIALIZATION ******************************************/
void INIT_HW_COMM (void){
    
    UART_Start();                                   //Inicia bloque UART para la comunicación serial
    UART_UartPutString("CY8CKIT-044 USB-UART\n");
        
    isr_button2_StartEx(RUN_STOP);                  //Interrupción para activar/desactivar el motor

}



/*** ACTIVACIÓN DEL LED INDICADOR ***/
void update_LED_status(void)
{
    if (SYSTEM_STATE.run)           //Motor en marcha
    {
      Pin_LED_Green_Write(0);            //Encendemos LED verde
      Pin_LED_Red_Write(1);             //Apagamos LED rojo
    }
    if (!SYSTEM_STATE.run)          //Motor parado
    {
      Pin_LED_Green_Write(1);            //Apagamos LEDs 
      Pin_LED_Red_Write(1);
    }
    if (SYSTEM_STATE.error)         //Hay un error, LED rojo intermitente
    {
      int16 ts= check_errors()*100;
      Pin_LED_Green_Write(1);
      Pin_LED_Red_Write(~Pin_LED_Red_Read());
      CyDelay(ts);
      Pin_LED_Red_Write(~Pin_LED_Red_Read());
      CyDelay(ts);
    }
}


/*********** CREACIÓN DEL PAQUETE PARA ENVIAR LOS DATOS POR PUERTO SERIE*****************/

/* Tiene la siguiente forma:                                                            */
/*                                                                                      */
/*          HEADER                      BODY                        TAIL                */
/* [H=<byte0><byte1>...<byteN>] <byte0><byte1>...<byteK> [T=<byte0><byte1>...<byteM>]   */
/*                                                                                      */
/****************************************************************************************/



void BCP_trans(void)
{
    uint8 i = 0; //Variable usada como índice para moverse por el array de datos creado
    
    /* Hasta que no termine de transmitir los datos, no se pasa a crear un nuevo array */
    if (UART_SpiUartGetTxBufferSize()){return;/*Escape de BCP_trans()*/}; 
    //Si hay datos en la FIFO es uno, y sale de la función 
    /* TX software buffer is enabled: Returns the number of elements currently used 
    in the transmit buffer. This number does not include used entries in the TX FIFO. 
    The transmit buffer size is zero until the TX FIFO is not full. */
    
    /*================================== HEADER =======================================*/
    //Cabecera par aindicar el comienzo de la transmisión y realizar la sincronización
    bcpTxbuffer[i++] = 0x0A; 
    bcpTxbuffer[i++] = 0x0B;
    
    
    
    /*=================================== BODY ========================================*/
    /* Hay que invertir el orden de los bytes, MSB-LSB */
    
    /***  Velocidad de rotación real del motor  ***/
    bcpTxbuffer[i++] = (uint8)((*pw_real & 0xFF000000) >> 24);
    bcpTxbuffer[i++] = (uint8)((*pw_real & 0x00FF0000) >> 16);
    bcpTxbuffer[i++] = (uint8)((*pw_real & 0x0000FF00) >> 8);
    bcpTxbuffer[i++] = (uint8)(*pw_real & 0x000000FF);
    
    
    /***  Velocidad de referencia medida por el potenciómetro  ***/
    bcpTxbuffer[i++] = (uint8)((*pw_ref & 0xFF00) >> 8);
    bcpTxbuffer[i++] = (uint8)(*pw_ref & 0x00FF);
    
    
    /***  Error  ***/
    bcpTxbuffer[i++] = (uint8)((*p_ek & 0xFF00) >> 8);
    bcpTxbuffer[i++] = (uint8)(*p_ek & 0x00FF);
    
    
    /***  PID_result  ***/
    bcpTxbuffer[i++] = pPID_result[3];
    bcpTxbuffer[i++] = pPID_result[2];
    bcpTxbuffer[i++] = pPID_result[1];
    bcpTxbuffer[i++] = pPID_result[0];
    

    /*** Duty-cycle, salida del PID  ***/
    bcpTxbuffer[i++] = (uint8)(duty & 0x000000FF);
    
    
    /***  Tensión Vin  ***/
    bcpTxbuffer[i++] = (uint8)((vin & 0xFF000000) >> 24);
    bcpTxbuffer[i++] = (uint8)((vin & 0x00FF0000) >> 16);
    bcpTxbuffer[i++] = (uint8)((vin & 0x0000FF00) >> 8);
    bcpTxbuffer[i++] = (uint8)(vin & 0x000000FF);   
    
    
    /***  Over Current Protection  ***/
    /* Valor de Iref */
    bcpTxbuffer[i++] = (uint8)((I_ref & 0xFF00) >> 8);
    bcpTxbuffer[i++] = (uint8)(I_ref & 0x00FF);  
    
    /***  Tiempo transcurrido  ***/
    bcpTxbuffer[i++] = (uint8)(p_tiempo[3]);
    bcpTxbuffer[i++] = (uint8)(p_tiempo[2]);
    bcpTxbuffer[i++] = (uint8)(p_tiempo[1]);
    bcpTxbuffer[i++] = (uint8)(p_tiempo[0]);
    
    /*======================================= TAIL ============================================*/
    bcpTxbuffer[i++] = 0xFF;
    bcpTxbuffer[i++] = 0xFF;

    
    /* Se transmite el resultado */
    UART_SpiUartPutArray(bcpTxbuffer, i);
    /*******************************************************************************************/
    /*       void SCB_SpiUartPutArray(const uint16/uint8 wrBuf[], uint32 count)                */
    /*                                                                                         */
    /* const uint16/uint8 wrBuf[]: pointer to an array of data to be placed in transmit        */
    /*                             buffer. The width of the data to be transmitted depends     */     
    /*                             on TX data width selection (the data bit counting starts    */
    /*                             from LSB for each array element).                           */
    /* uint32 count: number of data elements to be placed in the transmit buffer.              */
    /*******************************************************************************************/
}