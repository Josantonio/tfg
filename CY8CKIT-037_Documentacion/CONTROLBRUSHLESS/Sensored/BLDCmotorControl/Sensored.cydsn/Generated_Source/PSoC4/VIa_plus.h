/*******************************************************************************
* File Name: VIa_plus.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_VIa_plus_H) /* Pins VIa_plus_H */
#define CY_PINS_VIa_plus_H

#include "cytypes.h"
#include "cyfitter.h"
#include "VIa_plus_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} VIa_plus_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   VIa_plus_Read(void);
void    VIa_plus_Write(uint8 value);
uint8   VIa_plus_ReadDataReg(void);
#if defined(VIa_plus__PC) || (CY_PSOC4_4200L) 
    void    VIa_plus_SetDriveMode(uint8 mode);
#endif
void    VIa_plus_SetInterruptMode(uint16 position, uint16 mode);
uint8   VIa_plus_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void VIa_plus_Sleep(void); 
void VIa_plus_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(VIa_plus__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define VIa_plus_DRIVE_MODE_BITS        (3)
    #define VIa_plus_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - VIa_plus_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the VIa_plus_SetDriveMode() function.
         *  @{
         */
        #define VIa_plus_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define VIa_plus_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define VIa_plus_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define VIa_plus_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define VIa_plus_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define VIa_plus_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define VIa_plus_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define VIa_plus_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define VIa_plus_MASK               VIa_plus__MASK
#define VIa_plus_SHIFT              VIa_plus__SHIFT
#define VIa_plus_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in VIa_plus_SetInterruptMode() function.
     *  @{
     */
        #define VIa_plus_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define VIa_plus_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define VIa_plus_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define VIa_plus_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(VIa_plus__SIO)
    #define VIa_plus_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(VIa_plus__PC) && (CY_PSOC4_4200L)
    #define VIa_plus_USBIO_ENABLE               ((uint32)0x80000000u)
    #define VIa_plus_USBIO_DISABLE              ((uint32)(~VIa_plus_USBIO_ENABLE))
    #define VIa_plus_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define VIa_plus_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define VIa_plus_USBIO_ENTER_SLEEP          ((uint32)((1u << VIa_plus_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << VIa_plus_USBIO_SUSPEND_DEL_SHIFT)))
    #define VIa_plus_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << VIa_plus_USBIO_SUSPEND_SHIFT)))
    #define VIa_plus_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << VIa_plus_USBIO_SUSPEND_DEL_SHIFT)))
    #define VIa_plus_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(VIa_plus__PC)
    /* Port Configuration */
    #define VIa_plus_PC                 (* (reg32 *) VIa_plus__PC)
#endif
/* Pin State */
#define VIa_plus_PS                     (* (reg32 *) VIa_plus__PS)
/* Data Register */
#define VIa_plus_DR                     (* (reg32 *) VIa_plus__DR)
/* Input Buffer Disable Override */
#define VIa_plus_INP_DIS                (* (reg32 *) VIa_plus__PC2)

/* Interrupt configuration Registers */
#define VIa_plus_INTCFG                 (* (reg32 *) VIa_plus__INTCFG)
#define VIa_plus_INTSTAT                (* (reg32 *) VIa_plus__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define VIa_plus_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(VIa_plus__SIO)
    #define VIa_plus_SIO_REG            (* (reg32 *) VIa_plus__SIO)
#endif /* (VIa_plus__SIO_CFG) */

/* USBIO registers */
#if !defined(VIa_plus__PC) && (CY_PSOC4_4200L)
    #define VIa_plus_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define VIa_plus_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define VIa_plus_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define VIa_plus_DRIVE_MODE_SHIFT       (0x00u)
#define VIa_plus_DRIVE_MODE_MASK        (0x07u << VIa_plus_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins VIa_plus_H */


/* [] END OF FILE */
