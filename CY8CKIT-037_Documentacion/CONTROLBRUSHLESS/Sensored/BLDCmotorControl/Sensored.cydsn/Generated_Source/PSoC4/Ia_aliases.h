/*******************************************************************************
* File Name: Ia.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Ia_ALIASES_H) /* Pins Ia_ALIASES_H */
#define CY_PINS_Ia_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define Ia_0			(Ia__0__PC)
#define Ia_0_PS		(Ia__0__PS)
#define Ia_0_PC		(Ia__0__PC)
#define Ia_0_DR		(Ia__0__DR)
#define Ia_0_SHIFT	(Ia__0__SHIFT)
#define Ia_0_INTR	((uint16)((uint16)0x0003u << (Ia__0__SHIFT*2u)))

#define Ia_INTR_ALL	 ((uint16)(Ia_0_INTR))


#endif /* End Pins Ia_ALIASES_H */


/* [] END OF FILE */
