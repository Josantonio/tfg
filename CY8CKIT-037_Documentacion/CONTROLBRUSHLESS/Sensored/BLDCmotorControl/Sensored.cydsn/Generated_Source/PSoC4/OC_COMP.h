/*******************************************************************************
* File Name: OC_COMP.h
* Version 2.20
*
* Description:
*  This file contains the function prototypes and constants used in
*  the Low Power Comparator component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_COMPARATOR_OC_COMP_H)
#define CY_COMPARATOR_OC_COMP_H

#include "cytypes.h"
#include "cyfitter.h"


extern uint8  OC_COMP_initVar;


/***************************************
*  Conditional Compilation Parameters
****************************************/

#define OC_COMP_CY_LPCOMP_V0 (CYIPBLOCK_m0s8lpcomp_VERSION == 0u) 
#define OC_COMP_CY_LPCOMP_V2 (CYIPBLOCK_m0s8lpcomp_VERSION >= 2u) 


/**************************************
*        Function Prototypes
**************************************/

void    OC_COMP_Start(void);
void    OC_COMP_Init(void);
void    OC_COMP_Enable(void);
void    OC_COMP_Stop(void);
void    OC_COMP_SetSpeed(uint32 speed);
void    OC_COMP_SetInterruptMode(uint32 mode);
uint32  OC_COMP_GetInterruptSource(void);
void    OC_COMP_ClearInterrupt(uint32 interruptMask);
void    OC_COMP_SetInterrupt(uint32 interruptMask);
void    OC_COMP_SetHysteresis(uint32 hysteresis);
uint32  OC_COMP_GetCompare(void);
uint32  OC_COMP_ZeroCal(void);
void    OC_COMP_LoadTrim(uint32 trimVal);
void    OC_COMP_Sleep(void);
void    OC_COMP_Wakeup(void);
void    OC_COMP_SaveConfig(void);
void    OC_COMP_RestoreConfig(void);
#if(OC_COMP_CY_LPCOMP_V2)
    void    OC_COMP_SetOutputMode(uint32 mode);
    void    OC_COMP_SetInterruptMask(uint32 interruptMask);
    uint32  OC_COMP_GetInterruptMask(void);
    uint32  OC_COMP_GetInterruptSourceMasked(void);
#endif /* (OC_COMP_CY_LPCOMP_V2) */


/**************************************
*           API Constants
**************************************/

#if(OC_COMP_CY_LPCOMP_V2)
    /* Constants for OC_COMP_SetOutputMode(), mode parameter */
    #define OC_COMP_OUT_PULSE      (0x00u)
    #define OC_COMP_OUT_SYNC       (0x01u)
    #define OC_COMP_OUT_DIRECT     (0x02u)
#endif /* (OC_COMP_CY_LPCOMP_V2) */

#define OC_COMP_INTR_PARAM_MASK    (0x03u)
#define OC_COMP_SPEED_PARAM_MASK   (0x03u)

/* Constants for OC_COMP_SetSpeed(), speed parameter */
#define OC_COMP_MED_SPEED          (0x00u)
#define OC_COMP_HIGH_SPEED         (0x01u)
#define OC_COMP_LOW_SPEED          (0x02u)

/* Constants for OC_COMP_SetInterruptMode(), mode parameter */
#define OC_COMP_INTR_DISABLE       (0x00u)
#define OC_COMP_INTR_RISING        (0x01u)
#define OC_COMP_INTR_FALLING       (0x02u)
#define OC_COMP_INTR_BOTH          (0x03u)

/* Constants for OC_COMP_SetHysteresis(), hysteresis parameter */
#define OC_COMP_HYST_ENABLE        (0x00u)
#define OC_COMP_HYST_DISABLE       (0x01u)

/* Constants for OC_COMP_ZeroCal() */
#define OC_COMP_TRIMA_MASK         (0x1Fu)
#define OC_COMP_TRIMA_SIGNBIT      (4u)
#define OC_COMP_TRIMA_MAX_VALUE    (15u)
#define OC_COMP_TRIMB_MASK         (0x1Fu)
#define OC_COMP_TRIMB_SHIFT        (8u)
#define OC_COMP_TRIMB_SIGNBIT      (4u)
#define OC_COMP_TRIMB_MAX_VALUE    (15u)

/* Constants for OC_COMP_GetInterruptSource() and 
* OC_COMP_ClearInterrupt(), interruptMask parameter 
*/
#define OC_COMP_INTR_SHIFT         (OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_SHIFT)
#define OC_COMP_INTR               ((uint32)0x01u << OC_COMP_INTR_SHIFT)

/* Constants for OC_COMP_SetInterrupt(), interruptMask parameter */
#define OC_COMP_INTR_SET_SHIFT     (OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_SET_SHIFT)
#define OC_COMP_INTR_SET           ((uint32)0x01u << OC_COMP_INTR_SHIFT)

#if(OC_COMP_CY_LPCOMP_V2)
    /* Constants for OC_COMP_GetInterruptMask() and 
    * OC_COMP_SetInterruptMask(), interruptMask parameter 
    */
    #define OC_COMP_INTR_MASK_SHIFT    (OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASK_SHIFT)
    #define OC_COMP_INTR_MASK          ((uint32)0x01u << OC_COMP_INTR_MASK_SHIFT)

    /* Constants for OC_COMP_GetInterruptSourceMasked() */ 
    #define OC_COMP_INTR_MASKED_SHIFT  (OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASKED_SHIFT)
    #define OC_COMP_INTR_MASKED        ((uint32)0x01u << OC_COMP_INTR_MASKED_SHIFT)
#endif /* (OC_COMP_CY_LPCOMP_V2) */


/***************************************
* Enumerated Types and Parameters 
***************************************/

/* Enumerated Types LPCompSpeedType, Used in parameter Speed */
#define OC_COMP__LPC_LOW_SPEED 2
#define OC_COMP__LPC_MED_SPEED 0
#define OC_COMP__LPC_HIGH_SPEED 1


/* Enumerated Types LPCompInterruptType, Used in parameter Interrupt */
#define OC_COMP__LPC_INT_DISABLE 0
#define OC_COMP__LPC_INT_RISING 1
#define OC_COMP__LPC_INT_FALLING 2
#define OC_COMP__LPC_INT_BOTH 3


/* Enumerated Types LPCompHysteresisType, Used in parameter Hysteresis */
#define OC_COMP__LPC_DISABLE_HYST 1
#define OC_COMP__LPC_ENABLE_HYST 0


/* Enumerated Types OutputModeType, Used in parameter OutputMode */
#define OC_COMP__OUT_MODE_SYNC 1
#define OC_COMP__OUT_MODE_DIRECT 2
#define OC_COMP__OUT_MODE_PULSE 0



/***************************************
*   Initial Parameter Constants
****************************************/

#define OC_COMP_INTERRUPT    (1u)
#define OC_COMP_SPEED        (0u)
#define OC_COMP_HYSTERESIS   (0u)
#if (OC_COMP_CY_LPCOMP_V2)
    #define OC_COMP_OUT_MODE       (0u)
    #define OC_COMP_INTERRUPT_EN   (0u)
#endif /* (OC_COMP_CY_LPCOMP_V2) */


/**************************************
*             Registers
**************************************/

#define OC_COMP_CONFIG_REG     (*(reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_CONFIG)
#define OC_COMP_CONFIG_PTR     ( (reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_CONFIG)

#define OC_COMP_DFT_REG        (*(reg32 *)CYREG_LPCOMP_DFT)
#define OC_COMP_DFT_PTR        ( (reg32 *)CYREG_LPCOMP_DFT)

#define OC_COMP_INTR_REG       (*(reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR)
#define OC_COMP_INTR_PTR       ( (reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR)

#define OC_COMP_INTR_SET_REG   (*(reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_SET)
#define OC_COMP_INTR_SET_PTR   ( (reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_SET)

#define OC_COMP_TRIMA_REG      (*(reg32 *)OC_COMP_cy_psoc4_lpcomp_1__TRIM_A)
#define OC_COMP_TRIMA_PTR      ( (reg32 *)OC_COMP_cy_psoc4_lpcomp_1__TRIM_A)

#define OC_COMP_TRIMB_REG      (*(reg32 *)OC_COMP_cy_psoc4_lpcomp_1__TRIM_B)
#define OC_COMP_TRIMB_PTR      ( (reg32 *)OC_COMP_cy_psoc4_lpcomp_1__TRIM_B)

#if(OC_COMP_CY_LPCOMP_V2)
    #define OC_COMP_INTR_MASK_REG    (*(reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASK) 
    #define OC_COMP_INTR_MASK_PTR    ( (reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASK) 

    #define OC_COMP_INTR_MASKED_REG  (*(reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASKED) 
    #define OC_COMP_INTR_MASKED_PTR  ( (reg32 *)OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASKED) 
#endif /* (OC_COMP_CY_LPCOMP_V2) */


/***************************************
*        Registers Constants
***************************************/

#define OC_COMP_CONFIG_REG_SHIFT           (OC_COMP_cy_psoc4_lpcomp_1__LPCOMP_CONFIG_SHIFT)

/* OC_COMPOC_COMP_CONFIG_REG */
#define OC_COMP_CONFIG_SPEED_MODE_SHIFT    (0u)    /* [1:0]    Operating mode for the comparator      */
#define OC_COMP_CONFIG_HYST_SHIFT          (2u)    /* [2]      Add 10mV hysteresis to the comparator: 0-enable, 1-disable */
#define OC_COMP_CONFIG_INTR_SHIFT          (4u)    /* [5:4]    Sets Pulse/Interrupt mode              */
#define OC_COMP_CONFIG_OUT_SHIFT           (6u)    /* [6]      Current output value of the comparator    */
#define OC_COMP_CONFIG_EN_SHIFT            (7u)    /* [7]      Enable comparator */
#if(OC_COMP_CY_LPCOMP_V2)
    #define OC_COMP_CONFIG_DSI_BYPASS_SHIFT    (16u)   /* [16]   Bypass comparator output synchronization for DSI output  */
    #define OC_COMP_CONFIG_DSI_LEVEL_SHIFT     (17u)   /* [17]   Comparator DSI (trigger) out level: 0-pulse, 1-level  */
#endif /* (OC_COMP_CY_LPCOMP_V2) */

#define OC_COMP_CONFIG_SPEED_MODE_MASK     (((uint32) 0x03u << OC_COMP_CONFIG_SPEED_MODE_SHIFT) << \
                                                    OC_COMP_CONFIG_REG_SHIFT)

#define OC_COMP_CONFIG_HYST                (((uint32) 0x01u << OC_COMP_CONFIG_HYST_SHIFT) << \
                                                    OC_COMP_CONFIG_REG_SHIFT)

#define OC_COMP_CONFIG_INTR_MASK           (((uint32) 0x03u << OC_COMP_CONFIG_INTR_SHIFT) << \
                                                    OC_COMP_CONFIG_REG_SHIFT)

#define OC_COMP_CONFIG_OUT                 (((uint32) 0x01u << OC_COMP_CONFIG_OUT_SHIFT) << \
                                                    OC_COMP_CONFIG_REG_SHIFT)

#define OC_COMP_CONFIG_EN                  (((uint32) 0x01u << OC_COMP_CONFIG_EN_SHIFT) << \
                                                    OC_COMP_CONFIG_REG_SHIFT)
#if(OC_COMP_CY_LPCOMP_V2)
    #define OC_COMP_CONFIG_DSI_BYPASS          (((uint32) 0x01u << OC_COMP_CONFIG_DSI_BYPASS_SHIFT) << \
                                                        (OC_COMP_CONFIG_REG_SHIFT/2))

    #define OC_COMP_CONFIG_DSI_LEVEL           (((uint32) 0x01u << OC_COMP_CONFIG_DSI_LEVEL_SHIFT) << \
                                                        (OC_COMP_CONFIG_REG_SHIFT/2))
#endif /* (OC_COMP_CY_LPCOMP_V2) */


/* OC_COMPOC_COMP_DFT_REG */
#define OC_COMP_DFT_CAL_EN_SHIFT    (0u)    /* [0] Calibration enable */

#define OC_COMP_DFT_CAL_EN          ((uint32) 0x01u << OC_COMP_DFT_CAL_EN_SHIFT)


/***************************************
*       Init Macros Definitions
***************************************/

#define OC_COMP_GET_CONFIG_SPEED_MODE(mode)    ((uint32) ((((uint32) (mode) << OC_COMP_CONFIG_SPEED_MODE_SHIFT) << \
                                                            OC_COMP_CONFIG_REG_SHIFT) & \
                                                            OC_COMP_CONFIG_SPEED_MODE_MASK))

#define OC_COMP_GET_CONFIG_HYST(hysteresis)    ((0u != (hysteresis)) ? (OC_COMP_CONFIG_HYST) : (0u))

#define OC_COMP_GET_CONFIG_INTR(intType)   ((uint32) ((((uint32)(intType) << OC_COMP_CONFIG_INTR_SHIFT) << \
                                                    OC_COMP_CONFIG_REG_SHIFT) & \
                                                    OC_COMP_CONFIG_INTR_MASK))
#if(OC_COMP_CY_LPCOMP_V2)
    #define OC_COMP_GET_CONFIG_DSI_BYPASS(bypass)  ((0u != ((bypass) & OC_COMP_OUT_DIRECT)) ? \
                                                                    (OC_COMP_CONFIG_DSI_BYPASS) : (0u))
   
    #define OC_COMP_GET_CONFIG_DSI_LEVEL(level)    ((0u != ((level) & OC_COMP_OUT_SYNC)) ? \
                                                                    (OC_COMP_CONFIG_DSI_LEVEL) : (0u))
    
    #define OC_COMP_GET_INTR_MASK(mask)            ((0u != (mask)) ? (OC_COMP_INTR_MASK) : (0u))
#endif /* (OC_COMP_CY_LPCOMP_V2) */

#if(OC_COMP_CY_LPCOMP_V0)
    #define OC_COMP_CONFIG_REG_DEFAULT (OC_COMP_GET_CONFIG_SPEED_MODE(OC_COMP_SPEED) |\
                                                 OC_COMP_GET_CONFIG_HYST(OC_COMP_HYSTERESIS))
#else
    #define OC_COMP_CONFIG_REG_DEFAULT (OC_COMP_GET_CONFIG_SPEED_MODE(OC_COMP_SPEED) |\
                                                 OC_COMP_GET_CONFIG_HYST(OC_COMP_HYSTERESIS)  |\
                                                 OC_COMP_GET_CONFIG_DSI_BYPASS(OC_COMP_OUT_MODE) |\
                                                 OC_COMP_GET_CONFIG_DSI_LEVEL(OC_COMP_OUT_MODE))
#endif /* (OC_COMP_CY_LPCOMP_V0) */

#if(OC_COMP_CY_LPCOMP_V2)
    #define OC_COMP_INTR_MASK_REG_DEFAULT  (OC_COMP_GET_INTR_MASK(OC_COMP_INTERRUPT_EN))
#endif /* (OC_COMP_CY_LPCOMP_V2) */


/***************************************
* The following code is DEPRECATED and 
* should not be used in new projects.
***************************************/

#define OC_COMP_CONFIG_FILT_SHIFT          (3u)    
#define OC_COMP_CONFIG_FILT                ((uint32)((uint32)((uint32)0x01u << \
                                                    OC_COMP_CONFIG_FILT_SHIFT) << OC_COMP_CONFIG_REG_SHIFT))

#define OC_COMP_DIGITAL_FILTER             (0u)

/* OC_COMP_SetFilter() parameters */
#define OC_COMP_FILT_DISABLE               (0x00u)
#define OC_COMP_FILT_ENABLE                (0x01u)

/* OC_COMP_SetSpeed() parameters */
#define OC_COMP_MEDSPEED                   (OC_COMP_MED_SPEED)
#define OC_COMP_HIGHSPEED                  (OC_COMP_HIGH_SPEED)
#define OC_COMP_LOWSPEED                   (OC_COMP_LOW_SPEED)

void    OC_COMP_SetFilter(uint32 filter);

#endif    /* CY_COMPARATOR_OC_COMP_H */


/* [] END OF FILE */
