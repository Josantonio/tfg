/*******************************************************************************
* File Name: VIa_o.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_VIa_o_ALIASES_H) /* Pins VIa_o_ALIASES_H */
#define CY_PINS_VIa_o_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define VIa_o_0			(VIa_o__0__PC)
#define VIa_o_0_PS		(VIa_o__0__PS)
#define VIa_o_0_PC		(VIa_o__0__PC)
#define VIa_o_0_DR		(VIa_o__0__DR)
#define VIa_o_0_SHIFT	(VIa_o__0__SHIFT)
#define VIa_o_0_INTR	((uint16)((uint16)0x0003u << (VIa_o__0__SHIFT*2u)))

#define VIa_o_INTR_ALL	 ((uint16)(VIa_o_0_INTR))


#endif /* End Pins VIa_o_ALIASES_H */


/* [] END OF FILE */
