/*******************************************************************************
* File Name: ADC_SAR_Wref_PM.c
* Version 2.50
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ADC_SAR_Wref.h"


/***************************************
* Local data allocation
***************************************/

static ADC_SAR_Wref_BACKUP_STRUCT  ADC_SAR_Wref_backup =
{
    ADC_SAR_Wref_DISABLED,
    0u    
};


/*******************************************************************************
* Function Name: ADC_SAR_Wref_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_SAR_Wref_SaveConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_SAR_Wref_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_SAR_Wref_RestoreConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_SAR_Wref_Sleep
********************************************************************************
*
* Summary:
*  Stops the ADC operation and saves the configuration registers and component
*  enable state. Should be called just prior to entering sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_SAR_Wref_backup - modified.
*
*******************************************************************************/
void ADC_SAR_Wref_Sleep(void)
{
    /* During deepsleep/ hibernate mode keep SARMUX active, i.e. do not open
    *   all switches (disconnect), to be used for ADFT
    */
    ADC_SAR_Wref_backup.dftRegVal = ADC_SAR_Wref_SAR_DFT_CTRL_REG & (uint32)~ADC_SAR_Wref_ADFT_OVERRIDE;
    ADC_SAR_Wref_SAR_DFT_CTRL_REG |= ADC_SAR_Wref_ADFT_OVERRIDE;
    if((ADC_SAR_Wref_SAR_CTRL_REG  & ADC_SAR_Wref_ENABLE) != 0u)
    {
        if((ADC_SAR_Wref_SAR_SAMPLE_CTRL_REG & ADC_SAR_Wref_CONTINUOUS_EN) != 0u)
        {
            ADC_SAR_Wref_backup.enableState = ADC_SAR_Wref_ENABLED | ADC_SAR_Wref_STARTED;
        }
        else
        {
            ADC_SAR_Wref_backup.enableState = ADC_SAR_Wref_ENABLED;
        }
        ADC_SAR_Wref_StopConvert();
        ADC_SAR_Wref_Stop();
        
        /* Disable the SAR internal pump before entering the chip low power mode */
        if((ADC_SAR_Wref_SAR_CTRL_REG & ADC_SAR_Wref_BOOSTPUMP_EN) != 0u)
        {
            ADC_SAR_Wref_SAR_CTRL_REG &= (uint32)~ADC_SAR_Wref_BOOSTPUMP_EN;
            ADC_SAR_Wref_backup.enableState |= ADC_SAR_Wref_BOOSTPUMP_ENABLED;
        }
    }
    else
    {
        ADC_SAR_Wref_backup.enableState = ADC_SAR_Wref_DISABLED;
    }
}


/*******************************************************************************
* Function Name: ADC_SAR_Wref_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component enable state and configuration registers.
*  This should be called just after awaking from sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_SAR_Wref_backup - used.
*
*******************************************************************************/
void ADC_SAR_Wref_Wakeup(void)
{
    ADC_SAR_Wref_SAR_DFT_CTRL_REG = ADC_SAR_Wref_backup.dftRegVal;
    if(ADC_SAR_Wref_backup.enableState != ADC_SAR_Wref_DISABLED)
    {
        /* Enable the SAR internal pump  */
        if((ADC_SAR_Wref_backup.enableState & ADC_SAR_Wref_BOOSTPUMP_ENABLED) != 0u)
        {
            ADC_SAR_Wref_SAR_CTRL_REG |= ADC_SAR_Wref_BOOSTPUMP_EN;
        }
        ADC_SAR_Wref_Enable();
        if((ADC_SAR_Wref_backup.enableState & ADC_SAR_Wref_STARTED) != 0u)
        {
            ADC_SAR_Wref_StartConvert();
        }
    }
}
/* [] END OF FILE */
