/*******************************************************************************
* File Name: I_ref_source.c
* Version 1.10
*
* Description:
*  This file provides the source code of APIs for the IDAC_P4 component.
*
*******************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "I_ref_source.h"

uint32 I_ref_source_initVar = 0u;


/*******************************************************************************
* Function Name: I_ref_source_Init
********************************************************************************
*
* Summary:
*  Initializes IDAC registers with initial values provided from customizer.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  None
*
*******************************************************************************/
void I_ref_source_Init(void)
{
    uint32 regVal;
    uint8 enableInterrupts;

    /* Set initial configuration */
    enableInterrupts = CyEnterCriticalSection();
    
    #if(I_ref_source_MODE_SOURCE == I_ref_source_IDAC_POLARITY)
        regVal  = I_ref_source_CSD_TRIM1_REG & ~(I_ref_source_CSD_IDAC_TRIM1_MASK);
        regVal |=  (I_ref_source_SFLASH_TRIM1_REG & I_ref_source_CSD_IDAC_TRIM1_MASK);
        I_ref_source_CSD_TRIM1_REG = regVal;
    #else
        regVal  = I_ref_source_CSD_TRIM2_REG & ~(I_ref_source_CSD_IDAC_TRIM2_MASK);
        regVal |=  (I_ref_source_SFLASH_TRIM2_REG & I_ref_source_CSD_IDAC_TRIM2_MASK);
        I_ref_source_CSD_TRIM2_REG = regVal;
    #endif /* (I_ref_source_MODE_SOURCE == I_ref_source_IDAC_POLARITY) */

    /* clear previous values */
    I_ref_source_IDAC_CONTROL_REG &= ((uint32)~((uint32)I_ref_source_IDAC_VALUE_MASK <<
        I_ref_source_IDAC_VALUE_POSITION)) | ((uint32)~((uint32)I_ref_source_IDAC_MODE_MASK <<
        I_ref_source_IDAC_MODE_POSITION))  | ((uint32)~((uint32)I_ref_source_IDAC_RANGE_MASK  <<
        I_ref_source_IDAC_RANGE_POSITION));

    I_ref_source_IDAC_POLARITY_CONTROL_REG &= (~(uint32)((uint32)I_ref_source_IDAC_POLARITY_MASK <<
        I_ref_source_IDAC_POLARITY_POSITION));

    /* set new configuration */
    I_ref_source_IDAC_CONTROL_REG |= (((uint32)I_ref_source_IDAC_INIT_VALUE <<
        I_ref_source_IDAC_VALUE_POSITION) | ((uint32)I_ref_source_IDAC_RANGE <<
        I_ref_source_IDAC_RANGE_POSITION));

    I_ref_source_IDAC_POLARITY_CONTROL_REG |= ((uint32)I_ref_source_IDAC_POLARITY <<
                                                           I_ref_source_IDAC_POLARITY_POSITION);

    CyExitCriticalSection(enableInterrupts);

}


/*******************************************************************************
* Function Name: I_ref_source_Enable
********************************************************************************
*
* Summary:
*  Enables IDAC operations.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  None
*
*******************************************************************************/
void I_ref_source_Enable(void)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    /* Enable the IDAC */
    I_ref_source_IDAC_CONTROL_REG |= ((uint32)I_ref_source_IDAC_EN_MODE <<
                                                  I_ref_source_IDAC_MODE_POSITION);
    I_ref_source_IDAC_POLARITY_CONTROL_REG |= ((uint32)I_ref_source_IDAC_CSD_EN <<
                                                           I_ref_source_IDAC_CSD_EN_POSITION);
    CyExitCriticalSection(enableInterrupts);

}


/*******************************************************************************
* Function Name: I_ref_source_Start
********************************************************************************
*
* Summary:
*  Starts the IDAC hardware.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  I_ref_source_initVar
*
*******************************************************************************/
void I_ref_source_Start(void)
{
    if(0u == I_ref_source_initVar)
    {
        I_ref_source_Init();
        I_ref_source_initVar = 1u;
    }

    I_ref_source_Enable();

}


/*******************************************************************************
* Function Name: I_ref_source_Stop
********************************************************************************
*
* Summary:
*  Stops the IDAC hardware.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  None
*
*******************************************************************************/
void I_ref_source_Stop(void)
{
    uint8 enableInterrupts;

    enableInterrupts = CyEnterCriticalSection();

    /* Disable the IDAC */
    I_ref_source_IDAC_CONTROL_REG &= ((uint32)~((uint32)I_ref_source_IDAC_MODE_MASK <<
        I_ref_source_IDAC_MODE_POSITION));
    CyExitCriticalSection(enableInterrupts);
}


/*******************************************************************************
* Function Name: I_ref_source_SetValue
********************************************************************************
*
* Summary:
*  Sets the IDAC value.
*
* Parameters:
*  uint32 value
*
* Return:
*  None
*
* Global variables:
*  None
*
*******************************************************************************/
void I_ref_source_SetValue(uint32 value)
{
    uint8 enableInterrupts;
    uint32 newRegisterValue;

    enableInterrupts = CyEnterCriticalSection();

    #if(I_ref_source_IDAC_VALUE_POSITION != 0u)
        newRegisterValue = ((I_ref_source_IDAC_CONTROL_REG & (~(uint32)((uint32)I_ref_source_IDAC_VALUE_MASK <<
        I_ref_source_IDAC_VALUE_POSITION))) | (value << I_ref_source_IDAC_VALUE_POSITION));
    #else
        newRegisterValue = ((I_ref_source_IDAC_CONTROL_REG & (~(uint32)I_ref_source_IDAC_VALUE_MASK)) | value);
    #endif /* I_ref_source_IDAC_VALUE_POSITION != 0u */

    I_ref_source_IDAC_CONTROL_REG = newRegisterValue;

    CyExitCriticalSection(enableInterrupts);
}

/* [] END OF FILE */
