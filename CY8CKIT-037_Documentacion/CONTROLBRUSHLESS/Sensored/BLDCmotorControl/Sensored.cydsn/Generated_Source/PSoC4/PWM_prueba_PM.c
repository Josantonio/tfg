/*******************************************************************************
* File Name: PWM_prueba_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "PWM_prueba.h"

static PWM_prueba_backupStruct PWM_prueba_backup;


/*******************************************************************************
* Function Name: PWM_prueba_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_prueba_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void PWM_prueba_SaveConfig(void) 
{

    #if(!PWM_prueba_UsingFixedFunction)
        #if(!PWM_prueba_PWMModeIsCenterAligned)
            PWM_prueba_backup.PWMPeriod = PWM_prueba_ReadPeriod();
        #endif /* (!PWM_prueba_PWMModeIsCenterAligned) */
        PWM_prueba_backup.PWMUdb = PWM_prueba_ReadCounter();
        #if (PWM_prueba_UseStatus)
            PWM_prueba_backup.InterruptMaskValue = PWM_prueba_STATUS_MASK;
        #endif /* (PWM_prueba_UseStatus) */

        #if(PWM_prueba_DeadBandMode == PWM_prueba__B_PWM__DBM_256_CLOCKS || \
            PWM_prueba_DeadBandMode == PWM_prueba__B_PWM__DBM_2_4_CLOCKS)
            PWM_prueba_backup.PWMdeadBandValue = PWM_prueba_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(PWM_prueba_KillModeMinTime)
             PWM_prueba_backup.PWMKillCounterPeriod = PWM_prueba_ReadKillTime();
        #endif /* (PWM_prueba_KillModeMinTime) */

        #if(PWM_prueba_UseControl)
            PWM_prueba_backup.PWMControlRegister = PWM_prueba_ReadControlRegister();
        #endif /* (PWM_prueba_UseControl) */
    #endif  /* (!PWM_prueba_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PWM_prueba_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_prueba_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_prueba_RestoreConfig(void) 
{
        #if(!PWM_prueba_UsingFixedFunction)
            #if(!PWM_prueba_PWMModeIsCenterAligned)
                PWM_prueba_WritePeriod(PWM_prueba_backup.PWMPeriod);
            #endif /* (!PWM_prueba_PWMModeIsCenterAligned) */

            PWM_prueba_WriteCounter(PWM_prueba_backup.PWMUdb);

            #if (PWM_prueba_UseStatus)
                PWM_prueba_STATUS_MASK = PWM_prueba_backup.InterruptMaskValue;
            #endif /* (PWM_prueba_UseStatus) */

            #if(PWM_prueba_DeadBandMode == PWM_prueba__B_PWM__DBM_256_CLOCKS || \
                PWM_prueba_DeadBandMode == PWM_prueba__B_PWM__DBM_2_4_CLOCKS)
                PWM_prueba_WriteDeadTime(PWM_prueba_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(PWM_prueba_KillModeMinTime)
                PWM_prueba_WriteKillTime(PWM_prueba_backup.PWMKillCounterPeriod);
            #endif /* (PWM_prueba_KillModeMinTime) */

            #if(PWM_prueba_UseControl)
                PWM_prueba_WriteControlRegister(PWM_prueba_backup.PWMControlRegister);
            #endif /* (PWM_prueba_UseControl) */
        #endif  /* (!PWM_prueba_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: PWM_prueba_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_prueba_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void PWM_prueba_Sleep(void) 
{
    #if(PWM_prueba_UseControl)
        if(PWM_prueba_CTRL_ENABLE == (PWM_prueba_CONTROL & PWM_prueba_CTRL_ENABLE))
        {
            /*Component is enabled */
            PWM_prueba_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            PWM_prueba_backup.PWMEnableState = 0u;
        }
    #endif /* (PWM_prueba_UseControl) */

    /* Stop component */
    PWM_prueba_Stop();

    /* Save registers configuration */
    PWM_prueba_SaveConfig();
}


/*******************************************************************************
* Function Name: PWM_prueba_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_prueba_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_prueba_Wakeup(void) 
{
     /* Restore registers values */
    PWM_prueba_RestoreConfig();

    if(PWM_prueba_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        PWM_prueba_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
