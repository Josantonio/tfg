/*******************************************************************************
* File Name: VIa_o.c  
* Version 2.20
*
* Description:
*  This file contains APIs to set up the Pins component for low power modes.
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "VIa_o.h"

static VIa_o_BACKUP_STRUCT  VIa_o_backup = {0u, 0u, 0u};


/*******************************************************************************
* Function Name: VIa_o_Sleep
****************************************************************************//**
*
* \brief Stores the pin configuration and prepares the pin for entering chip 
*  deep-sleep/hibernate modes. This function applies only to SIO and USBIO pins.
*  It should not be called for GPIO or GPIO_OVT pins.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None 
*  
* \sideeffect
*  For SIO pins, this function configures the pin input threshold to CMOS and
*  drive level to Vddio. This is needed for SIO pins when in device 
*  deep-sleep/hibernate modes.
*
* \funcusage
*  \snippet VIa_o_SUT.c usage_VIa_o_Sleep_Wakeup
*******************************************************************************/
void VIa_o_Sleep(void)
{
    #if defined(VIa_o__PC)
        VIa_o_backup.pcState = VIa_o_PC;
    #else
        #if (CY_PSOC4_4200L)
            /* Save the regulator state and put the PHY into suspend mode */
            VIa_o_backup.usbState = VIa_o_CR1_REG;
            VIa_o_USB_POWER_REG |= VIa_o_USBIO_ENTER_SLEEP;
            VIa_o_CR1_REG &= VIa_o_USBIO_CR1_OFF;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(VIa_o__SIO)
        VIa_o_backup.sioState = VIa_o_SIO_REG;
        /* SIO requires unregulated output buffer and single ended input buffer */
        VIa_o_SIO_REG &= (uint32)(~VIa_o_SIO_LPM_MASK);
    #endif  
}


/*******************************************************************************
* Function Name: VIa_o_Wakeup
****************************************************************************//**
*
* \brief Restores the pin configuration that was saved during Pin_Sleep(). This 
* function applies only to SIO and USBIO pins. It should not be called for
* GPIO or GPIO_OVT pins.
*
* For USBIO pins, the wakeup is only triggered for falling edge interrupts.
*
* <b>Note</b> This function is available in PSoC 4 only.
*
* \return 
*  None
*  
* \funcusage
*  Refer to VIa_o_Sleep() for an example usage.
*******************************************************************************/
void VIa_o_Wakeup(void)
{
    #if defined(VIa_o__PC)
        VIa_o_PC = VIa_o_backup.pcState;
    #else
        #if (CY_PSOC4_4200L)
            /* Restore the regulator state and come out of suspend mode */
            VIa_o_USB_POWER_REG &= VIa_o_USBIO_EXIT_SLEEP_PH1;
            VIa_o_CR1_REG = VIa_o_backup.usbState;
            VIa_o_USB_POWER_REG &= VIa_o_USBIO_EXIT_SLEEP_PH2;
        #endif
    #endif
    #if defined(CYIPBLOCK_m0s8ioss_VERSION) && defined(VIa_o__SIO)
        VIa_o_SIO_REG = VIa_o_backup.sioState;
    #endif
}


/* [] END OF FILE */
