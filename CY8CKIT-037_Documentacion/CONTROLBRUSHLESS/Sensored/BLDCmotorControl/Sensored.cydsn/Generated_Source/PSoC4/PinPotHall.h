/*******************************************************************************
* File Name: PinPotHall.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinPotHall_H) /* Pins PinPotHall_H */
#define CY_PINS_PinPotHall_H

#include "cytypes.h"
#include "cyfitter.h"
#include "PinPotHall_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} PinPotHall_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   PinPotHall_Read(void);
void    PinPotHall_Write(uint8 value);
uint8   PinPotHall_ReadDataReg(void);
#if defined(PinPotHall__PC) || (CY_PSOC4_4200L) 
    void    PinPotHall_SetDriveMode(uint8 mode);
#endif
void    PinPotHall_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinPotHall_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void PinPotHall_Sleep(void); 
void PinPotHall_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(PinPotHall__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define PinPotHall_DRIVE_MODE_BITS        (3)
    #define PinPotHall_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - PinPotHall_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the PinPotHall_SetDriveMode() function.
         *  @{
         */
        #define PinPotHall_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define PinPotHall_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define PinPotHall_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define PinPotHall_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define PinPotHall_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define PinPotHall_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define PinPotHall_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define PinPotHall_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define PinPotHall_MASK               PinPotHall__MASK
#define PinPotHall_SHIFT              PinPotHall__SHIFT
#define PinPotHall_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinPotHall_SetInterruptMode() function.
     *  @{
     */
        #define PinPotHall_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define PinPotHall_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define PinPotHall_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define PinPotHall_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(PinPotHall__SIO)
    #define PinPotHall_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(PinPotHall__PC) && (CY_PSOC4_4200L)
    #define PinPotHall_USBIO_ENABLE               ((uint32)0x80000000u)
    #define PinPotHall_USBIO_DISABLE              ((uint32)(~PinPotHall_USBIO_ENABLE))
    #define PinPotHall_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define PinPotHall_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define PinPotHall_USBIO_ENTER_SLEEP          ((uint32)((1u << PinPotHall_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << PinPotHall_USBIO_SUSPEND_DEL_SHIFT)))
    #define PinPotHall_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << PinPotHall_USBIO_SUSPEND_SHIFT)))
    #define PinPotHall_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << PinPotHall_USBIO_SUSPEND_DEL_SHIFT)))
    #define PinPotHall_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(PinPotHall__PC)
    /* Port Configuration */
    #define PinPotHall_PC                 (* (reg32 *) PinPotHall__PC)
#endif
/* Pin State */
#define PinPotHall_PS                     (* (reg32 *) PinPotHall__PS)
/* Data Register */
#define PinPotHall_DR                     (* (reg32 *) PinPotHall__DR)
/* Input Buffer Disable Override */
#define PinPotHall_INP_DIS                (* (reg32 *) PinPotHall__PC2)

/* Interrupt configuration Registers */
#define PinPotHall_INTCFG                 (* (reg32 *) PinPotHall__INTCFG)
#define PinPotHall_INTSTAT                (* (reg32 *) PinPotHall__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define PinPotHall_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(PinPotHall__SIO)
    #define PinPotHall_SIO_REG            (* (reg32 *) PinPotHall__SIO)
#endif /* (PinPotHall__SIO_CFG) */

/* USBIO registers */
#if !defined(PinPotHall__PC) && (CY_PSOC4_4200L)
    #define PinPotHall_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define PinPotHall_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define PinPotHall_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define PinPotHall_DRIVE_MODE_SHIFT       (0x00u)
#define PinPotHall_DRIVE_MODE_MASK        (0x07u << PinPotHall_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins PinPotHall_H */


/* [] END OF FILE */
