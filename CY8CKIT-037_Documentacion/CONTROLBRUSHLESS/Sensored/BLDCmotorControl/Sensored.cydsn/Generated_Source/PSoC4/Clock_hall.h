/*******************************************************************************
* File Name: Clock_hall.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_Clock_hall_H)
#define CY_CLOCK_Clock_hall_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void Clock_hall_StartEx(uint32 alignClkDiv);
#define Clock_hall_Start() \
    Clock_hall_StartEx(Clock_hall__PA_DIV_ID)

#else

void Clock_hall_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void Clock_hall_Stop(void);

void Clock_hall_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 Clock_hall_GetDividerRegister(void);
uint8  Clock_hall_GetFractionalDividerRegister(void);

#define Clock_hall_Enable()                         Clock_hall_Start()
#define Clock_hall_Disable()                        Clock_hall_Stop()
#define Clock_hall_SetDividerRegister(clkDivider, reset)  \
    Clock_hall_SetFractionalDividerRegister((clkDivider), 0u)
#define Clock_hall_SetDivider(clkDivider)           Clock_hall_SetDividerRegister((clkDivider), 1u)
#define Clock_hall_SetDividerValue(clkDivider)      Clock_hall_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define Clock_hall_DIV_ID     Clock_hall__DIV_ID

#define Clock_hall_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define Clock_hall_CTRL_REG   (*(reg32 *)Clock_hall__CTRL_REGISTER)
#define Clock_hall_DIV_REG    (*(reg32 *)Clock_hall__DIV_REGISTER)

#define Clock_hall_CMD_DIV_SHIFT          (0u)
#define Clock_hall_CMD_PA_DIV_SHIFT       (8u)
#define Clock_hall_CMD_DISABLE_SHIFT      (30u)
#define Clock_hall_CMD_ENABLE_SHIFT       (31u)

#define Clock_hall_CMD_DISABLE_MASK       ((uint32)((uint32)1u << Clock_hall_CMD_DISABLE_SHIFT))
#define Clock_hall_CMD_ENABLE_MASK        ((uint32)((uint32)1u << Clock_hall_CMD_ENABLE_SHIFT))

#define Clock_hall_DIV_FRAC_MASK  (0x000000F8u)
#define Clock_hall_DIV_FRAC_SHIFT (3u)
#define Clock_hall_DIV_INT_MASK   (0xFFFFFF00u)
#define Clock_hall_DIV_INT_SHIFT  (8u)

#else 

#define Clock_hall_DIV_REG        (*(reg32 *)Clock_hall__REGISTER)
#define Clock_hall_ENABLE_REG     Clock_hall_DIV_REG
#define Clock_hall_DIV_FRAC_MASK  Clock_hall__FRAC_MASK
#define Clock_hall_DIV_FRAC_SHIFT (16u)
#define Clock_hall_DIV_INT_MASK   Clock_hall__DIVIDER_MASK
#define Clock_hall_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_Clock_hall_H) */

/* [] END OF FILE */
