/*******************************************************************************
* File Name: AH.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_AH_ALIASES_H) /* Pins AH_ALIASES_H */
#define CY_PINS_AH_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define AH_0			(AH__0__PC)
#define AH_0_PS		(AH__0__PS)
#define AH_0_PC		(AH__0__PC)
#define AH_0_DR		(AH__0__DR)
#define AH_0_SHIFT	(AH__0__SHIFT)
#define AH_0_INTR	((uint16)((uint16)0x0003u << (AH__0__SHIFT*2u)))

#define AH_INTR_ALL	 ((uint16)(AH_0_INTR))


#endif /* End Pins AH_ALIASES_H */


/* [] END OF FILE */
