/*******************************************************************************
* File Name: clock_speed.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_clock_speed_H)
#define CY_CLOCK_clock_speed_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void clock_speed_StartEx(uint32 alignClkDiv);
#define clock_speed_Start() \
    clock_speed_StartEx(clock_speed__PA_DIV_ID)

#else

void clock_speed_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void clock_speed_Stop(void);

void clock_speed_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 clock_speed_GetDividerRegister(void);
uint8  clock_speed_GetFractionalDividerRegister(void);

#define clock_speed_Enable()                         clock_speed_Start()
#define clock_speed_Disable()                        clock_speed_Stop()
#define clock_speed_SetDividerRegister(clkDivider, reset)  \
    clock_speed_SetFractionalDividerRegister((clkDivider), 0u)
#define clock_speed_SetDivider(clkDivider)           clock_speed_SetDividerRegister((clkDivider), 1u)
#define clock_speed_SetDividerValue(clkDivider)      clock_speed_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define clock_speed_DIV_ID     clock_speed__DIV_ID

#define clock_speed_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define clock_speed_CTRL_REG   (*(reg32 *)clock_speed__CTRL_REGISTER)
#define clock_speed_DIV_REG    (*(reg32 *)clock_speed__DIV_REGISTER)

#define clock_speed_CMD_DIV_SHIFT          (0u)
#define clock_speed_CMD_PA_DIV_SHIFT       (8u)
#define clock_speed_CMD_DISABLE_SHIFT      (30u)
#define clock_speed_CMD_ENABLE_SHIFT       (31u)

#define clock_speed_CMD_DISABLE_MASK       ((uint32)((uint32)1u << clock_speed_CMD_DISABLE_SHIFT))
#define clock_speed_CMD_ENABLE_MASK        ((uint32)((uint32)1u << clock_speed_CMD_ENABLE_SHIFT))

#define clock_speed_DIV_FRAC_MASK  (0x000000F8u)
#define clock_speed_DIV_FRAC_SHIFT (3u)
#define clock_speed_DIV_INT_MASK   (0xFFFFFF00u)
#define clock_speed_DIV_INT_SHIFT  (8u)

#else 

#define clock_speed_DIV_REG        (*(reg32 *)clock_speed__REGISTER)
#define clock_speed_ENABLE_REG     clock_speed_DIV_REG
#define clock_speed_DIV_FRAC_MASK  clock_speed__FRAC_MASK
#define clock_speed_DIV_FRAC_SHIFT (16u)
#define clock_speed_DIV_INT_MASK   clock_speed__DIVIDER_MASK
#define clock_speed_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_clock_speed_H) */

/* [] END OF FILE */
