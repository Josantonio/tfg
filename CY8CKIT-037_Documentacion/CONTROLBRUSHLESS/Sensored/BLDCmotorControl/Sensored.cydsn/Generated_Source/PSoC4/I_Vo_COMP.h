/*******************************************************************************
* File Name: I_Vo_COMP.h
* Version 2.20
*
* Description:
*  This file contains the function prototypes and constants used in
*  the Low Power Comparator component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_COMPARATOR_I_Vo_COMP_H)
#define CY_COMPARATOR_I_Vo_COMP_H

#include "cytypes.h"
#include "cyfitter.h"


extern uint8  I_Vo_COMP_initVar;


/***************************************
*  Conditional Compilation Parameters
****************************************/

#define I_Vo_COMP_CY_LPCOMP_V0 (CYIPBLOCK_m0s8lpcomp_VERSION == 0u) 
#define I_Vo_COMP_CY_LPCOMP_V2 (CYIPBLOCK_m0s8lpcomp_VERSION >= 2u) 


/**************************************
*        Function Prototypes
**************************************/

void    I_Vo_COMP_Start(void);
void    I_Vo_COMP_Init(void);
void    I_Vo_COMP_Enable(void);
void    I_Vo_COMP_Stop(void);
void    I_Vo_COMP_SetSpeed(uint32 speed);
void    I_Vo_COMP_SetInterruptMode(uint32 mode);
uint32  I_Vo_COMP_GetInterruptSource(void);
void    I_Vo_COMP_ClearInterrupt(uint32 interruptMask);
void    I_Vo_COMP_SetInterrupt(uint32 interruptMask);
void    I_Vo_COMP_SetHysteresis(uint32 hysteresis);
uint32  I_Vo_COMP_GetCompare(void);
uint32  I_Vo_COMP_ZeroCal(void);
void    I_Vo_COMP_LoadTrim(uint32 trimVal);
void    I_Vo_COMP_Sleep(void);
void    I_Vo_COMP_Wakeup(void);
void    I_Vo_COMP_SaveConfig(void);
void    I_Vo_COMP_RestoreConfig(void);
#if(I_Vo_COMP_CY_LPCOMP_V2)
    void    I_Vo_COMP_SetOutputMode(uint32 mode);
    void    I_Vo_COMP_SetInterruptMask(uint32 interruptMask);
    uint32  I_Vo_COMP_GetInterruptMask(void);
    uint32  I_Vo_COMP_GetInterruptSourceMasked(void);
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */


/**************************************
*           API Constants
**************************************/

#if(I_Vo_COMP_CY_LPCOMP_V2)
    /* Constants for I_Vo_COMP_SetOutputMode(), mode parameter */
    #define I_Vo_COMP_OUT_PULSE      (0x00u)
    #define I_Vo_COMP_OUT_SYNC       (0x01u)
    #define I_Vo_COMP_OUT_DIRECT     (0x02u)
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */

#define I_Vo_COMP_INTR_PARAM_MASK    (0x03u)
#define I_Vo_COMP_SPEED_PARAM_MASK   (0x03u)

/* Constants for I_Vo_COMP_SetSpeed(), speed parameter */
#define I_Vo_COMP_MED_SPEED          (0x00u)
#define I_Vo_COMP_HIGH_SPEED         (0x01u)
#define I_Vo_COMP_LOW_SPEED          (0x02u)

/* Constants for I_Vo_COMP_SetInterruptMode(), mode parameter */
#define I_Vo_COMP_INTR_DISABLE       (0x00u)
#define I_Vo_COMP_INTR_RISING        (0x01u)
#define I_Vo_COMP_INTR_FALLING       (0x02u)
#define I_Vo_COMP_INTR_BOTH          (0x03u)

/* Constants for I_Vo_COMP_SetHysteresis(), hysteresis parameter */
#define I_Vo_COMP_HYST_ENABLE        (0x00u)
#define I_Vo_COMP_HYST_DISABLE       (0x01u)

/* Constants for I_Vo_COMP_ZeroCal() */
#define I_Vo_COMP_TRIMA_MASK         (0x1Fu)
#define I_Vo_COMP_TRIMA_SIGNBIT      (4u)
#define I_Vo_COMP_TRIMA_MAX_VALUE    (15u)
#define I_Vo_COMP_TRIMB_MASK         (0x1Fu)
#define I_Vo_COMP_TRIMB_SHIFT        (8u)
#define I_Vo_COMP_TRIMB_SIGNBIT      (4u)
#define I_Vo_COMP_TRIMB_MAX_VALUE    (15u)

/* Constants for I_Vo_COMP_GetInterruptSource() and 
* I_Vo_COMP_ClearInterrupt(), interruptMask parameter 
*/
#define I_Vo_COMP_INTR_SHIFT         (I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_SHIFT)
#define I_Vo_COMP_INTR               ((uint32)0x01u << I_Vo_COMP_INTR_SHIFT)

/* Constants for I_Vo_COMP_SetInterrupt(), interruptMask parameter */
#define I_Vo_COMP_INTR_SET_SHIFT     (I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_SET_SHIFT)
#define I_Vo_COMP_INTR_SET           ((uint32)0x01u << I_Vo_COMP_INTR_SHIFT)

#if(I_Vo_COMP_CY_LPCOMP_V2)
    /* Constants for I_Vo_COMP_GetInterruptMask() and 
    * I_Vo_COMP_SetInterruptMask(), interruptMask parameter 
    */
    #define I_Vo_COMP_INTR_MASK_SHIFT    (I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASK_SHIFT)
    #define I_Vo_COMP_INTR_MASK          ((uint32)0x01u << I_Vo_COMP_INTR_MASK_SHIFT)

    /* Constants for I_Vo_COMP_GetInterruptSourceMasked() */ 
    #define I_Vo_COMP_INTR_MASKED_SHIFT  (I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASKED_SHIFT)
    #define I_Vo_COMP_INTR_MASKED        ((uint32)0x01u << I_Vo_COMP_INTR_MASKED_SHIFT)
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */


/***************************************
* Enumerated Types and Parameters 
***************************************/

/* Enumerated Types LPCompSpeedType, Used in parameter Speed */
#define I_Vo_COMP__LPC_LOW_SPEED 2
#define I_Vo_COMP__LPC_MED_SPEED 0
#define I_Vo_COMP__LPC_HIGH_SPEED 1


/* Enumerated Types LPCompInterruptType, Used in parameter Interrupt */
#define I_Vo_COMP__LPC_INT_DISABLE 0
#define I_Vo_COMP__LPC_INT_RISING 1
#define I_Vo_COMP__LPC_INT_FALLING 2
#define I_Vo_COMP__LPC_INT_BOTH 3


/* Enumerated Types LPCompHysteresisType, Used in parameter Hysteresis */
#define I_Vo_COMP__LPC_DISABLE_HYST 1
#define I_Vo_COMP__LPC_ENABLE_HYST 0


/* Enumerated Types OutputModeType, Used in parameter OutputMode */
#define I_Vo_COMP__OUT_MODE_SYNC 1
#define I_Vo_COMP__OUT_MODE_DIRECT 2
#define I_Vo_COMP__OUT_MODE_PULSE 0



/***************************************
*   Initial Parameter Constants
****************************************/

#define I_Vo_COMP_INTERRUPT    (1u)
#define I_Vo_COMP_SPEED        (0u)
#define I_Vo_COMP_HYSTERESIS   (0u)
#if (I_Vo_COMP_CY_LPCOMP_V2)
    #define I_Vo_COMP_OUT_MODE       (0u)
    #define I_Vo_COMP_INTERRUPT_EN   (0u)
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */


/**************************************
*             Registers
**************************************/

#define I_Vo_COMP_CONFIG_REG     (*(reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_CONFIG)
#define I_Vo_COMP_CONFIG_PTR     ( (reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_CONFIG)

#define I_Vo_COMP_DFT_REG        (*(reg32 *)CYREG_LPCOMP_DFT)
#define I_Vo_COMP_DFT_PTR        ( (reg32 *)CYREG_LPCOMP_DFT)

#define I_Vo_COMP_INTR_REG       (*(reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR)
#define I_Vo_COMP_INTR_PTR       ( (reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR)

#define I_Vo_COMP_INTR_SET_REG   (*(reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_SET)
#define I_Vo_COMP_INTR_SET_PTR   ( (reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_SET)

#define I_Vo_COMP_TRIMA_REG      (*(reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__TRIM_A)
#define I_Vo_COMP_TRIMA_PTR      ( (reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__TRIM_A)

#define I_Vo_COMP_TRIMB_REG      (*(reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__TRIM_B)
#define I_Vo_COMP_TRIMB_PTR      ( (reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__TRIM_B)

#if(I_Vo_COMP_CY_LPCOMP_V2)
    #define I_Vo_COMP_INTR_MASK_REG    (*(reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASK) 
    #define I_Vo_COMP_INTR_MASK_PTR    ( (reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASK) 

    #define I_Vo_COMP_INTR_MASKED_REG  (*(reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASKED) 
    #define I_Vo_COMP_INTR_MASKED_PTR  ( (reg32 *)I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_INTR_MASKED) 
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */


/***************************************
*        Registers Constants
***************************************/

#define I_Vo_COMP_CONFIG_REG_SHIFT           (I_Vo_COMP_cy_psoc4_lpcomp_1__LPCOMP_CONFIG_SHIFT)

/* I_Vo_COMPI_Vo_COMP_CONFIG_REG */
#define I_Vo_COMP_CONFIG_SPEED_MODE_SHIFT    (0u)    /* [1:0]    Operating mode for the comparator      */
#define I_Vo_COMP_CONFIG_HYST_SHIFT          (2u)    /* [2]      Add 10mV hysteresis to the comparator: 0-enable, 1-disable */
#define I_Vo_COMP_CONFIG_INTR_SHIFT          (4u)    /* [5:4]    Sets Pulse/Interrupt mode              */
#define I_Vo_COMP_CONFIG_OUT_SHIFT           (6u)    /* [6]      Current output value of the comparator    */
#define I_Vo_COMP_CONFIG_EN_SHIFT            (7u)    /* [7]      Enable comparator */
#if(I_Vo_COMP_CY_LPCOMP_V2)
    #define I_Vo_COMP_CONFIG_DSI_BYPASS_SHIFT    (16u)   /* [16]   Bypass comparator output synchronization for DSI output  */
    #define I_Vo_COMP_CONFIG_DSI_LEVEL_SHIFT     (17u)   /* [17]   Comparator DSI (trigger) out level: 0-pulse, 1-level  */
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */

#define I_Vo_COMP_CONFIG_SPEED_MODE_MASK     (((uint32) 0x03u << I_Vo_COMP_CONFIG_SPEED_MODE_SHIFT) << \
                                                    I_Vo_COMP_CONFIG_REG_SHIFT)

#define I_Vo_COMP_CONFIG_HYST                (((uint32) 0x01u << I_Vo_COMP_CONFIG_HYST_SHIFT) << \
                                                    I_Vo_COMP_CONFIG_REG_SHIFT)

#define I_Vo_COMP_CONFIG_INTR_MASK           (((uint32) 0x03u << I_Vo_COMP_CONFIG_INTR_SHIFT) << \
                                                    I_Vo_COMP_CONFIG_REG_SHIFT)

#define I_Vo_COMP_CONFIG_OUT                 (((uint32) 0x01u << I_Vo_COMP_CONFIG_OUT_SHIFT) << \
                                                    I_Vo_COMP_CONFIG_REG_SHIFT)

#define I_Vo_COMP_CONFIG_EN                  (((uint32) 0x01u << I_Vo_COMP_CONFIG_EN_SHIFT) << \
                                                    I_Vo_COMP_CONFIG_REG_SHIFT)
#if(I_Vo_COMP_CY_LPCOMP_V2)
    #define I_Vo_COMP_CONFIG_DSI_BYPASS          (((uint32) 0x01u << I_Vo_COMP_CONFIG_DSI_BYPASS_SHIFT) << \
                                                        (I_Vo_COMP_CONFIG_REG_SHIFT/2))

    #define I_Vo_COMP_CONFIG_DSI_LEVEL           (((uint32) 0x01u << I_Vo_COMP_CONFIG_DSI_LEVEL_SHIFT) << \
                                                        (I_Vo_COMP_CONFIG_REG_SHIFT/2))
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */


/* I_Vo_COMPI_Vo_COMP_DFT_REG */
#define I_Vo_COMP_DFT_CAL_EN_SHIFT    (0u)    /* [0] Calibration enable */

#define I_Vo_COMP_DFT_CAL_EN          ((uint32) 0x01u << I_Vo_COMP_DFT_CAL_EN_SHIFT)


/***************************************
*       Init Macros Definitions
***************************************/

#define I_Vo_COMP_GET_CONFIG_SPEED_MODE(mode)    ((uint32) ((((uint32) (mode) << I_Vo_COMP_CONFIG_SPEED_MODE_SHIFT) << \
                                                            I_Vo_COMP_CONFIG_REG_SHIFT) & \
                                                            I_Vo_COMP_CONFIG_SPEED_MODE_MASK))

#define I_Vo_COMP_GET_CONFIG_HYST(hysteresis)    ((0u != (hysteresis)) ? (I_Vo_COMP_CONFIG_HYST) : (0u))

#define I_Vo_COMP_GET_CONFIG_INTR(intType)   ((uint32) ((((uint32)(intType) << I_Vo_COMP_CONFIG_INTR_SHIFT) << \
                                                    I_Vo_COMP_CONFIG_REG_SHIFT) & \
                                                    I_Vo_COMP_CONFIG_INTR_MASK))
#if(I_Vo_COMP_CY_LPCOMP_V2)
    #define I_Vo_COMP_GET_CONFIG_DSI_BYPASS(bypass)  ((0u != ((bypass) & I_Vo_COMP_OUT_DIRECT)) ? \
                                                                    (I_Vo_COMP_CONFIG_DSI_BYPASS) : (0u))
   
    #define I_Vo_COMP_GET_CONFIG_DSI_LEVEL(level)    ((0u != ((level) & I_Vo_COMP_OUT_SYNC)) ? \
                                                                    (I_Vo_COMP_CONFIG_DSI_LEVEL) : (0u))
    
    #define I_Vo_COMP_GET_INTR_MASK(mask)            ((0u != (mask)) ? (I_Vo_COMP_INTR_MASK) : (0u))
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */

#if(I_Vo_COMP_CY_LPCOMP_V0)
    #define I_Vo_COMP_CONFIG_REG_DEFAULT (I_Vo_COMP_GET_CONFIG_SPEED_MODE(I_Vo_COMP_SPEED) |\
                                                 I_Vo_COMP_GET_CONFIG_HYST(I_Vo_COMP_HYSTERESIS))
#else
    #define I_Vo_COMP_CONFIG_REG_DEFAULT (I_Vo_COMP_GET_CONFIG_SPEED_MODE(I_Vo_COMP_SPEED) |\
                                                 I_Vo_COMP_GET_CONFIG_HYST(I_Vo_COMP_HYSTERESIS)  |\
                                                 I_Vo_COMP_GET_CONFIG_DSI_BYPASS(I_Vo_COMP_OUT_MODE) |\
                                                 I_Vo_COMP_GET_CONFIG_DSI_LEVEL(I_Vo_COMP_OUT_MODE))
#endif /* (I_Vo_COMP_CY_LPCOMP_V0) */

#if(I_Vo_COMP_CY_LPCOMP_V2)
    #define I_Vo_COMP_INTR_MASK_REG_DEFAULT  (I_Vo_COMP_GET_INTR_MASK(I_Vo_COMP_INTERRUPT_EN))
#endif /* (I_Vo_COMP_CY_LPCOMP_V2) */


/***************************************
* The following code is DEPRECATED and 
* should not be used in new projects.
***************************************/

#define I_Vo_COMP_CONFIG_FILT_SHIFT          (3u)    
#define I_Vo_COMP_CONFIG_FILT                ((uint32)((uint32)((uint32)0x01u << \
                                                    I_Vo_COMP_CONFIG_FILT_SHIFT) << I_Vo_COMP_CONFIG_REG_SHIFT))

#define I_Vo_COMP_DIGITAL_FILTER             (0u)

/* I_Vo_COMP_SetFilter() parameters */
#define I_Vo_COMP_FILT_DISABLE               (0x00u)
#define I_Vo_COMP_FILT_ENABLE                (0x01u)

/* I_Vo_COMP_SetSpeed() parameters */
#define I_Vo_COMP_MEDSPEED                   (I_Vo_COMP_MED_SPEED)
#define I_Vo_COMP_HIGHSPEED                  (I_Vo_COMP_HIGH_SPEED)
#define I_Vo_COMP_LOWSPEED                   (I_Vo_COMP_LOW_SPEED)

void    I_Vo_COMP_SetFilter(uint32 filter);

#endif    /* CY_COMPARATOR_I_Vo_COMP_H */


/* [] END OF FILE */
