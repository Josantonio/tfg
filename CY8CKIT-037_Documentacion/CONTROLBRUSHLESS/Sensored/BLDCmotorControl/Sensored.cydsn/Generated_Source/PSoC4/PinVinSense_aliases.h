/*******************************************************************************
* File Name: PinVinSense.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinVinSense_ALIASES_H) /* Pins PinVinSense_ALIASES_H */
#define CY_PINS_PinVinSense_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define PinVinSense_0			(PinVinSense__0__PC)
#define PinVinSense_0_PS		(PinVinSense__0__PS)
#define PinVinSense_0_PC		(PinVinSense__0__PC)
#define PinVinSense_0_DR		(PinVinSense__0__DR)
#define PinVinSense_0_SHIFT	(PinVinSense__0__SHIFT)
#define PinVinSense_0_INTR	((uint16)((uint16)0x0003u << (PinVinSense__0__SHIFT*2u)))

#define PinVinSense_INTR_ALL	 ((uint16)(PinVinSense_0_INTR))


#endif /* End Pins PinVinSense_ALIASES_H */


/* [] END OF FILE */
