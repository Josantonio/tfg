/*******************************************************************************
* File Name: I_ref_PM.c
* Version 1.10
*
* Description:
*  This file provides Low power mode APIs for IDAC_P4 component.
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "I_ref.h"


static I_ref_BACKUP_STRUCT I_ref_backup;


/*******************************************************************************
* Function Name: I_ref_SaveConfig
********************************************************************************
*
* Summary:
*  Saves component state before sleep
* Parameters:
*  None
*
* Return:
*  None
*
* Global Variables:
*  None
*
*******************************************************************************/
void I_ref_SaveConfig(void)
{
    /* All registers are retention - nothing to save */
}


/*******************************************************************************
* Function Name: I_ref_Sleep
********************************************************************************
*
* Summary:
*  Calls _SaveConfig() function
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void I_ref_Sleep(void)
{
        if(0u != (I_ref_IDAC_CONTROL_REG & ((uint32)I_ref_IDAC_MODE_MASK <<
        I_ref_IDAC_MODE_POSITION)))
        {
            I_ref_backup.enableState = 1u;
        }
        else
        {
            I_ref_backup.enableState = 0u;
        }

    I_ref_Stop();
    I_ref_SaveConfig();
}


/*******************************************************************************
* Function Name: I_ref_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores component state after wakeup
* Parameters:
*  None
*
* Return:
*  None
*
* Global Variables:
*  None
*
*******************************************************************************/
void I_ref_RestoreConfig(void)
{
    /* All registers are retention - nothing to save */
}


/*******************************************************************************
* Function Name: I_ref_Wakeup
********************************************************************************
*
* Summary:
*  Calls _RestoreConfig() function
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void I_ref_Wakeup(void)
{
    /* Restore IDAC register settings */
    I_ref_RestoreConfig();
    if(I_ref_backup.enableState == 1u)
    {
        /* Enable operation */
        I_ref_Enable();
    } /* Do nothing if the component was disabled before */

}


/* [] END OF FILE */
