/*******************************************************************************
* File Name: I_Vo_COMP.c
* Version 2.20
*
* Description:
*  This file provides the power management source code APIs for the
*  Analog Low Power Comparator component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "I_Vo_COMP.h"


/*******************************************************************************
* Function Name: I_Vo_COMP_SaveConfig
********************************************************************************
*
* Summary:
*  Empty function. Included for consistency with other components.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void I_Vo_COMP_SaveConfig(void)
{

}


/*******************************************************************************
* Function Name: I_Vo_COMP_RestoreConfig
********************************************************************************
*
* Summary:
*  Empty function. Included for consistency with other components.
*
* Parameters:
*  None
*
* Return:
*  None
*
********************************************************************************/
void I_Vo_COMP_RestoreConfig(void)
{

}


/*******************************************************************************
* Function Name: I_Vo_COMP_Sleep
********************************************************************************
*
* Summary:
*  Empty function. Included for consistency with other components.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void I_Vo_COMP_Sleep(void)
{

}


/*******************************************************************************
* Function Name: I_Vo_COMP_Wakeup
********************************************************************************
*
* Summary:
*  Empty function. Included for consistency with other components.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void I_Vo_COMP_Wakeup(void)
{

}


/* [] END OF FILE */

