/*******************************************************************************
* File Name: CH.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CH_ALIASES_H) /* Pins CH_ALIASES_H */
#define CY_PINS_CH_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define CH_0			(CH__0__PC)
#define CH_0_PS		(CH__0__PS)
#define CH_0_PC		(CH__0__PC)
#define CH_0_DR		(CH__0__DR)
#define CH_0_SHIFT	(CH__0__SHIFT)
#define CH_0_INTR	((uint16)((uint16)0x0003u << (CH__0__SHIFT*2u)))

#define CH_INTR_ALL	 ((uint16)(CH_0_INTR))


#endif /* End Pins CH_ALIASES_H */


/* [] END OF FILE */
