/*******************************************************************************
* File Name: PinProbe.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinProbe_ALIASES_H) /* Pins PinProbe_ALIASES_H */
#define CY_PINS_PinProbe_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define PinProbe_0			(PinProbe__0__PC)
#define PinProbe_0_PS		(PinProbe__0__PS)
#define PinProbe_0_PC		(PinProbe__0__PC)
#define PinProbe_0_DR		(PinProbe__0__DR)
#define PinProbe_0_SHIFT	(PinProbe__0__SHIFT)
#define PinProbe_0_INTR	((uint16)((uint16)0x0003u << (PinProbe__0__SHIFT*2u)))

#define PinProbe_INTR_ALL	 ((uint16)(PinProbe_0_INTR))


#endif /* End Pins PinProbe_ALIASES_H */


/* [] END OF FILE */
