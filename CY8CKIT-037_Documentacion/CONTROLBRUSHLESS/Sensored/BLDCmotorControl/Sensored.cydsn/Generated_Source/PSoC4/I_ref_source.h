/*******************************************************************************
* File Name: I_ref_source.h
* Version 1.10
*
* Description:
*  This file provides constants and parameter values for the IDAC_P4
*  component.
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_IDAC_I_ref_source_H)
#define CY_IDAC_I_ref_source_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h"


/***************************************
* Internal Type defines
***************************************/

/* Structure to save state before go to sleep */
typedef struct
{
    uint8  enableState;
} I_ref_source_BACKUP_STRUCT;


extern uint32 I_ref_source_initVar;

/**************************************
*    Enumerated Types and Parameters
**************************************/

/* IDAC polarity setting constants */
#define I_ref_source__SOURCE 0
#define I_ref_source__SINK 1


/* IDAC range setting constants */
#define I_ref_source__LOWRANGE 0
#define I_ref_source__HIGHRANGE 1


/* IDAC polarity setting definitions */
#define I_ref_source_MODE_SOURCE    (I_ref_source__SOURCE)
#define I_ref_source_MODE_SINK      (I_ref_source__SINK)

/* IDAC RANGE setting definitions */
#define I_ref_source_MODE_LOWRANGE  (I_ref_source__LOWRANGE)
#define I_ref_source_MODE_HIGHRANGE (I_ref_source__HIGHRANGE)

/***************************************
*   Conditional Compilation Parameters
****************************************/

#define I_ref_source_IDAC_RESOLUTION    (8u)
#define I_ref_source_IDAC_RANGE         (0u)
#define I_ref_source_IDAC_POLARITY      (0u)


/***************************************
*    Initial Parameter Constants
***************************************/
#define I_ref_source_IDAC_INIT_VALUE    (146u)




/***************************************
*        Function Prototypes
***************************************/

void I_ref_source_Init(void);
void I_ref_source_Enable(void);
void I_ref_source_Start(void);
void I_ref_source_Stop(void);
void I_ref_source_SetValue(uint32  value);
void I_ref_source_SaveConfig(void);
void I_ref_source_Sleep(void);
void I_ref_source_RestoreConfig(void);
void I_ref_source_Wakeup(void);


/***************************************
*            API Constants
***************************************/

#define I_ref_source_IDAC_EN_MODE           (3u)
#define I_ref_source_IDAC_CSD_EN            (1u)
#define I_ref_source_IDAC_CSD_EN_POSITION   (31u)

#define I_ref_source_IDAC_VALUE_POSITION    (I_ref_source_cy_psoc4_idac__CSD_IDAC_SHIFT)

#define I_ref_source_IDAC_MODE_SHIFT        (8u)
#define I_ref_source_IDAC_MODE_POSITION     ((uint32)I_ref_source_cy_psoc4_idac__CSD_IDAC_SHIFT +\
                                                 I_ref_source_IDAC_MODE_SHIFT)

#define I_ref_source_IDAC_RANGE_SHIFT       (10u)
#define I_ref_source_IDAC_RANGE_POSITION    ((uint32)I_ref_source_cy_psoc4_idac__CSD_IDAC_SHIFT +\
                                                 I_ref_source_IDAC_RANGE_SHIFT)

#define I_ref_source_IDAC_POLARITY_POSITION ((uint32)I_ref_source_cy_psoc4_idac__POLARITY_SHIFT)

#define I_ref_source_IDAC_TRIM1_POSITION    ((uint32)I_ref_source_cy_psoc4_idac__CSD_TRIM1_SHIFT)
#define I_ref_source_IDAC_TRIM2_POSITION    ((uint32)I_ref_source_cy_psoc4_idac__CSD_TRIM2_SHIFT)

#define I_ref_source_IDAC_CDS_EN_MASK       (0x80000000u)

#if(I_ref_source_IDAC_RESOLUTION == 8u)
  #define I_ref_source_IDAC_VALUE_MASK      (0xFFu)
#else
  #define I_ref_source_IDAC_VALUE_MASK      (0x7Fu)
#endif /* (I_ref_source_IDAC_RESOLUTION == 8u) */

#define I_ref_source_IDAC_MODE_MASK         (3u)
#define I_ref_source_IDAC_RANGE_MASK        (1u)
#define I_ref_source_IDAC_POLARITY_MASK     (1u)

#define I_ref_source_CSD_IDAC_TRIM1_MASK    (0x0000000FuL << I_ref_source_IDAC_TRIM1_POSITION)
#define I_ref_source_CSD_IDAC_TRIM2_MASK    (0x0000000FuL << I_ref_source_IDAC_TRIM2_POSITION)


/***************************************
*        Registers
***************************************/

#define I_ref_source_IDAC_CONTROL_REG    (*(reg32 *)I_ref_source_cy_psoc4_idac__CSD_IDAC)
#define I_ref_source_IDAC_CONTROL_PTR    ( (reg32 *)I_ref_source_cy_psoc4_idac__CSD_IDAC)

#define I_ref_source_IDAC_POLARITY_CONTROL_REG    (*(reg32 *)I_ref_source_cy_psoc4_idac__CONTROL)
#define I_ref_source_IDAC_POLARITY_CONTROL_PTR    ( (reg32 *)I_ref_source_cy_psoc4_idac__CONTROL)

#define I_ref_source_CSD_TRIM1_REG       (*(reg32 *)I_ref_source_cy_psoc4_idac__CSD_TRIM1)
#define I_ref_source_CSD_TRIM1_PTR       ( (reg32 *)I_ref_source_cy_psoc4_idac__CSD_TRIM1)

#define I_ref_source_CSD_TRIM2_REG       (*(reg32 *)I_ref_source_cy_psoc4_idac__CSD_TRIM2)
#define I_ref_source_CSD_TRIM2_PTR       ( (reg32 *)I_ref_source_cy_psoc4_idac__CSD_TRIM2)

#if (CY_PSOC4_4100M || CY_PSOC4_4200M)
    #if(I_ref_source_cy_psoc4_idac__IDAC_NUMBER > 2u)
        #define I_ref_source_SFLASH_TRIM1_REG       (*(reg8 *)CYREG_SFLASH_CSD1_TRIM1_HVIDAC)
        #define I_ref_source_SFLASH_TRIM1_PTR       ( (reg8 *)CYREG_SFLASH_CSD1_TRIM1_HVIDAC)
        
        #define I_ref_source_SFLASH_TRIM2_REG       (*(reg8 *)CYREG_SFLASH_CSD1_TRIM2_HVIDAC)
        #define I_ref_source_SFLASH_TRIM2_PTR       ( (reg8 *)CYREG_SFLASH_CSD1_TRIM2_HVIDAC)
    #else
        #define I_ref_source_SFLASH_TRIM1_REG       (*(reg8 *)CYREG_SFLASH_CSD_TRIM1_HVIDAC)
        #define I_ref_source_SFLASH_TRIM1_PTR       ( (reg8 *)CYREG_SFLASH_CSD_TRIM1_HVIDAC)
        
        #define I_ref_source_SFLASH_TRIM2_REG       (*(reg8 *)CYREG_SFLASH_CSD_TRIM2_HVIDAC)
        #define I_ref_source_SFLASH_TRIM2_PTR       ( (reg8 *)CYREG_SFLASH_CSD_TRIM2_HVIDAC)
    #endif /* (I_ref_source_cy_psoc4_idac__IDAC_NUMBER > 2u) */
#else
    #define I_ref_source_SFLASH_TRIM1_REG       (*(reg8 *)CYREG_SFLASH_CSD_TRIM1_HVIDAC)
    #define I_ref_source_SFLASH_TRIM1_PTR       ( (reg8 *)CYREG_SFLASH_CSD_TRIM1_HVIDAC)
    
    #define I_ref_source_SFLASH_TRIM2_REG       (*(reg8 *)CYREG_SFLASH_CSD_TRIM2_HVIDAC)
    #define I_ref_source_SFLASH_TRIM2_PTR       ( (reg8 *)CYREG_SFLASH_CSD_TRIM2_HVIDAC)
#endif /* (CY_PSOC4_4100M || CY_PSOC4_4200M) */

#endif /* CY_IDAC_I_ref_source_H */

/* [] END OF FILE */
