/*******************************************************************************
* File Name: Ia.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Ia_H) /* Pins Ia_H */
#define CY_PINS_Ia_H

#include "cytypes.h"
#include "cyfitter.h"
#include "Ia_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} Ia_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   Ia_Read(void);
void    Ia_Write(uint8 value);
uint8   Ia_ReadDataReg(void);
#if defined(Ia__PC) || (CY_PSOC4_4200L) 
    void    Ia_SetDriveMode(uint8 mode);
#endif
void    Ia_SetInterruptMode(uint16 position, uint16 mode);
uint8   Ia_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void Ia_Sleep(void); 
void Ia_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(Ia__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define Ia_DRIVE_MODE_BITS        (3)
    #define Ia_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - Ia_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the Ia_SetDriveMode() function.
         *  @{
         */
        #define Ia_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define Ia_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define Ia_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define Ia_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define Ia_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define Ia_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define Ia_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define Ia_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define Ia_MASK               Ia__MASK
#define Ia_SHIFT              Ia__SHIFT
#define Ia_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Ia_SetInterruptMode() function.
     *  @{
     */
        #define Ia_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define Ia_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define Ia_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define Ia_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(Ia__SIO)
    #define Ia_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(Ia__PC) && (CY_PSOC4_4200L)
    #define Ia_USBIO_ENABLE               ((uint32)0x80000000u)
    #define Ia_USBIO_DISABLE              ((uint32)(~Ia_USBIO_ENABLE))
    #define Ia_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define Ia_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define Ia_USBIO_ENTER_SLEEP          ((uint32)((1u << Ia_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << Ia_USBIO_SUSPEND_DEL_SHIFT)))
    #define Ia_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << Ia_USBIO_SUSPEND_SHIFT)))
    #define Ia_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << Ia_USBIO_SUSPEND_DEL_SHIFT)))
    #define Ia_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(Ia__PC)
    /* Port Configuration */
    #define Ia_PC                 (* (reg32 *) Ia__PC)
#endif
/* Pin State */
#define Ia_PS                     (* (reg32 *) Ia__PS)
/* Data Register */
#define Ia_DR                     (* (reg32 *) Ia__DR)
/* Input Buffer Disable Override */
#define Ia_INP_DIS                (* (reg32 *) Ia__PC2)

/* Interrupt configuration Registers */
#define Ia_INTCFG                 (* (reg32 *) Ia__INTCFG)
#define Ia_INTSTAT                (* (reg32 *) Ia__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define Ia_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(Ia__SIO)
    #define Ia_SIO_REG            (* (reg32 *) Ia__SIO)
#endif /* (Ia__SIO_CFG) */

/* USBIO registers */
#if !defined(Ia__PC) && (CY_PSOC4_4200L)
    #define Ia_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define Ia_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define Ia_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define Ia_DRIVE_MODE_SHIFT       (0x00u)
#define Ia_DRIVE_MODE_MASK        (0x07u << Ia_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins Ia_H */


/* [] END OF FILE */
