/*******************************************************************************
* File Name: Hall_clock.h
* Version 3.30
*
* Description:
*  Contains the prototypes and constants for the functions available to the
*  PWM user module.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PWM_Hall_clock_H)
#define CY_PWM_Hall_clock_H

#include "cyfitter.h"
#include "cytypes.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 Hall_clock_initVar;


/***************************************
* Conditional Compilation Parameters
***************************************/
#define Hall_clock_Resolution                     (16u)
#define Hall_clock_UsingFixedFunction             (0u)
#define Hall_clock_DeadBandMode                   (0u)
#define Hall_clock_KillModeMinTime                (0u)
#define Hall_clock_KillMode                       (0u)
#define Hall_clock_PWMMode                        (0u)
#define Hall_clock_PWMModeIsCenterAligned         (0u)
#define Hall_clock_DeadBandUsed                   (0u)
#define Hall_clock_DeadBand2_4                    (0u)

#if !defined(Hall_clock_PWMUDB_genblk8_stsreg__REMOVED)
    #define Hall_clock_UseStatus                  (1u)
#else
    #define Hall_clock_UseStatus                  (0u)
#endif /* !defined(Hall_clock_PWMUDB_genblk8_stsreg__REMOVED) */

#if !defined(Hall_clock_PWMUDB_genblk1_ctrlreg__REMOVED)
    #define Hall_clock_UseControl                 (1u)
#else
    #define Hall_clock_UseControl                 (0u)
#endif /* !defined(Hall_clock_PWMUDB_genblk1_ctrlreg__REMOVED) */

#define Hall_clock_UseOneCompareMode              (1u)
#define Hall_clock_MinimumKillTime                (1u)
#define Hall_clock_EnableMode                     (0u)

#define Hall_clock_CompareMode1SW                 (0u)
#define Hall_clock_CompareMode2SW                 (0u)

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component PWM_v3_30 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */

/* Use Kill Mode Enumerated Types */
#define Hall_clock__B_PWM__DISABLED 0
#define Hall_clock__B_PWM__ASYNCHRONOUS 1
#define Hall_clock__B_PWM__SINGLECYCLE 2
#define Hall_clock__B_PWM__LATCHED 3
#define Hall_clock__B_PWM__MINTIME 4


/* Use Dead Band Mode Enumerated Types */
#define Hall_clock__B_PWM__DBMDISABLED 0
#define Hall_clock__B_PWM__DBM_2_4_CLOCKS 1
#define Hall_clock__B_PWM__DBM_256_CLOCKS 2


/* Used PWM Mode Enumerated Types */
#define Hall_clock__B_PWM__ONE_OUTPUT 0
#define Hall_clock__B_PWM__TWO_OUTPUTS 1
#define Hall_clock__B_PWM__DUAL_EDGE 2
#define Hall_clock__B_PWM__CENTER_ALIGN 3
#define Hall_clock__B_PWM__DITHER 5
#define Hall_clock__B_PWM__HARDWARESELECT 4


/* Used PWM Compare Mode Enumerated Types */
#define Hall_clock__B_PWM__LESS_THAN 1
#define Hall_clock__B_PWM__LESS_THAN_OR_EQUAL 2
#define Hall_clock__B_PWM__GREATER_THAN 3
#define Hall_clock__B_PWM__GREATER_THAN_OR_EQUAL_TO 4
#define Hall_clock__B_PWM__EQUAL 0
#define Hall_clock__B_PWM__FIRMWARE 5



/***************************************
* Data Struct Definition
***************************************/


/**************************************************************************
 * Sleep Wakeup Backup structure for PWM Component
 *************************************************************************/
typedef struct
{

    uint8 PWMEnableState;

    #if(!Hall_clock_UsingFixedFunction)
        uint16 PWMUdb;               /* PWM Current Counter value  */
        #if(!Hall_clock_PWMModeIsCenterAligned)
            uint16 PWMPeriod;
        #endif /* (!Hall_clock_PWMModeIsCenterAligned) */
        #if (Hall_clock_UseStatus)
            uint8 InterruptMaskValue;   /* PWM Current Interrupt Mask */
        #endif /* (Hall_clock_UseStatus) */

        /* Backup for Deadband parameters */
        #if(Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_256_CLOCKS || \
            Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_2_4_CLOCKS)
            uint8 PWMdeadBandValue; /* Dead Band Counter Current Value */
        #endif /* deadband count is either 2-4 clocks or 256 clocks */

        /* Backup Kill Mode Counter*/
        #if(Hall_clock_KillModeMinTime)
            uint8 PWMKillCounterPeriod; /* Kill Mode period value */
        #endif /* (Hall_clock_KillModeMinTime) */

        /* Backup control register */
        #if(Hall_clock_UseControl)
            uint8 PWMControlRegister; /* PWM Control Register value */
        #endif /* (Hall_clock_UseControl) */

    #endif /* (!Hall_clock_UsingFixedFunction) */

}Hall_clock_backupStruct;


/***************************************
*        Function Prototypes
 **************************************/

void    Hall_clock_Start(void) ;
void    Hall_clock_Stop(void) ;

#if (Hall_clock_UseStatus || Hall_clock_UsingFixedFunction)
    void  Hall_clock_SetInterruptMode(uint8 interruptMode) ;
    uint8 Hall_clock_ReadStatusRegister(void) ;
#endif /* (Hall_clock_UseStatus || Hall_clock_UsingFixedFunction) */

#define Hall_clock_GetInterruptSource() Hall_clock_ReadStatusRegister()

#if (Hall_clock_UseControl)
    uint8 Hall_clock_ReadControlRegister(void) ;
    void  Hall_clock_WriteControlRegister(uint8 control)
          ;
#endif /* (Hall_clock_UseControl) */

#if (Hall_clock_UseOneCompareMode)
   #if (Hall_clock_CompareMode1SW)
       void    Hall_clock_SetCompareMode(uint8 comparemode)
               ;
   #endif /* (Hall_clock_CompareMode1SW) */
#else
    #if (Hall_clock_CompareMode1SW)
        void    Hall_clock_SetCompareMode1(uint8 comparemode)
                ;
    #endif /* (Hall_clock_CompareMode1SW) */
    #if (Hall_clock_CompareMode2SW)
        void    Hall_clock_SetCompareMode2(uint8 comparemode)
                ;
    #endif /* (Hall_clock_CompareMode2SW) */
#endif /* (Hall_clock_UseOneCompareMode) */

#if (!Hall_clock_UsingFixedFunction)
    uint16   Hall_clock_ReadCounter(void) ;
    uint16 Hall_clock_ReadCapture(void) ;

    #if (Hall_clock_UseStatus)
            void Hall_clock_ClearFIFO(void) ;
    #endif /* (Hall_clock_UseStatus) */

    void    Hall_clock_WriteCounter(uint16 counter)
            ;
#endif /* (!Hall_clock_UsingFixedFunction) */

void    Hall_clock_WritePeriod(uint16 period)
        ;
uint16 Hall_clock_ReadPeriod(void) ;

#if (Hall_clock_UseOneCompareMode)
    void    Hall_clock_WriteCompare(uint16 compare)
            ;
    uint16 Hall_clock_ReadCompare(void) ;
#else
    void    Hall_clock_WriteCompare1(uint16 compare)
            ;
    uint16 Hall_clock_ReadCompare1(void) ;
    void    Hall_clock_WriteCompare2(uint16 compare)
            ;
    uint16 Hall_clock_ReadCompare2(void) ;
#endif /* (Hall_clock_UseOneCompareMode) */


#if (Hall_clock_DeadBandUsed)
    void    Hall_clock_WriteDeadTime(uint8 deadtime) ;
    uint8   Hall_clock_ReadDeadTime(void) ;
#endif /* (Hall_clock_DeadBandUsed) */

#if ( Hall_clock_KillModeMinTime)
    void Hall_clock_WriteKillTime(uint8 killtime) ;
    uint8 Hall_clock_ReadKillTime(void) ;
#endif /* ( Hall_clock_KillModeMinTime) */

void Hall_clock_Init(void) ;
void Hall_clock_Enable(void) ;
void Hall_clock_Sleep(void) ;
void Hall_clock_Wakeup(void) ;
void Hall_clock_SaveConfig(void) ;
void Hall_clock_RestoreConfig(void) ;


/***************************************
*         Initialization Values
**************************************/
#define Hall_clock_INIT_PERIOD_VALUE          (49920u)
#define Hall_clock_INIT_COMPARE_VALUE1        (24960u)
#define Hall_clock_INIT_COMPARE_VALUE2        (63u)
#define Hall_clock_INIT_INTERRUPTS_MODE       (uint8)(((uint8)(0u <<   \
                                                    Hall_clock_STATUS_TC_INT_EN_MASK_SHIFT)) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    Hall_clock_STATUS_CMP2_INT_EN_MASK_SHIFT)) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    Hall_clock_STATUS_CMP1_INT_EN_MASK_SHIFT )) | \
                                                    (uint8)((uint8)(0u <<  \
                                                    Hall_clock_STATUS_KILL_INT_EN_MASK_SHIFT )))
#define Hall_clock_DEFAULT_COMPARE2_MODE      (uint8)((uint8)1u <<  Hall_clock_CTRL_CMPMODE2_SHIFT)
#define Hall_clock_DEFAULT_COMPARE1_MODE      (uint8)((uint8)1u <<  Hall_clock_CTRL_CMPMODE1_SHIFT)
#define Hall_clock_INIT_DEAD_TIME             (1u)


/********************************
*         Registers
******************************** */

#if (Hall_clock_UsingFixedFunction)
   #define Hall_clock_PERIOD_LSB              (*(reg16 *) Hall_clock_PWMHW__PER0)
   #define Hall_clock_PERIOD_LSB_PTR          ( (reg16 *) Hall_clock_PWMHW__PER0)
   #define Hall_clock_COMPARE1_LSB            (*(reg16 *) Hall_clock_PWMHW__CNT_CMP0)
   #define Hall_clock_COMPARE1_LSB_PTR        ( (reg16 *) Hall_clock_PWMHW__CNT_CMP0)
   #define Hall_clock_COMPARE2_LSB            (0x00u)
   #define Hall_clock_COMPARE2_LSB_PTR        (0x00u)
   #define Hall_clock_COUNTER_LSB             (*(reg16 *) Hall_clock_PWMHW__CNT_CMP0)
   #define Hall_clock_COUNTER_LSB_PTR         ( (reg16 *) Hall_clock_PWMHW__CNT_CMP0)
   #define Hall_clock_CAPTURE_LSB             (*(reg16 *) Hall_clock_PWMHW__CAP0)
   #define Hall_clock_CAPTURE_LSB_PTR         ( (reg16 *) Hall_clock_PWMHW__CAP0)
   #define Hall_clock_RT1                     (*(reg8 *)  Hall_clock_PWMHW__RT1)
   #define Hall_clock_RT1_PTR                 ( (reg8 *)  Hall_clock_PWMHW__RT1)

#else
   #if (Hall_clock_Resolution == 8u) /* 8bit - PWM */

       #if(Hall_clock_PWMModeIsCenterAligned)
           #define Hall_clock_PERIOD_LSB      (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__D1_REG)
           #define Hall_clock_PERIOD_LSB_PTR  ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u0__D1_REG)
       #else
           #define Hall_clock_PERIOD_LSB      (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__F0_REG)
           #define Hall_clock_PERIOD_LSB_PTR  ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u0__F0_REG)
       #endif /* (Hall_clock_PWMModeIsCenterAligned) */

       #define Hall_clock_COMPARE1_LSB        (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__D0_REG)
       #define Hall_clock_COMPARE1_LSB_PTR    ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u0__D0_REG)
       #define Hall_clock_COMPARE2_LSB        (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__D1_REG)
       #define Hall_clock_COMPARE2_LSB_PTR    ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u0__D1_REG)
       #define Hall_clock_COUNTERCAP_LSB      (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__A1_REG)
       #define Hall_clock_COUNTERCAP_LSB_PTR  ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u0__A1_REG)
       #define Hall_clock_COUNTER_LSB         (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__A0_REG)
       #define Hall_clock_COUNTER_LSB_PTR     ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u0__A0_REG)
       #define Hall_clock_CAPTURE_LSB         (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__F1_REG)
       #define Hall_clock_CAPTURE_LSB_PTR     ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u0__F1_REG)

   #else
        #if(CY_PSOC3) /* 8-bit address space */
            #if(Hall_clock_PWMModeIsCenterAligned)
               #define Hall_clock_PERIOD_LSB      (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__D1_REG)
               #define Hall_clock_PERIOD_LSB_PTR  ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__D1_REG)
            #else
               #define Hall_clock_PERIOD_LSB      (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__F0_REG)
               #define Hall_clock_PERIOD_LSB_PTR  ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__F0_REG)
            #endif /* (Hall_clock_PWMModeIsCenterAligned) */

            #define Hall_clock_COMPARE1_LSB       (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__D0_REG)
            #define Hall_clock_COMPARE1_LSB_PTR   ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__D0_REG)
            #define Hall_clock_COMPARE2_LSB       (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__D1_REG)
            #define Hall_clock_COMPARE2_LSB_PTR   ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__D1_REG)
            #define Hall_clock_COUNTERCAP_LSB     (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__A1_REG)
            #define Hall_clock_COUNTERCAP_LSB_PTR ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__A1_REG)
            #define Hall_clock_COUNTER_LSB        (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__A0_REG)
            #define Hall_clock_COUNTER_LSB_PTR    ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__A0_REG)
            #define Hall_clock_CAPTURE_LSB        (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__F1_REG)
            #define Hall_clock_CAPTURE_LSB_PTR    ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__F1_REG)
        #else
            #if(Hall_clock_PWMModeIsCenterAligned)
               #define Hall_clock_PERIOD_LSB      (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_D1_REG)
               #define Hall_clock_PERIOD_LSB_PTR  ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_D1_REG)
            #else
               #define Hall_clock_PERIOD_LSB      (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_F0_REG)
               #define Hall_clock_PERIOD_LSB_PTR  ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_F0_REG)
            #endif /* (Hall_clock_PWMModeIsCenterAligned) */

            #define Hall_clock_COMPARE1_LSB       (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_D0_REG)
            #define Hall_clock_COMPARE1_LSB_PTR   ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_D0_REG)
            #define Hall_clock_COMPARE2_LSB       (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_D1_REG)
            #define Hall_clock_COMPARE2_LSB_PTR   ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_D1_REG)
            #define Hall_clock_COUNTERCAP_LSB     (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_A1_REG)
            #define Hall_clock_COUNTERCAP_LSB_PTR ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_A1_REG)
            #define Hall_clock_COUNTER_LSB        (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_A0_REG)
            #define Hall_clock_COUNTER_LSB_PTR    ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_A0_REG)
            #define Hall_clock_CAPTURE_LSB        (*(reg16 *) Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_F1_REG)
            #define Hall_clock_CAPTURE_LSB_PTR    ((reg16 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__16BIT_F1_REG)
        #endif /* (CY_PSOC3) */

       #define Hall_clock_AUX_CONTROLDP1          (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u1__DP_AUX_CTL_REG)
       #define Hall_clock_AUX_CONTROLDP1_PTR      ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u1__DP_AUX_CTL_REG)

   #endif /* (Hall_clock_Resolution == 8) */

   #define Hall_clock_COUNTERCAP_LSB_PTR_8BIT ( (reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__A1_REG)
   #define Hall_clock_AUX_CONTROLDP0          (*(reg8 *)  Hall_clock_PWMUDB_sP16_pwmdp_u0__DP_AUX_CTL_REG)
   #define Hall_clock_AUX_CONTROLDP0_PTR      ((reg8 *)   Hall_clock_PWMUDB_sP16_pwmdp_u0__DP_AUX_CTL_REG)

#endif /* (Hall_clock_UsingFixedFunction) */

#if(Hall_clock_KillModeMinTime )
    #define Hall_clock_KILLMODEMINTIME        (*(reg8 *)  Hall_clock_PWMUDB_sKM_killmodecounterdp_u0__D0_REG)
    #define Hall_clock_KILLMODEMINTIME_PTR    ((reg8 *)   Hall_clock_PWMUDB_sKM_killmodecounterdp_u0__D0_REG)
    /* Fixed Function Block has no Kill Mode parameters because it is Asynchronous only */
#endif /* (Hall_clock_KillModeMinTime ) */

#if(Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_256_CLOCKS)
    #define Hall_clock_DEADBAND_COUNT         (*(reg8 *)  Hall_clock_PWMUDB_sDB255_deadbandcounterdp_u0__D0_REG)
    #define Hall_clock_DEADBAND_COUNT_PTR     ((reg8 *)   Hall_clock_PWMUDB_sDB255_deadbandcounterdp_u0__D0_REG)
    #define Hall_clock_DEADBAND_LSB_PTR       ((reg8 *)   Hall_clock_PWMUDB_sDB255_deadbandcounterdp_u0__A0_REG)
    #define Hall_clock_DEADBAND_LSB           (*(reg8 *)  Hall_clock_PWMUDB_sDB255_deadbandcounterdp_u0__A0_REG)
#elif(Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_2_4_CLOCKS)
    
    /* In Fixed Function Block these bits are in the control blocks control register */
    #if (Hall_clock_UsingFixedFunction)
        #define Hall_clock_DEADBAND_COUNT         (*(reg8 *)  Hall_clock_PWMHW__CFG0)
        #define Hall_clock_DEADBAND_COUNT_PTR     ((reg8 *)   Hall_clock_PWMHW__CFG0)
        #define Hall_clock_DEADBAND_COUNT_MASK    (uint8)((uint8)0x03u << Hall_clock_DEADBAND_COUNT_SHIFT)

        /* As defined by the Register Map as DEADBAND_PERIOD[1:0] in CFG0 */
        #define Hall_clock_DEADBAND_COUNT_SHIFT   (0x06u)
    #else
        /* Lower two bits of the added control register define the count 1-3 */
        #define Hall_clock_DEADBAND_COUNT         (*(reg8 *)  Hall_clock_PWMUDB_genblk7_dbctrlreg__CONTROL_REG)
        #define Hall_clock_DEADBAND_COUNT_PTR     ((reg8 *)   Hall_clock_PWMUDB_genblk7_dbctrlreg__CONTROL_REG)
        #define Hall_clock_DEADBAND_COUNT_MASK    (uint8)((uint8)0x03u << Hall_clock_DEADBAND_COUNT_SHIFT)

        /* As defined by the verilog implementation of the Control Register */
        #define Hall_clock_DEADBAND_COUNT_SHIFT   (0x00u)
    #endif /* (Hall_clock_UsingFixedFunction) */
#endif /* (Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_256_CLOCKS) */



#if (Hall_clock_UsingFixedFunction)
    #define Hall_clock_STATUS                 (*(reg8 *) Hall_clock_PWMHW__SR0)
    #define Hall_clock_STATUS_PTR             ((reg8 *) Hall_clock_PWMHW__SR0)
    #define Hall_clock_STATUS_MASK            (*(reg8 *) Hall_clock_PWMHW__SR0)
    #define Hall_clock_STATUS_MASK_PTR        ((reg8 *) Hall_clock_PWMHW__SR0)
    #define Hall_clock_CONTROL                (*(reg8 *) Hall_clock_PWMHW__CFG0)
    #define Hall_clock_CONTROL_PTR            ((reg8 *) Hall_clock_PWMHW__CFG0)
    #define Hall_clock_CONTROL2               (*(reg8 *) Hall_clock_PWMHW__CFG1)
    #define Hall_clock_CONTROL3               (*(reg8 *) Hall_clock_PWMHW__CFG2)
    #define Hall_clock_GLOBAL_ENABLE          (*(reg8 *) Hall_clock_PWMHW__PM_ACT_CFG)
    #define Hall_clock_GLOBAL_ENABLE_PTR      ( (reg8 *) Hall_clock_PWMHW__PM_ACT_CFG)
    #define Hall_clock_GLOBAL_STBY_ENABLE     (*(reg8 *) Hall_clock_PWMHW__PM_STBY_CFG)
    #define Hall_clock_GLOBAL_STBY_ENABLE_PTR ( (reg8 *) Hall_clock_PWMHW__PM_STBY_CFG)


    /***********************************
    *          Constants
    ***********************************/

    /* Fixed Function Block Chosen */
    #define Hall_clock_BLOCK_EN_MASK          (Hall_clock_PWMHW__PM_ACT_MSK)
    #define Hall_clock_BLOCK_STBY_EN_MASK     (Hall_clock_PWMHW__PM_STBY_MSK)
    
    /* Control Register definitions */
    #define Hall_clock_CTRL_ENABLE_SHIFT      (0x00u)

    /* As defined by Register map as MODE_CFG bits in CFG2*/
    #define Hall_clock_CTRL_CMPMODE1_SHIFT    (0x04u)

    /* As defined by Register map */
    #define Hall_clock_CTRL_DEAD_TIME_SHIFT   (0x06u)  

    /* Fixed Function Block Only CFG register bit definitions */
    /*  Set to compare mode */
    #define Hall_clock_CFG0_MODE              (0x02u)   

    /* Enable the block to run */
    #define Hall_clock_CFG0_ENABLE            (0x01u)   
    
    /* As defined by Register map as DB bit in CFG0 */
    #define Hall_clock_CFG0_DB                (0x20u)   

    /* Control Register Bit Masks */
    #define Hall_clock_CTRL_ENABLE            (uint8)((uint8)0x01u << Hall_clock_CTRL_ENABLE_SHIFT)
    #define Hall_clock_CTRL_RESET             (uint8)((uint8)0x01u << Hall_clock_CTRL_RESET_SHIFT)
    #define Hall_clock_CTRL_CMPMODE2_MASK     (uint8)((uint8)0x07u << Hall_clock_CTRL_CMPMODE2_SHIFT)
    #define Hall_clock_CTRL_CMPMODE1_MASK     (uint8)((uint8)0x07u << Hall_clock_CTRL_CMPMODE1_SHIFT)

    /* Control2 Register Bit Masks */
    /* As defined in Register Map, Part of the TMRX_CFG1 register */
    #define Hall_clock_CTRL2_IRQ_SEL_SHIFT    (0x00u)
    #define Hall_clock_CTRL2_IRQ_SEL          (uint8)((uint8)0x01u << Hall_clock_CTRL2_IRQ_SEL_SHIFT)

    /* Status Register Bit Locations */
    /* As defined by Register map as TC in SR0 */
    #define Hall_clock_STATUS_TC_SHIFT        (0x07u)   
    
    /* As defined by the Register map as CAP_CMP in SR0 */
    #define Hall_clock_STATUS_CMP1_SHIFT      (0x06u)   

    /* Status Register Interrupt Enable Bit Locations */
    #define Hall_clock_STATUS_KILL_INT_EN_MASK_SHIFT          (0x00u)
    #define Hall_clock_STATUS_TC_INT_EN_MASK_SHIFT            (Hall_clock_STATUS_TC_SHIFT - 4u)
    #define Hall_clock_STATUS_CMP2_INT_EN_MASK_SHIFT          (0x00u)
    #define Hall_clock_STATUS_CMP1_INT_EN_MASK_SHIFT          (Hall_clock_STATUS_CMP1_SHIFT - 4u)

    /* Status Register Bit Masks */
    #define Hall_clock_STATUS_TC              (uint8)((uint8)0x01u << Hall_clock_STATUS_TC_SHIFT)
    #define Hall_clock_STATUS_CMP1            (uint8)((uint8)0x01u << Hall_clock_STATUS_CMP1_SHIFT)

    /* Status Register Interrupt Bit Masks */
    #define Hall_clock_STATUS_TC_INT_EN_MASK              (uint8)((uint8)Hall_clock_STATUS_TC >> 4u)
    #define Hall_clock_STATUS_CMP1_INT_EN_MASK            (uint8)((uint8)Hall_clock_STATUS_CMP1 >> 4u)

    /*RT1 Synch Constants */
    #define Hall_clock_RT1_SHIFT             (0x04u)

    /* Sync TC and CMP bit masks */
    #define Hall_clock_RT1_MASK              (uint8)((uint8)0x03u << Hall_clock_RT1_SHIFT)
    #define Hall_clock_SYNC                  (uint8)((uint8)0x03u << Hall_clock_RT1_SHIFT)
    #define Hall_clock_SYNCDSI_SHIFT         (0x00u)

    /* Sync all DSI inputs */
    #define Hall_clock_SYNCDSI_MASK          (uint8)((uint8)0x0Fu << Hall_clock_SYNCDSI_SHIFT)

    /* Sync all DSI inputs */
    #define Hall_clock_SYNCDSI_EN            (uint8)((uint8)0x0Fu << Hall_clock_SYNCDSI_SHIFT)


#else
    #define Hall_clock_STATUS                (*(reg8 *)   Hall_clock_PWMUDB_genblk8_stsreg__STATUS_REG )
    #define Hall_clock_STATUS_PTR            ((reg8 *)    Hall_clock_PWMUDB_genblk8_stsreg__STATUS_REG )
    #define Hall_clock_STATUS_MASK           (*(reg8 *)   Hall_clock_PWMUDB_genblk8_stsreg__MASK_REG)
    #define Hall_clock_STATUS_MASK_PTR       ((reg8 *)    Hall_clock_PWMUDB_genblk8_stsreg__MASK_REG)
    #define Hall_clock_STATUS_AUX_CTRL       (*(reg8 *)   Hall_clock_PWMUDB_genblk8_stsreg__STATUS_AUX_CTL_REG)
    #define Hall_clock_CONTROL               (*(reg8 *)   Hall_clock_PWMUDB_genblk1_ctrlreg__CONTROL_REG)
    #define Hall_clock_CONTROL_PTR           ((reg8 *)    Hall_clock_PWMUDB_genblk1_ctrlreg__CONTROL_REG)


    /***********************************
    *          Constants
    ***********************************/

    /* Control Register bit definitions */
    #define Hall_clock_CTRL_ENABLE_SHIFT      (0x07u)
    #define Hall_clock_CTRL_RESET_SHIFT       (0x06u)
    #define Hall_clock_CTRL_CMPMODE2_SHIFT    (0x03u)
    #define Hall_clock_CTRL_CMPMODE1_SHIFT    (0x00u)
    #define Hall_clock_CTRL_DEAD_TIME_SHIFT   (0x00u)   /* No Shift Needed for UDB block */
    
    /* Control Register Bit Masks */
    #define Hall_clock_CTRL_ENABLE            (uint8)((uint8)0x01u << Hall_clock_CTRL_ENABLE_SHIFT)
    #define Hall_clock_CTRL_RESET             (uint8)((uint8)0x01u << Hall_clock_CTRL_RESET_SHIFT)
    #define Hall_clock_CTRL_CMPMODE2_MASK     (uint8)((uint8)0x07u << Hall_clock_CTRL_CMPMODE2_SHIFT)
    #define Hall_clock_CTRL_CMPMODE1_MASK     (uint8)((uint8)0x07u << Hall_clock_CTRL_CMPMODE1_SHIFT)

    /* Status Register Bit Locations */
    #define Hall_clock_STATUS_KILL_SHIFT          (0x05u)
    #define Hall_clock_STATUS_FIFONEMPTY_SHIFT    (0x04u)
    #define Hall_clock_STATUS_FIFOFULL_SHIFT      (0x03u)
    #define Hall_clock_STATUS_TC_SHIFT            (0x02u)
    #define Hall_clock_STATUS_CMP2_SHIFT          (0x01u)
    #define Hall_clock_STATUS_CMP1_SHIFT          (0x00u)

    /* Status Register Interrupt Enable Bit Locations - UDB Status Interrupt Mask match Status Bit Locations*/
    #define Hall_clock_STATUS_KILL_INT_EN_MASK_SHIFT          (Hall_clock_STATUS_KILL_SHIFT)
    #define Hall_clock_STATUS_FIFONEMPTY_INT_EN_MASK_SHIFT    (Hall_clock_STATUS_FIFONEMPTY_SHIFT)
    #define Hall_clock_STATUS_FIFOFULL_INT_EN_MASK_SHIFT      (Hall_clock_STATUS_FIFOFULL_SHIFT)
    #define Hall_clock_STATUS_TC_INT_EN_MASK_SHIFT            (Hall_clock_STATUS_TC_SHIFT)
    #define Hall_clock_STATUS_CMP2_INT_EN_MASK_SHIFT          (Hall_clock_STATUS_CMP2_SHIFT)
    #define Hall_clock_STATUS_CMP1_INT_EN_MASK_SHIFT          (Hall_clock_STATUS_CMP1_SHIFT)

    /* Status Register Bit Masks */
    #define Hall_clock_STATUS_KILL            (uint8)((uint8)0x00u << Hall_clock_STATUS_KILL_SHIFT )
    #define Hall_clock_STATUS_FIFOFULL        (uint8)((uint8)0x01u << Hall_clock_STATUS_FIFOFULL_SHIFT)
    #define Hall_clock_STATUS_FIFONEMPTY      (uint8)((uint8)0x01u << Hall_clock_STATUS_FIFONEMPTY_SHIFT)
    #define Hall_clock_STATUS_TC              (uint8)((uint8)0x01u << Hall_clock_STATUS_TC_SHIFT)
    #define Hall_clock_STATUS_CMP2            (uint8)((uint8)0x01u << Hall_clock_STATUS_CMP2_SHIFT)
    #define Hall_clock_STATUS_CMP1            (uint8)((uint8)0x01u << Hall_clock_STATUS_CMP1_SHIFT)

    /* Status Register Interrupt Bit Masks  - UDB Status Interrupt Mask match Status Bit Locations */
    #define Hall_clock_STATUS_KILL_INT_EN_MASK            (Hall_clock_STATUS_KILL)
    #define Hall_clock_STATUS_FIFOFULL_INT_EN_MASK        (Hall_clock_STATUS_FIFOFULL)
    #define Hall_clock_STATUS_FIFONEMPTY_INT_EN_MASK      (Hall_clock_STATUS_FIFONEMPTY)
    #define Hall_clock_STATUS_TC_INT_EN_MASK              (Hall_clock_STATUS_TC)
    #define Hall_clock_STATUS_CMP2_INT_EN_MASK            (Hall_clock_STATUS_CMP2)
    #define Hall_clock_STATUS_CMP1_INT_EN_MASK            (Hall_clock_STATUS_CMP1)

    /* Datapath Auxillary Control Register bit definitions */
    #define Hall_clock_AUX_CTRL_FIFO0_CLR         (0x01u)
    #define Hall_clock_AUX_CTRL_FIFO1_CLR         (0x02u)
    #define Hall_clock_AUX_CTRL_FIFO0_LVL         (0x04u)
    #define Hall_clock_AUX_CTRL_FIFO1_LVL         (0x08u)
    #define Hall_clock_STATUS_ACTL_INT_EN_MASK    (0x10u) /* As defined for the ACTL Register */
#endif /* Hall_clock_UsingFixedFunction */

#endif  /* CY_PWM_Hall_clock_H */


/* [] END OF FILE */
