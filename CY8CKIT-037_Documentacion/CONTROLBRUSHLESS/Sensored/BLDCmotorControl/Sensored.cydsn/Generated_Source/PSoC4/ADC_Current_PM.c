/*******************************************************************************
* File Name: ADC_Current_PM.c
* Version 2.50
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ADC_Current.h"


/***************************************
* Local data allocation
***************************************/

static ADC_Current_BACKUP_STRUCT  ADC_Current_backup =
{
    ADC_Current_DISABLED,
    0u    
};


/*******************************************************************************
* Function Name: ADC_Current_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_Current_SaveConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_Current_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_Current_RestoreConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_Current_Sleep
********************************************************************************
*
* Summary:
*  Stops the ADC operation and saves the configuration registers and component
*  enable state. Should be called just prior to entering sleep.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_Current_backup - modified.
*
*******************************************************************************/
void ADC_Current_Sleep(void)
{
    /* During deepsleep/ hibernate mode keep SARMUX active, i.e. do not open
    *   all switches (disconnect), to be used for ADFT
    */
    ADC_Current_backup.dftRegVal = ADC_Current_SAR_DFT_CTRL_REG & (uint32)~ADC_Current_ADFT_OVERRIDE;
    ADC_Current_SAR_DFT_CTRL_REG |= ADC_Current_ADFT_OVERRIDE;
    if((ADC_Current_SAR_CTRL_REG  & ADC_Current_ENABLE) != 0u)
    {
        if((ADC_Current_SAR_SAMPLE_CTRL_REG & ADC_Current_CONTINUOUS_EN) != 0u)
        {
            ADC_Current_backup.enableState = ADC_Current_ENABLED | ADC_Current_STARTED;
        }
        else
        {
            ADC_Current_backup.enableState = ADC_Current_ENABLED;
        }
        ADC_Current_StopConvert();
        ADC_Current_Stop();
        
        /* Disable the SAR internal pump before entering the chip low power mode */
        if((ADC_Current_SAR_CTRL_REG & ADC_Current_BOOSTPUMP_EN) != 0u)
        {
            ADC_Current_SAR_CTRL_REG &= (uint32)~ADC_Current_BOOSTPUMP_EN;
            ADC_Current_backup.enableState |= ADC_Current_BOOSTPUMP_ENABLED;
        }
    }
    else
    {
        ADC_Current_backup.enableState = ADC_Current_DISABLED;
    }
}


/*******************************************************************************
* Function Name: ADC_Current_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component enable state and configuration registers.
*  This should be called just after awaking from sleep mode.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_Current_backup - used.
*
*******************************************************************************/
void ADC_Current_Wakeup(void)
{
    ADC_Current_SAR_DFT_CTRL_REG = ADC_Current_backup.dftRegVal;
    if(ADC_Current_backup.enableState != ADC_Current_DISABLED)
    {
        /* Enable the SAR internal pump  */
        if((ADC_Current_backup.enableState & ADC_Current_BOOSTPUMP_ENABLED) != 0u)
        {
            ADC_Current_SAR_CTRL_REG |= ADC_Current_BOOSTPUMP_EN;
        }
        ADC_Current_Enable();
        if((ADC_Current_backup.enableState & ADC_Current_STARTED) != 0u)
        {
            ADC_Current_StartConvert();
        }
    }
}
/* [] END OF FILE */
