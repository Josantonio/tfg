/*******************************************************************************
* File Name: Hall_clock_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "Hall_clock.h"

static Hall_clock_backupStruct Hall_clock_backup;


/*******************************************************************************
* Function Name: Hall_clock_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  Hall_clock_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void Hall_clock_SaveConfig(void) 
{

    #if(!Hall_clock_UsingFixedFunction)
        #if(!Hall_clock_PWMModeIsCenterAligned)
            Hall_clock_backup.PWMPeriod = Hall_clock_ReadPeriod();
        #endif /* (!Hall_clock_PWMModeIsCenterAligned) */
        Hall_clock_backup.PWMUdb = Hall_clock_ReadCounter();
        #if (Hall_clock_UseStatus)
            Hall_clock_backup.InterruptMaskValue = Hall_clock_STATUS_MASK;
        #endif /* (Hall_clock_UseStatus) */

        #if(Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_256_CLOCKS || \
            Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_2_4_CLOCKS)
            Hall_clock_backup.PWMdeadBandValue = Hall_clock_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(Hall_clock_KillModeMinTime)
             Hall_clock_backup.PWMKillCounterPeriod = Hall_clock_ReadKillTime();
        #endif /* (Hall_clock_KillModeMinTime) */

        #if(Hall_clock_UseControl)
            Hall_clock_backup.PWMControlRegister = Hall_clock_ReadControlRegister();
        #endif /* (Hall_clock_UseControl) */
    #endif  /* (!Hall_clock_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Hall_clock_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  Hall_clock_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Hall_clock_RestoreConfig(void) 
{
        #if(!Hall_clock_UsingFixedFunction)
            #if(!Hall_clock_PWMModeIsCenterAligned)
                Hall_clock_WritePeriod(Hall_clock_backup.PWMPeriod);
            #endif /* (!Hall_clock_PWMModeIsCenterAligned) */

            Hall_clock_WriteCounter(Hall_clock_backup.PWMUdb);

            #if (Hall_clock_UseStatus)
                Hall_clock_STATUS_MASK = Hall_clock_backup.InterruptMaskValue;
            #endif /* (Hall_clock_UseStatus) */

            #if(Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_256_CLOCKS || \
                Hall_clock_DeadBandMode == Hall_clock__B_PWM__DBM_2_4_CLOCKS)
                Hall_clock_WriteDeadTime(Hall_clock_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(Hall_clock_KillModeMinTime)
                Hall_clock_WriteKillTime(Hall_clock_backup.PWMKillCounterPeriod);
            #endif /* (Hall_clock_KillModeMinTime) */

            #if(Hall_clock_UseControl)
                Hall_clock_WriteControlRegister(Hall_clock_backup.PWMControlRegister);
            #endif /* (Hall_clock_UseControl) */
        #endif  /* (!Hall_clock_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: Hall_clock_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  Hall_clock_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void Hall_clock_Sleep(void) 
{
    #if(Hall_clock_UseControl)
        if(Hall_clock_CTRL_ENABLE == (Hall_clock_CONTROL & Hall_clock_CTRL_ENABLE))
        {
            /*Component is enabled */
            Hall_clock_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            Hall_clock_backup.PWMEnableState = 0u;
        }
    #endif /* (Hall_clock_UseControl) */

    /* Stop component */
    Hall_clock_Stop();

    /* Save registers configuration */
    Hall_clock_SaveConfig();
}


/*******************************************************************************
* Function Name: Hall_clock_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  Hall_clock_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Hall_clock_Wakeup(void) 
{
     /* Restore registers values */
    Hall_clock_RestoreConfig();

    if(Hall_clock_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        Hall_clock_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
