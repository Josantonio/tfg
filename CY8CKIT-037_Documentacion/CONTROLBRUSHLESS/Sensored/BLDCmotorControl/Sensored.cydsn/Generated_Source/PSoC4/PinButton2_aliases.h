/*******************************************************************************
* File Name: PinButton2.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinButton2_ALIASES_H) /* Pins PinButton2_ALIASES_H */
#define CY_PINS_PinButton2_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define PinButton2_0			(PinButton2__0__PC)
#define PinButton2_0_PS		(PinButton2__0__PS)
#define PinButton2_0_PC		(PinButton2__0__PC)
#define PinButton2_0_DR		(PinButton2__0__DR)
#define PinButton2_0_SHIFT	(PinButton2__0__SHIFT)
#define PinButton2_0_INTR	((uint16)((uint16)0x0003u << (PinButton2__0__SHIFT*2u)))

#define PinButton2_INTR_ALL	 ((uint16)(PinButton2_0_INTR))


#endif /* End Pins PinButton2_ALIASES_H */


/* [] END OF FILE */
