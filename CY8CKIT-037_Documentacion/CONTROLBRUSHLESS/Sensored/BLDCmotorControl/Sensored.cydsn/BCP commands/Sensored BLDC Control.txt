;----------------RX8 packet structure------------------------------------------------------------
;Header = {0xAA}
;Data = {4 bytes int W_real variable} {4 bytes int W_ref variable} {2 bytes int duty variable}
;Tail = {0xFF}
;----------------------------------------------------------------------------------------------
RX8 [H=0A 0B] @1W_real @0W_real @1W_ref @0W_ref @3error @2error @1error @0error @3PID @2PID @1PID @0PID @3duty @2duty @1duty @0duty @3vin @2vin @1vin @0vin @3tiempo @2tiempo @1tiempo @0tiempo @1pwmTs @0pwmTs @3delay @2delay @1delay @0delay [T=FF FF]