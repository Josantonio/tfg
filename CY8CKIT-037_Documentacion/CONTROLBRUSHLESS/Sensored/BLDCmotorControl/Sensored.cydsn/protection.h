 /*=============================================================================================
 *                                  PROTECTION.h                                              
 *
 * Archivo de cabecera, definición de constantes, variable y funciones relacionadas con el
 * archivo de programa protection.c 
 *
===============================================================================================*/


#pragma once                            //Indica al precompilador que sólo lo incluya una vez


#include <cytypes.h>                    //Introduce los tipos de variables


#define R10             (uint16)(1000)            //1KOhm
#define R9              (uint16)(19100)           //19.1kOhm
#define HIGH_VIN_TH     (uint16)(27000)           //En mV, 27V
#define LOW_VIN_TH      (uint16)(15000)           //15 V


#define R83             (uint16)(1800)            //1.8kOhm
#define R34             (uint8)(30)               //30mOhm
#define OP_GAIN         (uint8)(3)                //Ganancia del operacional V/V


typedef enum error /* Variable que almacena el tipo de error que se ha dado */
{
    hall,
    voltage,
    over_current,
}type_error;

/*** VARIABLES EXTERNAS ***/


extern type_error state_error;
extern uint32 vin;
//extern uint8* p_vin;
extern uint16 vin_bits;

extern uint16 Vref_Imax;
extern uint16 I_ref;
extern uint8 oc;


/*** FUNCIONES ***/
int check_errors(void);
void INIT_HW_PROTECTION(void);
