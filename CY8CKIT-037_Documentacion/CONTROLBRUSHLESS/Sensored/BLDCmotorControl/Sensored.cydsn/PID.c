/*==============================================================================================
 *                                           PID.C                                                
 *
 * + Creación de una función para asignar los parámetros al controlador PID
 * + Función que realiza el control PID propiamente dicho
 * + Definición de la variable PID, tipo estructura previamwente definida que contiene los 
 *   parámetros del PID.
 * + Declaración y/o inicialización de variables necesarias para llevar a cabo el control de
 *   la velocidad.
 *
===============================================================================================*/

#include <project.h>
#include "stdio.h" // uso sprintf
#include "math.h"
#include "motor.h"
#include "communication.h"
#include "protection.h"
#include "PID.h"

my_struct_PID PID;


uint8 duty;                               //Duty 
int16 e_k[3];                             //Error instantáneo y pasados
float PID_result[2];

uint8* pPID_result = (uint8*)&PID_result[0];
int16* p_ek = (int16*)&e_k[0];              //Puntero al error instantáneo


/*************** FUNCIÓN PARA ASIGNAR PARÁMETROS DEL PID ****************/
void asign_param_PID(float Kp, float Ki, float Kd, float Tn)
{
   PID.Kp = Kp;                    //Constante proporcional Kp = 1.2*T/L
   PID.Ki = Ki;                    //Parámetro integral ki = 2*L 
   PID.Kd = Kd;                    //Parámetro derivativo Kd = 0.5*L
   PID.Tn = Tn;                    //Tiempo de muestreo del controlador en ms
}


/************** FUNCIÓN QUE REALIZA EL CONTROL PID  *******************/
void controlPID(void)
{
    /* Error instantáneo */
    e_k[0] = *pw_ref - *pw_real;
    
    /* Parte integral */
    PID_result[0] = PID_result[1] + PID.Ki*PID.Tn/2*(e_k[0]+e_k[1]);
    
    /* Parte derivativa */
    PID_result[0] += PID.Kd/PID.Tn*(e_k[0]-(e_k[1] << 1)+e_k[2]);
    
    /*Parte proporcional */
    PID_result[0] += PID.Kp*(e_k[0]-e_k[1]);
    
    /* Anti Wind-up */
    if (PID_result[0] < 0.0){PID_result[0] = 0.0;}  
    if (PID_result[0] > 24.0){PID_result[0] = 24.0;} 
        
    /* Actualizar valores pasados */
    e_k[2] = e_k[1];
    e_k[1] = e_k[0];
    PID_result[1] = PID_result[0];
     
    /* Escribir el nuevo duty-cycle 
     * El controlador PID da como salida un valor
     * de tensión, hay que convertirlo a duty */
    duty = (uint8)(PID_result[0]/24.0*255.0);
    
    PWM_Driver_WriteCompare2(duty);
}
