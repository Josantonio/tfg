/*==============================================================================================
 *                                           PROTECTION.C                                                
 *
 * + Interrupción para la protección por exceso de corriente
 * + Función que comprueba si hay errores en los sensores hall o en la tensión de entrada
 * + Inicialización del hardware y variables necesarias para la protección de la shield
 *                          
===============================================================================================*/


/*** ARCHIVOS DE CABECERA ***/
#include <project.h>
#include "stdio.h" 
#include "math.h"
#include "motor.h"
#include "communication.h"
#include "protection.h"
#include "PID.h"

type_error state_error;


uint16 I_ref;           //Valor de Iref para crear tensión de comparación
    
uint16 vin_bits;        //Medida de Vin en bits, del convertidor ADC
uint32 vin;             // Vin en milivoltios


/******************* INTERRUPCIÓN DEBIDA A EXCESO DE CORRIENTE *******************************/
CY_ISR(ISR_OC_HANDLER)
{
    SYSTEM_STATE.error = TRUE;                      // Sistema en estado de error
    state_error = over_current;                     // Tipo de error, sobrecorriente
    OC_COMP_ClearInterrupt(OC_COMP_INTR_RISING);    //Limpia interrupción
}



/************************* COMPROBACIÓN DE ERRORES ********************************************/
int check_errors(void)
{
    /*** Lectura de Vin ***/
    vin_bits = ADC_SAR_GetResult16(1);              //nº de bits
    vin = ADC_SAR_CountsTo_mVolts(1,vin_bits);      //En milivoltios
    vin *= (R10+R9)/R10;                            //Divisor de tensión
    
    /*** COMPROBACIÓN DE VIN, TENSIÓN DE ALIMENTACIÓN DE LA SHIELD ***/
    if (vin < LOW_VIN_TH || vin_bits > HIGH_VIN_TH )
    {
        state_error = voltage;
        SYSTEM_STATE.error = TRUE;
        return 5;
    }    
    
    /*** ERROR EN LOS SENSORES HALL ***/
    if (Hall_Error_Read())
    {
        state_error = hall;
        SYSTEM_STATE.error = TRUE;
        return 5;
    }
    
    /*** ERROR POR SOBRECORRIENTE ***/
    if (state_error == over_current)
    {
        return 1;
    }
    
    /*** NO HAY ERRORES ***/
    else 
    {
        SYSTEM_STATE.error = FALSE;
        return 0;
    }
}


/***************************** HARDWARE INITIALIZATION ****************************************/
void INIT_HW_PROTECTION(void)
{
    OC_COMP_Start();                              // OP-AMP en forma de comparador
    I_ref_source_Start();                         // Fuente de corriente de precisión
    
    isr_oc_StartEx(ISR_OC_HANDLER);               //Cabecera de la interrupción, over-current
    
    
    /* Corriente de referencia, para inyectar a la resistencia y producir una tensión 
       a la entrada negativa del comparador */
    I_ref = (uint16)(OP_GAIN*R34*MOTOR.MaxCurr/R83);
    I_ref_source_SetValue(I_ref);               //Se ajusta el valor para la fuente de corriente
}


