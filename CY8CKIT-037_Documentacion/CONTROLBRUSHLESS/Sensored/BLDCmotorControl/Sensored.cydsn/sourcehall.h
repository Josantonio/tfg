
/**************** GENERACIÓN DE SEÑALES HALL ***************************/

/** Archivo de cabecera */

/* sourcehall.h */

//#ifndef SOURCEHALL_H
//#define SOURCEHALL_H
#pragma once  //Para que el preprocesador sólo añada una vez la cabecera

#include <cytypes.h>

 /*** Rango en nº de bits real que lee el ADC, debido a las resistencias ***/
#define ADC_SAR_hall_RESOLUTION ((uint8) (12)) // 12 bits de resolución
#define MAX_ADC_LECTURE (uint32)((330 + 10e3)/(2*330 +10e3)*(pow(2,ADC_SAR_hall_RESOLUTION)/2-1)) //Signed, se pierde resolución
#define MIN_ADC_LECTURE (uint32)((330)/(2*330 +10e3)*(pow(2,ADC_SAR_hall_RESOLUTION)/2-1))

/**** Frecuencia eléctrica máxima y mínima, en Hertzios ****/
#define F_MAX ((uint16)(MOTOR.maxSpdRpm*MOTOR.NPolePairs/60))
#define F_MIN ((uint16)(MOTOR.minSpdRpm*MOTOR.NPolePairs/60))
#define T_CLOCK_HALL ((uint32)(1/12e6*1e12)) //Período del reloj en ps, redondeando. 
    
/**** Valores extremos para escribir en el PWM y cambiar su frecuencia ***/
#define PERIOD_MAX (uint16)(1/(6*F_MIN*T_CLOCK_HALL*1e-12)) //El 6 es debido a que son 6 pasos para formar un período
#define PERIOD_MIN (uint16)(1/(6*F_MAX*T_CLOCK_HALL*1e-12))
//El compare tb habrá que modificarlo y será justo la mitad.
 
uint16 adc_hall_result; //Variable para almacenar el resultado de la lectura
uint16 duty_hall; //La mitad de la anterior
    
char strHall[100]; //String para mostrar variables por puerto serie


/* Declaración de funciones */  
void Set_hall_freq(void); //Cambiar la frecuencia de las señales hall generadas, en función de un potenciómetro
void Init_SourceHall_HW(void); //Iniciar hardware
void show_hall_var (void);
