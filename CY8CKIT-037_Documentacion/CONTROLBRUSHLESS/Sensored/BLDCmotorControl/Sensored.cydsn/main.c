/* ====================================================================================================
 *                                                                                                           
 *                                 CONTROL DE MOTORES BRUSHLESS CON PSOC            
 * 
 *                      Trabajo fin de grado, Ingeniería Electrónica Industrial
 *                                      Universidad de Granada
 * 
 *                                                  2020
 *
 *  Archivo principal del programa, control con sensores hall y PID para la regulación de la velocidad 
 * de rotación. Este programa permite la activación/desactivación de un motor brushless DC mediante un 
 * botón, además se realiza un control de la velocidad de rotación por medio de un potenciómetro, y 
 * contiene sistemas de seguridad que protegen al motor y la shield frente a corrientes excesivas, 
 * fallos en los sensores hall y desviaciones en la tensión de alimentación.
 *
 * Shield utilizada: CY8CKIT - 037
 * PSoC 4, CYKIT-042
 *
 * Autor: José Antonio Negro Espejo
 * Tutores: Diego Pedro Morales Santos, Noel Rodríguez Santiago
 *
 * ===================================================================================================*/



#include <project.h>
#include "stdio.h"                  //Uso sprintf, entrada y salida de datos estándar
#include "math.h"                   //Operaciones matemáticas
#include "motor.h"                  //Otros archivos de cabecera del proyecto
#include "communication.h"
#include "protection.h"
#include "PID.h"

int main(void)
{
    
    
    
    asign_param_motor(4, 335, 4000, 600, 24, FALSE, 3000);          
    /* =========================================
     * Función para almacenar los parámetros    *
     * característicos del motor, en el         * 
     * siguiente orden:                         *
     * 1 - nº Pares de polos                    *
     * 2 - Ke [RPM/V]                           *
     * 3 - Vel máxima (rpm)                     *
     * 4 - Vel mínima (rpm)                     *
     * 5 - Tensión de alimentación (V)          *
     * 6 - Sentido de giro:                     *
     *          -FALSE:  Horario                *
     *          -TRUE: Anti-horario             *
     * 7 - Max corriente (mA)                   *
     *==========================================*/

    
    asign_param_PID (0.0016, 0.0748, 0, 2000e-6);         
     /* =========================================
     * Función para almacenar los parámetros     *
     * del control PID                           *
     * 1 - Constante proporcional Kc             *
     * 2 - Constante integral Ki                 *
     * 3 - Constante derivativa Kd               *
     * 4 - Período de muestreo Tn, en ms         *
     * =========================================*/

    
    CyGlobalIntEnable;                              /* Enable global interrupts. */

    INIT_HW_MOTOR();            /* Inicializa el hardware relacionado con el control del motor 
                                   y establece el sistema al estado inicial. */
    INIT_HW_COMM();             // Inicializa el hardware relacionado con la comunicación.
    INIT_HW_PROTECTION();       /* Inicializa elementos relacionados con la protección y seguridad 
                                   del motor y la shield */
                 
    
    for(;;)
    {
        check_errors();                         // Comprobación de errores
        update_LED_status();                    //Actualizar estado de los LEDs indicadores
        
        /* HAY ERRORES o MOTOR PARADO */
        
        if(SYSTEM_STATE.error || !SYSTEM_STATE.run){
            Control_Reg_Write(0x00);            // 0000 0000, Parar motor desactivando salidas y PWM. 
            Reset_Variables();                  //Resetear variables
        }
     
        
        /* MOTOR EN MARCHA SIN ERRORES */
        
        if(SYSTEM_STATE.run && !SYSTEM_STATE.error){ 
            //Activa PWM y salidas de control de los transistores
            if (!MOTOR.Dir){Control_Reg_Write(0x03);}
            if (MOTOR.Dir){Control_Reg_Write(0x07);}
             
            BCP_trans();                         //Transmisión de datos al PC

            
            /* ACTIVAR PID CADA Tn */
            if (count >= PID.Tn*1e6/PWM_PERIOD )  
            {  
                get_wref();                     //Medida de la velocidad de referencia
                controlPID();                   //Función que realiza el control
                count = 0;                      //Reseteamos contador
            }
        }
    }
}

