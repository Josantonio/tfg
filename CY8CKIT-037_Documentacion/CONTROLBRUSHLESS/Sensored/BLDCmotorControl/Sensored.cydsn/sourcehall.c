
/*************** SOURCE HALL ************************/

/* Creación de unas señales que simulan los sensores hall, para comprobar el correcto   *
 * funcionamiento de la conmutación del motor, y de las señales PWM de los transistores *
 * la frecuencia de las señales hall simuladas podrá variarse con el potenciómetro de   *
 * la shield, entre los rangos de frecuencia permisibles para el motor                  *
 */

#include <project.h>
#include "stdio.h" // uso sprintf
#include "math.h"
#include "motor.h"
#include "sourcehall.h"

/** Inicialización de los componentes hardware que proporcionan la señal hall simulada **/
void Init_SourceHall_HW(void)
{
    ADC_SAR_hall_Start();
    Hall_clock_Start(); 
    
}

/** Leer el potenciómetro de la shield y modificar la frecuencia **/
void Set_hall_freq(void)
{
    /* Lectura potenciómetro */
    ADC_SAR_hall_StartConvert(); // Comenzamos conversión
    ADC_SAR_hall_IsEndConversion(ADC_SAR_hall_WAIT_FOR_RESULT); //Esperamos a que termine
    /* Cálculo del período (nº bits a asignar al bloque PWM y compare value) */
    adc_hall_result = (ADC_SAR_hall_GetResult16(0)-MIN_ADC_LECTURE)*(PERIOD_MAX-PERIOD_MIN)/(MAX_ADC_LECTURE-MIN_ADC_LECTURE)+PERIOD_MIN;
    duty_hall = 0.5*adc_hall_result; 
    
    Hall_clock_WriteCompare(duty_hall); //Duty cycle del 50%, señal cuadrada pura
    Hall_clock_WritePeriod(adc_hall_result);
    
}

/** Mostrar variables por el puerto serie **/
void show_hall_var (void)
{
   UART_UartPutString("HALL SIGNAL:\n");
   sprintf(strHall,"Tmax: %d, Tmin: %d, PWM_write: %d, Compare: %d\n", PERIOD_MAX, PERIOD_MIN, adc_hall_result, duty_hall);
   UART_UartPutString(strHall);
   sprintf(strHall,"Fmax: %d, Fmin: %d, Tclockhall: %lu, Max ADC value: %lu, Min ADC value: %lu\n", F_MAX, F_MIN, T_CLOCK_HALL, MAX_ADC_LECTURE,MIN_ADC_LECTURE);
   UART_UartPutString(strHall);
}
