/*******************************************************************************
* File Name: CH.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CH_H) /* Pins CH_H */
#define CY_PINS_CH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CH_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} CH_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   CH_Read(void);
void    CH_Write(uint8 value);
uint8   CH_ReadDataReg(void);
#if defined(CH__PC) || (CY_PSOC4_4200L) 
    void    CH_SetDriveMode(uint8 mode);
#endif
void    CH_SetInterruptMode(uint16 position, uint16 mode);
uint8   CH_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void CH_Sleep(void); 
void CH_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(CH__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define CH_DRIVE_MODE_BITS        (3)
    #define CH_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - CH_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the CH_SetDriveMode() function.
         *  @{
         */
        #define CH_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define CH_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define CH_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define CH_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define CH_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define CH_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define CH_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define CH_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define CH_MASK               CH__MASK
#define CH_SHIFT              CH__SHIFT
#define CH_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in CH_SetInterruptMode() function.
     *  @{
     */
        #define CH_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define CH_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define CH_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define CH_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(CH__SIO)
    #define CH_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(CH__PC) && (CY_PSOC4_4200L)
    #define CH_USBIO_ENABLE               ((uint32)0x80000000u)
    #define CH_USBIO_DISABLE              ((uint32)(~CH_USBIO_ENABLE))
    #define CH_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define CH_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define CH_USBIO_ENTER_SLEEP          ((uint32)((1u << CH_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << CH_USBIO_SUSPEND_DEL_SHIFT)))
    #define CH_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << CH_USBIO_SUSPEND_SHIFT)))
    #define CH_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << CH_USBIO_SUSPEND_DEL_SHIFT)))
    #define CH_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(CH__PC)
    /* Port Configuration */
    #define CH_PC                 (* (reg32 *) CH__PC)
#endif
/* Pin State */
#define CH_PS                     (* (reg32 *) CH__PS)
/* Data Register */
#define CH_DR                     (* (reg32 *) CH__DR)
/* Input Buffer Disable Override */
#define CH_INP_DIS                (* (reg32 *) CH__PC2)

/* Interrupt configuration Registers */
#define CH_INTCFG                 (* (reg32 *) CH__INTCFG)
#define CH_INTSTAT                (* (reg32 *) CH__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define CH_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(CH__SIO)
    #define CH_SIO_REG            (* (reg32 *) CH__SIO)
#endif /* (CH__SIO_CFG) */

/* USBIO registers */
#if !defined(CH__PC) && (CY_PSOC4_4200L)
    #define CH_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define CH_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define CH_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define CH_DRIVE_MODE_SHIFT       (0x00u)
#define CH_DRIVE_MODE_MASK        (0x07u << CH_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins CH_H */


/* [] END OF FILE */
