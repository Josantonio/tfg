/*******************************************************************************
* File Name: PinPot.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinPot_H) /* Pins PinPot_H */
#define CY_PINS_PinPot_H

#include "cytypes.h"
#include "cyfitter.h"
#include "PinPot_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} PinPot_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   PinPot_Read(void);
void    PinPot_Write(uint8 value);
uint8   PinPot_ReadDataReg(void);
#if defined(PinPot__PC) || (CY_PSOC4_4200L) 
    void    PinPot_SetDriveMode(uint8 mode);
#endif
void    PinPot_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinPot_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void PinPot_Sleep(void); 
void PinPot_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(PinPot__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define PinPot_DRIVE_MODE_BITS        (3)
    #define PinPot_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - PinPot_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the PinPot_SetDriveMode() function.
         *  @{
         */
        #define PinPot_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define PinPot_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define PinPot_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define PinPot_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define PinPot_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define PinPot_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define PinPot_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define PinPot_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define PinPot_MASK               PinPot__MASK
#define PinPot_SHIFT              PinPot__SHIFT
#define PinPot_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinPot_SetInterruptMode() function.
     *  @{
     */
        #define PinPot_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define PinPot_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define PinPot_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define PinPot_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(PinPot__SIO)
    #define PinPot_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(PinPot__PC) && (CY_PSOC4_4200L)
    #define PinPot_USBIO_ENABLE               ((uint32)0x80000000u)
    #define PinPot_USBIO_DISABLE              ((uint32)(~PinPot_USBIO_ENABLE))
    #define PinPot_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define PinPot_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define PinPot_USBIO_ENTER_SLEEP          ((uint32)((1u << PinPot_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << PinPot_USBIO_SUSPEND_DEL_SHIFT)))
    #define PinPot_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << PinPot_USBIO_SUSPEND_SHIFT)))
    #define PinPot_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << PinPot_USBIO_SUSPEND_DEL_SHIFT)))
    #define PinPot_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(PinPot__PC)
    /* Port Configuration */
    #define PinPot_PC                 (* (reg32 *) PinPot__PC)
#endif
/* Pin State */
#define PinPot_PS                     (* (reg32 *) PinPot__PS)
/* Data Register */
#define PinPot_DR                     (* (reg32 *) PinPot__DR)
/* Input Buffer Disable Override */
#define PinPot_INP_DIS                (* (reg32 *) PinPot__PC2)

/* Interrupt configuration Registers */
#define PinPot_INTCFG                 (* (reg32 *) PinPot__INTCFG)
#define PinPot_INTSTAT                (* (reg32 *) PinPot__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define PinPot_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(PinPot__SIO)
    #define PinPot_SIO_REG            (* (reg32 *) PinPot__SIO)
#endif /* (PinPot__SIO_CFG) */

/* USBIO registers */
#if !defined(PinPot__PC) && (CY_PSOC4_4200L)
    #define PinPot_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define PinPot_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define PinPot_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define PinPot_DRIVE_MODE_SHIFT       (0x00u)
#define PinPot_DRIVE_MODE_MASK        (0x07u << PinPot_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins PinPot_H */


/* [] END OF FILE */
