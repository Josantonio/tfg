/*******************************************************************************
* File Name: PinVinSense.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinVinSense_H) /* Pins PinVinSense_H */
#define CY_PINS_PinVinSense_H

#include "cytypes.h"
#include "cyfitter.h"
#include "PinVinSense_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} PinVinSense_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   PinVinSense_Read(void);
void    PinVinSense_Write(uint8 value);
uint8   PinVinSense_ReadDataReg(void);
#if defined(PinVinSense__PC) || (CY_PSOC4_4200L) 
    void    PinVinSense_SetDriveMode(uint8 mode);
#endif
void    PinVinSense_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinVinSense_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void PinVinSense_Sleep(void); 
void PinVinSense_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(PinVinSense__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define PinVinSense_DRIVE_MODE_BITS        (3)
    #define PinVinSense_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - PinVinSense_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the PinVinSense_SetDriveMode() function.
         *  @{
         */
        #define PinVinSense_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define PinVinSense_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define PinVinSense_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define PinVinSense_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define PinVinSense_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define PinVinSense_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define PinVinSense_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define PinVinSense_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define PinVinSense_MASK               PinVinSense__MASK
#define PinVinSense_SHIFT              PinVinSense__SHIFT
#define PinVinSense_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinVinSense_SetInterruptMode() function.
     *  @{
     */
        #define PinVinSense_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define PinVinSense_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define PinVinSense_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define PinVinSense_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(PinVinSense__SIO)
    #define PinVinSense_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(PinVinSense__PC) && (CY_PSOC4_4200L)
    #define PinVinSense_USBIO_ENABLE               ((uint32)0x80000000u)
    #define PinVinSense_USBIO_DISABLE              ((uint32)(~PinVinSense_USBIO_ENABLE))
    #define PinVinSense_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define PinVinSense_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define PinVinSense_USBIO_ENTER_SLEEP          ((uint32)((1u << PinVinSense_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << PinVinSense_USBIO_SUSPEND_DEL_SHIFT)))
    #define PinVinSense_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << PinVinSense_USBIO_SUSPEND_SHIFT)))
    #define PinVinSense_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << PinVinSense_USBIO_SUSPEND_DEL_SHIFT)))
    #define PinVinSense_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(PinVinSense__PC)
    /* Port Configuration */
    #define PinVinSense_PC                 (* (reg32 *) PinVinSense__PC)
#endif
/* Pin State */
#define PinVinSense_PS                     (* (reg32 *) PinVinSense__PS)
/* Data Register */
#define PinVinSense_DR                     (* (reg32 *) PinVinSense__DR)
/* Input Buffer Disable Override */
#define PinVinSense_INP_DIS                (* (reg32 *) PinVinSense__PC2)

/* Interrupt configuration Registers */
#define PinVinSense_INTCFG                 (* (reg32 *) PinVinSense__INTCFG)
#define PinVinSense_INTSTAT                (* (reg32 *) PinVinSense__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define PinVinSense_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(PinVinSense__SIO)
    #define PinVinSense_SIO_REG            (* (reg32 *) PinVinSense__SIO)
#endif /* (PinVinSense__SIO_CFG) */

/* USBIO registers */
#if !defined(PinVinSense__PC) && (CY_PSOC4_4200L)
    #define PinVinSense_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define PinVinSense_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define PinVinSense_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define PinVinSense_DRIVE_MODE_SHIFT       (0x00u)
#define PinVinSense_DRIVE_MODE_MASK        (0x07u << PinVinSense_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins PinVinSense_H */


/* [] END OF FILE */
