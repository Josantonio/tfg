/*******************************************************************************
* File Name: AL.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_AL_H) /* Pins AL_H */
#define CY_PINS_AL_H

#include "cytypes.h"
#include "cyfitter.h"
#include "AL_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} AL_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   AL_Read(void);
void    AL_Write(uint8 value);
uint8   AL_ReadDataReg(void);
#if defined(AL__PC) || (CY_PSOC4_4200L) 
    void    AL_SetDriveMode(uint8 mode);
#endif
void    AL_SetInterruptMode(uint16 position, uint16 mode);
uint8   AL_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void AL_Sleep(void); 
void AL_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(AL__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define AL_DRIVE_MODE_BITS        (3)
    #define AL_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - AL_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the AL_SetDriveMode() function.
         *  @{
         */
        #define AL_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define AL_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define AL_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define AL_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define AL_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define AL_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define AL_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define AL_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define AL_MASK               AL__MASK
#define AL_SHIFT              AL__SHIFT
#define AL_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in AL_SetInterruptMode() function.
     *  @{
     */
        #define AL_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define AL_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define AL_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define AL_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(AL__SIO)
    #define AL_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(AL__PC) && (CY_PSOC4_4200L)
    #define AL_USBIO_ENABLE               ((uint32)0x80000000u)
    #define AL_USBIO_DISABLE              ((uint32)(~AL_USBIO_ENABLE))
    #define AL_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define AL_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define AL_USBIO_ENTER_SLEEP          ((uint32)((1u << AL_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << AL_USBIO_SUSPEND_DEL_SHIFT)))
    #define AL_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << AL_USBIO_SUSPEND_SHIFT)))
    #define AL_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << AL_USBIO_SUSPEND_DEL_SHIFT)))
    #define AL_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(AL__PC)
    /* Port Configuration */
    #define AL_PC                 (* (reg32 *) AL__PC)
#endif
/* Pin State */
#define AL_PS                     (* (reg32 *) AL__PS)
/* Data Register */
#define AL_DR                     (* (reg32 *) AL__DR)
/* Input Buffer Disable Override */
#define AL_INP_DIS                (* (reg32 *) AL__PC2)

/* Interrupt configuration Registers */
#define AL_INTCFG                 (* (reg32 *) AL__INTCFG)
#define AL_INTSTAT                (* (reg32 *) AL__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define AL_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(AL__SIO)
    #define AL_SIO_REG            (* (reg32 *) AL__SIO)
#endif /* (AL__SIO_CFG) */

/* USBIO registers */
#if !defined(AL__PC) && (CY_PSOC4_4200L)
    #define AL_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define AL_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define AL_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define AL_DRIVE_MODE_SHIFT       (0x00u)
#define AL_DRIVE_MODE_MASK        (0x07u << AL_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins AL_H */


/* [] END OF FILE */
