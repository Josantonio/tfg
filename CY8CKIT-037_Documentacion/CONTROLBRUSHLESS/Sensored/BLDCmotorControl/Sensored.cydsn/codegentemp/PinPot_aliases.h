/*******************************************************************************
* File Name: PinPot.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinPot_ALIASES_H) /* Pins PinPot_ALIASES_H */
#define CY_PINS_PinPot_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define PinPot_0			(PinPot__0__PC)
#define PinPot_0_PS		(PinPot__0__PS)
#define PinPot_0_PC		(PinPot__0__PC)
#define PinPot_0_DR		(PinPot__0__DR)
#define PinPot_0_SHIFT	(PinPot__0__SHIFT)
#define PinPot_0_INTR	((uint16)((uint16)0x0003u << (PinPot__0__SHIFT*2u)))

#define PinPot_INTR_ALL	 ((uint16)(PinPot_0_INTR))


#endif /* End Pins PinPot_ALIASES_H */


/* [] END OF FILE */
