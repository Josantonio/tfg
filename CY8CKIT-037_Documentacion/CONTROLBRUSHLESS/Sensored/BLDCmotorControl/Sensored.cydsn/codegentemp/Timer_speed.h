/*******************************************************************************
* File Name: Timer_speed.h
* Version 2.10
*
* Description:
*  This file provides constants and parameter values for the Timer_speed
*  component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_TCPWM_Timer_speed_H)
#define CY_TCPWM_Timer_speed_H


#include "CyLib.h"
#include "cytypes.h"
#include "cyfitter.h"


/*******************************************************************************
* Internal Type defines
*******************************************************************************/

/* Structure to save state before go to sleep */
typedef struct
{
    uint8  enableState;
} Timer_speed_BACKUP_STRUCT;


/*******************************************************************************
* Variables
*******************************************************************************/
extern uint8  Timer_speed_initVar;


/***************************************
*   Conditional Compilation Parameters
****************************************/

#define Timer_speed_CY_TCPWM_V2                    (CYIPBLOCK_m0s8tcpwm_VERSION == 2u)
#define Timer_speed_CY_TCPWM_4000                  (CY_PSOC4_4000)

/* TCPWM Configuration */
#define Timer_speed_CONFIG                         (1lu)

/* Quad Mode */
/* Parameters */
#define Timer_speed_QUAD_ENCODING_MODES            (0lu)
#define Timer_speed_QUAD_AUTO_START                (1lu)

/* Signal modes */
#define Timer_speed_QUAD_INDEX_SIGNAL_MODE         (0lu)
#define Timer_speed_QUAD_PHIA_SIGNAL_MODE          (3lu)
#define Timer_speed_QUAD_PHIB_SIGNAL_MODE          (3lu)
#define Timer_speed_QUAD_STOP_SIGNAL_MODE          (0lu)

/* Signal present */
#define Timer_speed_QUAD_INDEX_SIGNAL_PRESENT      (0lu)
#define Timer_speed_QUAD_STOP_SIGNAL_PRESENT       (0lu)

/* Interrupt Mask */
#define Timer_speed_QUAD_INTERRUPT_MASK            (1lu)

/* Timer/Counter Mode */
/* Parameters */
#define Timer_speed_TC_RUN_MODE                    (0lu)
#define Timer_speed_TC_COUNTER_MODE                (1lu)
#define Timer_speed_TC_COMP_CAP_MODE               (2lu)
#define Timer_speed_TC_PRESCALER                   (0lu)

/* Signal modes */
#define Timer_speed_TC_RELOAD_SIGNAL_MODE          (0lu)
#define Timer_speed_TC_COUNT_SIGNAL_MODE           (3lu)
#define Timer_speed_TC_START_SIGNAL_MODE           (0lu)
#define Timer_speed_TC_STOP_SIGNAL_MODE            (0lu)
#define Timer_speed_TC_CAPTURE_SIGNAL_MODE         (1lu)

/* Signal present */
#define Timer_speed_TC_RELOAD_SIGNAL_PRESENT       (0lu)
#define Timer_speed_TC_COUNT_SIGNAL_PRESENT        (0lu)
#define Timer_speed_TC_START_SIGNAL_PRESENT        (0lu)
#define Timer_speed_TC_STOP_SIGNAL_PRESENT         (0lu)
#define Timer_speed_TC_CAPTURE_SIGNAL_PRESENT      (1lu)

/* Interrupt Mask */
#define Timer_speed_TC_INTERRUPT_MASK              (3lu)

/* PWM Mode */
/* Parameters */
#define Timer_speed_PWM_KILL_EVENT                 (0lu)
#define Timer_speed_PWM_STOP_EVENT                 (0lu)
#define Timer_speed_PWM_MODE                       (4lu)
#define Timer_speed_PWM_OUT_N_INVERT               (0lu)
#define Timer_speed_PWM_OUT_INVERT                 (0lu)
#define Timer_speed_PWM_ALIGN                      (0lu)
#define Timer_speed_PWM_RUN_MODE                   (0lu)
#define Timer_speed_PWM_DEAD_TIME_CYCLE            (0lu)
#define Timer_speed_PWM_PRESCALER                  (0lu)

/* Signal modes */
#define Timer_speed_PWM_RELOAD_SIGNAL_MODE         (0lu)
#define Timer_speed_PWM_COUNT_SIGNAL_MODE          (3lu)
#define Timer_speed_PWM_START_SIGNAL_MODE          (0lu)
#define Timer_speed_PWM_STOP_SIGNAL_MODE           (0lu)
#define Timer_speed_PWM_SWITCH_SIGNAL_MODE         (0lu)

/* Signal present */
#define Timer_speed_PWM_RELOAD_SIGNAL_PRESENT      (0lu)
#define Timer_speed_PWM_COUNT_SIGNAL_PRESENT       (0lu)
#define Timer_speed_PWM_START_SIGNAL_PRESENT       (0lu)
#define Timer_speed_PWM_STOP_SIGNAL_PRESENT        (0lu)
#define Timer_speed_PWM_SWITCH_SIGNAL_PRESENT      (0lu)

/* Interrupt Mask */
#define Timer_speed_PWM_INTERRUPT_MASK             (1lu)


/***************************************
*    Initial Parameter Constants
***************************************/

/* Timer/Counter Mode */
#define Timer_speed_TC_PERIOD_VALUE                (60000lu)
#define Timer_speed_TC_COMPARE_VALUE               (65535lu)
#define Timer_speed_TC_COMPARE_BUF_VALUE           (65535lu)
#define Timer_speed_TC_COMPARE_SWAP                (0lu)

/* PWM Mode */
#define Timer_speed_PWM_PERIOD_VALUE               (65535lu)
#define Timer_speed_PWM_PERIOD_BUF_VALUE           (65535lu)
#define Timer_speed_PWM_PERIOD_SWAP                (0lu)
#define Timer_speed_PWM_COMPARE_VALUE              (65535lu)
#define Timer_speed_PWM_COMPARE_BUF_VALUE          (65535lu)
#define Timer_speed_PWM_COMPARE_SWAP               (0lu)


/***************************************
*    Enumerated Types and Parameters
***************************************/

#define Timer_speed__LEFT 0
#define Timer_speed__RIGHT 1
#define Timer_speed__CENTER 2
#define Timer_speed__ASYMMETRIC 3

#define Timer_speed__X1 0
#define Timer_speed__X2 1
#define Timer_speed__X4 2

#define Timer_speed__PWM 4
#define Timer_speed__PWM_DT 5
#define Timer_speed__PWM_PR 6

#define Timer_speed__INVERSE 1
#define Timer_speed__DIRECT 0

#define Timer_speed__CAPTURE 2
#define Timer_speed__COMPARE 0

#define Timer_speed__TRIG_LEVEL 3
#define Timer_speed__TRIG_RISING 0
#define Timer_speed__TRIG_FALLING 1
#define Timer_speed__TRIG_BOTH 2

#define Timer_speed__INTR_MASK_TC 1
#define Timer_speed__INTR_MASK_CC_MATCH 2
#define Timer_speed__INTR_MASK_NONE 0
#define Timer_speed__INTR_MASK_TC_CC 3

#define Timer_speed__UNCONFIG 8
#define Timer_speed__TIMER 1
#define Timer_speed__QUAD 3
#define Timer_speed__PWM_SEL 7

#define Timer_speed__COUNT_UP 0
#define Timer_speed__COUNT_DOWN 1
#define Timer_speed__COUNT_UPDOWN0 2
#define Timer_speed__COUNT_UPDOWN1 3


/* Prescaler */
#define Timer_speed_PRESCALE_DIVBY1                ((uint32)(0u << Timer_speed_PRESCALER_SHIFT))
#define Timer_speed_PRESCALE_DIVBY2                ((uint32)(1u << Timer_speed_PRESCALER_SHIFT))
#define Timer_speed_PRESCALE_DIVBY4                ((uint32)(2u << Timer_speed_PRESCALER_SHIFT))
#define Timer_speed_PRESCALE_DIVBY8                ((uint32)(3u << Timer_speed_PRESCALER_SHIFT))
#define Timer_speed_PRESCALE_DIVBY16               ((uint32)(4u << Timer_speed_PRESCALER_SHIFT))
#define Timer_speed_PRESCALE_DIVBY32               ((uint32)(5u << Timer_speed_PRESCALER_SHIFT))
#define Timer_speed_PRESCALE_DIVBY64               ((uint32)(6u << Timer_speed_PRESCALER_SHIFT))
#define Timer_speed_PRESCALE_DIVBY128              ((uint32)(7u << Timer_speed_PRESCALER_SHIFT))

/* TCPWM set modes */
#define Timer_speed_MODE_TIMER_COMPARE             ((uint32)(Timer_speed__COMPARE         <<  \
                                                                  Timer_speed_MODE_SHIFT))
#define Timer_speed_MODE_TIMER_CAPTURE             ((uint32)(Timer_speed__CAPTURE         <<  \
                                                                  Timer_speed_MODE_SHIFT))
#define Timer_speed_MODE_QUAD                      ((uint32)(Timer_speed__QUAD            <<  \
                                                                  Timer_speed_MODE_SHIFT))
#define Timer_speed_MODE_PWM                       ((uint32)(Timer_speed__PWM             <<  \
                                                                  Timer_speed_MODE_SHIFT))
#define Timer_speed_MODE_PWM_DT                    ((uint32)(Timer_speed__PWM_DT          <<  \
                                                                  Timer_speed_MODE_SHIFT))
#define Timer_speed_MODE_PWM_PR                    ((uint32)(Timer_speed__PWM_PR          <<  \
                                                                  Timer_speed_MODE_SHIFT))

/* Quad Modes */
#define Timer_speed_MODE_X1                        ((uint32)(Timer_speed__X1              <<  \
                                                                  Timer_speed_QUAD_MODE_SHIFT))
#define Timer_speed_MODE_X2                        ((uint32)(Timer_speed__X2              <<  \
                                                                  Timer_speed_QUAD_MODE_SHIFT))
#define Timer_speed_MODE_X4                        ((uint32)(Timer_speed__X4              <<  \
                                                                  Timer_speed_QUAD_MODE_SHIFT))

/* Counter modes */
#define Timer_speed_COUNT_UP                       ((uint32)(Timer_speed__COUNT_UP        <<  \
                                                                  Timer_speed_UPDOWN_SHIFT))
#define Timer_speed_COUNT_DOWN                     ((uint32)(Timer_speed__COUNT_DOWN      <<  \
                                                                  Timer_speed_UPDOWN_SHIFT))
#define Timer_speed_COUNT_UPDOWN0                  ((uint32)(Timer_speed__COUNT_UPDOWN0   <<  \
                                                                  Timer_speed_UPDOWN_SHIFT))
#define Timer_speed_COUNT_UPDOWN1                  ((uint32)(Timer_speed__COUNT_UPDOWN1   <<  \
                                                                  Timer_speed_UPDOWN_SHIFT))

/* PWM output invert */
#define Timer_speed_INVERT_LINE                    ((uint32)(Timer_speed__INVERSE         <<  \
                                                                  Timer_speed_INV_OUT_SHIFT))
#define Timer_speed_INVERT_LINE_N                  ((uint32)(Timer_speed__INVERSE         <<  \
                                                                  Timer_speed_INV_COMPL_OUT_SHIFT))

/* Trigger modes */
#define Timer_speed_TRIG_RISING                    ((uint32)Timer_speed__TRIG_RISING)
#define Timer_speed_TRIG_FALLING                   ((uint32)Timer_speed__TRIG_FALLING)
#define Timer_speed_TRIG_BOTH                      ((uint32)Timer_speed__TRIG_BOTH)
#define Timer_speed_TRIG_LEVEL                     ((uint32)Timer_speed__TRIG_LEVEL)

/* Interrupt mask */
#define Timer_speed_INTR_MASK_TC                   ((uint32)Timer_speed__INTR_MASK_TC)
#define Timer_speed_INTR_MASK_CC_MATCH             ((uint32)Timer_speed__INTR_MASK_CC_MATCH)

/* PWM Output Controls */
#define Timer_speed_CC_MATCH_SET                   (0x00u)
#define Timer_speed_CC_MATCH_CLEAR                 (0x01u)
#define Timer_speed_CC_MATCH_INVERT                (0x02u)
#define Timer_speed_CC_MATCH_NO_CHANGE             (0x03u)
#define Timer_speed_OVERLOW_SET                    (0x00u)
#define Timer_speed_OVERLOW_CLEAR                  (0x04u)
#define Timer_speed_OVERLOW_INVERT                 (0x08u)
#define Timer_speed_OVERLOW_NO_CHANGE              (0x0Cu)
#define Timer_speed_UNDERFLOW_SET                  (0x00u)
#define Timer_speed_UNDERFLOW_CLEAR                (0x10u)
#define Timer_speed_UNDERFLOW_INVERT               (0x20u)
#define Timer_speed_UNDERFLOW_NO_CHANGE            (0x30u)

/* PWM Align */
#define Timer_speed_PWM_MODE_LEFT                  (Timer_speed_CC_MATCH_CLEAR        |   \
                                                         Timer_speed_OVERLOW_SET           |   \
                                                         Timer_speed_UNDERFLOW_NO_CHANGE)
#define Timer_speed_PWM_MODE_RIGHT                 (Timer_speed_CC_MATCH_SET          |   \
                                                         Timer_speed_OVERLOW_NO_CHANGE     |   \
                                                         Timer_speed_UNDERFLOW_CLEAR)
#define Timer_speed_PWM_MODE_ASYM                  (Timer_speed_CC_MATCH_INVERT       |   \
                                                         Timer_speed_OVERLOW_SET           |   \
                                                         Timer_speed_UNDERFLOW_CLEAR)

#if (Timer_speed_CY_TCPWM_V2)
    #if(Timer_speed_CY_TCPWM_4000)
        #define Timer_speed_PWM_MODE_CENTER                (Timer_speed_CC_MATCH_INVERT       |   \
                                                                 Timer_speed_OVERLOW_NO_CHANGE     |   \
                                                                 Timer_speed_UNDERFLOW_CLEAR)
    #else
        #define Timer_speed_PWM_MODE_CENTER                (Timer_speed_CC_MATCH_INVERT       |   \
                                                                 Timer_speed_OVERLOW_SET           |   \
                                                                 Timer_speed_UNDERFLOW_CLEAR)
    #endif /* (Timer_speed_CY_TCPWM_4000) */
#else
    #define Timer_speed_PWM_MODE_CENTER                (Timer_speed_CC_MATCH_INVERT       |   \
                                                             Timer_speed_OVERLOW_NO_CHANGE     |   \
                                                             Timer_speed_UNDERFLOW_CLEAR)
#endif /* (Timer_speed_CY_TCPWM_NEW) */

/* Command operations without condition */
#define Timer_speed_CMD_CAPTURE                    (0u)
#define Timer_speed_CMD_RELOAD                     (8u)
#define Timer_speed_CMD_STOP                       (16u)
#define Timer_speed_CMD_START                      (24u)

/* Status */
#define Timer_speed_STATUS_DOWN                    (1u)
#define Timer_speed_STATUS_RUNNING                 (2u)


/***************************************
*        Function Prototypes
****************************************/

void   Timer_speed_Init(void);
void   Timer_speed_Enable(void);
void   Timer_speed_Start(void);
void   Timer_speed_Stop(void);

void   Timer_speed_SetMode(uint32 mode);
void   Timer_speed_SetCounterMode(uint32 counterMode);
void   Timer_speed_SetPWMMode(uint32 modeMask);
void   Timer_speed_SetQDMode(uint32 qdMode);

void   Timer_speed_SetPrescaler(uint32 prescaler);
void   Timer_speed_TriggerCommand(uint32 mask, uint32 command);
void   Timer_speed_SetOneShot(uint32 oneShotEnable);
uint32 Timer_speed_ReadStatus(void);

void   Timer_speed_SetPWMSyncKill(uint32 syncKillEnable);
void   Timer_speed_SetPWMStopOnKill(uint32 stopOnKillEnable);
void   Timer_speed_SetPWMDeadTime(uint32 deadTime);
void   Timer_speed_SetPWMInvert(uint32 mask);

void   Timer_speed_SetInterruptMode(uint32 interruptMask);
uint32 Timer_speed_GetInterruptSourceMasked(void);
uint32 Timer_speed_GetInterruptSource(void);
void   Timer_speed_ClearInterrupt(uint32 interruptMask);
void   Timer_speed_SetInterrupt(uint32 interruptMask);

void   Timer_speed_WriteCounter(uint32 count);
uint32 Timer_speed_ReadCounter(void);

uint32 Timer_speed_ReadCapture(void);
uint32 Timer_speed_ReadCaptureBuf(void);

void   Timer_speed_WritePeriod(uint32 period);
uint32 Timer_speed_ReadPeriod(void);
void   Timer_speed_WritePeriodBuf(uint32 periodBuf);
uint32 Timer_speed_ReadPeriodBuf(void);

void   Timer_speed_WriteCompare(uint32 compare);
uint32 Timer_speed_ReadCompare(void);
void   Timer_speed_WriteCompareBuf(uint32 compareBuf);
uint32 Timer_speed_ReadCompareBuf(void);

void   Timer_speed_SetPeriodSwap(uint32 swapEnable);
void   Timer_speed_SetCompareSwap(uint32 swapEnable);

void   Timer_speed_SetCaptureMode(uint32 triggerMode);
void   Timer_speed_SetReloadMode(uint32 triggerMode);
void   Timer_speed_SetStartMode(uint32 triggerMode);
void   Timer_speed_SetStopMode(uint32 triggerMode);
void   Timer_speed_SetCountMode(uint32 triggerMode);

void   Timer_speed_SaveConfig(void);
void   Timer_speed_RestoreConfig(void);
void   Timer_speed_Sleep(void);
void   Timer_speed_Wakeup(void);


/***************************************
*             Registers
***************************************/

#define Timer_speed_BLOCK_CONTROL_REG              (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TCPWM_CTRL )
#define Timer_speed_BLOCK_CONTROL_PTR              ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TCPWM_CTRL )
#define Timer_speed_COMMAND_REG                    (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TCPWM_CMD )
#define Timer_speed_COMMAND_PTR                    ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TCPWM_CMD )
#define Timer_speed_INTRRUPT_CAUSE_REG             (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TCPWM_INTR_CAUSE )
#define Timer_speed_INTRRUPT_CAUSE_PTR             ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TCPWM_INTR_CAUSE )
#define Timer_speed_CONTROL_REG                    (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__CTRL )
#define Timer_speed_CONTROL_PTR                    ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__CTRL )
#define Timer_speed_STATUS_REG                     (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__STATUS )
#define Timer_speed_STATUS_PTR                     ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__STATUS )
#define Timer_speed_COUNTER_REG                    (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__COUNTER )
#define Timer_speed_COUNTER_PTR                    ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__COUNTER )
#define Timer_speed_COMP_CAP_REG                   (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__CC )
#define Timer_speed_COMP_CAP_PTR                   ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__CC )
#define Timer_speed_COMP_CAP_BUF_REG               (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__CC_BUFF )
#define Timer_speed_COMP_CAP_BUF_PTR               ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__CC_BUFF )
#define Timer_speed_PERIOD_REG                     (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__PERIOD )
#define Timer_speed_PERIOD_PTR                     ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__PERIOD )
#define Timer_speed_PERIOD_BUF_REG                 (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__PERIOD_BUFF )
#define Timer_speed_PERIOD_BUF_PTR                 ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__PERIOD_BUFF )
#define Timer_speed_TRIG_CONTROL0_REG              (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TR_CTRL0 )
#define Timer_speed_TRIG_CONTROL0_PTR              ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TR_CTRL0 )
#define Timer_speed_TRIG_CONTROL1_REG              (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TR_CTRL1 )
#define Timer_speed_TRIG_CONTROL1_PTR              ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TR_CTRL1 )
#define Timer_speed_TRIG_CONTROL2_REG              (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TR_CTRL2 )
#define Timer_speed_TRIG_CONTROL2_PTR              ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__TR_CTRL2 )
#define Timer_speed_INTERRUPT_REQ_REG              (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__INTR )
#define Timer_speed_INTERRUPT_REQ_PTR              ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__INTR )
#define Timer_speed_INTERRUPT_SET_REG              (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__INTR_SET )
#define Timer_speed_INTERRUPT_SET_PTR              ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__INTR_SET )
#define Timer_speed_INTERRUPT_MASK_REG             (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__INTR_MASK )
#define Timer_speed_INTERRUPT_MASK_PTR             ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__INTR_MASK )
#define Timer_speed_INTERRUPT_MASKED_REG           (*(reg32 *) Timer_speed_cy_m0s8_tcpwm_1__INTR_MASKED )
#define Timer_speed_INTERRUPT_MASKED_PTR           ( (reg32 *) Timer_speed_cy_m0s8_tcpwm_1__INTR_MASKED )


/***************************************
*       Registers Constants
***************************************/

/* Mask */
#define Timer_speed_MASK                           ((uint32)Timer_speed_cy_m0s8_tcpwm_1__TCPWM_CTRL_MASK)

/* Shift constants for control register */
#define Timer_speed_RELOAD_CC_SHIFT                (0u)
#define Timer_speed_RELOAD_PERIOD_SHIFT            (1u)
#define Timer_speed_PWM_SYNC_KILL_SHIFT            (2u)
#define Timer_speed_PWM_STOP_KILL_SHIFT            (3u)
#define Timer_speed_PRESCALER_SHIFT                (8u)
#define Timer_speed_UPDOWN_SHIFT                   (16u)
#define Timer_speed_ONESHOT_SHIFT                  (18u)
#define Timer_speed_QUAD_MODE_SHIFT                (20u)
#define Timer_speed_INV_OUT_SHIFT                  (20u)
#define Timer_speed_INV_COMPL_OUT_SHIFT            (21u)
#define Timer_speed_MODE_SHIFT                     (24u)

/* Mask constants for control register */
#define Timer_speed_RELOAD_CC_MASK                 ((uint32)(Timer_speed_1BIT_MASK        <<  \
                                                                            Timer_speed_RELOAD_CC_SHIFT))
#define Timer_speed_RELOAD_PERIOD_MASK             ((uint32)(Timer_speed_1BIT_MASK        <<  \
                                                                            Timer_speed_RELOAD_PERIOD_SHIFT))
#define Timer_speed_PWM_SYNC_KILL_MASK             ((uint32)(Timer_speed_1BIT_MASK        <<  \
                                                                            Timer_speed_PWM_SYNC_KILL_SHIFT))
#define Timer_speed_PWM_STOP_KILL_MASK             ((uint32)(Timer_speed_1BIT_MASK        <<  \
                                                                            Timer_speed_PWM_STOP_KILL_SHIFT))
#define Timer_speed_PRESCALER_MASK                 ((uint32)(Timer_speed_8BIT_MASK        <<  \
                                                                            Timer_speed_PRESCALER_SHIFT))
#define Timer_speed_UPDOWN_MASK                    ((uint32)(Timer_speed_2BIT_MASK        <<  \
                                                                            Timer_speed_UPDOWN_SHIFT))
#define Timer_speed_ONESHOT_MASK                   ((uint32)(Timer_speed_1BIT_MASK        <<  \
                                                                            Timer_speed_ONESHOT_SHIFT))
#define Timer_speed_QUAD_MODE_MASK                 ((uint32)(Timer_speed_3BIT_MASK        <<  \
                                                                            Timer_speed_QUAD_MODE_SHIFT))
#define Timer_speed_INV_OUT_MASK                   ((uint32)(Timer_speed_2BIT_MASK        <<  \
                                                                            Timer_speed_INV_OUT_SHIFT))
#define Timer_speed_MODE_MASK                      ((uint32)(Timer_speed_3BIT_MASK        <<  \
                                                                            Timer_speed_MODE_SHIFT))

/* Shift constants for trigger control register 1 */
#define Timer_speed_CAPTURE_SHIFT                  (0u)
#define Timer_speed_COUNT_SHIFT                    (2u)
#define Timer_speed_RELOAD_SHIFT                   (4u)
#define Timer_speed_STOP_SHIFT                     (6u)
#define Timer_speed_START_SHIFT                    (8u)

/* Mask constants for trigger control register 1 */
#define Timer_speed_CAPTURE_MASK                   ((uint32)(Timer_speed_2BIT_MASK        <<  \
                                                                  Timer_speed_CAPTURE_SHIFT))
#define Timer_speed_COUNT_MASK                     ((uint32)(Timer_speed_2BIT_MASK        <<  \
                                                                  Timer_speed_COUNT_SHIFT))
#define Timer_speed_RELOAD_MASK                    ((uint32)(Timer_speed_2BIT_MASK        <<  \
                                                                  Timer_speed_RELOAD_SHIFT))
#define Timer_speed_STOP_MASK                      ((uint32)(Timer_speed_2BIT_MASK        <<  \
                                                                  Timer_speed_STOP_SHIFT))
#define Timer_speed_START_MASK                     ((uint32)(Timer_speed_2BIT_MASK        <<  \
                                                                  Timer_speed_START_SHIFT))

/* MASK */
#define Timer_speed_1BIT_MASK                      ((uint32)0x01u)
#define Timer_speed_2BIT_MASK                      ((uint32)0x03u)
#define Timer_speed_3BIT_MASK                      ((uint32)0x07u)
#define Timer_speed_6BIT_MASK                      ((uint32)0x3Fu)
#define Timer_speed_8BIT_MASK                      ((uint32)0xFFu)
#define Timer_speed_16BIT_MASK                     ((uint32)0xFFFFu)

/* Shift constant for status register */
#define Timer_speed_RUNNING_STATUS_SHIFT           (30u)


/***************************************
*    Initial Constants
***************************************/

#define Timer_speed_CTRL_QUAD_BASE_CONFIG                                                          \
        (((uint32)(Timer_speed_QUAD_ENCODING_MODES     << Timer_speed_QUAD_MODE_SHIFT))       |\
         ((uint32)(Timer_speed_CONFIG                  << Timer_speed_MODE_SHIFT)))

#define Timer_speed_CTRL_PWM_BASE_CONFIG                                                           \
        (((uint32)(Timer_speed_PWM_STOP_EVENT          << Timer_speed_PWM_STOP_KILL_SHIFT))   |\
         ((uint32)(Timer_speed_PWM_OUT_INVERT          << Timer_speed_INV_OUT_SHIFT))         |\
         ((uint32)(Timer_speed_PWM_OUT_N_INVERT        << Timer_speed_INV_COMPL_OUT_SHIFT))   |\
         ((uint32)(Timer_speed_PWM_MODE                << Timer_speed_MODE_SHIFT)))

#define Timer_speed_CTRL_PWM_RUN_MODE                                                              \
            ((uint32)(Timer_speed_PWM_RUN_MODE         << Timer_speed_ONESHOT_SHIFT))
            
#define Timer_speed_CTRL_PWM_ALIGN                                                                 \
            ((uint32)(Timer_speed_PWM_ALIGN            << Timer_speed_UPDOWN_SHIFT))

#define Timer_speed_CTRL_PWM_KILL_EVENT                                                            \
             ((uint32)(Timer_speed_PWM_KILL_EVENT      << Timer_speed_PWM_SYNC_KILL_SHIFT))

#define Timer_speed_CTRL_PWM_DEAD_TIME_CYCLE                                                       \
            ((uint32)(Timer_speed_PWM_DEAD_TIME_CYCLE  << Timer_speed_PRESCALER_SHIFT))

#define Timer_speed_CTRL_PWM_PRESCALER                                                             \
            ((uint32)(Timer_speed_PWM_PRESCALER        << Timer_speed_PRESCALER_SHIFT))

#define Timer_speed_CTRL_TIMER_BASE_CONFIG                                                         \
        (((uint32)(Timer_speed_TC_PRESCALER            << Timer_speed_PRESCALER_SHIFT))       |\
         ((uint32)(Timer_speed_TC_COUNTER_MODE         << Timer_speed_UPDOWN_SHIFT))          |\
         ((uint32)(Timer_speed_TC_RUN_MODE             << Timer_speed_ONESHOT_SHIFT))         |\
         ((uint32)(Timer_speed_TC_COMP_CAP_MODE        << Timer_speed_MODE_SHIFT)))
        
#define Timer_speed_QUAD_SIGNALS_MODES                                                             \
        (((uint32)(Timer_speed_QUAD_PHIA_SIGNAL_MODE   << Timer_speed_COUNT_SHIFT))           |\
         ((uint32)(Timer_speed_QUAD_INDEX_SIGNAL_MODE  << Timer_speed_RELOAD_SHIFT))          |\
         ((uint32)(Timer_speed_QUAD_STOP_SIGNAL_MODE   << Timer_speed_STOP_SHIFT))            |\
         ((uint32)(Timer_speed_QUAD_PHIB_SIGNAL_MODE   << Timer_speed_START_SHIFT)))

#define Timer_speed_PWM_SIGNALS_MODES                                                              \
        (((uint32)(Timer_speed_PWM_SWITCH_SIGNAL_MODE  << Timer_speed_CAPTURE_SHIFT))         |\
         ((uint32)(Timer_speed_PWM_COUNT_SIGNAL_MODE   << Timer_speed_COUNT_SHIFT))           |\
         ((uint32)(Timer_speed_PWM_RELOAD_SIGNAL_MODE  << Timer_speed_RELOAD_SHIFT))          |\
         ((uint32)(Timer_speed_PWM_STOP_SIGNAL_MODE    << Timer_speed_STOP_SHIFT))            |\
         ((uint32)(Timer_speed_PWM_START_SIGNAL_MODE   << Timer_speed_START_SHIFT)))

#define Timer_speed_TIMER_SIGNALS_MODES                                                            \
        (((uint32)(Timer_speed_TC_CAPTURE_SIGNAL_MODE  << Timer_speed_CAPTURE_SHIFT))         |\
         ((uint32)(Timer_speed_TC_COUNT_SIGNAL_MODE    << Timer_speed_COUNT_SHIFT))           |\
         ((uint32)(Timer_speed_TC_RELOAD_SIGNAL_MODE   << Timer_speed_RELOAD_SHIFT))          |\
         ((uint32)(Timer_speed_TC_STOP_SIGNAL_MODE     << Timer_speed_STOP_SHIFT))            |\
         ((uint32)(Timer_speed_TC_START_SIGNAL_MODE    << Timer_speed_START_SHIFT)))
        
#define Timer_speed_TIMER_UPDOWN_CNT_USED                                                          \
                ((Timer_speed__COUNT_UPDOWN0 == Timer_speed_TC_COUNTER_MODE)                  ||\
                 (Timer_speed__COUNT_UPDOWN1 == Timer_speed_TC_COUNTER_MODE))

#define Timer_speed_PWM_UPDOWN_CNT_USED                                                            \
                ((Timer_speed__CENTER == Timer_speed_PWM_ALIGN)                               ||\
                 (Timer_speed__ASYMMETRIC == Timer_speed_PWM_ALIGN))               
        
#define Timer_speed_PWM_PR_INIT_VALUE              (1u)
#define Timer_speed_QUAD_PERIOD_INIT_VALUE         (0x8000u)



#endif /* End CY_TCPWM_Timer_speed_H */

/* [] END OF FILE */
