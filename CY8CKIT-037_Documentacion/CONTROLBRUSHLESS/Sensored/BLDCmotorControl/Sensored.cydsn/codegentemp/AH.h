/*******************************************************************************
* File Name: AH.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_AH_H) /* Pins AH_H */
#define CY_PINS_AH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "AH_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} AH_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   AH_Read(void);
void    AH_Write(uint8 value);
uint8   AH_ReadDataReg(void);
#if defined(AH__PC) || (CY_PSOC4_4200L) 
    void    AH_SetDriveMode(uint8 mode);
#endif
void    AH_SetInterruptMode(uint16 position, uint16 mode);
uint8   AH_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void AH_Sleep(void); 
void AH_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(AH__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define AH_DRIVE_MODE_BITS        (3)
    #define AH_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - AH_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the AH_SetDriveMode() function.
         *  @{
         */
        #define AH_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define AH_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define AH_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define AH_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define AH_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define AH_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define AH_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define AH_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define AH_MASK               AH__MASK
#define AH_SHIFT              AH__SHIFT
#define AH_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in AH_SetInterruptMode() function.
     *  @{
     */
        #define AH_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define AH_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define AH_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define AH_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(AH__SIO)
    #define AH_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(AH__PC) && (CY_PSOC4_4200L)
    #define AH_USBIO_ENABLE               ((uint32)0x80000000u)
    #define AH_USBIO_DISABLE              ((uint32)(~AH_USBIO_ENABLE))
    #define AH_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define AH_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define AH_USBIO_ENTER_SLEEP          ((uint32)((1u << AH_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << AH_USBIO_SUSPEND_DEL_SHIFT)))
    #define AH_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << AH_USBIO_SUSPEND_SHIFT)))
    #define AH_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << AH_USBIO_SUSPEND_DEL_SHIFT)))
    #define AH_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(AH__PC)
    /* Port Configuration */
    #define AH_PC                 (* (reg32 *) AH__PC)
#endif
/* Pin State */
#define AH_PS                     (* (reg32 *) AH__PS)
/* Data Register */
#define AH_DR                     (* (reg32 *) AH__DR)
/* Input Buffer Disable Override */
#define AH_INP_DIS                (* (reg32 *) AH__PC2)

/* Interrupt configuration Registers */
#define AH_INTCFG                 (* (reg32 *) AH__INTCFG)
#define AH_INTSTAT                (* (reg32 *) AH__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define AH_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(AH__SIO)
    #define AH_SIO_REG            (* (reg32 *) AH__SIO)
#endif /* (AH__SIO_CFG) */

/* USBIO registers */
#if !defined(AH__PC) && (CY_PSOC4_4200L)
    #define AH_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define AH_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define AH_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define AH_DRIVE_MODE_SHIFT       (0x00u)
#define AH_DRIVE_MODE_MASK        (0x07u << AH_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins AH_H */


/* [] END OF FILE */
