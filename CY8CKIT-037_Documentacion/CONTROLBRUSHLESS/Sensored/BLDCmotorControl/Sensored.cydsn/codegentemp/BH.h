/*******************************************************************************
* File Name: BH.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_BH_H) /* Pins BH_H */
#define CY_PINS_BH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "BH_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} BH_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   BH_Read(void);
void    BH_Write(uint8 value);
uint8   BH_ReadDataReg(void);
#if defined(BH__PC) || (CY_PSOC4_4200L) 
    void    BH_SetDriveMode(uint8 mode);
#endif
void    BH_SetInterruptMode(uint16 position, uint16 mode);
uint8   BH_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void BH_Sleep(void); 
void BH_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(BH__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define BH_DRIVE_MODE_BITS        (3)
    #define BH_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - BH_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the BH_SetDriveMode() function.
         *  @{
         */
        #define BH_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define BH_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define BH_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define BH_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define BH_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define BH_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define BH_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define BH_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define BH_MASK               BH__MASK
#define BH_SHIFT              BH__SHIFT
#define BH_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in BH_SetInterruptMode() function.
     *  @{
     */
        #define BH_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define BH_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define BH_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define BH_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(BH__SIO)
    #define BH_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(BH__PC) && (CY_PSOC4_4200L)
    #define BH_USBIO_ENABLE               ((uint32)0x80000000u)
    #define BH_USBIO_DISABLE              ((uint32)(~BH_USBIO_ENABLE))
    #define BH_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define BH_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define BH_USBIO_ENTER_SLEEP          ((uint32)((1u << BH_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << BH_USBIO_SUSPEND_DEL_SHIFT)))
    #define BH_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << BH_USBIO_SUSPEND_SHIFT)))
    #define BH_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << BH_USBIO_SUSPEND_DEL_SHIFT)))
    #define BH_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(BH__PC)
    /* Port Configuration */
    #define BH_PC                 (* (reg32 *) BH__PC)
#endif
/* Pin State */
#define BH_PS                     (* (reg32 *) BH__PS)
/* Data Register */
#define BH_DR                     (* (reg32 *) BH__DR)
/* Input Buffer Disable Override */
#define BH_INP_DIS                (* (reg32 *) BH__PC2)

/* Interrupt configuration Registers */
#define BH_INTCFG                 (* (reg32 *) BH__INTCFG)
#define BH_INTSTAT                (* (reg32 *) BH__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define BH_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(BH__SIO)
    #define BH_SIO_REG            (* (reg32 *) BH__SIO)
#endif /* (BH__SIO_CFG) */

/* USBIO registers */
#if !defined(BH__PC) && (CY_PSOC4_4200L)
    #define BH_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define BH_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define BH_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define BH_DRIVE_MODE_SHIFT       (0x00u)
#define BH_DRIVE_MODE_MASK        (0x07u << BH_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins BH_H */


/* [] END OF FILE */
