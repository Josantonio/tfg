/*******************************************************************************
* File Name: BH.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_BH_ALIASES_H) /* Pins BH_ALIASES_H */
#define CY_PINS_BH_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define BH_0			(BH__0__PC)
#define BH_0_PS		(BH__0__PS)
#define BH_0_PC		(BH__0__PC)
#define BH_0_DR		(BH__0__DR)
#define BH_0_SHIFT	(BH__0__SHIFT)
#define BH_0_INTR	((uint16)((uint16)0x0003u << (BH__0__SHIFT*2u)))

#define BH_INTR_ALL	 ((uint16)(BH_0_INTR))


#endif /* End Pins BH_ALIASES_H */


/* [] END OF FILE */
