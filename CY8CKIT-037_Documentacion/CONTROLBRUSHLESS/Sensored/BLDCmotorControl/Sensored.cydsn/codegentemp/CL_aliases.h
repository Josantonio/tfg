/*******************************************************************************
* File Name: CL.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CL_ALIASES_H) /* Pins CL_ALIASES_H */
#define CY_PINS_CL_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define CL_0			(CL__0__PC)
#define CL_0_PS		(CL__0__PS)
#define CL_0_PC		(CL__0__PC)
#define CL_0_DR		(CL__0__DR)
#define CL_0_SHIFT	(CL__0__SHIFT)
#define CL_0_INTR	((uint16)((uint16)0x0003u << (CL__0__SHIFT*2u)))

#define CL_INTR_ALL	 ((uint16)(CL_0_INTR))


#endif /* End Pins CL_ALIASES_H */


/* [] END OF FILE */
