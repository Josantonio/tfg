/*******************************************************************************
* File Name: CL.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CL_H) /* Pins CL_H */
#define CY_PINS_CL_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CL_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} CL_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   CL_Read(void);
void    CL_Write(uint8 value);
uint8   CL_ReadDataReg(void);
#if defined(CL__PC) || (CY_PSOC4_4200L) 
    void    CL_SetDriveMode(uint8 mode);
#endif
void    CL_SetInterruptMode(uint16 position, uint16 mode);
uint8   CL_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void CL_Sleep(void); 
void CL_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(CL__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define CL_DRIVE_MODE_BITS        (3)
    #define CL_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - CL_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the CL_SetDriveMode() function.
         *  @{
         */
        #define CL_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define CL_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define CL_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define CL_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define CL_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define CL_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define CL_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define CL_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define CL_MASK               CL__MASK
#define CL_SHIFT              CL__SHIFT
#define CL_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in CL_SetInterruptMode() function.
     *  @{
     */
        #define CL_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define CL_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define CL_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define CL_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(CL__SIO)
    #define CL_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(CL__PC) && (CY_PSOC4_4200L)
    #define CL_USBIO_ENABLE               ((uint32)0x80000000u)
    #define CL_USBIO_DISABLE              ((uint32)(~CL_USBIO_ENABLE))
    #define CL_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define CL_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define CL_USBIO_ENTER_SLEEP          ((uint32)((1u << CL_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << CL_USBIO_SUSPEND_DEL_SHIFT)))
    #define CL_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << CL_USBIO_SUSPEND_SHIFT)))
    #define CL_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << CL_USBIO_SUSPEND_DEL_SHIFT)))
    #define CL_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(CL__PC)
    /* Port Configuration */
    #define CL_PC                 (* (reg32 *) CL__PC)
#endif
/* Pin State */
#define CL_PS                     (* (reg32 *) CL__PS)
/* Data Register */
#define CL_DR                     (* (reg32 *) CL__DR)
/* Input Buffer Disable Override */
#define CL_INP_DIS                (* (reg32 *) CL__PC2)

/* Interrupt configuration Registers */
#define CL_INTCFG                 (* (reg32 *) CL__INTCFG)
#define CL_INTSTAT                (* (reg32 *) CL__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define CL_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(CL__SIO)
    #define CL_SIO_REG            (* (reg32 *) CL__SIO)
#endif /* (CL__SIO_CFG) */

/* USBIO registers */
#if !defined(CL__PC) && (CY_PSOC4_4200L)
    #define CL_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define CL_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define CL_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define CL_DRIVE_MODE_SHIFT       (0x00u)
#define CL_DRIVE_MODE_MASK        (0x07u << CL_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins CL_H */


/* [] END OF FILE */
