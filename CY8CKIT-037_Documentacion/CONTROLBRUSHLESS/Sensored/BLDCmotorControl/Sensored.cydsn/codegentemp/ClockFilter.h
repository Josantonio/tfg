/*******************************************************************************
* File Name: ClockFilter.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_ClockFilter_H)
#define CY_CLOCK_ClockFilter_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void ClockFilter_StartEx(uint32 alignClkDiv);
#define ClockFilter_Start() \
    ClockFilter_StartEx(ClockFilter__PA_DIV_ID)

#else

void ClockFilter_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void ClockFilter_Stop(void);

void ClockFilter_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 ClockFilter_GetDividerRegister(void);
uint8  ClockFilter_GetFractionalDividerRegister(void);

#define ClockFilter_Enable()                         ClockFilter_Start()
#define ClockFilter_Disable()                        ClockFilter_Stop()
#define ClockFilter_SetDividerRegister(clkDivider, reset)  \
    ClockFilter_SetFractionalDividerRegister((clkDivider), 0u)
#define ClockFilter_SetDivider(clkDivider)           ClockFilter_SetDividerRegister((clkDivider), 1u)
#define ClockFilter_SetDividerValue(clkDivider)      ClockFilter_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define ClockFilter_DIV_ID     ClockFilter__DIV_ID

#define ClockFilter_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define ClockFilter_CTRL_REG   (*(reg32 *)ClockFilter__CTRL_REGISTER)
#define ClockFilter_DIV_REG    (*(reg32 *)ClockFilter__DIV_REGISTER)

#define ClockFilter_CMD_DIV_SHIFT          (0u)
#define ClockFilter_CMD_PA_DIV_SHIFT       (8u)
#define ClockFilter_CMD_DISABLE_SHIFT      (30u)
#define ClockFilter_CMD_ENABLE_SHIFT       (31u)

#define ClockFilter_CMD_DISABLE_MASK       ((uint32)((uint32)1u << ClockFilter_CMD_DISABLE_SHIFT))
#define ClockFilter_CMD_ENABLE_MASK        ((uint32)((uint32)1u << ClockFilter_CMD_ENABLE_SHIFT))

#define ClockFilter_DIV_FRAC_MASK  (0x000000F8u)
#define ClockFilter_DIV_FRAC_SHIFT (3u)
#define ClockFilter_DIV_INT_MASK   (0xFFFFFF00u)
#define ClockFilter_DIV_INT_SHIFT  (8u)

#else 

#define ClockFilter_DIV_REG        (*(reg32 *)ClockFilter__REGISTER)
#define ClockFilter_ENABLE_REG     ClockFilter_DIV_REG
#define ClockFilter_DIV_FRAC_MASK  ClockFilter__FRAC_MASK
#define ClockFilter_DIV_FRAC_SHIFT (16u)
#define ClockFilter_DIV_INT_MASK   ClockFilter__DIVIDER_MASK
#define ClockFilter_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_ClockFilter_H) */

/* [] END OF FILE */
