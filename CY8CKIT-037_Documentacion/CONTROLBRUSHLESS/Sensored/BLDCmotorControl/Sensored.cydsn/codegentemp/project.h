/*******************************************************************************
* File Name: project.h
* 
* PSoC Creator  4.3
*
* Description:
* It contains references to all generated header files and should not be modified.
* This file is automatically generated by PSoC Creator.
*
********************************************************************************
* Copyright (c) 2007-2019 Cypress Semiconductor.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cyfitter_cfg.h"
#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cydisabledsheets.h"
#include "PinButton2.h"
#include "PinButton2_aliases.h"
#include "isr_button2.h"
#include "ClockFilter.h"
#include "UART.h"
#include "UART_SPI_UART.h"
#include "UART_PINS.h"
#include "UART_SPI_UART_PVT.h"
#include "UART_PVT.h"
#include "UART_BOOT.h"
#include "ClockPWM.h"
#include "Control_Reg.h"
#include "CL.h"
#include "CL_aliases.h"
#include "CH.h"
#include "CH_aliases.h"
#include "BL.h"
#include "BL_aliases.h"
#include "BH.h"
#include "BH_aliases.h"
#include "AL.h"
#include "AL_aliases.h"
#include "AH.h"
#include "AH_aliases.h"
#include "Hall1.h"
#include "Hall1_aliases.h"
#include "Hall2.h"
#include "Hall2_aliases.h"
#include "Hall3.h"
#include "Hall3_aliases.h"
#include "Pin_LED_Green.h"
#include "Pin_LED_Green_aliases.h"
#include "Timer_speed.h"
#include "isr_speed.h"
#include "clock_speed.h"
#include "Hall_Error.h"
#include "PWM_Driver.h"
#include "PinPot.h"
#include "PinPot_aliases.h"
#include "ADC_SAR.h"
#include "isr_Tc.h"
#include "PinVinSense.h"
#include "PinVinSense_aliases.h"
#include "Pin_LED_Red.h"
#include "Pin_LED_Red_aliases.h"
#include "Pin_IVref.h"
#include "Pin_IVref_aliases.h"
#include "Pin_IVo.h"
#include "Pin_IVo_aliases.h"
#include "isr_oc.h"
#include "OC_COMP.h"
#include "I_ref_source.h"
#include "UART_SCBCLK.h"
#include "UART_tx.h"
#include "UART_tx_aliases.h"
#include "UART_rx.h"
#include "UART_rx_aliases.h"
#include "ADC_SAR_IRQ.h"
#include "ADC_SAR_intClock.h"
#include "cy_em_eeprom.h"
#include "core_cm0_psoc4.h"
#include "CyFlash.h"
#include "CyLib.h"
#include "cyPm.h"
#include "cytypes.h"
#include "cypins.h"
#include "CyLFClk.h"

/*[]*/

