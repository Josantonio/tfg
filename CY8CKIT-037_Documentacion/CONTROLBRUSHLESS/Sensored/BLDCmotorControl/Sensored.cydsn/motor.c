/*==============================================================================================
 *                                  MOTOR.C                                                
 *
 * + Cálculo de la velocidad real de rotación del motor por medio de una interrupción
 * + Contador para el tiempo de muestreo del PID
 * + Función para asignar parámetros y características relevantes del motor
 * + Función para la inicialización de los componentes hardware relacionados con el control del
 *   motor, e inicialización de variables.
 * + Función para la obtención de la velocidad de referencia, establecida por el potenciómetro.
 *
===============================================================================================*/



/*** ARCHIVOS DE CABECERA ***/
#include <project.h>
#include "stdio.h"          
#include "math.h"
#include "motor.h"
#include "communication.h"
#include "protection.h"
#include "PID.h"


/*** DECLARACIÓN DE VARIABLES TIPO ESTRUCTURA, PREVIAMENTE DEFINIDAS ***/
my_struct_param MOTOR;          //Declaración de variable llamada MOTOR, tipo estructura my_struct_param
my_struct_system SYSTEM_STATE;  //Variable llamada SYSTEM_STATE, tipo estructura ya definida


/*** VARIABLES PARA LA MEDIDA DE LA VELOCIDAD ***/
uint16 n_periodos;       //Almacena el número de períodos transcurridos entre dos capturas
bool first_turn;         //Para saber si es la primera vuelta eléctrica
uint16 T0, T1;           //Almacena la primera y la segunda captura.
uint32 delay;            //Almacena el nº de períodos de reloj contados entre una captura y la siguiente
uint32 w_real[2];        //Velocidad real medida, array para almacenar valores pasados y filtrar
uint16 w_ref[9];         //Array para medida de la velocidad de referencia, y su filtrado.

uint32 count, pwmTs;     //Contadores para el tiempo de muestreo del controlador y del filtro de w_real


/*** DECLARACIÓN DE PUNTEROS PARA LA OBTENCIÓN DEL VALOR DE LAS VARIABLES DE LA VELOCIDAD ***/
uint32* pw_real = (uint32*)&w_real[0];            //Puntero hacia dirección que alacena valor de w_real
uint16* pw_ref = (uint16*)&w_ref[0];              //Puntero hacia dirección que alacena valor de w_ref


/*** VARIABLES PARA EL FILTRADO DE LA VELOCIDAD REAL OBTENIDA ***/
uint16 Ts = 200;          //Período de muestreo
uint16 fc = 10;           //Frecuencia de corte
float a;                  //Parámetro "a" del filtro


/*** Para la representación de los datos ***/
float tiempo;
uint8* p_tiempo = (uint8*)&tiempo;


/************************************** SPEED MEASUREMENT *********************************************/

CY_ISR(ISR_SPEED_MEASURE_HANDLER) 
{
    /* Hay que averiguar por qué se ha dado la interrupción */
    
    /* Problema -> si leemos el registro se borra, entonces se va a almacenar           *
     * en una variable intermedia de 8 bits para que, aunque se borre,se pueda comparar *
     * su valor en el segundo if                                                        *
     */    
    uint32 Interrupt_source;
 
    Interrupt_source = Timer_speed_GetInterruptSourceMasked();  
    
 
    /*============================== INTERRUPCIÓN DEBIDA A TC =========================================
     * Si la interrupción es porque ha llegado al final de la cuenta, y además el motor está en marcha
     * de esta forma evitamos que el timer cuente cuando esté el motor parado y la variable se desborde
     ================================================================================================*/
    
     if (Interrupt_source == Timer_speed_INTR_MASK_TC)
    {
        Timer_speed_ClearInterrupt(Timer_speed_INTR_MASK_TC); //Limpiamos interrupción debida a TC
        if (SYSTEM_STATE.run)
        {
            n_periodos++;       //Incremento variable que almacena el número de períodos transcurridos
        }
    }
    
    
    /*========================== INTERRUPCIÓN DEBIDA A CAPTURA ======================================*/
    
    if (Interrupt_source == Timer_speed_INTR_MASK_CC_MATCH){
       
        //Se limpia la interrupción debida a captura
        Timer_speed_ClearInterrupt(Timer_speed_INTR_MASK_CC_MATCH); 
                    
        if (first_turn){                        /* Es la primera vez */
            T0 = Timer_speed_ReadCapture();     //Se guarda la captura, está en n*bits
            n_periodos = 0;                     //Se resetea la cuenta de los períodos
            first_turn = FALSE;                 //La próxima vez ya no será la primera vuelta eléctrica  

        }else if (!first_turn){                              /* No es la primera vez */
            T1 = Timer_speed_ReadCapture();                 //Almacena la captura actual
            delay = (T0+n_periodos*TIMER_SPD_PERIOD-T1);    //Calcula el nº de ciclos ocurridos
            n_periodos = 0;                     //Limpia variable que cuenta los períodos almacenados
            T0 = T1;                                        //Actualiza valor de T0          
        }    
    }  
}



/*************************** CONTADOR PARA EL MUESTREO PID y FILTRO ***********************************/
CY_ISR(ISR_TC_HANDLER)                              //La interrupción se produce cada período del PWM 
{ 
    pwmTs++;                                        //Aumenta el contador para el filtro         
    if(SYSTEM_STATE.run){                           //Si el motor está en marcha
        count++;                                    //Aumenta contador para Tn dle PID        
    }

    if (pwmTs >= Ts/51)                              //Se aplica filtro paso-baja                         
    {
        if (delay !=0 ){                            //Siempre y cuando el motor esté en marcha 
        w_real[0] = a*w_real[1] + (1-a)*(60*CLCK_SPD_FREQ)/(MOTOR.NPolePairs*delay); //Filtro discreto
        w_real[1] = w_real[0];                      //Actualización valores pasados
        
        }
        pwmTs = 0;                                  //El contador se pone a 0 para la próxima vez
    }
    tiempo += 51.2e-6;                              //Cuenta el tiempo transcurrido
    
    PWM_Driver_ReadStatusRegister();                 //Lee registro para limpiar la interrupción
}



/*************** FUNCIÓN PARA ASIGNAR PARÁMETROS CARACTERÍSTICOS DEL MOTOR Y ESPECIFICACIONES ****************/
void asign_param_motor(uint8 PolePairs, uint16 Kv, uint16 MaxSpeed, uint16 MinSpeed, uint8 VDD, bool Dir, uint16 MaxCurr)
{
   MOTOR.NPolePairs = PolePairs;                    // Número de pares de polos 
   MOTOR.Kv = Kv;                                   //Constante eléctrica 
   MOTOR.maxSpdRpm = MaxSpeed;                      // Máx velocidad, en rpm
   MOTOR.minSpdRpm = MinSpeed;                      // Mínima velocidad 
   MOTOR.VDD = VDD;                                 //Tensión de alimentación del inversor
   MOTOR.Dir = Dir;                                 //Sentido de giro
   MOTOR.MaxCurr = MaxCurr;                         //Máxima corriente permitida, en mA
}



/******************************** HARDWARE INITIALIZATION *********************************************/
void INIT_HW_MOTOR (void)
{
    PWM_Driver_Start();                             //  PWM del driver
    Timer_speed_Start();                            //  Timer para medir la velocidad real
    ADC_SAR_Start();                                /*  Inicializa ADC para medidas de velocidad 
                                                        de referencia y tensión de alimentación */
    
    /* Motor parado: PWM y salidas desactivadas*/
    Control_Reg_Write(0x00);                        //Está en hexadecimal 0000 0000, 2 bytes
    
    /* Cabecera de la interrupción para la medida de la velocidad */
    isr_speed_StartEx(ISR_SPEED_MEASURE_HANDLER);
    /* Cabecera de la interrupción de terminal count para el PWM */
    isr_Tc_StartEx(ISR_TC_HANDLER);                 
    
    SYSTEM_STATE.run = FALSE;                       //Motor parado al inicio.
    SYSTEM_STATE.error = FALSE;                     //Sin errores
    
    first_turn = TRUE;                              //Primera vuelta eléctrica
    
    a = exp(-2*pi*fc*Ts*1e-6);                      //Parámetro del filtro IIR pra w_real
    //a = 0.0;                                        //Sin filtro
}

/************************** RESETEAR VARIABLES ********************************************/
void Reset_Variables (void)
{
   /* RESET DE VARIABLES */
   first_turn = TRUE;       //Variable que contabiliza la primera vuelta eléctrica 
   w_real[0] = 0;           //Velocidad real de rotación
   w_real[1] = 0;
   delay = 0;               //Tiempo en completar una vuelta eléctrica
   
   /* Velocidad "set point" medida por el potenciómetro */
   int lenght = sizeof(w_ref)/sizeof(w_ref[0]);
   for (int k=0;k<lenght;k++){
        w_ref[k] = 0;
   } 
   PID_result[1] = PID_result[0] = 0;

    
}


/************ OBTENER VELOCIDAD DE REFERENCIA *************************/
void get_wref (void)
{
    /* Parámetros y variables del filtro de media móvil */
    uint32 suma = 0;                            //Variable para almacenar la suma de las muestras
    uint8 M = 3;                               //Número de muestras

    
    //Actualizamos valores pasados para el filtro
    for (int j=M-1; j>0; j--)
    {
        w_ref[j] = w_ref[j-1];
    }
    
    //Tomamos nuevo valor del potenciómetro y convertimos a RPM
    w_ref[0] = (ADC_SAR_GetResult16(0)-A)*(MOTOR.maxSpdRpm-MOTOR.minSpdRpm)/(B-A) + MOTOR.minSpdRpm;
    
    /*** Limitamos por seguridad ***/
    if(w_ref[0] > MOTOR.maxSpdRpm){ w_ref[0] = MOTOR.maxSpdRpm;}
    if(w_ref[0] < MOTOR.minSpdRpm){ w_ref[0] = MOTOR.minSpdRpm;}
    
    // Filtro FIR, promediado 
     for (int j=0; j<M; j++)
    {
        suma += w_ref[j];
    }
    //Actualizamos nuevo valor
    w_ref[0] = suma/M;
    

    /* El filtro aplicado introduce un retardo en la medición */
}