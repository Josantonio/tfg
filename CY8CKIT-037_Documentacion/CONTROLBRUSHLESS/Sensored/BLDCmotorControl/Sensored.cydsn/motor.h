/*==============================================================================================
 *                                  MOTOR.H                                                 
 *
 * Definición de variables tipo estructura para lamacenar parámetros especificaciones del motor 
 * definición de variables usadas para el cálculo de la velocidad del motor y definición de     
 * funciones útiles para inicializar al hardware y asignar los parámetros.
 *
===============================================================================================*/



#pragma once                        //Indica al precompilador que sólo lo incluya una vez
#include <cytypes.h>                //Introduce los tipos de variables  


/**** Definición de variable tipo booleana ****/
typedef unsigned char bool;
#define FALSE 0
#define TRUE (!FALSE)


/**** Constantes para la medida de la velocidad ****/
#define TIMER_SPD_PERIOD    ((uint16)(60000))               //Período del bloque timer_speed
#define CLCK_SPD_FREQ       ((uint32)(100000))              //Frecuencia del bloque de reloj, 100kHz


/**** Constantes para la conversión ADC ****/
#define RESOLUCION (uint16)(pow(2,11)-1)                    //Resolucion en bits, 2047 bits, son 11 bits y no 12 por ser signed
#define A          (uint16)(330/(2*330+10e3)*RESOLUCION)
#define B          (uint16)((330+10e3)/(2*330+10e3)*RESOLUCION)



/**** Frecuencia eléctrica máxima y mínima, en Hertzios ****/
#define F_MAX ((uint16)(MOTOR.maxSpdRpm*MOTOR.NPolePairs/60))
#define F_MIN ((uint16)(MOTOR.minSpdRpm*MOTOR.NPolePairs/60))
#define pi ((float)(3.141592653))



/**** Declaración de estructura que contiene las especificaciones técnicas del motor ****/
typedef struct
{ 
   uint8   NPolePairs;      //Número de pares de polos
   uint16  Kv;              //Constante eléctrica, w = Ke*V
   uint16  maxSpdRpm;       //Máxima velocidad, en rpm
   uint16  minSpdRpm;       //Mínima velocidad 
   uint8   VDD;             //Tensión de alimentación del driver
   bool    Dir;             //Sentido de giro del motor, 0 horario, 1 anti-horario
   uint16  MaxCurr;         //Máxima corriente permitida
    
}my_struct_param;



/**** Variable para almacenar el estado en el que se encuentra el sistema ****/
typedef struct
{
    bool run;                                //Motor en marcha o parado
    bool error;                              //Estado de error  
    
}my_struct_system;



/**** Declaración de variables externas ****/
extern my_struct_param MOTOR;              //Variable llamada MOTOR, tipo estructura my_struct_param
extern my_struct_system SYSTEM_STATE;      //Variable llamada SYSTEM_STATE, tipo estructura ya definida
extern uint32 w_real[2];                   //Velocidad de rotación real medida
extern uint16 w_ref[9];                    //Velocidad de referencia leída en el potenciómetro
extern uint32 count, pwmTs;                //Variables para llevar la cuenta del nº períodos del PWM transcurridos

extern uint32* pw_real;                    //Puntero hacia dirección que alacena valor de w_real
extern uint16* pw_ref;                     //Puntero hacia dirección que alacena valor de w_ref
extern uint8* p_a;

extern uint16 Ts;                          //Período de muestreo, Ts en us
extern uint16 fc;                          //Frecuencia de corte del filtro paso baja (Hz)

extern uint8* p_tiempo;                    //Puntero hacia la variable 'tiempo'



/**** Declaración de funciones ****/
                                           //Asignar parámetros característicos del motor
void asign_param_motor(uint8 PolePairs, uint16 Ke, uint16 MaxSpeed, uint16 MinSpeed, uint8 VDD, bool Dir, uint16 MaxCurr);  
void INIT_HW_MOTOR (void);                 //Inicializadión de los componentes hardware
void get_wref (void);                      //Lectura del potenciómetro y obtención de w_ref
void Reset_Variables (void);               //Reset de algunas variables