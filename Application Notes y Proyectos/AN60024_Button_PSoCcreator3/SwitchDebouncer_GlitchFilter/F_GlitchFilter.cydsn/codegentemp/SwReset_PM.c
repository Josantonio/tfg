/*******************************************************************************
* File Name: SwReset_PM.c
* Version 1.80
*
* Description:
*  This file contains the setup, control, and status commands to support 
*  the component operation in the low power mode. 
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "SwReset.h"

/* Check for removal by optimization */
#if !defined(SwReset_Sync_ctrl_reg__REMOVED)

static SwReset_BACKUP_STRUCT  SwReset_backup = {0u};

    
/*******************************************************************************
* Function Name: SwReset_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void SwReset_SaveConfig(void) 
{
    SwReset_backup.controlState = SwReset_Control;
}


/*******************************************************************************
* Function Name: SwReset_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*
*******************************************************************************/
void SwReset_RestoreConfig(void) 
{
     SwReset_Control = SwReset_backup.controlState;
}


/*******************************************************************************
* Function Name: SwReset_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void SwReset_Sleep(void) 
{
    SwReset_SaveConfig();
}


/*******************************************************************************
* Function Name: SwReset_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component after waking up from the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void SwReset_Wakeup(void)  
{
    SwReset_RestoreConfig();
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
