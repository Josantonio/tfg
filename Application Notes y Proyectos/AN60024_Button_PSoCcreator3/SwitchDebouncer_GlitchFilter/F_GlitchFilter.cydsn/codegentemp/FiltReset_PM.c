/*******************************************************************************
* File Name: FiltReset_PM.c
* Version 1.80
*
* Description:
*  This file contains the setup, control, and status commands to support 
*  the component operation in the low power mode. 
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "FiltReset.h"

/* Check for removal by optimization */
#if !defined(FiltReset_Sync_ctrl_reg__REMOVED)

static FiltReset_BACKUP_STRUCT  FiltReset_backup = {0u};

    
/*******************************************************************************
* Function Name: FiltReset_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void FiltReset_SaveConfig(void) 
{
    FiltReset_backup.controlState = FiltReset_Control;
}


/*******************************************************************************
* Function Name: FiltReset_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*
*******************************************************************************/
void FiltReset_RestoreConfig(void) 
{
     FiltReset_Control = FiltReset_backup.controlState;
}


/*******************************************************************************
* Function Name: FiltReset_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void FiltReset_Sleep(void) 
{
    FiltReset_SaveConfig();
}


/*******************************************************************************
* Function Name: FiltReset_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component after waking up from the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void FiltReset_Wakeup(void)  
{
    FiltReset_RestoreConfig();
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
