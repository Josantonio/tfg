/*******************************************************************************
* File Name: SwClk.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_SwClk_H)
#define CY_CLOCK_SwClk_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void SwClk_Start(void) ;
void SwClk_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void SwClk_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void SwClk_StandbyPower(uint8 state) ;
void SwClk_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 SwClk_GetDividerRegister(void) ;
void SwClk_SetModeRegister(uint8 modeBitMask) ;
void SwClk_ClearModeRegister(uint8 modeBitMask) ;
uint8 SwClk_GetModeRegister(void) ;
void SwClk_SetSourceRegister(uint8 clkSource) ;
uint8 SwClk_GetSourceRegister(void) ;
#if defined(SwClk__CFG3)
void SwClk_SetPhaseRegister(uint8 clkPhase) ;
uint8 SwClk_GetPhaseRegister(void) ;
#endif /* defined(SwClk__CFG3) */

#define SwClk_Enable()                       SwClk_Start()
#define SwClk_Disable()                      SwClk_Stop()
#define SwClk_SetDivider(clkDivider)         SwClk_SetDividerRegister(clkDivider, 1u)
#define SwClk_SetDividerValue(clkDivider)    SwClk_SetDividerRegister((clkDivider) - 1u, 1u)
#define SwClk_SetMode(clkMode)               SwClk_SetModeRegister(clkMode)
#define SwClk_SetSource(clkSource)           SwClk_SetSourceRegister(clkSource)
#if defined(SwClk__CFG3)
#define SwClk_SetPhase(clkPhase)             SwClk_SetPhaseRegister(clkPhase)
#define SwClk_SetPhaseValue(clkPhase)        SwClk_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(SwClk__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define SwClk_CLKEN              (* (reg8 *) SwClk__PM_ACT_CFG)
#define SwClk_CLKEN_PTR          ((reg8 *) SwClk__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define SwClk_CLKSTBY            (* (reg8 *) SwClk__PM_STBY_CFG)
#define SwClk_CLKSTBY_PTR        ((reg8 *) SwClk__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define SwClk_DIV_LSB            (* (reg8 *) SwClk__CFG0)
#define SwClk_DIV_LSB_PTR        ((reg8 *) SwClk__CFG0)
#define SwClk_DIV_PTR            ((reg16 *) SwClk__CFG0)

/* Clock MSB divider configuration register. */
#define SwClk_DIV_MSB            (* (reg8 *) SwClk__CFG1)
#define SwClk_DIV_MSB_PTR        ((reg8 *) SwClk__CFG1)

/* Mode and source configuration register */
#define SwClk_MOD_SRC            (* (reg8 *) SwClk__CFG2)
#define SwClk_MOD_SRC_PTR        ((reg8 *) SwClk__CFG2)

#if defined(SwClk__CFG3)
/* Analog clock phase configuration register */
#define SwClk_PHASE              (* (reg8 *) SwClk__CFG3)
#define SwClk_PHASE_PTR          ((reg8 *) SwClk__CFG3)
#endif /* defined(SwClk__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define SwClk_CLKEN_MASK         SwClk__PM_ACT_MSK
#define SwClk_CLKSTBY_MASK       SwClk__PM_STBY_MSK

/* CFG2 field masks */
#define SwClk_SRC_SEL_MSK        SwClk__CFG2_SRC_SEL_MASK
#define SwClk_MODE_MASK          (~(SwClk_SRC_SEL_MSK))

#if defined(SwClk__CFG3)
/* CFG3 phase mask */
#define SwClk_PHASE_MASK         SwClk__CFG3_PHASE_DLY_MASK
#endif /* defined(SwClk__CFG3) */

#endif /* CY_CLOCK_SwClk_H */


/* [] END OF FILE */
