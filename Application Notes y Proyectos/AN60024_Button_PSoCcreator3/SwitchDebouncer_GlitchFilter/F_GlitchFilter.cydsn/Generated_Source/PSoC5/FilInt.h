/*******************************************************************************
* File Name: FilInt.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_FilInt_H)
#define CY_ISR_FilInt_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void FilInt_Start(void);
void FilInt_StartEx(cyisraddress address);
void FilInt_Stop(void);

CY_ISR_PROTO(FilInt_Interrupt);

void FilInt_SetVector(cyisraddress address);
cyisraddress FilInt_GetVector(void);

void FilInt_SetPriority(uint8 priority);
uint8 FilInt_GetPriority(void);

void FilInt_Enable(void);
uint8 FilInt_GetState(void);
void FilInt_Disable(void);

void FilInt_SetPending(void);
void FilInt_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the FilInt ISR. */
#define FilInt_INTC_VECTOR            ((reg32 *) FilInt__INTC_VECT)

/* Address of the FilInt ISR priority. */
#define FilInt_INTC_PRIOR             ((reg8 *) FilInt__INTC_PRIOR_REG)

/* Priority of the FilInt interrupt. */
#define FilInt_INTC_PRIOR_NUMBER      FilInt__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable FilInt interrupt. */
#define FilInt_INTC_SET_EN            ((reg32 *) FilInt__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the FilInt interrupt. */
#define FilInt_INTC_CLR_EN            ((reg32 *) FilInt__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the FilInt interrupt state to pending. */
#define FilInt_INTC_SET_PD            ((reg32 *) FilInt__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the FilInt interrupt. */
#define FilInt_INTC_CLR_PD            ((reg32 *) FilInt__INTC_CLR_PD_REG)


#endif /* CY_ISR_FilInt_H */


/* [] END OF FILE */
