/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


uint8 count; /* of transition of input pin 'SW' */
uint8 Filter_count;

CY_ISR(SwInt_ISR){ //Cuando nos llega una interrupción reseteamos los flip flops
    SwReset_Write(1); /* Clear interrupt source */
    count++;
}

CY_ISR(FiltInt_ISR){
    FiltReset_Write(1); /* Clear flip-flops state */
    Filter_count++;
}

int main(void)
{
    uint8 temp, temp2; /* local copy of count variable */
    char string [100]; 
    
    /* Initialization code */
    UART_Start();
    UART_PutString("Switch bounce sensitive design\n");

    SwInt_StartEx(SwInt_ISR);
    SwReset_Write(1); /* source of interrupt (reset) */
    SwInt_ClearPending(); /* in case an interrupt is still pending */
    
    FiltInt_StartEx(FiltInt_ISR);
    FiltReset_Write(1); /* Clear flip-flops state */
    FiltInt_ClearPending();
    
    CyGlobalIntEnable; /* Enable global interrupts. */
    // Cuidado no poner esto antes, si no, es muy posible que empiece en 1 porque activa las
    // interrupciones antes de resetear los flip-flops

    for(;;)
    {
      /* Grab a copy of the shared count variable, and display the copy.
       * this ensures the interrupt handler will not change the count
       * variable while it is being displayed.
       */
        CYGlobalIntDisable;
        temp = count;
        CYGlobalIntEnable;
        
        CYGlobalIntDisable;
        temp2 = Filter_count;
        CYGlobalIntEnable;
        
        sprintf(string, "Count: %d  Filt. count: %d\n",temp, temp2);        
        UART_PutString(string); 

    }
}

/* [] END OF FILE */
