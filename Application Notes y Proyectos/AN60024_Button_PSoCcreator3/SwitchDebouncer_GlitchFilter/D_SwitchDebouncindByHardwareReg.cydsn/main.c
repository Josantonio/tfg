/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


uint8 count; /* of transition of input pin 'SW' */
uint8 filtered_count;

CY_ISR(SwInt_ISR){ //Cuando nos llega una interrupción reseteamos los flip flops
    SwReset_Write(1); /* Clear interrupt source */
    count++;
}

int main(void)
{
    uint8 temp; /* local copy of count variable */

    char string [100]; 
    
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Initialization code */
    UART_Start();
    UART_PutString("Switch bounce sensitive design\n");
    
    SwReset_Write(1); /* source of interrupt (reset) */
    SwInt_ClearPending(); /* in case an interrupt is still pending */
    
    SwInt_StartEx(SwInt_ISR);

    uint8 switches[2] = {0,0}; // [0] = current, [1] = previus
    switches[0] = switches[1] = PinReg_Status; // 0 = pressed, 1 = not pressed
    
    for(;;)
    {
      /* Grab a copy of the shared count variable, and display the copy.
       * this ensures the interrupt handler will not change the count
       * variable while it is being displayed.
       */
        CYGlobalIntDisable;
        temp = count;
        CYGlobalIntEnable;
        
        /* Update the current and previus switch read values */
        switches[1] = switches[0];
        switches[0] = PinReg_Status; //El estado del registro cambia 20 veces por segundo, 
        // este tiempo de muestreo lo establece el reloj
        
        /* Increment counter if a switch transitions either way */
        if(switches[0] != switches[1]){
            filtered_count++;
        }
        
        sprintf(string, "Count: %d\n, Filtered count: %d\n",temp, filtered_count);        
        UART_PutString(string); 

    }
}

/* [] END OF FILE */
