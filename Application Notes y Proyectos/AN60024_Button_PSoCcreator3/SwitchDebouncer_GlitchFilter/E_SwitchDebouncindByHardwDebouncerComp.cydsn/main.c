/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


uint8 count; /* of transition of input pin 'SW' */
uint8 filtered_count;

CY_ISR(SwInt_ISR){ //Cuando nos llega una interrupción reseteamos los flip flops
    SwReset_Write(1); /* Clear interrupt source */
    count++;
}

CY_ISR(FiltInt_ISR){
    /* No need to clear any interrupt source, interrupt component should be configured
     * for RISING_EDGE mode.
     *
     * Read the debouncer status reg just to clear it, no need to check its contents
     * in this application.
     */
    FiltReg_Read(); //Se limpia el registro, en caso de que queramos saber si ha sido 
    //la interrupción debida a flanco de subida o bajada habría que añadir código
    //leyendo el valor del registro 0 -> bajada, 1 -> subida
    
    filtered_count++;
}

int main(void)
{
    uint8 temp, temp2; /* local copy of count variable */
    char string [100]; 

    /* Initialization code */
    UART_Start();
    UART_PutString("Switch bounce sensitive design\n");
    
    SwReset_Write(1); /* source of interrupt (reset) */
    SwReset_Write(0);
    SwInt_ClearPending(); /* in case an interrupt is still pending */
    
    SwInt_StartEx(SwInt_ISR);
    FiltInt_StartEx(FiltInt_ISR);
    FiltInt_ClearPending(); /* in case an interrupt is still pending */

    CyGlobalIntEnable; /* Enable global interrupts. */
    /* Ya no hace falta hacer esto por software, se encarga el debouncer
    uint8 switches[2] = {0,0}; // [0] = current, [1] = previus
    switches[0] = switches[1] = SW_Read(); // 0 = pressed, 1 = not pressed
    */
    
    for(;;)
    {
      /* Grab a copy of the shared count variable, and display the copy.
       * this ensures the interrupt handler will not change the count
       * variable while it is being displayed.
       */
        CYGlobalIntDisable;
        temp = count;
        CyGlobalIntEnable;  /* macro */
        
        /* Grab a copy of the shared filtered count variable, and display the
		* copy. This is so that the interrupt handler won't change the count
        * variable while it's being displayed.
        */
        CyGlobalIntDisable; /* macro */
        temp2 = filtered_count;
        CyGlobalIntEnable;  /* macro */

        /* Tampoco hace falta hacer esto
        switches[1] = switches[0];
        switches[0] = SW_Read();
        

        if(switches[0] != switches[1]){
            filtered_count++;
        }
        */
        sprintf(string, "Count: %d\n, Filtered count: %d\n",temp, temp2);        
        UART_PutString(string); 
    }
}

/* [] END OF FILE */
