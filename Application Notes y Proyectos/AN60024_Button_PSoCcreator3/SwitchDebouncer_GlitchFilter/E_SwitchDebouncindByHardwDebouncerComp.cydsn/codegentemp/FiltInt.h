/*******************************************************************************
* File Name: FiltInt.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_FiltInt_H)
#define CY_ISR_FiltInt_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void FiltInt_Start(void);
void FiltInt_StartEx(cyisraddress address);
void FiltInt_Stop(void);

CY_ISR_PROTO(FiltInt_Interrupt);

void FiltInt_SetVector(cyisraddress address);
cyisraddress FiltInt_GetVector(void);

void FiltInt_SetPriority(uint8 priority);
uint8 FiltInt_GetPriority(void);

void FiltInt_Enable(void);
uint8 FiltInt_GetState(void);
void FiltInt_Disable(void);

void FiltInt_SetPending(void);
void FiltInt_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the FiltInt ISR. */
#define FiltInt_INTC_VECTOR            ((reg32 *) FiltInt__INTC_VECT)

/* Address of the FiltInt ISR priority. */
#define FiltInt_INTC_PRIOR             ((reg8 *) FiltInt__INTC_PRIOR_REG)

/* Priority of the FiltInt interrupt. */
#define FiltInt_INTC_PRIOR_NUMBER      FiltInt__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable FiltInt interrupt. */
#define FiltInt_INTC_SET_EN            ((reg32 *) FiltInt__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the FiltInt interrupt. */
#define FiltInt_INTC_CLR_EN            ((reg32 *) FiltInt__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the FiltInt interrupt state to pending. */
#define FiltInt_INTC_SET_PD            ((reg32 *) FiltInt__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the FiltInt interrupt. */
#define FiltInt_INTC_CLR_PD            ((reg32 *) FiltInt__INTC_CLR_PD_REG)


#endif /* CY_ISR_FiltInt_H */


/* [] END OF FILE */
