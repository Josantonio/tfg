/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


uint8 count; /* of transition of input pin 'SW' */
uint8 filtered_count;

CY_ISR(SwInt_ISR){ //Cuando nos llega una interrupción reseteamos los flip flops
    SwReset_Write(1); /* Clear interrupt source */
    count++;
}

CY_ISR(SampInt_ISR){
    static uint8 init = 0;
    static uint8 switches[2] = {0,0}; // [0] = current, [1] = previus
    uint8 temp; /* local copy of switch state read */
    
    /* Interrupting from clock, no interrupt source to clear */
    temp = SW_Read(); /* Read the pin value */
    
    /* Switch variable initialization should be done only once, the first time the function is called.*/
    if(!init) //Si init es 0 se inicializa
    {
        init = 1; //Para indicar que ya se ha realizado la inicialización
        switches[0] = switches[1] = temp;
    }
    else
    { /* Increment the count if the switch transitions either way */
        switches[1] = switches[0]; //El valor actual se guarda en el pasado
        switches[0] = temp; //Se actualiza al valor actual
        if(switches[0] != switches[1]) //Si ha cambiado de estado se incrementa el contador
        {
            filtered_count++;
        }
        
    }
} /* En of SampInt_ISR */

int main(void)
{
    char string [100]; 
    uint8 temp; /* Local copy of shared count variable */    
    uint8 temp2;

    /* Initialization code */
    UART_Start();
    
    SwInt_StartEx(SwInt_ISR);
    SampInt_StartEx(SampInt_ISR);
    
    SwReset_Write(1); /* source of interrupt (reset) */
    SwReset_Write(0);
    SwInt_ClearPending(); /* in case an interrupt is still pending */
    SampInt_ClearPending(); /* in case an interrupt is still pending */
    
    CyGlobalIntEnable; /* Enable global interrupts. */
      
    for(;;)
    {
      /* Grab a copy of the shared count variable, and display the copy.
       * this ensures the interrupt handler will not change the count
       * variable while it is being displayed.
       */
        CYGlobalIntDisable;
        temp = count;
        CYGlobalIntEnable;
        
        CyGlobalIntDisable; /* macro */
        temp2 = filtered_count;
        CyGlobalIntEnable;  /* macro */

        sprintf(string, "Count: %d\n, Filtered count: %d\n",temp, temp2);        
        UART_PutString(string); 

    }
}

/* [] END OF FILE */
