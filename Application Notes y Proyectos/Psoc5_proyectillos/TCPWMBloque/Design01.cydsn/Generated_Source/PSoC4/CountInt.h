/*******************************************************************************
* File Name: CountInt.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_CountInt_H)
#define CY_ISR_CountInt_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void CountInt_Start(void);
void CountInt_StartEx(cyisraddress address);
void CountInt_Stop(void);

CY_ISR_PROTO(CountInt_Interrupt);

void CountInt_SetVector(cyisraddress address);
cyisraddress CountInt_GetVector(void);

void CountInt_SetPriority(uint8 priority);
uint8 CountInt_GetPriority(void);

void CountInt_Enable(void);
uint8 CountInt_GetState(void);
void CountInt_Disable(void);

void CountInt_SetPending(void);
void CountInt_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the CountInt ISR. */
#define CountInt_INTC_VECTOR            ((reg32 *) CountInt__INTC_VECT)

/* Address of the CountInt ISR priority. */
#define CountInt_INTC_PRIOR             ((reg32 *) CountInt__INTC_PRIOR_REG)

/* Priority of the CountInt interrupt. */
#define CountInt_INTC_PRIOR_NUMBER      CountInt__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable CountInt interrupt. */
#define CountInt_INTC_SET_EN            ((reg32 *) CountInt__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the CountInt interrupt. */
#define CountInt_INTC_CLR_EN            ((reg32 *) CountInt__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the CountInt interrupt state to pending. */
#define CountInt_INTC_SET_PD            ((reg32 *) CountInt__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the CountInt interrupt. */
#define CountInt_INTC_CLR_PD            ((reg32 *) CountInt__INTC_CLR_PD_REG)



#endif /* CY_ISR_CountInt_H */


/* [] END OF FILE */
