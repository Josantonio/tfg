/*******************************************************************************
* File Name: CounterInt.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_CounterInt_H)
#define CY_ISR_CounterInt_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void CounterInt_Start(void);
void CounterInt_StartEx(cyisraddress address);
void CounterInt_Stop(void);

CY_ISR_PROTO(CounterInt_Interrupt);

void CounterInt_SetVector(cyisraddress address);
cyisraddress CounterInt_GetVector(void);

void CounterInt_SetPriority(uint8 priority);
uint8 CounterInt_GetPriority(void);

void CounterInt_Enable(void);
uint8 CounterInt_GetState(void);
void CounterInt_Disable(void);

void CounterInt_SetPending(void);
void CounterInt_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the CounterInt ISR. */
#define CounterInt_INTC_VECTOR            ((reg32 *) CounterInt__INTC_VECT)

/* Address of the CounterInt ISR priority. */
#define CounterInt_INTC_PRIOR             ((reg32 *) CounterInt__INTC_PRIOR_REG)

/* Priority of the CounterInt interrupt. */
#define CounterInt_INTC_PRIOR_NUMBER      CounterInt__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable CounterInt interrupt. */
#define CounterInt_INTC_SET_EN            ((reg32 *) CounterInt__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the CounterInt interrupt. */
#define CounterInt_INTC_CLR_EN            ((reg32 *) CounterInt__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the CounterInt interrupt state to pending. */
#define CounterInt_INTC_SET_PD            ((reg32 *) CounterInt__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the CounterInt interrupt. */
#define CounterInt_INTC_CLR_PD            ((reg32 *) CounterInt__INTC_CLR_PD_REG)



#endif /* CY_ISR_CounterInt_H */


/* [] END OF FILE */
