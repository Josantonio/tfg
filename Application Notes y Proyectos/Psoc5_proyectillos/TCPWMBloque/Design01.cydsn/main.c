/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/*
* Prueba del bloque TCPWM como contador, cuenta las veces que se pulsa un botón, en este caso
* se pulsa 2 veces y a la tercera cambia el estado de un LED
*/

#include "project.h"

CY_ISR(Count_Int_Handler){
    Pin_Green_Write( ~ Pin_Green_Read() ); //Escribimos la salida negada, conmutamos el valor del LED
    Counter_ClearInterrupt(Counter_INTR_MASK_TC); //Limpiamos que se haya producido la interrupción
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    Counter_Start(); //Inicializamos el bloque contador
    CountInt_StartEx(Count_Int_Handler); //Inicializamos la interrupción
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
