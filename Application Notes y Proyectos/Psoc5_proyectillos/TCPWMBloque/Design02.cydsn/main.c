/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
/*
* Ejemplo de código, un contador cuenta el número de ciclos de reloj que transcurren
* entre una vez que se pulsa un botón y la siguiente vez, mediante una interrupción
* cada vez que se produce una captura del recuento un LED cambia su estado y el número de ciclos
* se muestran por comunicación UART
*/

#include "project.h"
#include <stdio.h>            //definitions for all standard io functions
//Librería necesaria para el uso de la función sprintf

#include "count.h" //incluimos la función manual creada


CY_ISR(IntVel){ //Definimos la función cabecera de la interrupción, y lo que se realiza cuando ocurre
    Pin_LED_Write( ~ Pin_LED_Read()); //Se cambia el estado del LED
    Pin_Int_Write( ~ Pin_Int_Read()); //Para medir cuando se activa la interrupción
    
    countshow(); //Lllamamos a la función que muestra la cuenta actual, definida en los otros archivos
    
    CounterSpd_ClearInterrupt(CounterSpd_INTR_MASK_TC); //Limpiamos que se ha producido la interrupción
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    //Inicializamos los bloques y la interrupción
    CounterSpd_Start();
    isr_speed_StartEx(IntVel);
    clock1_Start();
    Clock25K_Start();
    PWM_Start();
    UART_Start();
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    
    

    for(;;)
    {
        /* Place your application code here. */
    }
}
/* [] END OF FILE */
