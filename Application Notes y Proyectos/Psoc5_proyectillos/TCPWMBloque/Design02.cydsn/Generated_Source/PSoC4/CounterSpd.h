/*******************************************************************************
* File Name: CounterSpd.h
* Version 2.10
*
* Description:
*  This file provides constants and parameter values for the CounterSpd
*  component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_TCPWM_CounterSpd_H)
#define CY_TCPWM_CounterSpd_H


#include "CyLib.h"
#include "cytypes.h"
#include "cyfitter.h"


/*******************************************************************************
* Internal Type defines
*******************************************************************************/

/* Structure to save state before go to sleep */
typedef struct
{
    uint8  enableState;
} CounterSpd_BACKUP_STRUCT;


/*******************************************************************************
* Variables
*******************************************************************************/
extern uint8  CounterSpd_initVar;


/***************************************
*   Conditional Compilation Parameters
****************************************/

#define CounterSpd_CY_TCPWM_V2                    (CYIPBLOCK_m0s8tcpwm_VERSION == 2u)
#define CounterSpd_CY_TCPWM_4000                  (CY_PSOC4_4000)

/* TCPWM Configuration */
#define CounterSpd_CONFIG                         (1lu)

/* Quad Mode */
/* Parameters */
#define CounterSpd_QUAD_ENCODING_MODES            (0lu)
#define CounterSpd_QUAD_AUTO_START                (1lu)

/* Signal modes */
#define CounterSpd_QUAD_INDEX_SIGNAL_MODE         (0lu)
#define CounterSpd_QUAD_PHIA_SIGNAL_MODE          (3lu)
#define CounterSpd_QUAD_PHIB_SIGNAL_MODE          (3lu)
#define CounterSpd_QUAD_STOP_SIGNAL_MODE          (0lu)

/* Signal present */
#define CounterSpd_QUAD_INDEX_SIGNAL_PRESENT      (0lu)
#define CounterSpd_QUAD_STOP_SIGNAL_PRESENT       (0lu)

/* Interrupt Mask */
#define CounterSpd_QUAD_INTERRUPT_MASK            (1lu)

/* Timer/Counter Mode */
/* Parameters */
#define CounterSpd_TC_RUN_MODE                    (0lu)
#define CounterSpd_TC_COUNTER_MODE                (1lu)
#define CounterSpd_TC_COMP_CAP_MODE               (2lu)
#define CounterSpd_TC_PRESCALER                   (0lu)

/* Signal modes */
#define CounterSpd_TC_RELOAD_SIGNAL_MODE          (0lu)
#define CounterSpd_TC_COUNT_SIGNAL_MODE           (0lu)
#define CounterSpd_TC_START_SIGNAL_MODE           (0lu)
#define CounterSpd_TC_STOP_SIGNAL_MODE            (0lu)
#define CounterSpd_TC_CAPTURE_SIGNAL_MODE         (1lu)

/* Signal present */
#define CounterSpd_TC_RELOAD_SIGNAL_PRESENT       (0lu)
#define CounterSpd_TC_COUNT_SIGNAL_PRESENT        (1lu)
#define CounterSpd_TC_START_SIGNAL_PRESENT        (0lu)
#define CounterSpd_TC_STOP_SIGNAL_PRESENT         (0lu)
#define CounterSpd_TC_CAPTURE_SIGNAL_PRESENT      (1lu)

/* Interrupt Mask */
#define CounterSpd_TC_INTERRUPT_MASK              (2lu)

/* PWM Mode */
/* Parameters */
#define CounterSpd_PWM_KILL_EVENT                 (0lu)
#define CounterSpd_PWM_STOP_EVENT                 (0lu)
#define CounterSpd_PWM_MODE                       (4lu)
#define CounterSpd_PWM_OUT_N_INVERT               (0lu)
#define CounterSpd_PWM_OUT_INVERT                 (0lu)
#define CounterSpd_PWM_ALIGN                      (0lu)
#define CounterSpd_PWM_RUN_MODE                   (0lu)
#define CounterSpd_PWM_DEAD_TIME_CYCLE            (0lu)
#define CounterSpd_PWM_PRESCALER                  (0lu)

/* Signal modes */
#define CounterSpd_PWM_RELOAD_SIGNAL_MODE         (0lu)
#define CounterSpd_PWM_COUNT_SIGNAL_MODE          (3lu)
#define CounterSpd_PWM_START_SIGNAL_MODE          (0lu)
#define CounterSpd_PWM_STOP_SIGNAL_MODE           (0lu)
#define CounterSpd_PWM_SWITCH_SIGNAL_MODE         (0lu)

/* Signal present */
#define CounterSpd_PWM_RELOAD_SIGNAL_PRESENT      (0lu)
#define CounterSpd_PWM_COUNT_SIGNAL_PRESENT       (0lu)
#define CounterSpd_PWM_START_SIGNAL_PRESENT       (0lu)
#define CounterSpd_PWM_STOP_SIGNAL_PRESENT        (0lu)
#define CounterSpd_PWM_SWITCH_SIGNAL_PRESENT      (0lu)

/* Interrupt Mask */
#define CounterSpd_PWM_INTERRUPT_MASK             (1lu)


/***************************************
*    Initial Parameter Constants
***************************************/

/* Timer/Counter Mode */
#define CounterSpd_TC_PERIOD_VALUE                (65535lu)
#define CounterSpd_TC_COMPARE_VALUE               (65535lu)
#define CounterSpd_TC_COMPARE_BUF_VALUE           (65535lu)
#define CounterSpd_TC_COMPARE_SWAP                (0lu)

/* PWM Mode */
#define CounterSpd_PWM_PERIOD_VALUE               (65535lu)
#define CounterSpd_PWM_PERIOD_BUF_VALUE           (65535lu)
#define CounterSpd_PWM_PERIOD_SWAP                (0lu)
#define CounterSpd_PWM_COMPARE_VALUE              (65535lu)
#define CounterSpd_PWM_COMPARE_BUF_VALUE          (65535lu)
#define CounterSpd_PWM_COMPARE_SWAP               (0lu)


/***************************************
*    Enumerated Types and Parameters
***************************************/

#define CounterSpd__LEFT 0
#define CounterSpd__RIGHT 1
#define CounterSpd__CENTER 2
#define CounterSpd__ASYMMETRIC 3

#define CounterSpd__X1 0
#define CounterSpd__X2 1
#define CounterSpd__X4 2

#define CounterSpd__PWM 4
#define CounterSpd__PWM_DT 5
#define CounterSpd__PWM_PR 6

#define CounterSpd__INVERSE 1
#define CounterSpd__DIRECT 0

#define CounterSpd__CAPTURE 2
#define CounterSpd__COMPARE 0

#define CounterSpd__TRIG_LEVEL 3
#define CounterSpd__TRIG_RISING 0
#define CounterSpd__TRIG_FALLING 1
#define CounterSpd__TRIG_BOTH 2

#define CounterSpd__INTR_MASK_TC 1
#define CounterSpd__INTR_MASK_CC_MATCH 2
#define CounterSpd__INTR_MASK_NONE 0
#define CounterSpd__INTR_MASK_TC_CC 3

#define CounterSpd__UNCONFIG 8
#define CounterSpd__TIMER 1
#define CounterSpd__QUAD 3
#define CounterSpd__PWM_SEL 7

#define CounterSpd__COUNT_UP 0
#define CounterSpd__COUNT_DOWN 1
#define CounterSpd__COUNT_UPDOWN0 2
#define CounterSpd__COUNT_UPDOWN1 3


/* Prescaler */
#define CounterSpd_PRESCALE_DIVBY1                ((uint32)(0u << CounterSpd_PRESCALER_SHIFT))
#define CounterSpd_PRESCALE_DIVBY2                ((uint32)(1u << CounterSpd_PRESCALER_SHIFT))
#define CounterSpd_PRESCALE_DIVBY4                ((uint32)(2u << CounterSpd_PRESCALER_SHIFT))
#define CounterSpd_PRESCALE_DIVBY8                ((uint32)(3u << CounterSpd_PRESCALER_SHIFT))
#define CounterSpd_PRESCALE_DIVBY16               ((uint32)(4u << CounterSpd_PRESCALER_SHIFT))
#define CounterSpd_PRESCALE_DIVBY32               ((uint32)(5u << CounterSpd_PRESCALER_SHIFT))
#define CounterSpd_PRESCALE_DIVBY64               ((uint32)(6u << CounterSpd_PRESCALER_SHIFT))
#define CounterSpd_PRESCALE_DIVBY128              ((uint32)(7u << CounterSpd_PRESCALER_SHIFT))

/* TCPWM set modes */
#define CounterSpd_MODE_TIMER_COMPARE             ((uint32)(CounterSpd__COMPARE         <<  \
                                                                  CounterSpd_MODE_SHIFT))
#define CounterSpd_MODE_TIMER_CAPTURE             ((uint32)(CounterSpd__CAPTURE         <<  \
                                                                  CounterSpd_MODE_SHIFT))
#define CounterSpd_MODE_QUAD                      ((uint32)(CounterSpd__QUAD            <<  \
                                                                  CounterSpd_MODE_SHIFT))
#define CounterSpd_MODE_PWM                       ((uint32)(CounterSpd__PWM             <<  \
                                                                  CounterSpd_MODE_SHIFT))
#define CounterSpd_MODE_PWM_DT                    ((uint32)(CounterSpd__PWM_DT          <<  \
                                                                  CounterSpd_MODE_SHIFT))
#define CounterSpd_MODE_PWM_PR                    ((uint32)(CounterSpd__PWM_PR          <<  \
                                                                  CounterSpd_MODE_SHIFT))

/* Quad Modes */
#define CounterSpd_MODE_X1                        ((uint32)(CounterSpd__X1              <<  \
                                                                  CounterSpd_QUAD_MODE_SHIFT))
#define CounterSpd_MODE_X2                        ((uint32)(CounterSpd__X2              <<  \
                                                                  CounterSpd_QUAD_MODE_SHIFT))
#define CounterSpd_MODE_X4                        ((uint32)(CounterSpd__X4              <<  \
                                                                  CounterSpd_QUAD_MODE_SHIFT))

/* Counter modes */
#define CounterSpd_COUNT_UP                       ((uint32)(CounterSpd__COUNT_UP        <<  \
                                                                  CounterSpd_UPDOWN_SHIFT))
#define CounterSpd_COUNT_DOWN                     ((uint32)(CounterSpd__COUNT_DOWN      <<  \
                                                                  CounterSpd_UPDOWN_SHIFT))
#define CounterSpd_COUNT_UPDOWN0                  ((uint32)(CounterSpd__COUNT_UPDOWN0   <<  \
                                                                  CounterSpd_UPDOWN_SHIFT))
#define CounterSpd_COUNT_UPDOWN1                  ((uint32)(CounterSpd__COUNT_UPDOWN1   <<  \
                                                                  CounterSpd_UPDOWN_SHIFT))

/* PWM output invert */
#define CounterSpd_INVERT_LINE                    ((uint32)(CounterSpd__INVERSE         <<  \
                                                                  CounterSpd_INV_OUT_SHIFT))
#define CounterSpd_INVERT_LINE_N                  ((uint32)(CounterSpd__INVERSE         <<  \
                                                                  CounterSpd_INV_COMPL_OUT_SHIFT))

/* Trigger modes */
#define CounterSpd_TRIG_RISING                    ((uint32)CounterSpd__TRIG_RISING)
#define CounterSpd_TRIG_FALLING                   ((uint32)CounterSpd__TRIG_FALLING)
#define CounterSpd_TRIG_BOTH                      ((uint32)CounterSpd__TRIG_BOTH)
#define CounterSpd_TRIG_LEVEL                     ((uint32)CounterSpd__TRIG_LEVEL)

/* Interrupt mask */
#define CounterSpd_INTR_MASK_TC                   ((uint32)CounterSpd__INTR_MASK_TC)
#define CounterSpd_INTR_MASK_CC_MATCH             ((uint32)CounterSpd__INTR_MASK_CC_MATCH)

/* PWM Output Controls */
#define CounterSpd_CC_MATCH_SET                   (0x00u)
#define CounterSpd_CC_MATCH_CLEAR                 (0x01u)
#define CounterSpd_CC_MATCH_INVERT                (0x02u)
#define CounterSpd_CC_MATCH_NO_CHANGE             (0x03u)
#define CounterSpd_OVERLOW_SET                    (0x00u)
#define CounterSpd_OVERLOW_CLEAR                  (0x04u)
#define CounterSpd_OVERLOW_INVERT                 (0x08u)
#define CounterSpd_OVERLOW_NO_CHANGE              (0x0Cu)
#define CounterSpd_UNDERFLOW_SET                  (0x00u)
#define CounterSpd_UNDERFLOW_CLEAR                (0x10u)
#define CounterSpd_UNDERFLOW_INVERT               (0x20u)
#define CounterSpd_UNDERFLOW_NO_CHANGE            (0x30u)

/* PWM Align */
#define CounterSpd_PWM_MODE_LEFT                  (CounterSpd_CC_MATCH_CLEAR        |   \
                                                         CounterSpd_OVERLOW_SET           |   \
                                                         CounterSpd_UNDERFLOW_NO_CHANGE)
#define CounterSpd_PWM_MODE_RIGHT                 (CounterSpd_CC_MATCH_SET          |   \
                                                         CounterSpd_OVERLOW_NO_CHANGE     |   \
                                                         CounterSpd_UNDERFLOW_CLEAR)
#define CounterSpd_PWM_MODE_ASYM                  (CounterSpd_CC_MATCH_INVERT       |   \
                                                         CounterSpd_OVERLOW_SET           |   \
                                                         CounterSpd_UNDERFLOW_CLEAR)

#if (CounterSpd_CY_TCPWM_V2)
    #if(CounterSpd_CY_TCPWM_4000)
        #define CounterSpd_PWM_MODE_CENTER                (CounterSpd_CC_MATCH_INVERT       |   \
                                                                 CounterSpd_OVERLOW_NO_CHANGE     |   \
                                                                 CounterSpd_UNDERFLOW_CLEAR)
    #else
        #define CounterSpd_PWM_MODE_CENTER                (CounterSpd_CC_MATCH_INVERT       |   \
                                                                 CounterSpd_OVERLOW_SET           |   \
                                                                 CounterSpd_UNDERFLOW_CLEAR)
    #endif /* (CounterSpd_CY_TCPWM_4000) */
#else
    #define CounterSpd_PWM_MODE_CENTER                (CounterSpd_CC_MATCH_INVERT       |   \
                                                             CounterSpd_OVERLOW_NO_CHANGE     |   \
                                                             CounterSpd_UNDERFLOW_CLEAR)
#endif /* (CounterSpd_CY_TCPWM_NEW) */

/* Command operations without condition */
#define CounterSpd_CMD_CAPTURE                    (0u)
#define CounterSpd_CMD_RELOAD                     (8u)
#define CounterSpd_CMD_STOP                       (16u)
#define CounterSpd_CMD_START                      (24u)

/* Status */
#define CounterSpd_STATUS_DOWN                    (1u)
#define CounterSpd_STATUS_RUNNING                 (2u)


/***************************************
*        Function Prototypes
****************************************/

void   CounterSpd_Init(void);
void   CounterSpd_Enable(void);
void   CounterSpd_Start(void);
void   CounterSpd_Stop(void);

void   CounterSpd_SetMode(uint32 mode);
void   CounterSpd_SetCounterMode(uint32 counterMode);
void   CounterSpd_SetPWMMode(uint32 modeMask);
void   CounterSpd_SetQDMode(uint32 qdMode);

void   CounterSpd_SetPrescaler(uint32 prescaler);
void   CounterSpd_TriggerCommand(uint32 mask, uint32 command);
void   CounterSpd_SetOneShot(uint32 oneShotEnable);
uint32 CounterSpd_ReadStatus(void);

void   CounterSpd_SetPWMSyncKill(uint32 syncKillEnable);
void   CounterSpd_SetPWMStopOnKill(uint32 stopOnKillEnable);
void   CounterSpd_SetPWMDeadTime(uint32 deadTime);
void   CounterSpd_SetPWMInvert(uint32 mask);

void   CounterSpd_SetInterruptMode(uint32 interruptMask);
uint32 CounterSpd_GetInterruptSourceMasked(void);
uint32 CounterSpd_GetInterruptSource(void);
void   CounterSpd_ClearInterrupt(uint32 interruptMask);
void   CounterSpd_SetInterrupt(uint32 interruptMask);

void   CounterSpd_WriteCounter(uint32 count);
uint32 CounterSpd_ReadCounter(void);

uint32 CounterSpd_ReadCapture(void);
uint32 CounterSpd_ReadCaptureBuf(void);

void   CounterSpd_WritePeriod(uint32 period);
uint32 CounterSpd_ReadPeriod(void);
void   CounterSpd_WritePeriodBuf(uint32 periodBuf);
uint32 CounterSpd_ReadPeriodBuf(void);

void   CounterSpd_WriteCompare(uint32 compare);
uint32 CounterSpd_ReadCompare(void);
void   CounterSpd_WriteCompareBuf(uint32 compareBuf);
uint32 CounterSpd_ReadCompareBuf(void);

void   CounterSpd_SetPeriodSwap(uint32 swapEnable);
void   CounterSpd_SetCompareSwap(uint32 swapEnable);

void   CounterSpd_SetCaptureMode(uint32 triggerMode);
void   CounterSpd_SetReloadMode(uint32 triggerMode);
void   CounterSpd_SetStartMode(uint32 triggerMode);
void   CounterSpd_SetStopMode(uint32 triggerMode);
void   CounterSpd_SetCountMode(uint32 triggerMode);

void   CounterSpd_SaveConfig(void);
void   CounterSpd_RestoreConfig(void);
void   CounterSpd_Sleep(void);
void   CounterSpd_Wakeup(void);


/***************************************
*             Registers
***************************************/

#define CounterSpd_BLOCK_CONTROL_REG              (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TCPWM_CTRL )
#define CounterSpd_BLOCK_CONTROL_PTR              ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TCPWM_CTRL )
#define CounterSpd_COMMAND_REG                    (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TCPWM_CMD )
#define CounterSpd_COMMAND_PTR                    ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TCPWM_CMD )
#define CounterSpd_INTRRUPT_CAUSE_REG             (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TCPWM_INTR_CAUSE )
#define CounterSpd_INTRRUPT_CAUSE_PTR             ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TCPWM_INTR_CAUSE )
#define CounterSpd_CONTROL_REG                    (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__CTRL )
#define CounterSpd_CONTROL_PTR                    ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__CTRL )
#define CounterSpd_STATUS_REG                     (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__STATUS )
#define CounterSpd_STATUS_PTR                     ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__STATUS )
#define CounterSpd_COUNTER_REG                    (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__COUNTER )
#define CounterSpd_COUNTER_PTR                    ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__COUNTER )
#define CounterSpd_COMP_CAP_REG                   (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__CC )
#define CounterSpd_COMP_CAP_PTR                   ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__CC )
#define CounterSpd_COMP_CAP_BUF_REG               (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__CC_BUFF )
#define CounterSpd_COMP_CAP_BUF_PTR               ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__CC_BUFF )
#define CounterSpd_PERIOD_REG                     (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__PERIOD )
#define CounterSpd_PERIOD_PTR                     ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__PERIOD )
#define CounterSpd_PERIOD_BUF_REG                 (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__PERIOD_BUFF )
#define CounterSpd_PERIOD_BUF_PTR                 ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__PERIOD_BUFF )
#define CounterSpd_TRIG_CONTROL0_REG              (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TR_CTRL0 )
#define CounterSpd_TRIG_CONTROL0_PTR              ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TR_CTRL0 )
#define CounterSpd_TRIG_CONTROL1_REG              (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TR_CTRL1 )
#define CounterSpd_TRIG_CONTROL1_PTR              ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TR_CTRL1 )
#define CounterSpd_TRIG_CONTROL2_REG              (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TR_CTRL2 )
#define CounterSpd_TRIG_CONTROL2_PTR              ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__TR_CTRL2 )
#define CounterSpd_INTERRUPT_REQ_REG              (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__INTR )
#define CounterSpd_INTERRUPT_REQ_PTR              ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__INTR )
#define CounterSpd_INTERRUPT_SET_REG              (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__INTR_SET )
#define CounterSpd_INTERRUPT_SET_PTR              ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__INTR_SET )
#define CounterSpd_INTERRUPT_MASK_REG             (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__INTR_MASK )
#define CounterSpd_INTERRUPT_MASK_PTR             ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__INTR_MASK )
#define CounterSpd_INTERRUPT_MASKED_REG           (*(reg32 *) CounterSpd_cy_m0s8_tcpwm_1__INTR_MASKED )
#define CounterSpd_INTERRUPT_MASKED_PTR           ( (reg32 *) CounterSpd_cy_m0s8_tcpwm_1__INTR_MASKED )


/***************************************
*       Registers Constants
***************************************/

/* Mask */
#define CounterSpd_MASK                           ((uint32)CounterSpd_cy_m0s8_tcpwm_1__TCPWM_CTRL_MASK)

/* Shift constants for control register */
#define CounterSpd_RELOAD_CC_SHIFT                (0u)
#define CounterSpd_RELOAD_PERIOD_SHIFT            (1u)
#define CounterSpd_PWM_SYNC_KILL_SHIFT            (2u)
#define CounterSpd_PWM_STOP_KILL_SHIFT            (3u)
#define CounterSpd_PRESCALER_SHIFT                (8u)
#define CounterSpd_UPDOWN_SHIFT                   (16u)
#define CounterSpd_ONESHOT_SHIFT                  (18u)
#define CounterSpd_QUAD_MODE_SHIFT                (20u)
#define CounterSpd_INV_OUT_SHIFT                  (20u)
#define CounterSpd_INV_COMPL_OUT_SHIFT            (21u)
#define CounterSpd_MODE_SHIFT                     (24u)

/* Mask constants for control register */
#define CounterSpd_RELOAD_CC_MASK                 ((uint32)(CounterSpd_1BIT_MASK        <<  \
                                                                            CounterSpd_RELOAD_CC_SHIFT))
#define CounterSpd_RELOAD_PERIOD_MASK             ((uint32)(CounterSpd_1BIT_MASK        <<  \
                                                                            CounterSpd_RELOAD_PERIOD_SHIFT))
#define CounterSpd_PWM_SYNC_KILL_MASK             ((uint32)(CounterSpd_1BIT_MASK        <<  \
                                                                            CounterSpd_PWM_SYNC_KILL_SHIFT))
#define CounterSpd_PWM_STOP_KILL_MASK             ((uint32)(CounterSpd_1BIT_MASK        <<  \
                                                                            CounterSpd_PWM_STOP_KILL_SHIFT))
#define CounterSpd_PRESCALER_MASK                 ((uint32)(CounterSpd_8BIT_MASK        <<  \
                                                                            CounterSpd_PRESCALER_SHIFT))
#define CounterSpd_UPDOWN_MASK                    ((uint32)(CounterSpd_2BIT_MASK        <<  \
                                                                            CounterSpd_UPDOWN_SHIFT))
#define CounterSpd_ONESHOT_MASK                   ((uint32)(CounterSpd_1BIT_MASK        <<  \
                                                                            CounterSpd_ONESHOT_SHIFT))
#define CounterSpd_QUAD_MODE_MASK                 ((uint32)(CounterSpd_3BIT_MASK        <<  \
                                                                            CounterSpd_QUAD_MODE_SHIFT))
#define CounterSpd_INV_OUT_MASK                   ((uint32)(CounterSpd_2BIT_MASK        <<  \
                                                                            CounterSpd_INV_OUT_SHIFT))
#define CounterSpd_MODE_MASK                      ((uint32)(CounterSpd_3BIT_MASK        <<  \
                                                                            CounterSpd_MODE_SHIFT))

/* Shift constants for trigger control register 1 */
#define CounterSpd_CAPTURE_SHIFT                  (0u)
#define CounterSpd_COUNT_SHIFT                    (2u)
#define CounterSpd_RELOAD_SHIFT                   (4u)
#define CounterSpd_STOP_SHIFT                     (6u)
#define CounterSpd_START_SHIFT                    (8u)

/* Mask constants for trigger control register 1 */
#define CounterSpd_CAPTURE_MASK                   ((uint32)(CounterSpd_2BIT_MASK        <<  \
                                                                  CounterSpd_CAPTURE_SHIFT))
#define CounterSpd_COUNT_MASK                     ((uint32)(CounterSpd_2BIT_MASK        <<  \
                                                                  CounterSpd_COUNT_SHIFT))
#define CounterSpd_RELOAD_MASK                    ((uint32)(CounterSpd_2BIT_MASK        <<  \
                                                                  CounterSpd_RELOAD_SHIFT))
#define CounterSpd_STOP_MASK                      ((uint32)(CounterSpd_2BIT_MASK        <<  \
                                                                  CounterSpd_STOP_SHIFT))
#define CounterSpd_START_MASK                     ((uint32)(CounterSpd_2BIT_MASK        <<  \
                                                                  CounterSpd_START_SHIFT))

/* MASK */
#define CounterSpd_1BIT_MASK                      ((uint32)0x01u)
#define CounterSpd_2BIT_MASK                      ((uint32)0x03u)
#define CounterSpd_3BIT_MASK                      ((uint32)0x07u)
#define CounterSpd_6BIT_MASK                      ((uint32)0x3Fu)
#define CounterSpd_8BIT_MASK                      ((uint32)0xFFu)
#define CounterSpd_16BIT_MASK                     ((uint32)0xFFFFu)

/* Shift constant for status register */
#define CounterSpd_RUNNING_STATUS_SHIFT           (30u)


/***************************************
*    Initial Constants
***************************************/

#define CounterSpd_CTRL_QUAD_BASE_CONFIG                                                          \
        (((uint32)(CounterSpd_QUAD_ENCODING_MODES     << CounterSpd_QUAD_MODE_SHIFT))       |\
         ((uint32)(CounterSpd_CONFIG                  << CounterSpd_MODE_SHIFT)))

#define CounterSpd_CTRL_PWM_BASE_CONFIG                                                           \
        (((uint32)(CounterSpd_PWM_STOP_EVENT          << CounterSpd_PWM_STOP_KILL_SHIFT))   |\
         ((uint32)(CounterSpd_PWM_OUT_INVERT          << CounterSpd_INV_OUT_SHIFT))         |\
         ((uint32)(CounterSpd_PWM_OUT_N_INVERT        << CounterSpd_INV_COMPL_OUT_SHIFT))   |\
         ((uint32)(CounterSpd_PWM_MODE                << CounterSpd_MODE_SHIFT)))

#define CounterSpd_CTRL_PWM_RUN_MODE                                                              \
            ((uint32)(CounterSpd_PWM_RUN_MODE         << CounterSpd_ONESHOT_SHIFT))
            
#define CounterSpd_CTRL_PWM_ALIGN                                                                 \
            ((uint32)(CounterSpd_PWM_ALIGN            << CounterSpd_UPDOWN_SHIFT))

#define CounterSpd_CTRL_PWM_KILL_EVENT                                                            \
             ((uint32)(CounterSpd_PWM_KILL_EVENT      << CounterSpd_PWM_SYNC_KILL_SHIFT))

#define CounterSpd_CTRL_PWM_DEAD_TIME_CYCLE                                                       \
            ((uint32)(CounterSpd_PWM_DEAD_TIME_CYCLE  << CounterSpd_PRESCALER_SHIFT))

#define CounterSpd_CTRL_PWM_PRESCALER                                                             \
            ((uint32)(CounterSpd_PWM_PRESCALER        << CounterSpd_PRESCALER_SHIFT))

#define CounterSpd_CTRL_TIMER_BASE_CONFIG                                                         \
        (((uint32)(CounterSpd_TC_PRESCALER            << CounterSpd_PRESCALER_SHIFT))       |\
         ((uint32)(CounterSpd_TC_COUNTER_MODE         << CounterSpd_UPDOWN_SHIFT))          |\
         ((uint32)(CounterSpd_TC_RUN_MODE             << CounterSpd_ONESHOT_SHIFT))         |\
         ((uint32)(CounterSpd_TC_COMP_CAP_MODE        << CounterSpd_MODE_SHIFT)))
        
#define CounterSpd_QUAD_SIGNALS_MODES                                                             \
        (((uint32)(CounterSpd_QUAD_PHIA_SIGNAL_MODE   << CounterSpd_COUNT_SHIFT))           |\
         ((uint32)(CounterSpd_QUAD_INDEX_SIGNAL_MODE  << CounterSpd_RELOAD_SHIFT))          |\
         ((uint32)(CounterSpd_QUAD_STOP_SIGNAL_MODE   << CounterSpd_STOP_SHIFT))            |\
         ((uint32)(CounterSpd_QUAD_PHIB_SIGNAL_MODE   << CounterSpd_START_SHIFT)))

#define CounterSpd_PWM_SIGNALS_MODES                                                              \
        (((uint32)(CounterSpd_PWM_SWITCH_SIGNAL_MODE  << CounterSpd_CAPTURE_SHIFT))         |\
         ((uint32)(CounterSpd_PWM_COUNT_SIGNAL_MODE   << CounterSpd_COUNT_SHIFT))           |\
         ((uint32)(CounterSpd_PWM_RELOAD_SIGNAL_MODE  << CounterSpd_RELOAD_SHIFT))          |\
         ((uint32)(CounterSpd_PWM_STOP_SIGNAL_MODE    << CounterSpd_STOP_SHIFT))            |\
         ((uint32)(CounterSpd_PWM_START_SIGNAL_MODE   << CounterSpd_START_SHIFT)))

#define CounterSpd_TIMER_SIGNALS_MODES                                                            \
        (((uint32)(CounterSpd_TC_CAPTURE_SIGNAL_MODE  << CounterSpd_CAPTURE_SHIFT))         |\
         ((uint32)(CounterSpd_TC_COUNT_SIGNAL_MODE    << CounterSpd_COUNT_SHIFT))           |\
         ((uint32)(CounterSpd_TC_RELOAD_SIGNAL_MODE   << CounterSpd_RELOAD_SHIFT))          |\
         ((uint32)(CounterSpd_TC_STOP_SIGNAL_MODE     << CounterSpd_STOP_SHIFT))            |\
         ((uint32)(CounterSpd_TC_START_SIGNAL_MODE    << CounterSpd_START_SHIFT)))
        
#define CounterSpd_TIMER_UPDOWN_CNT_USED                                                          \
                ((CounterSpd__COUNT_UPDOWN0 == CounterSpd_TC_COUNTER_MODE)                  ||\
                 (CounterSpd__COUNT_UPDOWN1 == CounterSpd_TC_COUNTER_MODE))

#define CounterSpd_PWM_UPDOWN_CNT_USED                                                            \
                ((CounterSpd__CENTER == CounterSpd_PWM_ALIGN)                               ||\
                 (CounterSpd__ASYMMETRIC == CounterSpd_PWM_ALIGN))               
        
#define CounterSpd_PWM_PR_INIT_VALUE              (1u)
#define CounterSpd_QUAD_PERIOD_INIT_VALUE         (0x8000u)



#endif /* End CY_TCPWM_CounterSpd_H */

/* [] END OF FILE */
