/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include "count.h" 

#include "project.h"
#include <stdio.h>

//#ifndef _COUNT_C_
//#define _COUNT_C_
uint16 speedCur = 3000;
uint16 preCntCaptur = 30000;
uint16 preSpeedCur = 0;

void countshow(){
    
   int CntCaptur = 0; //Creamos una variable de 32 bits unsigned integer para almacenar la cuenta
    CntCaptur = CounterSpd_ReadCapture(); //Leemos la cuenta
    speedCur = preCntCaptur - CntCaptur;
    
    if(speedCur > 5000) //Demasiado lento
        speedCur = 5000;
    speedCur = (preSpeedCur >> 2) + (preSpeedCur >> 1) + (speedCur >> 2); //Filtro
    
    preCntCaptur = CntCaptur;
    preSpeedCur = speedCur;
    
    sprintf(string,"%d\n" ,speedCur); //Necesitamos convertir el número a un acadena de caracteres
        // %d indica que es base 10, almacenamos la conversión en "string"
    
    UART_UartPutString("\r Numero de ciclos: "); //Monitor serial, muestra el número de ciclos de reloj contados
    UART_UartPutString( string );//mostramos la cadena de carácteres convertida
    UART_UartPutString("\n\r"); //Salto de línea y retroceso

}