/*******************************************************************************
* File Name: CounterSpd_PM.c
* Version 2.10
*
* Description:
*  This file contains the setup, control, and status commands to support
*  the component operations in the low power mode.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "CounterSpd.h"

static CounterSpd_BACKUP_STRUCT CounterSpd_backup;


/*******************************************************************************
* Function Name: CounterSpd_SaveConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to save here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void CounterSpd_SaveConfig(void)
{

}


/*******************************************************************************
* Function Name: CounterSpd_Sleep
********************************************************************************
*
* Summary:
*  Stops the component operation and saves the user configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void CounterSpd_Sleep(void)
{
    if(0u != (CounterSpd_BLOCK_CONTROL_REG & CounterSpd_MASK))
    {
        CounterSpd_backup.enableState = 1u;
    }
    else
    {
        CounterSpd_backup.enableState = 0u;
    }

    CounterSpd_Stop();
    CounterSpd_SaveConfig();
}


/*******************************************************************************
* Function Name: CounterSpd_RestoreConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to restore here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void CounterSpd_RestoreConfig(void)
{

}


/*******************************************************************************
* Function Name: CounterSpd_Wakeup
********************************************************************************
*
* Summary:
*  Restores the user configuration and restores the enable state.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void CounterSpd_Wakeup(void)
{
    CounterSpd_RestoreConfig();

    if(0u != CounterSpd_backup.enableState)
    {
        CounterSpd_Enable();
    }
}


/* [] END OF FILE */
