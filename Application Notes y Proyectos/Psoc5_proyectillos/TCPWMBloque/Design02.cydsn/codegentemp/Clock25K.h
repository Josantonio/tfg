/*******************************************************************************
* File Name: Clock25K.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_Clock25K_H)
#define CY_CLOCK_Clock25K_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void Clock25K_StartEx(uint32 alignClkDiv);
#define Clock25K_Start() \
    Clock25K_StartEx(Clock25K__PA_DIV_ID)

#else

void Clock25K_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void Clock25K_Stop(void);

void Clock25K_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 Clock25K_GetDividerRegister(void);
uint8  Clock25K_GetFractionalDividerRegister(void);

#define Clock25K_Enable()                         Clock25K_Start()
#define Clock25K_Disable()                        Clock25K_Stop()
#define Clock25K_SetDividerRegister(clkDivider, reset)  \
    Clock25K_SetFractionalDividerRegister((clkDivider), 0u)
#define Clock25K_SetDivider(clkDivider)           Clock25K_SetDividerRegister((clkDivider), 1u)
#define Clock25K_SetDividerValue(clkDivider)      Clock25K_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define Clock25K_DIV_ID     Clock25K__DIV_ID

#define Clock25K_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define Clock25K_CTRL_REG   (*(reg32 *)Clock25K__CTRL_REGISTER)
#define Clock25K_DIV_REG    (*(reg32 *)Clock25K__DIV_REGISTER)

#define Clock25K_CMD_DIV_SHIFT          (0u)
#define Clock25K_CMD_PA_DIV_SHIFT       (8u)
#define Clock25K_CMD_DISABLE_SHIFT      (30u)
#define Clock25K_CMD_ENABLE_SHIFT       (31u)

#define Clock25K_CMD_DISABLE_MASK       ((uint32)((uint32)1u << Clock25K_CMD_DISABLE_SHIFT))
#define Clock25K_CMD_ENABLE_MASK        ((uint32)((uint32)1u << Clock25K_CMD_ENABLE_SHIFT))

#define Clock25K_DIV_FRAC_MASK  (0x000000F8u)
#define Clock25K_DIV_FRAC_SHIFT (3u)
#define Clock25K_DIV_INT_MASK   (0xFFFFFF00u)
#define Clock25K_DIV_INT_SHIFT  (8u)

#else 

#define Clock25K_DIV_REG        (*(reg32 *)Clock25K__REGISTER)
#define Clock25K_ENABLE_REG     Clock25K_DIV_REG
#define Clock25K_DIV_FRAC_MASK  Clock25K__FRAC_MASK
#define Clock25K_DIV_FRAC_SHIFT (16u)
#define Clock25K_DIV_INT_MASK   Clock25K__DIVIDER_MASK
#define Clock25K_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_Clock25K_H) */

/* [] END OF FILE */
