// ======================================================================
// Comparador_ZC.v generated from TopDesign.cysch
// 02/18/2020 at 21:18
// This file is auto generated. ANY EDITS YOU MAKE MAY BE LOST WHEN THIS FILE IS REGENERATED!!!
// ======================================================================

/* -- WARNING: The following section of defines are deprecated and will be removed in a future release -- */
`define CYDEV_CHIP_DIE_LEOPARD 1
`define CYDEV_CHIP_REV_LEOPARD_PRODUCTION 3
`define CYDEV_CHIP_REV_LEOPARD_ES3 3
`define CYDEV_CHIP_REV_LEOPARD_ES2 1
`define CYDEV_CHIP_REV_LEOPARD_ES1 0
`define CYDEV_CHIP_DIE_PSOC5LP 2
`define CYDEV_CHIP_REV_PSOC5LP_PRODUCTION 0
`define CYDEV_CHIP_REV_PSOC5LP_ES0 0
`define CYDEV_CHIP_DIE_PSOC5TM 3
`define CYDEV_CHIP_REV_PSOC5TM_PRODUCTION 1
`define CYDEV_CHIP_REV_PSOC5TM_ES1 1
`define CYDEV_CHIP_REV_PSOC5TM_ES0 0
`define CYDEV_CHIP_DIE_TMA4 4
`define CYDEV_CHIP_REV_TMA4_PRODUCTION 17
`define CYDEV_CHIP_REV_TMA4_ES 17
`define CYDEV_CHIP_REV_TMA4_ES2 33
`define CYDEV_CHIP_DIE_PSOC4A 5
`define CYDEV_CHIP_REV_PSOC4A_PRODUCTION 17
`define CYDEV_CHIP_REV_PSOC4A_ES0 17
`define CYDEV_CHIP_DIE_PSOC6ABLE2 6
`define CYDEV_CHIP_REV_PSOC6ABLE2_ES 17
`define CYDEV_CHIP_REV_PSOC6ABLE2_PRODUCTION 33
`define CYDEV_CHIP_REV_PSOC6ABLE2_NO_UDB 33
`define CYDEV_CHIP_DIE_VOLANS 7
`define CYDEV_CHIP_REV_VOLANS_PRODUCTION 0
`define CYDEV_CHIP_DIE_BERRYPECKER 8
`define CYDEV_CHIP_REV_BERRYPECKER_PRODUCTION 0
`define CYDEV_CHIP_DIE_CRANE 9
`define CYDEV_CHIP_REV_CRANE_PRODUCTION 0
`define CYDEV_CHIP_DIE_FM3 10
`define CYDEV_CHIP_REV_FM3_PRODUCTION 0
`define CYDEV_CHIP_DIE_FM4 11
`define CYDEV_CHIP_REV_FM4_PRODUCTION 0
`define CYDEV_CHIP_DIE_EXPECT 2
`define CYDEV_CHIP_REV_EXPECT 0
`define CYDEV_CHIP_DIE_ACTUAL 2
/* -- WARNING: The previous section of defines are deprecated and will be removed in a future release -- */
`define CYDEV_CHIP_FAMILY_PSOC3 1
`define CYDEV_CHIP_FAMILY_PSOC4 2
`define CYDEV_CHIP_FAMILY_PSOC5 3
`define CYDEV_CHIP_FAMILY_PSOC6 4
`define CYDEV_CHIP_FAMILY_FM0P 5
`define CYDEV_CHIP_FAMILY_FM3 6
`define CYDEV_CHIP_FAMILY_FM4 7
`define CYDEV_CHIP_FAMILY_UNKNOWN 0
`define CYDEV_CHIP_MEMBER_UNKNOWN 0
`define CYDEV_CHIP_MEMBER_3A 1
`define CYDEV_CHIP_REVISION_3A_PRODUCTION 3
`define CYDEV_CHIP_REVISION_3A_ES3 3
`define CYDEV_CHIP_REVISION_3A_ES2 1
`define CYDEV_CHIP_REVISION_3A_ES1 0
`define CYDEV_CHIP_MEMBER_5B 2
`define CYDEV_CHIP_REVISION_5B_PRODUCTION 0
`define CYDEV_CHIP_REVISION_5B_ES0 0
`define CYDEV_CHIP_MEMBER_5A 3
`define CYDEV_CHIP_REVISION_5A_PRODUCTION 1
`define CYDEV_CHIP_REVISION_5A_ES1 1
`define CYDEV_CHIP_REVISION_5A_ES0 0
`define CYDEV_CHIP_MEMBER_4G 4
`define CYDEV_CHIP_REVISION_4G_PRODUCTION 17
`define CYDEV_CHIP_REVISION_4G_ES 17
`define CYDEV_CHIP_REVISION_4G_ES2 33
`define CYDEV_CHIP_MEMBER_4U 5
`define CYDEV_CHIP_REVISION_4U_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4E 6
`define CYDEV_CHIP_REVISION_4E_PRODUCTION 0
`define CYDEV_CHIP_REVISION_4E_CCG2_NO_USBPD 0
`define CYDEV_CHIP_MEMBER_4O 7
`define CYDEV_CHIP_REVISION_4O_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4R 8
`define CYDEV_CHIP_REVISION_4R_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4T 9
`define CYDEV_CHIP_REVISION_4T_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4N 10
`define CYDEV_CHIP_REVISION_4N_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4S 11
`define CYDEV_CHIP_REVISION_4S_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4Q 12
`define CYDEV_CHIP_REVISION_4Q_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4D 13
`define CYDEV_CHIP_REVISION_4D_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4J 14
`define CYDEV_CHIP_REVISION_4J_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4K 15
`define CYDEV_CHIP_REVISION_4K_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4V 16
`define CYDEV_CHIP_REVISION_4V_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4H 17
`define CYDEV_CHIP_REVISION_4H_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4A 18
`define CYDEV_CHIP_REVISION_4A_PRODUCTION 17
`define CYDEV_CHIP_REVISION_4A_ES0 17
`define CYDEV_CHIP_MEMBER_4F 19
`define CYDEV_CHIP_REVISION_4F_PRODUCTION 0
`define CYDEV_CHIP_REVISION_4F_PRODUCTION_256K 0
`define CYDEV_CHIP_REVISION_4F_PRODUCTION_256DMA 0
`define CYDEV_CHIP_MEMBER_4P 20
`define CYDEV_CHIP_REVISION_4P_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4M 21
`define CYDEV_CHIP_REVISION_4M_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4L 22
`define CYDEV_CHIP_REVISION_4L_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_4I 23
`define CYDEV_CHIP_REVISION_4I_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_6A 24
`define CYDEV_CHIP_REVISION_6A_ES 17
`define CYDEV_CHIP_REVISION_6A_PRODUCTION 33
`define CYDEV_CHIP_REVISION_6A_NO_UDB 33
`define CYDEV_CHIP_MEMBER_PDL_FM0P_TYPE1 25
`define CYDEV_CHIP_REVISION_PDL_FM0P_TYPE1_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_PDL_FM0P_TYPE2 26
`define CYDEV_CHIP_REVISION_PDL_FM0P_TYPE2_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_PDL_FM0P_TYPE3 27
`define CYDEV_CHIP_REVISION_PDL_FM0P_TYPE3_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_FM3 28
`define CYDEV_CHIP_REVISION_FM3_PRODUCTION 0
`define CYDEV_CHIP_MEMBER_FM4 29
`define CYDEV_CHIP_REVISION_FM4_PRODUCTION 0
`define CYDEV_CHIP_FAMILY_USED 3
`define CYDEV_CHIP_MEMBER_USED 2
`define CYDEV_CHIP_REVISION_USED 0
// Component: ZeroTerminal
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\ZeroTerminal"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\ZeroTerminal\ZeroTerminal.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\ZeroTerminal"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\ZeroTerminal\ZeroTerminal.v"
`endif

// Component: cy_analog_virtualmux_v1_0
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_analog_virtualmux_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_analog_virtualmux_v1_0\cy_analog_virtualmux_v1_0.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_analog_virtualmux_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_analog_virtualmux_v1_0\cy_analog_virtualmux_v1_0.v"
`endif

// PGA_v2_0(Gain=2, Power=1, VddaValue=5, Vref_Input=0, CY_API_CALLBACK_HEADER_INCLUDE=#include "cyapicallbacks.h", CY_COMMENT=, CY_COMPONENT_NAME=PGA_v2_0, CY_CONFIG_TITLE=PGA, CY_CONST_CONFIG=true, CY_CONTROL_FILE=<:default:>, CY_DATASHEET_FILE=<:default:>, CY_FITTER_NAME=PGA, CY_INSTANCE_SHORT_NAME=PGA, CY_MAJOR_VERSION=2, CY_MINOR_VERSION=0, CY_PDL_DRIVER_NAME=, CY_PDL_DRIVER_REQ_VERSION=, CY_PDL_DRIVER_SUBGROUP=, CY_PDL_DRIVER_VARIANT=, CY_REMOVE=false, CY_SUPPRESS_API_GEN=false, CY_VERSION=PSoC Creator  4.2, INSTANCE_NAME=PGA, )
module PGA_v2_0_0 (
    Vin,
    Vref,
    Vout);
    inout       Vin;
    electrical  Vin;
    inout       Vref;
    electrical  Vref;
    inout       Vout;
    electrical  Vout;


    electrical  Net_75;
          wire  Net_41;
          wire  Net_40;
    electrical  Net_17;
          wire  Net_39;
          wire  Net_38;
          wire  Net_37;

    cy_psoc3_scblock_v1_0 SC (
        .vin(Vin),
        .vref(Net_17),
        .vout(Vout),
        .modout_sync(Net_41),
        .aclk(Net_37),
        .clk_udb(Net_38),
        .dyn_cntl(Net_39),
        .bst_clk(Net_40));

    ZeroTerminal ZeroTerminal_1 (
        .z(Net_37));

    ZeroTerminal ZeroTerminal_2 (
        .z(Net_38));

    ZeroTerminal ZeroTerminal_3 (
        .z(Net_39));

    ZeroTerminal ZeroTerminal_4 (
        .z(Net_40));

	// cy_analog_virtualmux_1 (cy_analog_virtualmux_v1_0)
	cy_connect_v1_0 cy_analog_virtualmux_1_connect(Net_17, Net_75);
	defparam cy_analog_virtualmux_1_connect.sig_width = 1;

    cy_analog_noconnect_v1_0 cy_analog_noconnect_2 (
        .noconnect(Net_75));



endmodule

// Component: demux_v1_10
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\demux_v1_10"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\demux_v1_10\demux_v1_10.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\demux_v1_10"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\demux_v1_10\demux_v1_10.v"
`endif

// Component: cy_virtualmux_v1_0
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_virtualmux_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_virtualmux_v1_0\cy_virtualmux_v1_0.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_virtualmux_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_virtualmux_v1_0\cy_virtualmux_v1_0.v"
`endif

// VDAC8_v1_90(Data_Source=0, Initial_Value=100, Strobe_Mode=1, VDAC_Range=0, VDAC_Speed=2, Voltage=400, CY_API_CALLBACK_HEADER_INCLUDE=#include "cyapicallbacks.h", CY_COMMENT=, CY_COMPONENT_NAME=VDAC8_v1_90, CY_CONFIG_TITLE=VDAC8, CY_CONST_CONFIG=true, CY_CONTROL_FILE=<:default:>, CY_DATASHEET_FILE=<:default:>, CY_FITTER_NAME=WaveDAC8:VDAC8, CY_INSTANCE_SHORT_NAME=VDAC8, CY_MAJOR_VERSION=1, CY_MINOR_VERSION=90, CY_PDL_DRIVER_NAME=, CY_PDL_DRIVER_REQ_VERSION=, CY_PDL_DRIVER_SUBGROUP=, CY_PDL_DRIVER_VARIANT=, CY_REMOVE=false, CY_SUPPRESS_API_GEN=false, CY_VERSION=PSoC Creator  4.2, INSTANCE_NAME=WaveDAC8_VDAC8, )
module VDAC8_v1_90_1 (
    strobe,
    data,
    vOut);
    input       strobe;
    input      [7:0] data;
    inout       vOut;
    electrical  vOut;

    parameter Data_Source = 0;
    parameter Initial_Value = 100;
    parameter Strobe_Mode = 1;

    electrical  Net_77;
          wire  Net_83;
          wire  Net_82;
          wire  Net_81;

    cy_psoc3_vidac8_v1_0 viDAC8 (
        .reset(Net_83),
        .idir(Net_81),
        .data(data[7:0]),
        .strobe(strobe),
        .vout(vOut),
        .iout(Net_77),
        .ioff(Net_82),
        .strobe_udb(strobe));
    defparam viDAC8.is_all_if_any = 0;
    defparam viDAC8.reg_data = 0;

    ZeroTerminal ZeroTerminal_1 (
        .z(Net_81));

    ZeroTerminal ZeroTerminal_2 (
        .z(Net_82));

    ZeroTerminal ZeroTerminal_3 (
        .z(Net_83));

    cy_analog_noconnect_v1_0 cy_analog_noconnect_1 (
        .noconnect(Net_77));



endmodule

// Component: or_v1_0
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\or_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\or_v1_0\or_v1_0.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\or_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\or_v1_0\or_v1_0.v"
`endif

// Component: cydff_v1_30
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cydff_v1_30"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cydff_v1_30\cydff_v1_30.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cydff_v1_30"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cydff_v1_30\cydff_v1_30.v"
`endif

// WaveDAC8_v2_10(Clock_SRC=1, DAC_Range=0, DacModeReplString=VDAC8, Sample_Clock_Freq=100000, Wave1_Amplitude=1, Wave1_Data=128u,128u,129u,130u,131u,131u,132u,133u,134u,135u,135u,136u,137u,138u,139u,139u,140u,141u,142u,143u,143u,144u,145u,146u,146u,147u,148u,149u,150u,150u,151u,152u,153u,153u,154u,155u,156u,157u,157u,158u,159u,160u,160u,161u,162u,163u,163u,164u,165u,166u,166u,167u,168u,169u,169u,170u,171u,172u,172u,173u,174u,175u,175u,176u,177u,178u,178u,179u,180u,180u,181u,182u,183u,183u,184u,185u,185u,186u,187u,188u,188u,189u,190u,190u,191u,192u,192u,193u,194u,194u,195u,196u,196u,197u,198u,198u,199u,200u,200u,201u,202u,202u,203u,203u,204u,205u,205u,206u,207u,207u,208u,208u,209u,210u,210u,211u,211u,212u,213u,213u,214u,214u,215u,215u,216u,217u,217u,218u,218u,219u,219u,220u,220u,221u,221u,222u,222u,223u,223u,224u,224u,225u,225u,226u,226u,227u,227u,228u,228u,229u,229u,230u,230u,231u,231u,232u,232u,232u,233u,233u,234u,234u,234u,235u,235u,236u,236u,236u,237u,237u,238u,238u,238u,239u,239u,239u,240u,240u,240u,241u,241u,241u,242u,242u,242u,243u,243u,243u,244u,244u,244u,244u,245u,245u,245u,246u,246u,246u,246u,247u,247u,247u,247u,248u,248u,248u,248u,248u,249u,249u,249u,249u,249u,249u,250u,250u,250u,250u,250u,250u,251u,251u,251u,251u,251u,251u,251u,251u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,252u,251u,251u,251u,251u,251u,251u,251u,251u,251u,250u,250u,250u,250u,250u,250u,249u,249u,249u,249u,249u,248u,248u,248u,248u,248u,247u,247u,247u,247u,246u,246u,246u,246u,245u,245u,245u,245u,244u,244u,244u,243u,243u,243u,243u,242u,242u,242u,241u,241u,241u,240u,240u,240u,239u,239u,239u,238u,238u,237u,237u,237u,236u,236u,235u,235u,235u,234u,234u,233u,233u,233u,232u,232u,231u,231u,230u,230u,229u,229u,229u,228u,228u,227u,227u,226u,226u,225u,225u,224u,224u,223u,223u,222u,222u,221u,221u,220u,220u,219u,218u,218u,217u,217u,216u,216u,215u,215u,214u,213u,213u,212u,212u,211u,210u,210u,209u,209u,208u,207u,207u,206u,206u,205u,204u,204u,203u,203u,202u,201u,201u,200u,199u,199u,198u,197u,197u,196u,195u,195u,194u,193u,193u,192u,191u,191u,190u,189u,189u,188u,187u,186u,186u,185u,184u,184u,183u,182u,182u,181u,180u,179u,179u,178u,177u,176u,176u,175u,174u,174u,173u,172u,171u,171u,170u,169u,168u,168u,167u,166u,165u,165u,164u,163u,162u,162u,161u,160u,159u,158u,158u,157u,156u,155u,155u,154u,153u,152u,152u,151u,150u,149u,148u,148u,147u,146u,145u,144u,144u,143u,142u,141u,141u,140u,139u,138u,137u,137u,136u,135u,134u,133u,133u,132u,131u,130u,129u,129u,128u,127u,126u,126u,125u,124u,123u,122u,122u,121u,120u,119u,118u,118u,117u,116u,115u,114u,114u,113u,112u,111u,111u,110u,109u,108u,107u,107u,106u,105u,104u,103u,103u,102u,101u,100u,100u,99u,98u,97u,97u,96u,95u,94u,93u,93u,92u,91u,90u,90u,89u,88u,87u,87u,86u,85u,84u,84u,83u,82u,81u,81u,80u,79u,79u,78u,77u,76u,76u,75u,74u,73u,73u,72u,71u,71u,70u,69u,69u,68u,67u,66u,66u,65u,64u,64u,63u,62u,62u,61u,60u,60u,59u,58u,58u,57u,56u,56u,55u,54u,54u,53u,52u,52u,51u,51u,50u,49u,49u,48u,48u,47u,46u,46u,45u,45u,44u,43u,43u,42u,42u,41u,40u,40u,39u,39u,38u,38u,37u,37u,36u,35u,35u,34u,34u,33u,33u,32u,32u,31u,31u,30u,30u,29u,29u,28u,28u,27u,27u,26u,26u,26u,25u,25u,24u,24u,23u,23u,22u,22u,22u,21u,21u,20u,20u,20u,19u,19u,18u,18u,18u,17u,17u,16u,16u,16u,15u,15u,15u,14u,14u,14u,13u,13u,13u,12u,12u,12u,12u,11u,11u,11u,10u,10u,10u,10u,9u,9u,9u,9u,8u,8u,8u,8u,7u,7u,7u,7u,7u,6u,6u,6u,6u,6u,5u,5u,5u,5u,5u,5u,4u,4u,4u,4u,4u,4u,4u,4u,4u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,3u,4u,4u,4u,4u,4u,4u,4u,4u,5u,5u,5u,5u,5u,5u,6u,6u,6u,6u,6u,6u,7u,7u,7u,7u,7u,8u,8u,8u,8u,9u,9u,9u,9u,10u,10u,10u,11u,11u,11u,11u,12u,12u,12u,13u,13u,13u,14u,14u,14u,15u,15u,15u,16u,16u,16u,17u,17u,17u,18u,18u,19u,19u,19u,20u,20u,21u,21u,21u,22u,22u,23u,23u,23u,24u,24u,25u,25u,26u,26u,27u,27u,28u,28u,29u,29u,30u,30u,31u,31u,32u,32u,33u,33u,34u,34u,35u,35u,36u,36u,37u,37u,38u,38u,39u,40u,40u,41u,41u,42u,42u,43u,44u,44u,45u,45u,46u,47u,47u,48u,48u,49u,50u,50u,51u,52u,52u,53u,53u,54u,55u,55u,56u,57u,57u,58u,59u,59u,60u,61u,61u,62u,63u,63u,64u,65u,65u,66u,67u,67u,68u,69u,70u,70u,71u,72u,72u,73u,74u,75u,75u,76u,77u,77u,78u,79u,80u,80u,81u,82u,83u,83u,84u,85u,86u,86u,87u,88u,89u,89u,90u,91u,92u,92u,93u,94u,95u,95u,96u,97u,98u,98u,99u,100u,101u,102u,102u,103u,104u,105u,105u,106u,107u,108u,109u,109u,110u,111u,112u,112u,113u,114u,115u,116u,116u,117u,118u,119u,120u,120u,121u,122u,123u,124u,124u,125u,126u,127u, Wave1_DCOffset=0.51, Wave1_Length=991, Wave1_PhaseShift=0, Wave1_Type=0, Wave2_Amplitude=1, Wave2_Data=128u,132u,138u,143u,148u,152u,158u,162u,168u,172u,178u,182u,188u,192u,198u,202u,208u,213u,218u,222u,228u,232u,238u,242u,248u,252u,248u,242u,238u,232u,228u,222u,218u,212u,208u,202u,198u,192u,188u,182u,178u,173u,168u,162u,158u,152u,148u,143u,138u,132u,128u,122u,118u,112u,107u,102u,97u,93u,88u,83u,78u,73u,68u,62u,57u,52u,47u,42u,37u,33u,28u,23u,18u,13u,8u,3u,8u,13u,18u,23u,28u,33u,37u,42u,47u,52u,57u,62u,68u,73u,78u,83u,88u,93u,97u,102u,107u,112u,118u,122u, Wave2_DCOffset=0.51, Wave2_Length=100, Wave2_PhaseShift=0, Wave2_Type=2, CY_API_CALLBACK_HEADER_INCLUDE=#include "cyapicallbacks.h", CY_COMMENT=, CY_COMPONENT_NAME=WaveDAC8_v2_10, CY_CONFIG_TITLE=WaveDAC8, CY_CONST_CONFIG=true, CY_CONTROL_FILE=<:default:>, CY_DATASHEET_FILE=<:default:>, CY_FITTER_NAME=WaveDAC8, CY_INSTANCE_SHORT_NAME=WaveDAC8, CY_MAJOR_VERSION=2, CY_MINOR_VERSION=10, CY_PDL_DRIVER_NAME=, CY_PDL_DRIVER_REQ_VERSION=, CY_PDL_DRIVER_SUBGROUP=, CY_PDL_DRIVER_VARIANT=, CY_REMOVE=false, CY_SUPPRESS_API_GEN=false, CY_VERSION=PSoC Creator  4.2, INSTANCE_NAME=WaveDAC8, )
module WaveDAC8_v2_10_2 (
    Wave,
    ws,
    clock,
    wc1,
    wc2);
    inout       Wave;
    electrical  Wave;
    input       ws;
    input       clock;
    output      wc1;
    output      wc2;


          wire  Net_280;
    electrical  Net_273;
          wire  Net_202;
          wire  Net_201;
          wire [7:0] Net_171;
          wire [7:0] Net_170;
          wire  Net_339;
          wire  Net_341;
          wire  Net_153;
    electrical  Net_211;
          wire  Net_134;
          wire  Net_107;
          wire  Net_183;
          wire  Net_336;
          wire  Net_279;
          wire  Net_80;
    electrical  Net_247;
    electrical  Net_254;
    electrical  Net_256;
    electrical  Net_190;
    electrical  Net_189;

    cy_analog_noconnect_v1_0 cy_analog_noconnect_1 (
        .noconnect(Net_211));


	cy_clock_v1_0
		#(.id("77ce502a-772f-4ddc-8c8b-056b86c266f0/77086516-855e-4b7b-abbe-47b22f8543de"),
		  .source_clock_id(""),
		  .divisor(0),
		  .period("10000000000"),
		  .is_direct(0),
		  .is_digital(1))
		DacClk
		 (.clock_out(Net_279));


	// cy_analog_virtualmux_1 (cy_analog_virtualmux_v1_0)
	cy_connect_v1_0 cy_analog_virtualmux_1_connect(Net_189, Net_256);
	defparam cy_analog_virtualmux_1_connect.sig_width = 1;

	// cy_analog_virtualmux_2 (cy_analog_virtualmux_v1_0)
	cy_connect_v1_0 cy_analog_virtualmux_2_connect(Net_190, Net_211);
	defparam cy_analog_virtualmux_2_connect.sig_width = 1;

    cy_analog_noconnect_v1_0 cy_analog_noconnect_2 (
        .noconnect(Net_254));


	cy_dma_v1_0
		#(.drq_type(2'b10))
		Wave1_DMA
		 (.drq(Net_183),
		  .trq(1'b0),
		  .nrq(wc1));



	cy_dma_v1_0
		#(.drq_type(2'b10))
		Wave2_DMA
		 (.drq(Net_107),
		  .trq(1'b0),
		  .nrq(wc2));


    // -- De Mux start --
    if (1)
    begin : demux
        reg  tmp__demux_0_reg;
        reg  tmp__demux_1_reg;
        always @(Net_336 or Net_134)
        begin
            case (Net_134)
                1'b0:
                begin
                    tmp__demux_0_reg = Net_336;
                    tmp__demux_1_reg = 1'b0;
                end
                1'b1:
                begin
                    tmp__demux_0_reg = 1'b0;
                    tmp__demux_1_reg = Net_336;
                end
            endcase
        end
        assign Net_183 = tmp__demux_0_reg;
        assign Net_107 = tmp__demux_1_reg;
    end
    // -- De Mux end --

	// VirtualMux_1 (cy_virtualmux_v1_0)
	assign Net_336 = Net_279;

    VDAC8_v1_90_1 VDAC8 (
        .strobe(Net_336),
        .data(8'b00000000),
        .vOut(Net_189));
    defparam VDAC8.Data_Source = 0;
    defparam VDAC8.Initial_Value = 100;
    defparam VDAC8.Strobe_Mode = 1;

	// cy_analog_virtualmux_3 (cy_analog_virtualmux_v1_0)
	cy_connect_v1_0 cy_analog_virtualmux_3_connect(Wave, Net_256);
	defparam cy_analog_virtualmux_3_connect.sig_width = 1;


    assign Net_280 = Net_80 | Net_279;

    ZeroTerminal ZeroTerminal_1 (
        .z(Net_80));

    // -- DFF Start --
    reg  cydff_1;
    always @(posedge Net_336)
    begin
        cydff_1 <= ws;
    end
    assign Net_134 = cydff_1;
    // -- DFF End --



endmodule

// Component: cy_constant_v1_0
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_constant_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_constant_v1_0\cy_constant_v1_0.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_constant_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\cy_constant_v1_0\cy_constant_v1_0.v"
`endif

// Component: OneTerminal
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\OneTerminal"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\OneTerminal\OneTerminal.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\OneTerminal"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\OneTerminal\OneTerminal.v"
`endif

// Timer_v2_80(CaptureAlternatingFall=false, CaptureAlternatingRise=false, CaptureCount=2, CaptureCounterEnabled=false, CaptureInputEnabled=true, CaptureMode=1, CONTROL3=1, ControlRegRemoved=0, CtlModeReplacementString=SyncCtl, CyGetRegReplacementString=CY_GET_REG16, CySetRegReplacementString=CY_SET_REG16, DeviceFamily=PSoC5, EnableMode=0, FF16=true, FF8=false, FixedFunction=true, FixedFunctionUsed=1, HWCaptureCounterEnabled=false, InterruptOnCapture=false, InterruptOnFIFOFull=false, InterruptOnTC=false, IntOnCapture=0, IntOnFIFOFull=0, IntOnTC=0, NumberOfCaptures=1, param45=1, Period=65535, RegDefReplacementString=reg16, RegSizeReplacementString=uint16, Resolution=16, RstStatusReplacementString=rstSts, RunMode=0, SiliconRevision=0, SoftwareCaptureModeEnabled=false, SoftwareTriggerModeEnabled=false, TriggerInputEnabled=false, TriggerMode=0, UDB16=false, UDB24=false, UDB32=false, UDB8=false, UDBControlReg=false, UsesHWEnable=0, VerilogSectionReplacementString=sT16, CY_API_CALLBACK_HEADER_INCLUDE=#include "cyapicallbacks.h", CY_COMMENT=, CY_COMPONENT_NAME=Timer_v2_80, CY_CONFIG_TITLE=TimerTp, CY_CONST_CONFIG=true, CY_CONTROL_FILE=<:default:>, CY_DATASHEET_FILE=<:default:>, CY_FITTER_NAME=TimerTp, CY_INSTANCE_SHORT_NAME=TimerTp, CY_MAJOR_VERSION=2, CY_MINOR_VERSION=80, CY_PDL_DRIVER_NAME=, CY_PDL_DRIVER_REQ_VERSION=, CY_PDL_DRIVER_SUBGROUP=, CY_PDL_DRIVER_VARIANT=, CY_REMOVE=false, CY_SUPPRESS_API_GEN=false, CY_VERSION=PSoC Creator  4.2, INSTANCE_NAME=TimerTp, )
module Timer_v2_80_3 (
    reset,
    interrupt,
    enable,
    trigger,
    capture,
    capture_out,
    tc,
    clock);
    input       reset;
    output      interrupt;
    input       enable;
    input       trigger;
    input       capture;
    output      capture_out;
    output      tc;
    input       clock;

    parameter CaptureCount = 2;
    parameter CaptureCounterEnabled = 0;
    parameter DeviceFamily = "PSoC5";
    parameter InterruptOnCapture = 0;
    parameter InterruptOnTC = 0;
    parameter Resolution = 16;
    parameter SiliconRevision = "0";

          wire  Net_261;
          wire  Net_260;
          wire  Net_266;
          wire  Net_102;
          wire  Net_55;
          wire  Net_57;
          wire  Net_53;
          wire  Net_51;

    cy_psoc3_timer_v1_0 TimerHW (
        .timer_reset(reset),
        .capture(capture),
        .enable(Net_266),
        .kill(Net_260),
        .clock(clock),
        .tc(Net_51),
        .compare(Net_261),
        .interrupt(Net_57));

    ZeroTerminal ZeroTerminal_1 (
        .z(Net_260));

	// VirtualMux_2 (cy_virtualmux_v1_0)
	assign interrupt = Net_57;

	// VirtualMux_3 (cy_virtualmux_v1_0)
	assign tc = Net_51;

    OneTerminal OneTerminal_1 (
        .o(Net_102));

	// VirtualMux_1 (cy_virtualmux_v1_0)
	assign Net_266 = Net_102;



endmodule

// Component: B_UART_v2_50
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\B_UART_v2_50"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\B_UART_v2_50\B_UART_v2_50.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\B_UART_v2_50"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\B_UART_v2_50\B_UART_v2_50.v"
`endif

// UART_v2_50(Address1=0, Address2=0, BaudRate=9600, BreakBitsRX=13, BreakBitsTX=13, BreakDetect=false, CRCoutputsEn=false, Enable_RX=1, Enable_RXIntInterrupt=0, Enable_TX=1, Enable_TXIntInterrupt=0, EnableHWAddress=0, EnIntRXInterrupt=false, EnIntTXInterrupt=false, FlowControl=0, HalfDuplexEn=false, HwTXEnSignal=true, InternalClock=true, InternalClockToleranceMinus=3.93736842105263, InternalClockTolerancePlus=3.93736842105263, InternalClockUsed=1, InterruptOnAddDetect=0, InterruptOnAddressMatch=0, InterruptOnBreak=0, InterruptOnByteRcvd=1, InterruptOnOverrunError=0, InterruptOnParityError=0, InterruptOnStopError=0, InterruptOnTXComplete=false, InterruptOnTXFifoEmpty=false, InterruptOnTXFifoFull=false, InterruptOnTXFifoNotFull=false, IntOnAddressDetect=false, IntOnAddressMatch=false, IntOnBreak=false, IntOnByteRcvd=true, IntOnOverrunError=false, IntOnParityError=false, IntOnStopError=false, NumDataBits=8, NumStopBits=1, OverSamplingRate=8, ParityType=0, ParityTypeSw=false, RequiredClock=76800, RXAddressMode=0, RXBufferSize=4, RxBuffRegSizeReplacementString=uint8, RXEnable=true, TXBitClkGenDP=true, TXBufferSize=4, TxBuffRegSizeReplacementString=uint8, TXEnable=true, Use23Polling=true, CY_API_CALLBACK_HEADER_INCLUDE=#include "cyapicallbacks.h", CY_COMMENT=, CY_COMPONENT_NAME=UART_v2_50, CY_CONFIG_TITLE=UART, CY_CONST_CONFIG=true, CY_CONTROL_FILE=<:default:>, CY_DATASHEET_FILE=<:default:>, CY_FITTER_NAME=UART, CY_INSTANCE_SHORT_NAME=UART, CY_MAJOR_VERSION=2, CY_MINOR_VERSION=50, CY_PDL_DRIVER_NAME=, CY_PDL_DRIVER_REQ_VERSION=, CY_PDL_DRIVER_SUBGROUP=, CY_PDL_DRIVER_VARIANT=, CY_REMOVE=false, CY_SUPPRESS_API_GEN=false, CY_VERSION=PSoC Creator  4.2, INSTANCE_NAME=UART, )
module UART_v2_50_4 (
    cts_n,
    tx,
    rts_n,
    tx_en,
    clock,
    reset,
    rx,
    tx_interrupt,
    rx_interrupt,
    tx_data,
    tx_clk,
    rx_data,
    rx_clk);
    input       cts_n;
    output      tx;
    output      rts_n;
    output      tx_en;
    input       clock;
    input       reset;
    input       rx;
    output      tx_interrupt;
    output      rx_interrupt;
    output      tx_data;
    output      tx_clk;
    output      rx_data;
    output      rx_clk;

    parameter Address1 = 0;
    parameter Address2 = 0;
    parameter EnIntRXInterrupt = 0;
    parameter EnIntTXInterrupt = 0;
    parameter FlowControl = 0;
    parameter HalfDuplexEn = 0;
    parameter HwTXEnSignal = 1;
    parameter NumDataBits = 8;
    parameter NumStopBits = 1;
    parameter ParityType = 0;
    parameter RXEnable = 1;
    parameter TXEnable = 1;

          wire  Net_289;
          wire  Net_61;
          wire  Net_9;


	cy_clock_v1_0
		#(.id("b0162966-0060-4af5-82d1-fcb491ad7619/be0a0e37-ad17-42ca-b5a1-1a654d736358"),
		  .source_clock_id(""),
		  .divisor(0),
		  .period("13020833333.3333"),
		  .is_direct(0),
		  .is_digital(1))
		IntClock
		 (.clock_out(Net_9));


	// VirtualMux_1 (cy_virtualmux_v1_0)
	assign Net_61 = Net_9;

    B_UART_v2_50 BUART (
        .cts_n(cts_n),
        .tx(tx),
        .rts_n(rts_n),
        .tx_en(tx_en),
        .clock(Net_61),
        .reset(reset),
        .rx(rx),
        .tx_interrupt(tx_interrupt),
        .rx_interrupt(rx_interrupt),
        .tx_data(tx_data),
        .tx_clk(tx_clk),
        .rx_data(rx_data),
        .rx_clk(rx_clk));
    defparam BUART.Address1 = 0;
    defparam BUART.Address2 = 0;
    defparam BUART.BreakBitsRX = 13;
    defparam BUART.BreakBitsTX = 13;
    defparam BUART.BreakDetect = 0;
    defparam BUART.CRCoutputsEn = 0;
    defparam BUART.FlowControl = 0;
    defparam BUART.HalfDuplexEn = 0;
    defparam BUART.HwTXEnSignal = 1;
    defparam BUART.NumDataBits = 8;
    defparam BUART.NumStopBits = 1;
    defparam BUART.OverSampleCount = 8;
    defparam BUART.ParityType = 0;
    defparam BUART.ParityTypeSw = 0;
    defparam BUART.RXAddressMode = 0;
    defparam BUART.RXEnable = 1;
    defparam BUART.RXStatusIntEnable = 1;
    defparam BUART.TXBitClkGenDP = 1;
    defparam BUART.TXEnable = 1;
    defparam BUART.Use23Polling = 1;



endmodule

// Component: bScanComp_v1_10
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\bScanComp_v1_10"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\bScanComp_v1_10\bScanComp_v1_10.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\bScanComp_v1_10"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\bScanComp_v1_10\bScanComp_v1_10.v"
`endif

// Component: not_v1_0
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\not_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\not_v1_0\not_v1_0.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\not_v1_0"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyPrimitives\cyprimitives.cylib\not_v1_0\not_v1_0.v"
`endif

// Comp_v2_0(Hysteresis=0, Pd_Override=0, Polarity=0, PSOC5A=false, Speed=2, Sync=1, CY_API_CALLBACK_HEADER_INCLUDE=#include "cyapicallbacks.h", CY_COMMENT=, CY_COMPONENT_NAME=Comp_v2_0, CY_CONFIG_TITLE=Comp, CY_CONST_CONFIG=true, CY_CONTROL_FILE=<:default:>, CY_DATASHEET_FILE=<:default:>, CY_FITTER_NAME=ScanComp:Comp, CY_INSTANCE_SHORT_NAME=Comp, CY_MAJOR_VERSION=2, CY_MINOR_VERSION=0, CY_PDL_DRIVER_NAME=, CY_PDL_DRIVER_REQ_VERSION=, CY_PDL_DRIVER_SUBGROUP=, CY_PDL_DRIVER_VARIANT=, CY_REMOVE=false, CY_SUPPRESS_API_GEN=false, CY_VERSION=PSoC Creator  4.2, INSTANCE_NAME=ScanComp_Comp, )
module Comp_v2_0_5 (
    Vplus,
    CmpOut,
    Vminus,
    clock);
    inout       Vplus;
    electrical  Vplus;
    output      CmpOut;
    inout       Vminus;
    electrical  Vminus;
    input       clock;


          wire  Net_9;
          wire  Net_1;

    cy_psoc3_ctcomp_v1_0 ctComp (
        .vplus(Vplus),
        .vminus(Vminus),
        .cmpout(Net_1),
        .clk_udb(clock),
        .clock(clock));

	// VirtualMux_1 (cy_virtualmux_v1_0)
	assign CmpOut = Net_1;


    assign Net_9 = ~Net_1;



endmodule

// Component: ScanCompAMux_v1_10
`ifdef CY_BLK_DIR
`undef CY_BLK_DIR
`endif

`ifdef WARP
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\ScanCompAMux_v1_10"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\ScanCompAMux_v1_10\ScanCompAMux_v1_10.v"
`else
`define CY_BLK_DIR "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\ScanCompAMux_v1_10"
`include "C:\Program Files (x86)\Cypress\PSoC Creator\4.2\PSoC Creator\psoc\content\CyComponentLibrary\CyComponentLibrary.cylib\ScanCompAMux_v1_10\ScanCompAMux_v1_10.v"
`endif

// ScanComp_v1_10(ClockSource=0, FixedVoltage=125, FixedVoltageMv=2000, InternalModeExist=0, IntModeList=<?xml version="1.0" encoding="utf-16"?>\r\n<ArrayOfString xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">\r\n  <string>INTR_BOTH</string>\r\n  <string>INTR_BOTH</string>\r\n  <string>INTR_BOTH</string>\r\n</ArrayOfString>, OutputsEnable=false, SeqChannelsConfigTable=<?xml version="1.0" encoding="utf-16"?> <CyChannelsTable xmlns:Version="1_10">   <ChannelsList>     <CyChannelsTableRow>       <InterruptMode>INTR_BOTH</InterruptMode>       <DacVoltage>2000</DacVoltage>     </CyChannelsTableRow>     <CyChannelsTableRow>       <InterruptMode>INTR_BOTH</InterruptMode>       <DacVoltage>2000</DacVoltage>     </CyChannelsTableRow>     <CyChannelsTableRow>       <InterruptMode>INTR_BOTH</InterruptMode>       <DacVoltage>2000</DacVoltage>     </CyChannelsTableRow>   </ChannelsList> </CyChannelsTable>, SequencedChannels=3, SpeedPower=2, TimingFrequency=1752, TimingRate=292, TimingType=0, VDACRange=4, VoltageMode=0, CY_API_CALLBACK_HEADER_INCLUDE=#include "cyapicallbacks.h", CY_COMMENT=, CY_COMPONENT_NAME=ScanComp_v1_10, CY_CONFIG_TITLE=ScanComp, CY_CONST_CONFIG=true, CY_CONTROL_FILE=<:default:>, CY_DATASHEET_FILE=<:default:>, CY_FITTER_NAME=ScanComp, CY_INSTANCE_SHORT_NAME=ScanComp, CY_MAJOR_VERSION=1, CY_MINOR_VERSION=10, CY_PDL_DRIVER_NAME=, CY_PDL_DRIVER_REQ_VERSION=, CY_PDL_DRIVER_SUBGROUP=, CY_PDL_DRIVER_VARIANT=, CY_REMOVE=false, CY_SUPPRESS_API_GEN=false, CY_VERSION=PSoC Creator  4.2, INSTANCE_NAME=ScanComp, )
module ScanComp_v1_10_6 (
    clock,
    interrupt,
    eos,
    CmpOut0,
    CmpOut1,
    CmpOut2,
    CmpOut3,
    CmpOut4,
    CmpOut5,
    CmpOut6,
    CmpOut7,
    CmpOut8,
    CmpOut9,
    CmpOut10,
    CmpOut11,
    CmpOut12,
    CmpOut13,
    CmpOut14,
    CmpOut15,
    CmpOut16,
    CmpOut17,
    CmpOut18,
    CmpOut19,
    CmpOut20,
    CmpOut21,
    CmpOut22,
    CmpOut23,
    CmpOut24,
    CmpOut25,
    CmpOut26,
    CmpOut27,
    CmpOut28,
    CmpOut29,
    CmpOut30,
    CmpOut31,
    CmpOut32,
    CmpOut33,
    CmpOut34,
    CmpOut35,
    CmpOut36,
    CmpOut37,
    CmpOut38,
    CmpOut39,
    CmpOut40,
    CmpOut41,
    CmpOut42,
    CmpOut43,
    CmpOut44,
    CmpOut45,
    CmpOut46,
    CmpOut47,
    CmpOut48,
    CmpOut49,
    CmpOut50,
    CmpOut51,
    CmpOut52,
    CmpOut53,
    CmpOut54,
    CmpOut55,
    CmpOut56,
    CmpOut57,
    CmpOut58,
    CmpOut59,
    CmpOut60,
    CmpOut61,
    CmpOut62,
    CmpOut63,
    AIN_10,
    AIN_11,
    AIN1,
    AIN_20,
    AIN_21,
    AIN_30,
    AIN_31,
    AIN3,
    AIN_40,
    AIN_41,
    AIN_50,
    AIN_51,
    AIN5,
    AIN_60,
    AIN_61,
    AIN_70,
    AIN_71,
    AIN7,
    AIN_80,
    AIN_81,
    AIN_90,
    AIN_91,
    AIN9,
    AIN_100,
    AIN_101,
    AIN_110,
    AIN_111,
    AIN11,
    AIN_120,
    AIN_121,
    AIN_130,
    AIN_131,
    AIN13,
    AIN_140,
    AIN_141,
    AIN_150,
    AIN_151,
    AIN15,
    AIN_160,
    AIN_161,
    AIN_170,
    AIN_171,
    AIN17,
    AIN_180,
    AIN_181,
    AIN_190,
    AIN_191,
    AIN19,
    AIN_200,
    AIN_201,
    AIN_210,
    AIN_211,
    AIN21,
    AIN_220,
    AIN_221,
    AIN_230,
    AIN_231,
    AIN23,
    AIN_240,
    AIN_241,
    AIN_250,
    AIN_251,
    AIN25,
    AIN_260,
    AIN_261,
    AIN_270,
    AIN_271,
    AIN27,
    AIN_280,
    AIN_281,
    AIN_290,
    AIN_291,
    AIN29,
    AIN_300,
    AIN_301,
    AIN_310,
    AIN_311,
    AIN31,
    AIN_320,
    AIN_321,
    AIN_450,
    AIN_451,
    AIN_460,
    AIN_461,
    AIN_470,
    AIN_471,
    AIN_480,
    AIN_481,
    AIN_490,
    AIN_491,
    AIN_500,
    AIN_501,
    AIN_510,
    AIN_511,
    AIN_520,
    AIN_521,
    AIN_330,
    AIN_331,
    AIN33,
    AIN_340,
    AIN_341,
    AIN_350,
    AIN_351,
    AIN35,
    AIN_360,
    AIN_361,
    AIN_370,
    AIN_371,
    AIN37,
    AIN_380,
    AIN_381,
    AIN_390,
    AIN_391,
    AIN39,
    AIN_400,
    AIN_401,
    AIN_410,
    AIN_411,
    AIN41,
    AIN_420,
    AIN_421,
    AIN_430,
    AIN_431,
    AIN_440,
    AIN_441,
    AIN_530,
    AIN_531,
    AIN_540,
    AIN_541,
    AIN_550,
    AIN_551,
    AIN_560,
    AIN_561,
    AIN_570,
    AIN_571,
    AIN_580,
    AIN_581,
    AIN_590,
    AIN_591,
    AIN_600,
    AIN_601,
    AIN_610,
    AIN_611,
    AIN_620,
    AIN_621,
    AIN_630,
    AIN_631,
    AIN_640,
    AIN_641,
    Vminus);
    input       clock;
    output      interrupt;
    output      eos;
    output      CmpOut0;
    output      CmpOut1;
    output      CmpOut2;
    output      CmpOut3;
    output      CmpOut4;
    output      CmpOut5;
    output      CmpOut6;
    output      CmpOut7;
    output      CmpOut8;
    output      CmpOut9;
    output      CmpOut10;
    output      CmpOut11;
    output      CmpOut12;
    output      CmpOut13;
    output      CmpOut14;
    output      CmpOut15;
    output      CmpOut16;
    output      CmpOut17;
    output      CmpOut18;
    output      CmpOut19;
    output      CmpOut20;
    output      CmpOut21;
    output      CmpOut22;
    output      CmpOut23;
    output      CmpOut24;
    output      CmpOut25;
    output      CmpOut26;
    output      CmpOut27;
    output      CmpOut28;
    output      CmpOut29;
    output      CmpOut30;
    output      CmpOut31;
    output      CmpOut32;
    output      CmpOut33;
    output      CmpOut34;
    output      CmpOut35;
    output      CmpOut36;
    output      CmpOut37;
    output      CmpOut38;
    output      CmpOut39;
    output      CmpOut40;
    output      CmpOut41;
    output      CmpOut42;
    output      CmpOut43;
    output      CmpOut44;
    output      CmpOut45;
    output      CmpOut46;
    output      CmpOut47;
    output      CmpOut48;
    output      CmpOut49;
    output      CmpOut50;
    output      CmpOut51;
    output      CmpOut52;
    output      CmpOut53;
    output      CmpOut54;
    output      CmpOut55;
    output      CmpOut56;
    output      CmpOut57;
    output      CmpOut58;
    output      CmpOut59;
    output      CmpOut60;
    output      CmpOut61;
    output      CmpOut62;
    output      CmpOut63;
    inout       AIN_10;
    electrical  AIN_10;
    inout       AIN_11;
    electrical  AIN_11;
    inout       AIN1;
    electrical  AIN1;
    inout       AIN_20;
    electrical  AIN_20;
    inout       AIN_21;
    electrical  AIN_21;
    inout       AIN_30;
    electrical  AIN_30;
    inout       AIN_31;
    electrical  AIN_31;
    inout       AIN3;
    electrical  AIN3;
    inout       AIN_40;
    electrical  AIN_40;
    inout       AIN_41;
    electrical  AIN_41;
    inout       AIN_50;
    electrical  AIN_50;
    inout       AIN_51;
    electrical  AIN_51;
    inout       AIN5;
    electrical  AIN5;
    inout       AIN_60;
    electrical  AIN_60;
    inout       AIN_61;
    electrical  AIN_61;
    inout       AIN_70;
    electrical  AIN_70;
    inout       AIN_71;
    electrical  AIN_71;
    inout       AIN7;
    electrical  AIN7;
    inout       AIN_80;
    electrical  AIN_80;
    inout       AIN_81;
    electrical  AIN_81;
    inout       AIN_90;
    electrical  AIN_90;
    inout       AIN_91;
    electrical  AIN_91;
    inout       AIN9;
    electrical  AIN9;
    inout       AIN_100;
    electrical  AIN_100;
    inout       AIN_101;
    electrical  AIN_101;
    inout       AIN_110;
    electrical  AIN_110;
    inout       AIN_111;
    electrical  AIN_111;
    inout       AIN11;
    electrical  AIN11;
    inout       AIN_120;
    electrical  AIN_120;
    inout       AIN_121;
    electrical  AIN_121;
    inout       AIN_130;
    electrical  AIN_130;
    inout       AIN_131;
    electrical  AIN_131;
    inout       AIN13;
    electrical  AIN13;
    inout       AIN_140;
    electrical  AIN_140;
    inout       AIN_141;
    electrical  AIN_141;
    inout       AIN_150;
    electrical  AIN_150;
    inout       AIN_151;
    electrical  AIN_151;
    inout       AIN15;
    electrical  AIN15;
    inout       AIN_160;
    electrical  AIN_160;
    inout       AIN_161;
    electrical  AIN_161;
    inout       AIN_170;
    electrical  AIN_170;
    inout       AIN_171;
    electrical  AIN_171;
    inout       AIN17;
    electrical  AIN17;
    inout       AIN_180;
    electrical  AIN_180;
    inout       AIN_181;
    electrical  AIN_181;
    inout       AIN_190;
    electrical  AIN_190;
    inout       AIN_191;
    electrical  AIN_191;
    inout       AIN19;
    electrical  AIN19;
    inout       AIN_200;
    electrical  AIN_200;
    inout       AIN_201;
    electrical  AIN_201;
    inout       AIN_210;
    electrical  AIN_210;
    inout       AIN_211;
    electrical  AIN_211;
    inout       AIN21;
    electrical  AIN21;
    inout       AIN_220;
    electrical  AIN_220;
    inout       AIN_221;
    electrical  AIN_221;
    inout       AIN_230;
    electrical  AIN_230;
    inout       AIN_231;
    electrical  AIN_231;
    inout       AIN23;
    electrical  AIN23;
    inout       AIN_240;
    electrical  AIN_240;
    inout       AIN_241;
    electrical  AIN_241;
    inout       AIN_250;
    electrical  AIN_250;
    inout       AIN_251;
    electrical  AIN_251;
    inout       AIN25;
    electrical  AIN25;
    inout       AIN_260;
    electrical  AIN_260;
    inout       AIN_261;
    electrical  AIN_261;
    inout       AIN_270;
    electrical  AIN_270;
    inout       AIN_271;
    electrical  AIN_271;
    inout       AIN27;
    electrical  AIN27;
    inout       AIN_280;
    electrical  AIN_280;
    inout       AIN_281;
    electrical  AIN_281;
    inout       AIN_290;
    electrical  AIN_290;
    inout       AIN_291;
    electrical  AIN_291;
    inout       AIN29;
    electrical  AIN29;
    inout       AIN_300;
    electrical  AIN_300;
    inout       AIN_301;
    electrical  AIN_301;
    inout       AIN_310;
    electrical  AIN_310;
    inout       AIN_311;
    electrical  AIN_311;
    inout       AIN31;
    electrical  AIN31;
    inout       AIN_320;
    electrical  AIN_320;
    inout       AIN_321;
    electrical  AIN_321;
    inout       AIN_450;
    electrical  AIN_450;
    inout       AIN_451;
    electrical  AIN_451;
    inout       AIN_460;
    electrical  AIN_460;
    inout       AIN_461;
    electrical  AIN_461;
    inout       AIN_470;
    electrical  AIN_470;
    inout       AIN_471;
    electrical  AIN_471;
    inout       AIN_480;
    electrical  AIN_480;
    inout       AIN_481;
    electrical  AIN_481;
    inout       AIN_490;
    electrical  AIN_490;
    inout       AIN_491;
    electrical  AIN_491;
    inout       AIN_500;
    electrical  AIN_500;
    inout       AIN_501;
    electrical  AIN_501;
    inout       AIN_510;
    electrical  AIN_510;
    inout       AIN_511;
    electrical  AIN_511;
    inout       AIN_520;
    electrical  AIN_520;
    inout       AIN_521;
    electrical  AIN_521;
    inout       AIN_330;
    electrical  AIN_330;
    inout       AIN_331;
    electrical  AIN_331;
    inout       AIN33;
    electrical  AIN33;
    inout       AIN_340;
    electrical  AIN_340;
    inout       AIN_341;
    electrical  AIN_341;
    inout       AIN_350;
    electrical  AIN_350;
    inout       AIN_351;
    electrical  AIN_351;
    inout       AIN35;
    electrical  AIN35;
    inout       AIN_360;
    electrical  AIN_360;
    inout       AIN_361;
    electrical  AIN_361;
    inout       AIN_370;
    electrical  AIN_370;
    inout       AIN_371;
    electrical  AIN_371;
    inout       AIN37;
    electrical  AIN37;
    inout       AIN_380;
    electrical  AIN_380;
    inout       AIN_381;
    electrical  AIN_381;
    inout       AIN_390;
    electrical  AIN_390;
    inout       AIN_391;
    electrical  AIN_391;
    inout       AIN39;
    electrical  AIN39;
    inout       AIN_400;
    electrical  AIN_400;
    inout       AIN_401;
    electrical  AIN_401;
    inout       AIN_410;
    electrical  AIN_410;
    inout       AIN_411;
    electrical  AIN_411;
    inout       AIN41;
    electrical  AIN41;
    inout       AIN_420;
    electrical  AIN_420;
    inout       AIN_421;
    electrical  AIN_421;
    inout       AIN_430;
    electrical  AIN_430;
    inout       AIN_431;
    electrical  AIN_431;
    inout       AIN_440;
    electrical  AIN_440;
    inout       AIN_441;
    electrical  AIN_441;
    inout       AIN_530;
    electrical  AIN_530;
    inout       AIN_531;
    electrical  AIN_531;
    inout       AIN_540;
    electrical  AIN_540;
    inout       AIN_541;
    electrical  AIN_541;
    inout       AIN_550;
    electrical  AIN_550;
    inout       AIN_551;
    electrical  AIN_551;
    inout       AIN_560;
    electrical  AIN_560;
    inout       AIN_561;
    electrical  AIN_561;
    inout       AIN_570;
    electrical  AIN_570;
    inout       AIN_571;
    electrical  AIN_571;
    inout       AIN_580;
    electrical  AIN_580;
    inout       AIN_581;
    electrical  AIN_581;
    inout       AIN_590;
    electrical  AIN_590;
    inout       AIN_591;
    electrical  AIN_591;
    inout       AIN_600;
    electrical  AIN_600;
    inout       AIN_601;
    electrical  AIN_601;
    inout       AIN_610;
    electrical  AIN_610;
    inout       AIN_611;
    electrical  AIN_611;
    inout       AIN_620;
    electrical  AIN_620;
    inout       AIN_621;
    electrical  AIN_621;
    inout       AIN_630;
    electrical  AIN_630;
    inout       AIN_631;
    electrical  AIN_631;
    inout       AIN_640;
    electrical  AIN_640;
    inout       AIN_641;
    electrical  AIN_641;
    inout       Vminus;
    electrical  Vminus;

    parameter OutputsEnable = 0;
    parameter SequencedChannels = 3;

          wire  Net_778;
          wire  clk;
          wire [7:0] Net_780;
          wire [5:0] ch_addr;
          wire  Net_779;
          wire  Net_782;
          wire  Net_781;
          wire  Net_777;
          wire  Net_776;
          wire  Net_775;
    electrical  Net_8105;
          wire  Net_8227;
    electrical  Net_6163;
    electrical  Net_5517;
    electrical  V_minus;
    electrical  V_single;
    electrical  V_plus;
          wire  Net_5511;
          wire [63:0] ch;
          wire  Net_3474;

	// VirtualMux_1 (cy_virtualmux_v1_0)
	assign clk = Net_3474;


	cy_clock_v1_0
		#(.id("6ade4592-db9f-48f8-a765-f4b3b2888e3e/3830ea91-5340-4147-9ad7-597e825b22a7"),
		  .source_clock_id(""),
		  .divisor(0),
		  .period("570776255707.763"),
		  .is_direct(0),
		  .is_digital(1))
		Clock_int
		 (.clock_out(Net_3474));


	// cy_analog_virtualmux_2 (cy_analog_virtualmux_v1_0)
	cy_connect_v1_0 cy_analog_virtualmux_2_connect(Net_5517, V_single);
	defparam cy_analog_virtualmux_2_connect.sig_width = 1;

    bScanComp_v1_10 bScanComp (
        .clk(clk),
        .ch_addr(ch_addr[5:0]),
        .ch(ch[63:0]),
        .comp_o(Net_5511),
        .interrupt(interrupt),
        .drq(Net_8227),
        .eos(eos));
    defparam bScanComp.AvailableChannels = 3;
    defparam bScanComp.ChannelsBitValue = 1;
    defparam bScanComp.EdgeType0 = 2;
    defparam bScanComp.EdgeType1 = 2;
    defparam bScanComp.EdgeType10 = 3;
    defparam bScanComp.EdgeType11 = 3;
    defparam bScanComp.EdgeType12 = 3;
    defparam bScanComp.EdgeType13 = 3;
    defparam bScanComp.EdgeType14 = 3;
    defparam bScanComp.EdgeType15 = 3;
    defparam bScanComp.EdgeType16 = 3;
    defparam bScanComp.EdgeType17 = 3;
    defparam bScanComp.EdgeType18 = 3;
    defparam bScanComp.EdgeType19 = 3;
    defparam bScanComp.EdgeType2 = 2;
    defparam bScanComp.EdgeType20 = 3;
    defparam bScanComp.EdgeType21 = 3;
    defparam bScanComp.EdgeType22 = 3;
    defparam bScanComp.EdgeType23 = 3;
    defparam bScanComp.EdgeType24 = 3;
    defparam bScanComp.EdgeType25 = 3;
    defparam bScanComp.EdgeType26 = 3;
    defparam bScanComp.EdgeType27 = 3;
    defparam bScanComp.EdgeType28 = 3;
    defparam bScanComp.EdgeType29 = 3;
    defparam bScanComp.EdgeType3 = 3;
    defparam bScanComp.EdgeType30 = 3;
    defparam bScanComp.EdgeType31 = 3;
    defparam bScanComp.EdgeType32 = 3;
    defparam bScanComp.EdgeType33 = 3;
    defparam bScanComp.EdgeType34 = 3;
    defparam bScanComp.EdgeType35 = 3;
    defparam bScanComp.EdgeType36 = 3;
    defparam bScanComp.EdgeType37 = 3;
    defparam bScanComp.EdgeType38 = 3;
    defparam bScanComp.EdgeType39 = 3;
    defparam bScanComp.EdgeType4 = 3;
    defparam bScanComp.EdgeType40 = 3;
    defparam bScanComp.EdgeType41 = 3;
    defparam bScanComp.EdgeType42 = 3;
    defparam bScanComp.EdgeType43 = 3;
    defparam bScanComp.EdgeType44 = 3;
    defparam bScanComp.EdgeType45 = 3;
    defparam bScanComp.EdgeType46 = 3;
    defparam bScanComp.EdgeType47 = 3;
    defparam bScanComp.EdgeType48 = 3;
    defparam bScanComp.EdgeType49 = 3;
    defparam bScanComp.EdgeType5 = 3;
    defparam bScanComp.EdgeType50 = 3;
    defparam bScanComp.EdgeType51 = 3;
    defparam bScanComp.EdgeType52 = 3;
    defparam bScanComp.EdgeType53 = 3;
    defparam bScanComp.EdgeType54 = 3;
    defparam bScanComp.EdgeType55 = 3;
    defparam bScanComp.EdgeType56 = 3;
    defparam bScanComp.EdgeType57 = 3;
    defparam bScanComp.EdgeType58 = 3;
    defparam bScanComp.EdgeType59 = 3;
    defparam bScanComp.EdgeType6 = 3;
    defparam bScanComp.EdgeType60 = 3;
    defparam bScanComp.EdgeType61 = 3;
    defparam bScanComp.EdgeType62 = 3;
    defparam bScanComp.EdgeType63 = 3;
    defparam bScanComp.EdgeType7 = 3;
    defparam bScanComp.EdgeType8 = 3;
    defparam bScanComp.EdgeType9 = 3;
    defparam bScanComp.InterruptBitValue = 0;
    defparam bScanComp.OutputsEnable = 0;


    assign Net_777 = Net_776 | Net_3474;

	// cy_analog_virtualmux_1 (cy_analog_virtualmux_v1_0)
	cy_connect_v1_0 cy_analog_virtualmux_1_connect(Net_6163, Vminus);
	defparam cy_analog_virtualmux_1_connect.sig_width = 1;

    ZeroTerminal ZeroTerminal_8 (
        .z(Net_776));

    Comp_v2_0_5 Comp (
        .Vplus(Net_5517),
        .CmpOut(Net_5511),
        .Vminus(Net_6163),
        .clock(1'b0));

    // -- AMuxHw ScanCompAMuxSingle start -- ***
    
    // -- AMuxHw Decoder Start--
    
    reg [63:0] ScanCompAMuxSingle_Decoder_one_hot;
    reg [5:0] ScanCompAMuxSingle_Decoder_old_id;
    wire  ScanCompAMuxSingle_Decoder_is_active;
    wire  ScanCompAMuxSingle_Decoder_enable;
    
    assign ScanCompAMuxSingle_Decoder_enable = 1'b1;
    
    genvar ScanCompAMuxSingle_Decoder_i;
    
    assign ScanCompAMuxSingle_Decoder_is_active = (ch_addr[5:0] == ScanCompAMuxSingle_Decoder_old_id) && ScanCompAMuxSingle_Decoder_enable;
    
    always @(posedge clk)
    begin
        ScanCompAMuxSingle_Decoder_old_id = ch_addr[5:0];
    end
    
    generate
        for (ScanCompAMuxSingle_Decoder_i = 0; ScanCompAMuxSingle_Decoder_i < 64; ScanCompAMuxSingle_Decoder_i = ScanCompAMuxSingle_Decoder_i + 1 )
        begin : ScanCompAMuxSingle_OutBit
            always @(posedge clk)
            begin
                ScanCompAMuxSingle_Decoder_one_hot[ScanCompAMuxSingle_Decoder_i] <= (ScanCompAMuxSingle_Decoder_old_id == ScanCompAMuxSingle_Decoder_i) && ScanCompAMuxSingle_Decoder_is_active;
            end
        end
    endgenerate
    
    // -- AMuxHw Decoder End--
    
    // -- AMuxHw Primitive A --
    
    cy_psoc3_amux_v1_0 #(
        .muxin_width(64),
        .hw_control(1),
        .one_active(1),
        .init_mux_sel(64'h0),
        .api_type(2'b10))
        ScanCompAMuxSingle(
        .muxin({
            AIN_430,
            AIN_421,
            AIN41,
            AIN_410,
            AIN_401,
            AIN39,
            AIN_390,
            AIN_381,
            AIN37,
            AIN_370,
            AIN_361,
            AIN35,
            AIN_350,
            AIN_341,
            AIN33,
            AIN_330,
            AIN_321,
            AIN31,
            AIN_310,
            AIN_301,
            AIN29,
            AIN_290,
            AIN_281,
            AIN27,
            AIN_270,
            AIN_261,
            AIN25,
            AIN_250,
            AIN_241,
            AIN23,
            AIN_230,
            AIN_221,
            AIN21,
            AIN_210,
            AIN_201,
            AIN19,
            AIN_190,
            AIN_181,
            AIN17,
            AIN_170,
            AIN_161,
            AIN15,
            AIN_150,
            AIN_141,
            AIN13,
            AIN_130,
            AIN_121,
            AIN11,
            AIN_110,
            AIN_101,
            AIN9,
            AIN_90,
            AIN_81,
            AIN7,
            AIN_70,
            AIN_61,
            AIN5,
            AIN_50,
            AIN_41,
            AIN3,
            AIN_30,
            AIN_21,
            AIN1,
            AIN_10
            }),
        .hw_ctrl_en(ScanCompAMuxSingle_Decoder_one_hot[63:0]),
        .vout(V_single)
        );
    
    // -- AMuxHw ScanCompAMuxSingle end -- ***


    assign CmpOut0 = ch[0];

    assign CmpOut1 = ch[1];

    assign CmpOut2 = ch[2];

    assign CmpOut3 = ch[3];

    assign CmpOut4 = ch[4];

    assign CmpOut5 = ch[5];

    assign CmpOut6 = ch[6];

    assign CmpOut7 = ch[7];

    assign CmpOut8 = ch[8];

    assign CmpOut9 = ch[9];

    assign CmpOut10 = ch[10];

    assign CmpOut11 = ch[11];

    assign CmpOut12 = ch[12];

    assign CmpOut13 = ch[13];

    assign CmpOut14 = ch[14];

    assign CmpOut15 = ch[15];

    assign CmpOut16 = ch[16];

    assign CmpOut17 = ch[17];

    assign CmpOut18 = ch[18];

    assign CmpOut19 = ch[19];

    assign CmpOut20 = ch[20];

    assign CmpOut21 = ch[21];

    assign CmpOut22 = ch[22];

    assign CmpOut23 = ch[23];

    assign CmpOut24 = ch[24];

    assign CmpOut25 = ch[25];

    assign CmpOut26 = ch[26];

    assign CmpOut27 = ch[27];

    assign CmpOut28 = ch[28];

    assign CmpOut29 = ch[29];

    assign CmpOut30 = ch[30];

    assign CmpOut31 = ch[31];

    assign CmpOut32 = ch[32];

    assign CmpOut33 = ch[33];

    assign CmpOut34 = ch[34];

    assign CmpOut35 = ch[35];

    assign CmpOut36 = ch[36];

    assign CmpOut37 = ch[37];

    assign CmpOut38 = ch[38];

    assign CmpOut39 = ch[39];

    assign CmpOut40 = ch[40];

    assign CmpOut41 = ch[41];

    assign CmpOut42 = ch[42];

    assign CmpOut43 = ch[43];

    assign CmpOut44 = ch[44];

    assign CmpOut45 = ch[45];

    assign CmpOut46 = ch[46];

    assign CmpOut47 = ch[47];

    assign CmpOut48 = ch[48];

    assign CmpOut49 = ch[49];

    assign CmpOut50 = ch[50];

    assign CmpOut51 = ch[51];

    assign CmpOut52 = ch[52];

    assign CmpOut53 = ch[53];

    assign CmpOut54 = ch[54];

    assign CmpOut55 = ch[55];

    assign CmpOut56 = ch[56];

    assign CmpOut57 = ch[57];

    assign CmpOut58 = ch[58];

    assign CmpOut59 = ch[58];

    assign CmpOut60 = ch[60];

    assign CmpOut61 = ch[61];

    assign CmpOut62 = ch[62];

    assign CmpOut63 = ch[63];


endmodule

// top
module top ;

    electrical  Net_716;
    electrical  Net_715;
    electrical  Net_714;
    electrical  Net_713;
    electrical  Net_712;
    electrical  Net_711;
    electrical  Net_710;
    electrical  Net_709;
          wire  ClockPWM;
    electrical  Net_708;
    electrical  Net_707;
    electrical  Net_706;
    electrical  Net_705;
    electrical  Net_704;
    electrical  Net_703;
    electrical  Net_702;
    electrical  Net_701;
    electrical  Net_700;
    electrical  Net_699;
    electrical  Net_698;
    electrical  Net_697;
    electrical  Net_696;
    electrical  Net_695;
    electrical  Net_694;
    electrical  Net_693;
    electrical  Net_692;
    electrical  Net_691;
    electrical  Net_690;
    electrical  Net_689;
    electrical  Net_688;
    electrical  Net_687;
    electrical  Net_686;
    electrical  Net_685;
    electrical  Net_684;
    electrical  Net_683;
    electrical  Net_682;
    electrical  Net_681;
    electrical  Net_680;
    electrical  Net_679;
    electrical  Net_678;
    electrical  Net_677;
    electrical  Net_676;
    electrical  Net_675;
    electrical  Net_674;
    electrical  Net_673;
    electrical  Net_672;
    electrical  Net_671;
    electrical  Net_670;
    electrical  Net_669;
    electrical  Net_668;
    electrical  Net_667;
    electrical  Net_666;
    electrical  Net_665;
    electrical  Net_664;
    electrical  Net_663;
    electrical  Net_662;
    electrical  Net_661;
    electrical  Net_660;
    electrical  Net_659;
    electrical  Net_658;
    electrical  Net_657;
    electrical  Net_656;
    electrical  Net_655;
    electrical  Net_654;
    electrical  Net_653;
    electrical  Net_652;
    electrical  Net_651;
    electrical  Net_650;
    electrical  Net_649;
    electrical  Net_648;
    electrical  Net_647;
    electrical  Net_646;
    electrical  Net_645;
    electrical  Net_644;
    electrical  Net_643;
    electrical  Net_642;
    electrical  Net_641;
    electrical  Net_640;
    electrical  Net_639;
    electrical  Net_638;
    electrical  Net_637;
    electrical  Net_636;
    electrical  Net_635;
    electrical  Net_634;
    electrical  Net_633;
    electrical  Net_632;
    electrical  Net_631;
    electrical  Net_630;
    electrical  Net_629;
    electrical  Net_628;
    electrical  Net_627;
    electrical  Net_626;
    electrical  Net_625;
    electrical  Net_624;
    electrical  Net_623;
    electrical  Net_622;
    electrical  Net_621;
    electrical  Net_620;
    electrical  Net_619;
    electrical  Net_618;
    electrical  Net_617;
    electrical  Net_616;
    electrical  Net_615;
    electrical  Net_614;
    electrical  Net_613;
    electrical  Net_612;
    electrical  Net_611;
    electrical  Net_610;
    electrical  Net_609;
    electrical  Net_608;
    electrical  Net_607;
    electrical  Net_606;
    electrical  Net_605;
    electrical  Net_604;
    electrical  Net_603;
    electrical  Net_602;
    electrical  Net_601;
    electrical  Net_600;
    electrical  Net_599;
    electrical  Net_598;
    electrical  Net_597;
    electrical  Net_596;
    electrical  Net_595;
    electrical  Net_594;
    electrical  Net_593;
    electrical  Net_592;
    electrical  Net_591;
    electrical  Net_590;
    electrical  Net_589;
    electrical  Net_588;
    electrical  Net_587;
    electrical  Net_586;
    electrical  Net_585;
    electrical  Net_584;
    electrical  Net_583;
    electrical  Net_582;
    electrical  Net_581;
    electrical  Net_580;
    electrical  Net_579;
    electrical  Net_578;
    electrical  Net_577;
    electrical  Net_576;
    electrical  Net_575;
    electrical  Net_574;
    electrical  Net_573;
    electrical  Net_571;
    electrical  Net_569;
          wire  Net_567;
          wire  Net_566;
          wire  Net_565;
          wire  Net_564;
          wire  Net_563;
          wire  Net_562;
          wire  Net_561;
          wire  Net_560;
          wire  Net_559;
          wire  Net_558;
          wire  Net_557;
          wire  Net_556;
          wire  Net_555;
          wire  Net_554;
          wire  Net_553;
          wire  Net_552;
          wire  Net_551;
          wire  Net_550;
          wire  Net_549;
          wire  Net_548;
          wire  Net_547;
          wire  Net_546;
          wire  Net_545;
          wire  Net_544;
          wire  Net_543;
          wire  Net_542;
          wire  Net_541;
          wire  Net_540;
          wire  Net_539;
          wire  Net_538;
          wire  Net_537;
          wire  Net_536;
          wire  Net_535;
          wire  Net_534;
          wire  Net_533;
          wire  Net_532;
          wire  Net_531;
          wire  Net_530;
          wire  Net_529;
          wire  Net_528;
          wire  Net_527;
          wire  Net_526;
          wire  Net_525;
          wire  Net_524;
          wire  Net_523;
          wire  Net_522;
          wire  Net_521;
          wire  Net_520;
          wire  Net_519;
          wire  Net_518;
          wire  Net_517;
          wire  Net_516;
          wire  Net_515;
          wire  Net_514;
          wire  Net_513;
          wire  Net_512;
          wire  Net_511;
          wire  Net_510;
          wire  Net_509;
          wire  Net_508;
          wire  Net_507;
          wire  Net_506;
          wire  Net_505;
          wire  Net_504;
          wire  Net_503;
          wire  Net_501;
          wire  Net_468;
          wire  Net_467;
          wire  Net_466;
          wire  Net_465;
          wire  Net_464;
          wire  Net_463;
          wire  Net_462;
          wire  Net_461;
          wire  Net_460;
          wire  Net_459;
          wire  Net_458;
          wire  Net_457;
          wire  Net_433;
          wire  Net_432;
          wire  Net_431;
          wire  Net_430;
          wire  Net_429;
          wire  Net_443;
          wire  Net_171;
          wire  Net_170;
          wire  Net_169;
          wire  Net_57;
    electrical  Net_225;
    electrical  Net_572;
    electrical  Net_570;
    electrical  Net_568;
    electrical  Net_717;
          wire  Net_492;
          wire  Net_100;
          wire  Net_440;
          wire  Net_10;
          wire  Net_12;
    electrical  Net_342;
    electrical  Net_224;
          wire  Net_168;

    PGA_v2_0_0 PGA (
        .Vin(Net_224),
        .Vref(Net_225),
        .Vout(Net_342));


	cy_clock_v1_0
		#(.id("00619080-7543-45e8-8061-ab94560693b2"),
		  .source_clock_id(""),
		  .divisor(0),
		  .period("333333333.333333"),
		  .is_direct(0),
		  .is_digital(1))
		Clock
		 (.clock_out(ClockPWM));


    WaveDAC8_v2_10_2 WaveDAC8 (
        .Wave(Net_224),
        .ws(Net_168),
        .clock(1'b0),
        .wc1(Net_170),
        .wc2(Net_171));

    assign Net_168 = 1'h0;

	wire [0:0] tmpOE__Detect_C_net;
	wire [0:0] tmpFB_0__Detect_C_net;
	wire [0:0] tmpIO_0__Detect_C_net;
	wire [0:0] tmpINTERRUPT_0__Detect_C_net;
	electrical [0:0] tmpSIOVREF__Detect_C_net;

	cy_psoc3_pins_v1_10
		#(.id("77715107-f8d5-47e5-a629-0fb83101ac6b"),
		  .drive_mode(3'b000),
		  .ibuf_enabled(1'b0),
		  .init_dr_st(1'b0),
		  .input_clk_en(0),
		  .input_sync(1'b1),
		  .input_sync_mode(1'b0),
		  .intr_mode(2'b00),
		  .invert_in_clock(0),
		  .invert_in_clock_en(0),
		  .invert_in_reset(0),
		  .invert_out_clock(0),
		  .invert_out_clock_en(0),
		  .invert_out_reset(0),
		  .io_voltage(""),
		  .layout_mode("CONTIGUOUS"),
		  .oe_conn(1'b0),
		  .oe_reset(0),
		  .oe_sync(1'b0),
		  .output_clk_en(0),
		  .output_clock_mode(1'b0),
		  .output_conn(1'b0),
		  .output_mode(1'b0),
		  .output_reset(0),
		  .output_sync(1'b0),
		  .pa_in_clock(-1),
		  .pa_in_clock_en(-1),
		  .pa_in_reset(-1),
		  .pa_out_clock(-1),
		  .pa_out_clock_en(-1),
		  .pa_out_reset(-1),
		  .pin_aliases(""),
		  .pin_mode("A"),
		  .por_state(4),
		  .sio_group_cnt(0),
		  .sio_hyst(1'b1),
		  .sio_ibuf(""),
		  .sio_info(2'b00),
		  .sio_obuf(""),
		  .sio_refsel(""),
		  .sio_vtrip(""),
		  .sio_hifreq(""),
		  .sio_vohsel(""),
		  .slew_rate(1'b0),
		  .spanning(0),
		  .use_annotation(1'b0),
		  .vtrip(2'b10),
		  .width(1),
		  .ovt_hyst_trim(1'b0),
		  .ovt_needed(1'b0),
		  .ovt_slew_control(2'b00),
		  .input_buffer_sel(2'b00))
		Detect_C
		 (.oe(tmpOE__Detect_C_net),
		  .y({1'b0}),
		  .fb({tmpFB_0__Detect_C_net[0:0]}),
		  .analog({Net_568}),
		  .io({tmpIO_0__Detect_C_net[0:0]}),
		  .siovref(tmpSIOVREF__Detect_C_net),
		  .interrupt({tmpINTERRUPT_0__Detect_C_net[0:0]}),
		  .in_clock({1'b0}),
		  .in_clock_en({1'b1}),
		  .in_reset({1'b0}),
		  .out_clock({1'b0}),
		  .out_clock_en({1'b1}),
		  .out_reset({1'b0}));

	assign tmpOE__Detect_C_net = (`CYDEV_CHIP_MEMBER_USED == `CYDEV_CHIP_MEMBER_3A && `CYDEV_CHIP_REVISION_USED < `CYDEV_CHIP_REVISION_3A_ES3) ? ~{1'b1} : {1'b1};

	wire [0:0] tmpOE__Detect_B_net;
	wire [0:0] tmpFB_0__Detect_B_net;
	wire [0:0] tmpIO_0__Detect_B_net;
	wire [0:0] tmpINTERRUPT_0__Detect_B_net;
	electrical [0:0] tmpSIOVREF__Detect_B_net;

	cy_psoc3_pins_v1_10
		#(.id("1790ca3c-fcf9-46e9-a096-da261c6229ff"),
		  .drive_mode(3'b000),
		  .ibuf_enabled(1'b0),
		  .init_dr_st(1'b0),
		  .input_clk_en(0),
		  .input_sync(1'b1),
		  .input_sync_mode(1'b0),
		  .intr_mode(2'b00),
		  .invert_in_clock(0),
		  .invert_in_clock_en(0),
		  .invert_in_reset(0),
		  .invert_out_clock(0),
		  .invert_out_clock_en(0),
		  .invert_out_reset(0),
		  .io_voltage(""),
		  .layout_mode("CONTIGUOUS"),
		  .oe_conn(1'b0),
		  .oe_reset(0),
		  .oe_sync(1'b0),
		  .output_clk_en(0),
		  .output_clock_mode(1'b0),
		  .output_conn(1'b0),
		  .output_mode(1'b0),
		  .output_reset(0),
		  .output_sync(1'b0),
		  .pa_in_clock(-1),
		  .pa_in_clock_en(-1),
		  .pa_in_reset(-1),
		  .pa_out_clock(-1),
		  .pa_out_clock_en(-1),
		  .pa_out_reset(-1),
		  .pin_aliases(""),
		  .pin_mode("A"),
		  .por_state(4),
		  .sio_group_cnt(0),
		  .sio_hyst(1'b1),
		  .sio_ibuf(""),
		  .sio_info(2'b00),
		  .sio_obuf(""),
		  .sio_refsel(""),
		  .sio_vtrip(""),
		  .sio_hifreq(""),
		  .sio_vohsel(""),
		  .slew_rate(1'b0),
		  .spanning(0),
		  .use_annotation(1'b0),
		  .vtrip(2'b10),
		  .width(1),
		  .ovt_hyst_trim(1'b0),
		  .ovt_needed(1'b0),
		  .ovt_slew_control(2'b00),
		  .input_buffer_sel(2'b00))
		Detect_B
		 (.oe(tmpOE__Detect_B_net),
		  .y({1'b0}),
		  .fb({tmpFB_0__Detect_B_net[0:0]}),
		  .analog({Net_570}),
		  .io({tmpIO_0__Detect_B_net[0:0]}),
		  .siovref(tmpSIOVREF__Detect_B_net),
		  .interrupt({tmpINTERRUPT_0__Detect_B_net[0:0]}),
		  .in_clock({1'b0}),
		  .in_clock_en({1'b1}),
		  .in_reset({1'b0}),
		  .out_clock({1'b0}),
		  .out_clock_en({1'b1}),
		  .out_reset({1'b0}));

	assign tmpOE__Detect_B_net = (`CYDEV_CHIP_MEMBER_USED == `CYDEV_CHIP_MEMBER_3A && `CYDEV_CHIP_REVISION_USED < `CYDEV_CHIP_REVISION_3A_ES3) ? ~{1'b1} : {1'b1};

	wire [0:0] tmpOE__Detect_A_net;
	wire [0:0] tmpFB_0__Detect_A_net;
	wire [0:0] tmpIO_0__Detect_A_net;
	wire [0:0] tmpINTERRUPT_0__Detect_A_net;
	electrical [0:0] tmpSIOVREF__Detect_A_net;

	cy_psoc3_pins_v1_10
		#(.id("ec509684-922b-4cf4-9aa4-2e92a4fdd873"),
		  .drive_mode(3'b000),
		  .ibuf_enabled(1'b0),
		  .init_dr_st(1'b0),
		  .input_clk_en(0),
		  .input_sync(1'b1),
		  .input_sync_mode(1'b0),
		  .intr_mode(2'b00),
		  .invert_in_clock(0),
		  .invert_in_clock_en(0),
		  .invert_in_reset(0),
		  .invert_out_clock(0),
		  .invert_out_clock_en(0),
		  .invert_out_reset(0),
		  .io_voltage(""),
		  .layout_mode("CONTIGUOUS"),
		  .oe_conn(1'b0),
		  .oe_reset(0),
		  .oe_sync(1'b0),
		  .output_clk_en(0),
		  .output_clock_mode(1'b0),
		  .output_conn(1'b0),
		  .output_mode(1'b0),
		  .output_reset(0),
		  .output_sync(1'b0),
		  .pa_in_clock(-1),
		  .pa_in_clock_en(-1),
		  .pa_in_reset(-1),
		  .pa_out_clock(-1),
		  .pa_out_clock_en(-1),
		  .pa_out_reset(-1),
		  .pin_aliases(""),
		  .pin_mode("A"),
		  .por_state(4),
		  .sio_group_cnt(0),
		  .sio_hyst(1'b1),
		  .sio_ibuf(""),
		  .sio_info(2'b00),
		  .sio_obuf(""),
		  .sio_refsel(""),
		  .sio_vtrip(""),
		  .sio_hifreq(""),
		  .sio_vohsel(""),
		  .slew_rate(1'b0),
		  .spanning(0),
		  .use_annotation(1'b0),
		  .vtrip(2'b10),
		  .width(1),
		  .ovt_hyst_trim(1'b0),
		  .ovt_needed(1'b0),
		  .ovt_slew_control(2'b00),
		  .input_buffer_sel(2'b00))
		Detect_A
		 (.oe(tmpOE__Detect_A_net),
		  .y({1'b0}),
		  .fb({tmpFB_0__Detect_A_net[0:0]}),
		  .analog({Net_572}),
		  .io({tmpIO_0__Detect_A_net[0:0]}),
		  .siovref(tmpSIOVREF__Detect_A_net),
		  .interrupt({tmpINTERRUPT_0__Detect_A_net[0:0]}),
		  .in_clock({1'b0}),
		  .in_clock_en({1'b1}),
		  .in_reset({1'b0}),
		  .out_clock({1'b0}),
		  .out_clock_en({1'b1}),
		  .out_reset({1'b0}));

	assign tmpOE__Detect_A_net = (`CYDEV_CHIP_MEMBER_USED == `CYDEV_CHIP_MEMBER_3A && `CYDEV_CHIP_REVISION_USED < `CYDEV_CHIP_REVISION_3A_ES3) ? ~{1'b1} : {1'b1};

	wire [0:0] tmpOE__Neutro_net;
	wire [0:0] tmpFB_0__Neutro_net;
	wire [0:0] tmpIO_0__Neutro_net;
	wire [0:0] tmpINTERRUPT_0__Neutro_net;
	electrical [0:0] tmpSIOVREF__Neutro_net;

	cy_psoc3_pins_v1_10
		#(.id("1e0256aa-28ae-41d9-b816-7f4635486594"),
		  .drive_mode(3'b000),
		  .ibuf_enabled(1'b0),
		  .init_dr_st(1'b0),
		  .input_clk_en(0),
		  .input_sync(1'b1),
		  .input_sync_mode(1'b0),
		  .intr_mode(2'b00),
		  .invert_in_clock(0),
		  .invert_in_clock_en(0),
		  .invert_in_reset(0),
		  .invert_out_clock(0),
		  .invert_out_clock_en(0),
		  .invert_out_reset(0),
		  .io_voltage(""),
		  .layout_mode("CONTIGUOUS"),
		  .oe_conn(1'b0),
		  .oe_reset(0),
		  .oe_sync(1'b0),
		  .output_clk_en(0),
		  .output_clock_mode(1'b0),
		  .output_conn(1'b0),
		  .output_mode(1'b0),
		  .output_reset(0),
		  .output_sync(1'b0),
		  .pa_in_clock(-1),
		  .pa_in_clock_en(-1),
		  .pa_in_reset(-1),
		  .pa_out_clock(-1),
		  .pa_out_clock_en(-1),
		  .pa_out_reset(-1),
		  .pin_aliases(""),
		  .pin_mode("A"),
		  .por_state(4),
		  .sio_group_cnt(0),
		  .sio_hyst(1'b1),
		  .sio_ibuf(""),
		  .sio_info(2'b00),
		  .sio_obuf(""),
		  .sio_refsel(""),
		  .sio_vtrip(""),
		  .sio_hifreq(""),
		  .sio_vohsel(""),
		  .slew_rate(1'b0),
		  .spanning(0),
		  .use_annotation(1'b0),
		  .vtrip(2'b10),
		  .width(1),
		  .ovt_hyst_trim(1'b0),
		  .ovt_needed(1'b0),
		  .ovt_slew_control(2'b00),
		  .input_buffer_sel(2'b00))
		Neutro
		 (.oe(tmpOE__Neutro_net),
		  .y({1'b0}),
		  .fb({tmpFB_0__Neutro_net[0:0]}),
		  .analog({Net_717}),
		  .io({tmpIO_0__Neutro_net[0:0]}),
		  .siovref(tmpSIOVREF__Neutro_net),
		  .interrupt({tmpINTERRUPT_0__Neutro_net[0:0]}),
		  .in_clock({1'b0}),
		  .in_clock_en({1'b1}),
		  .in_reset({1'b0}),
		  .out_clock({1'b0}),
		  .out_clock_en({1'b1}),
		  .out_reset({1'b0}));

	assign tmpOE__Neutro_net = (`CYDEV_CHIP_MEMBER_USED == `CYDEV_CHIP_MEMBER_3A && `CYDEV_CHIP_REVISION_USED < `CYDEV_CHIP_REVISION_3A_ES3) ? ~{1'b1} : {1'b1};


	cy_isr_v1_0
		#(.int_type(2'b10))
		isr_Tp
		 (.int_signal(Net_440));


	wire [0:0] tmpOE__Signal100Hz_net;
	wire [0:0] tmpFB_0__Signal100Hz_net;
	wire [0:0] tmpIO_0__Signal100Hz_net;
	wire [0:0] tmpINTERRUPT_0__Signal100Hz_net;
	electrical [0:0] tmpSIOVREF__Signal100Hz_net;

	cy_psoc3_pins_v1_10
		#(.id("353ae2f5-d599-4a50-8afc-b7101017bc59"),
		  .drive_mode(3'b000),
		  .ibuf_enabled(1'b0),
		  .init_dr_st(1'b0),
		  .input_clk_en(0),
		  .input_sync(1'b1),
		  .input_sync_mode(1'b0),
		  .intr_mode(2'b00),
		  .invert_in_clock(0),
		  .invert_in_clock_en(0),
		  .invert_in_reset(0),
		  .invert_out_clock(0),
		  .invert_out_clock_en(0),
		  .invert_out_reset(0),
		  .io_voltage(""),
		  .layout_mode("CONTIGUOUS"),
		  .oe_conn(1'b0),
		  .oe_reset(0),
		  .oe_sync(1'b0),
		  .output_clk_en(0),
		  .output_clock_mode(1'b0),
		  .output_conn(1'b0),
		  .output_mode(1'b0),
		  .output_reset(0),
		  .output_sync(1'b0),
		  .pa_in_clock(-1),
		  .pa_in_clock_en(-1),
		  .pa_in_reset(-1),
		  .pa_out_clock(-1),
		  .pa_out_clock_en(-1),
		  .pa_out_reset(-1),
		  .pin_aliases(""),
		  .pin_mode("A"),
		  .por_state(4),
		  .sio_group_cnt(0),
		  .sio_hyst(1'b1),
		  .sio_ibuf(""),
		  .sio_info(2'b00),
		  .sio_obuf(""),
		  .sio_refsel(""),
		  .sio_vtrip(""),
		  .sio_hifreq(""),
		  .sio_vohsel(""),
		  .slew_rate(1'b0),
		  .spanning(0),
		  .use_annotation(1'b0),
		  .vtrip(2'b10),
		  .width(1),
		  .ovt_hyst_trim(1'b0),
		  .ovt_needed(1'b0),
		  .ovt_slew_control(2'b00),
		  .input_buffer_sel(2'b00))
		Signal100Hz
		 (.oe(tmpOE__Signal100Hz_net),
		  .y({1'b0}),
		  .fb({tmpFB_0__Signal100Hz_net[0:0]}),
		  .analog({Net_342}),
		  .io({tmpIO_0__Signal100Hz_net[0:0]}),
		  .siovref(tmpSIOVREF__Signal100Hz_net),
		  .interrupt({tmpINTERRUPT_0__Signal100Hz_net[0:0]}),
		  .in_clock({1'b0}),
		  .in_clock_en({1'b1}),
		  .in_reset({1'b0}),
		  .out_clock({1'b0}),
		  .out_clock_en({1'b1}),
		  .out_reset({1'b0}));

	assign tmpOE__Signal100Hz_net = (`CYDEV_CHIP_MEMBER_USED == `CYDEV_CHIP_MEMBER_3A && `CYDEV_CHIP_REVISION_USED < `CYDEV_CHIP_REVISION_3A_ES3) ? ~{1'b1} : {1'b1};


	cy_clock_v1_0
		#(.id("c0fb34bd-1044-4931-9788-16b01ce89812"),
		  .source_clock_id("75C2148C-3656-4d8a-846D-0CAE99AB6FF7"),
		  .divisor(0),
		  .period("0"),
		  .is_direct(1),
		  .is_digital(1))
		timer_clock
		 (.clock_out(Net_10));


    ZeroTerminal ZeroTerminal_1 (
        .z(Net_12));

    Timer_v2_80_3 TimerTp (
        .reset(Net_12),
        .interrupt(Net_440),
        .enable(1'b1),
        .trigger(1'b1),
        .capture(1'b0),
        .capture_out(Net_432),
        .tc(Net_433),
        .clock(Net_10));
    defparam TimerTp.CaptureCount = 2;
    defparam TimerTp.CaptureCounterEnabled = 0;
    defparam TimerTp.DeviceFamily = "PSoC5";
    defparam TimerTp.InterruptOnCapture = 0;
    defparam TimerTp.InterruptOnTC = 0;
    defparam TimerTp.Resolution = 16;
    defparam TimerTp.SiliconRevision = "0";


	cy_isr_v1_0
		#(.int_type(2'b00))
		isr_zc
		 (.int_signal(Net_492));


    UART_v2_50_4 UART (
        .cts_n(1'b0),
        .tx(Net_100),
        .rts_n(Net_458),
        .tx_en(Net_459),
        .clock(1'b0),
        .reset(1'b0),
        .rx(Net_462),
        .tx_interrupt(Net_463),
        .rx_interrupt(Net_464),
        .tx_data(Net_465),
        .tx_clk(Net_466),
        .rx_data(Net_467),
        .rx_clk(Net_468));
    defparam UART.Address1 = 0;
    defparam UART.Address2 = 0;
    defparam UART.EnIntRXInterrupt = 0;
    defparam UART.EnIntTXInterrupt = 0;
    defparam UART.FlowControl = 0;
    defparam UART.HalfDuplexEn = 0;
    defparam UART.HwTXEnSignal = 1;
    defparam UART.NumDataBits = 8;
    defparam UART.NumStopBits = 1;
    defparam UART.ParityType = 0;
    defparam UART.RXEnable = 1;
    defparam UART.TXEnable = 1;

	wire [0:0] tmpOE__Rx_1_net;
	wire [0:0] tmpIO_0__Rx_1_net;
	wire [0:0] tmpINTERRUPT_0__Rx_1_net;
	electrical [0:0] tmpSIOVREF__Rx_1_net;

	cy_psoc3_pins_v1_10
		#(.id("1425177d-0d0e-4468-8bcc-e638e5509a9b"),
		  .drive_mode(3'b001),
		  .ibuf_enabled(1'b1),
		  .init_dr_st(1'b0),
		  .input_clk_en(0),
		  .input_sync(1'b1),
		  .input_sync_mode(1'b0),
		  .intr_mode(2'b00),
		  .invert_in_clock(0),
		  .invert_in_clock_en(0),
		  .invert_in_reset(0),
		  .invert_out_clock(0),
		  .invert_out_clock_en(0),
		  .invert_out_reset(0),
		  .io_voltage(""),
		  .layout_mode("CONTIGUOUS"),
		  .oe_conn(1'b0),
		  .oe_reset(0),
		  .oe_sync(1'b0),
		  .output_clk_en(0),
		  .output_clock_mode(1'b0),
		  .output_conn(1'b0),
		  .output_mode(1'b0),
		  .output_reset(0),
		  .output_sync(1'b0),
		  .pa_in_clock(-1),
		  .pa_in_clock_en(-1),
		  .pa_in_reset(-1),
		  .pa_out_clock(-1),
		  .pa_out_clock_en(-1),
		  .pa_out_reset(-1),
		  .pin_aliases(""),
		  .pin_mode("I"),
		  .por_state(4),
		  .sio_group_cnt(0),
		  .sio_hyst(1'b1),
		  .sio_ibuf(""),
		  .sio_info(2'b00),
		  .sio_obuf(""),
		  .sio_refsel(""),
		  .sio_vtrip(""),
		  .sio_hifreq(""),
		  .sio_vohsel(""),
		  .slew_rate(1'b0),
		  .spanning(0),
		  .use_annotation(1'b0),
		  .vtrip(2'b00),
		  .width(1),
		  .ovt_hyst_trim(1'b0),
		  .ovt_needed(1'b0),
		  .ovt_slew_control(2'b00),
		  .input_buffer_sel(2'b00))
		Rx_1
		 (.oe(tmpOE__Rx_1_net),
		  .y({1'b0}),
		  .fb({Net_462}),
		  .io({tmpIO_0__Rx_1_net[0:0]}),
		  .siovref(tmpSIOVREF__Rx_1_net),
		  .interrupt({tmpINTERRUPT_0__Rx_1_net[0:0]}),
		  .in_clock({1'b0}),
		  .in_clock_en({1'b1}),
		  .in_reset({1'b0}),
		  .out_clock({1'b0}),
		  .out_clock_en({1'b1}),
		  .out_reset({1'b0}));

	assign tmpOE__Rx_1_net = (`CYDEV_CHIP_MEMBER_USED == `CYDEV_CHIP_MEMBER_3A && `CYDEV_CHIP_REVISION_USED < `CYDEV_CHIP_REVISION_3A_ES3) ? ~{1'b1} : {1'b1};

	wire [0:0] tmpOE__Tx_1_net;
	wire [0:0] tmpFB_0__Tx_1_net;
	wire [0:0] tmpIO_0__Tx_1_net;
	wire [0:0] tmpINTERRUPT_0__Tx_1_net;
	electrical [0:0] tmpSIOVREF__Tx_1_net;

	cy_psoc3_pins_v1_10
		#(.id("ed092b9b-d398-4703-be89-cebf998501f6"),
		  .drive_mode(3'b110),
		  .ibuf_enabled(1'b1),
		  .init_dr_st(1'b1),
		  .input_clk_en(0),
		  .input_sync(1'b1),
		  .input_sync_mode(1'b0),
		  .intr_mode(2'b00),
		  .invert_in_clock(0),
		  .invert_in_clock_en(0),
		  .invert_in_reset(0),
		  .invert_out_clock(0),
		  .invert_out_clock_en(0),
		  .invert_out_reset(0),
		  .io_voltage(""),
		  .layout_mode("CONTIGUOUS"),
		  .oe_conn(1'b0),
		  .oe_reset(0),
		  .oe_sync(1'b0),
		  .output_clk_en(0),
		  .output_clock_mode(1'b0),
		  .output_conn(1'b1),
		  .output_mode(1'b0),
		  .output_reset(0),
		  .output_sync(1'b0),
		  .pa_in_clock(-1),
		  .pa_in_clock_en(-1),
		  .pa_in_reset(-1),
		  .pa_out_clock(-1),
		  .pa_out_clock_en(-1),
		  .pa_out_reset(-1),
		  .pin_aliases(""),
		  .pin_mode("O"),
		  .por_state(4),
		  .sio_group_cnt(0),
		  .sio_hyst(1'b1),
		  .sio_ibuf(""),
		  .sio_info(2'b00),
		  .sio_obuf(""),
		  .sio_refsel(""),
		  .sio_vtrip(""),
		  .sio_hifreq(""),
		  .sio_vohsel(""),
		  .slew_rate(1'b0),
		  .spanning(0),
		  .use_annotation(1'b0),
		  .vtrip(2'b10),
		  .width(1),
		  .ovt_hyst_trim(1'b0),
		  .ovt_needed(1'b0),
		  .ovt_slew_control(2'b00),
		  .input_buffer_sel(2'b00))
		Tx_1
		 (.oe(tmpOE__Tx_1_net),
		  .y({Net_100}),
		  .fb({tmpFB_0__Tx_1_net[0:0]}),
		  .io({tmpIO_0__Tx_1_net[0:0]}),
		  .siovref(tmpSIOVREF__Tx_1_net),
		  .interrupt({tmpINTERRUPT_0__Tx_1_net[0:0]}),
		  .in_clock({1'b0}),
		  .in_clock_en({1'b1}),
		  .in_reset({1'b0}),
		  .out_clock({1'b0}),
		  .out_clock_en({1'b1}),
		  .out_reset({1'b0}));

	assign tmpOE__Tx_1_net = (`CYDEV_CHIP_MEMBER_USED == `CYDEV_CHIP_MEMBER_3A && `CYDEV_CHIP_REVISION_USED < `CYDEV_CHIP_REVISION_3A_ES3) ? ~{1'b1} : {1'b1};

    ScanComp_v1_10_6 ScanComp (
        .clock(1'b0),
        .interrupt(Net_492),
        .eos(Net_503),
        .CmpOut0(Net_504),
        .CmpOut1(Net_505),
        .CmpOut2(Net_506),
        .CmpOut3(Net_507),
        .CmpOut4(Net_508),
        .CmpOut5(Net_509),
        .CmpOut6(Net_510),
        .CmpOut7(Net_511),
        .CmpOut8(Net_512),
        .CmpOut9(Net_513),
        .CmpOut10(Net_514),
        .CmpOut11(Net_515),
        .CmpOut12(Net_516),
        .CmpOut13(Net_517),
        .CmpOut14(Net_518),
        .CmpOut15(Net_519),
        .CmpOut16(Net_520),
        .CmpOut17(Net_521),
        .CmpOut18(Net_522),
        .CmpOut19(Net_523),
        .CmpOut20(Net_524),
        .CmpOut21(Net_525),
        .CmpOut22(Net_526),
        .CmpOut23(Net_527),
        .CmpOut24(Net_528),
        .CmpOut25(Net_529),
        .CmpOut26(Net_530),
        .CmpOut27(Net_531),
        .CmpOut28(Net_532),
        .CmpOut29(Net_533),
        .CmpOut30(Net_534),
        .CmpOut31(Net_535),
        .CmpOut32(Net_536),
        .CmpOut33(Net_537),
        .CmpOut34(Net_538),
        .CmpOut35(Net_539),
        .CmpOut36(Net_540),
        .CmpOut37(Net_541),
        .CmpOut38(Net_542),
        .CmpOut39(Net_543),
        .CmpOut40(Net_544),
        .CmpOut41(Net_545),
        .CmpOut42(Net_546),
        .CmpOut43(Net_547),
        .CmpOut44(Net_548),
        .CmpOut45(Net_549),
        .CmpOut46(Net_550),
        .CmpOut47(Net_551),
        .CmpOut48(Net_552),
        .CmpOut49(Net_553),
        .CmpOut50(Net_554),
        .CmpOut51(Net_555),
        .CmpOut52(Net_556),
        .CmpOut53(Net_557),
        .CmpOut54(Net_558),
        .CmpOut55(Net_559),
        .CmpOut56(Net_560),
        .CmpOut57(Net_561),
        .CmpOut58(Net_562),
        .CmpOut59(Net_563),
        .CmpOut60(Net_564),
        .CmpOut61(Net_565),
        .CmpOut62(Net_566),
        .CmpOut63(Net_567),
        .AIN_10(Net_568),
        .AIN_11(Net_569),
        .AIN1(Net_570),
        .AIN_20(Net_571),
        .AIN_21(Net_572),
        .AIN_30(Net_573),
        .AIN_31(Net_574),
        .AIN3(Net_575),
        .AIN_40(Net_576),
        .AIN_41(Net_577),
        .AIN_50(Net_578),
        .AIN_51(Net_579),
        .AIN5(Net_580),
        .AIN_60(Net_581),
        .AIN_61(Net_582),
        .AIN_70(Net_583),
        .AIN_71(Net_584),
        .AIN7(Net_585),
        .AIN_80(Net_586),
        .AIN_81(Net_587),
        .AIN_90(Net_588),
        .AIN_91(Net_589),
        .AIN9(Net_590),
        .AIN_100(Net_591),
        .AIN_101(Net_592),
        .AIN_110(Net_593),
        .AIN_111(Net_594),
        .AIN11(Net_595),
        .AIN_120(Net_596),
        .AIN_121(Net_597),
        .AIN_130(Net_598),
        .AIN_131(Net_599),
        .AIN13(Net_600),
        .AIN_140(Net_601),
        .AIN_141(Net_602),
        .AIN_150(Net_603),
        .AIN_151(Net_604),
        .AIN15(Net_605),
        .AIN_160(Net_606),
        .AIN_161(Net_607),
        .AIN_170(Net_608),
        .AIN_171(Net_609),
        .AIN17(Net_610),
        .AIN_180(Net_611),
        .AIN_181(Net_612),
        .AIN_190(Net_613),
        .AIN_191(Net_614),
        .AIN19(Net_615),
        .AIN_200(Net_616),
        .AIN_201(Net_617),
        .AIN_210(Net_618),
        .AIN_211(Net_619),
        .AIN21(Net_620),
        .AIN_220(Net_621),
        .AIN_221(Net_622),
        .AIN_230(Net_623),
        .AIN_231(Net_624),
        .AIN23(Net_625),
        .AIN_240(Net_626),
        .AIN_241(Net_627),
        .AIN_250(Net_628),
        .AIN_251(Net_629),
        .AIN25(Net_630),
        .AIN_260(Net_631),
        .AIN_261(Net_632),
        .AIN_270(Net_633),
        .AIN_271(Net_634),
        .AIN27(Net_635),
        .AIN_280(Net_636),
        .AIN_281(Net_637),
        .AIN_290(Net_638),
        .AIN_291(Net_639),
        .AIN29(Net_640),
        .AIN_300(Net_641),
        .AIN_301(Net_642),
        .AIN_310(Net_643),
        .AIN_311(Net_644),
        .AIN31(Net_645),
        .AIN_320(Net_646),
        .AIN_321(Net_647),
        .AIN_450(Net_648),
        .AIN_451(Net_649),
        .AIN_460(Net_650),
        .AIN_461(Net_651),
        .AIN_470(Net_652),
        .AIN_471(Net_653),
        .AIN_480(Net_654),
        .AIN_481(Net_655),
        .AIN_490(Net_656),
        .AIN_491(Net_657),
        .AIN_500(Net_658),
        .AIN_501(Net_659),
        .AIN_510(Net_660),
        .AIN_511(Net_661),
        .AIN_520(Net_662),
        .AIN_521(Net_663),
        .AIN_330(Net_664),
        .AIN_331(Net_665),
        .AIN33(Net_666),
        .AIN_340(Net_667),
        .AIN_341(Net_668),
        .AIN_350(Net_669),
        .AIN_351(Net_670),
        .AIN35(Net_671),
        .AIN_360(Net_672),
        .AIN_361(Net_673),
        .AIN_370(Net_674),
        .AIN_371(Net_675),
        .AIN37(Net_676),
        .AIN_380(Net_677),
        .AIN_381(Net_678),
        .AIN_390(Net_679),
        .AIN_391(Net_680),
        .AIN39(Net_681),
        .AIN_400(Net_682),
        .AIN_401(Net_683),
        .AIN_410(Net_684),
        .AIN_411(Net_685),
        .AIN41(Net_686),
        .AIN_420(Net_687),
        .AIN_421(Net_688),
        .AIN_430(Net_689),
        .AIN_431(Net_690),
        .AIN_440(Net_691),
        .AIN_441(Net_692),
        .AIN_530(Net_693),
        .AIN_531(Net_694),
        .AIN_540(Net_695),
        .AIN_541(Net_696),
        .AIN_550(Net_697),
        .AIN_551(Net_698),
        .AIN_560(Net_699),
        .AIN_561(Net_700),
        .AIN_570(Net_701),
        .AIN_571(Net_702),
        .AIN_580(Net_703),
        .AIN_581(Net_704),
        .AIN_590(Net_705),
        .AIN_591(Net_706),
        .AIN_600(Net_707),
        .AIN_601(Net_708),
        .AIN_610(Net_709),
        .AIN_611(Net_710),
        .AIN_620(Net_711),
        .AIN_621(Net_712),
        .AIN_630(Net_713),
        .AIN_631(Net_714),
        .AIN_640(Net_715),
        .AIN_641(Net_716),
        .Vminus(Net_717));
    defparam ScanComp.OutputsEnable = 0;
    defparam ScanComp.SequencedChannels = 3;



endmodule

