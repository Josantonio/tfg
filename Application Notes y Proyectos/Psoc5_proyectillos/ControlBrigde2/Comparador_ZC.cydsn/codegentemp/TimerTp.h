/*******************************************************************************
* File Name: TimerTp.h
* Version 2.80
*
*  Description:
*     Contains the function prototypes and constants available to the timer
*     user module.
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_TIMER_TimerTp_H)
#define CY_TIMER_TimerTp_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 TimerTp_initVar;

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component Timer_v2_80 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */


/**************************************
*           Parameter Defaults
**************************************/

#define TimerTp_Resolution                 16u
#define TimerTp_UsingFixedFunction         1u
#define TimerTp_UsingHWCaptureCounter      0u
#define TimerTp_SoftwareCaptureMode        0u
#define TimerTp_SoftwareTriggerMode        0u
#define TimerTp_UsingHWEnable              0u
#define TimerTp_EnableTriggerMode          0u
#define TimerTp_InterruptOnCaptureCount    0u
#define TimerTp_RunModeUsed                0u
#define TimerTp_ControlRegRemoved          0u

#if defined(TimerTp_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG)
    #define TimerTp_UDB_CONTROL_REG_REMOVED            (0u)
#elif  (TimerTp_UsingFixedFunction)
    #define TimerTp_UDB_CONTROL_REG_REMOVED            (0u)
#else 
    #define TimerTp_UDB_CONTROL_REG_REMOVED            (1u)
#endif /* End TimerTp_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG */


/***************************************
*       Type defines
***************************************/


/**************************************************************************
 * Sleep Wakeup Backup structure for Timer Component
 *************************************************************************/
typedef struct
{
    uint8 TimerEnableState;
    #if(!TimerTp_UsingFixedFunction)

        uint16 TimerUdb;
        uint8 InterruptMaskValue;
        #if (TimerTp_UsingHWCaptureCounter)
            uint8 TimerCaptureCounter;
        #endif /* variable declarations for backing up non retention registers in CY_UDB_V1 */

        #if (!TimerTp_UDB_CONTROL_REG_REMOVED)
            uint8 TimerControlRegister;
        #endif /* variable declaration for backing up enable state of the Timer */
    #endif /* define backup variables only for UDB implementation. Fixed function registers are all retention */

}TimerTp_backupStruct;


/***************************************
*       Function Prototypes
***************************************/

void    TimerTp_Start(void) ;
void    TimerTp_Stop(void) ;

void    TimerTp_SetInterruptMode(uint8 interruptMode) ;
uint8   TimerTp_ReadStatusRegister(void) ;
/* Deprecated function. Do not use this in future. Retained for backward compatibility */
#define TimerTp_GetInterruptSource() TimerTp_ReadStatusRegister()

#if(!TimerTp_UDB_CONTROL_REG_REMOVED)
    uint8   TimerTp_ReadControlRegister(void) ;
    void    TimerTp_WriteControlRegister(uint8 control) ;
#endif /* (!TimerTp_UDB_CONTROL_REG_REMOVED) */

uint16  TimerTp_ReadPeriod(void) ;
void    TimerTp_WritePeriod(uint16 period) ;
uint16  TimerTp_ReadCounter(void) ;
void    TimerTp_WriteCounter(uint16 counter) ;
uint16  TimerTp_ReadCapture(void) ;
void    TimerTp_SoftwareCapture(void) ;

#if(!TimerTp_UsingFixedFunction) /* UDB Prototypes */
    #if (TimerTp_SoftwareCaptureMode)
        void    TimerTp_SetCaptureMode(uint8 captureMode) ;
    #endif /* (!TimerTp_UsingFixedFunction) */

    #if (TimerTp_SoftwareTriggerMode)
        void    TimerTp_SetTriggerMode(uint8 triggerMode) ;
    #endif /* (TimerTp_SoftwareTriggerMode) */

    #if (TimerTp_EnableTriggerMode)
        void    TimerTp_EnableTrigger(void) ;
        void    TimerTp_DisableTrigger(void) ;
    #endif /* (TimerTp_EnableTriggerMode) */


    #if(TimerTp_InterruptOnCaptureCount)
        void    TimerTp_SetInterruptCount(uint8 interruptCount) ;
    #endif /* (TimerTp_InterruptOnCaptureCount) */

    #if (TimerTp_UsingHWCaptureCounter)
        void    TimerTp_SetCaptureCount(uint8 captureCount) ;
        uint8   TimerTp_ReadCaptureCount(void) ;
    #endif /* (TimerTp_UsingHWCaptureCounter) */

    void TimerTp_ClearFIFO(void) ;
#endif /* UDB Prototypes */

/* Sleep Retention APIs */
void TimerTp_Init(void)          ;
void TimerTp_Enable(void)        ;
void TimerTp_SaveConfig(void)    ;
void TimerTp_RestoreConfig(void) ;
void TimerTp_Sleep(void)         ;
void TimerTp_Wakeup(void)        ;


/***************************************
*   Enumerated Types and Parameters
***************************************/

/* Enumerated Type B_Timer__CaptureModes, Used in Capture Mode */
#define TimerTp__B_TIMER__CM_NONE 0
#define TimerTp__B_TIMER__CM_RISINGEDGE 1
#define TimerTp__B_TIMER__CM_FALLINGEDGE 2
#define TimerTp__B_TIMER__CM_EITHEREDGE 3
#define TimerTp__B_TIMER__CM_SOFTWARE 4



/* Enumerated Type B_Timer__TriggerModes, Used in Trigger Mode */
#define TimerTp__B_TIMER__TM_NONE 0x00u
#define TimerTp__B_TIMER__TM_RISINGEDGE 0x04u
#define TimerTp__B_TIMER__TM_FALLINGEDGE 0x08u
#define TimerTp__B_TIMER__TM_EITHEREDGE 0x0Cu
#define TimerTp__B_TIMER__TM_SOFTWARE 0x10u


/***************************************
*    Initialial Parameter Constants
***************************************/

#define TimerTp_INIT_PERIOD             65535u
#define TimerTp_INIT_CAPTURE_MODE       ((uint8)((uint8)1u << TimerTp_CTRL_CAP_MODE_SHIFT))
#define TimerTp_INIT_TRIGGER_MODE       ((uint8)((uint8)0u << TimerTp_CTRL_TRIG_MODE_SHIFT))
#if (TimerTp_UsingFixedFunction)
    #define TimerTp_INIT_INTERRUPT_MODE (((uint8)((uint8)0u << TimerTp_STATUS_TC_INT_MASK_SHIFT)) | \
                                                  ((uint8)((uint8)0 << TimerTp_STATUS_CAPTURE_INT_MASK_SHIFT)))
#else
    #define TimerTp_INIT_INTERRUPT_MODE (((uint8)((uint8)0u << TimerTp_STATUS_TC_INT_MASK_SHIFT)) | \
                                                 ((uint8)((uint8)0 << TimerTp_STATUS_CAPTURE_INT_MASK_SHIFT)) | \
                                                 ((uint8)((uint8)0 << TimerTp_STATUS_FIFOFULL_INT_MASK_SHIFT)))
#endif /* (TimerTp_UsingFixedFunction) */
#define TimerTp_INIT_CAPTURE_COUNT      (2u)
#define TimerTp_INIT_INT_CAPTURE_COUNT  ((uint8)((uint8)(1u - 1u) << TimerTp_CTRL_INTCNT_SHIFT))


/***************************************
*           Registers
***************************************/

#if (TimerTp_UsingFixedFunction) /* Implementation Specific Registers and Register Constants */


    /***************************************
    *    Fixed Function Registers
    ***************************************/

    #define TimerTp_STATUS         (*(reg8 *) TimerTp_TimerHW__SR0 )
    /* In Fixed Function Block Status and Mask are the same register */
    #define TimerTp_STATUS_MASK    (*(reg8 *) TimerTp_TimerHW__SR0 )
    #define TimerTp_CONTROL        (*(reg8 *) TimerTp_TimerHW__CFG0)
    #define TimerTp_CONTROL2       (*(reg8 *) TimerTp_TimerHW__CFG1)
    #define TimerTp_CONTROL2_PTR   ( (reg8 *) TimerTp_TimerHW__CFG1)
    #define TimerTp_RT1            (*(reg8 *) TimerTp_TimerHW__RT1)
    #define TimerTp_RT1_PTR        ( (reg8 *) TimerTp_TimerHW__RT1)

    #if (CY_PSOC3 || CY_PSOC5LP)
        #define TimerTp_CONTROL3       (*(reg8 *) TimerTp_TimerHW__CFG2)
        #define TimerTp_CONTROL3_PTR   ( (reg8 *) TimerTp_TimerHW__CFG2)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    #define TimerTp_GLOBAL_ENABLE  (*(reg8 *) TimerTp_TimerHW__PM_ACT_CFG)
    #define TimerTp_GLOBAL_STBY_ENABLE  (*(reg8 *) TimerTp_TimerHW__PM_STBY_CFG)

    #define TimerTp_CAPTURE_LSB         (* (reg16 *) TimerTp_TimerHW__CAP0 )
    #define TimerTp_CAPTURE_LSB_PTR       ((reg16 *) TimerTp_TimerHW__CAP0 )
    #define TimerTp_PERIOD_LSB          (* (reg16 *) TimerTp_TimerHW__PER0 )
    #define TimerTp_PERIOD_LSB_PTR        ((reg16 *) TimerTp_TimerHW__PER0 )
    #define TimerTp_COUNTER_LSB         (* (reg16 *) TimerTp_TimerHW__CNT_CMP0 )
    #define TimerTp_COUNTER_LSB_PTR       ((reg16 *) TimerTp_TimerHW__CNT_CMP0 )


    /***************************************
    *    Register Constants
    ***************************************/

    /* Fixed Function Block Chosen */
    #define TimerTp_BLOCK_EN_MASK                     TimerTp_TimerHW__PM_ACT_MSK
    #define TimerTp_BLOCK_STBY_EN_MASK                TimerTp_TimerHW__PM_STBY_MSK

    /* Control Register Bit Locations */
    /* Interrupt Count - Not valid for Fixed Function Block */
    #define TimerTp_CTRL_INTCNT_SHIFT                  0x00u
    /* Trigger Polarity - Not valid for Fixed Function Block */
    #define TimerTp_CTRL_TRIG_MODE_SHIFT               0x00u
    /* Trigger Enable - Not valid for Fixed Function Block */
    #define TimerTp_CTRL_TRIG_EN_SHIFT                 0x00u
    /* Capture Polarity - Not valid for Fixed Function Block */
    #define TimerTp_CTRL_CAP_MODE_SHIFT                0x00u
    /* Timer Enable - As defined in Register Map, part of TMRX_CFG0 register */
    #define TimerTp_CTRL_ENABLE_SHIFT                  0x00u

    /* Control Register Bit Masks */
    #define TimerTp_CTRL_ENABLE                        ((uint8)((uint8)0x01u << TimerTp_CTRL_ENABLE_SHIFT))

    /* Control2 Register Bit Masks */
    /* As defined in Register Map, Part of the TMRX_CFG1 register */
    #define TimerTp_CTRL2_IRQ_SEL_SHIFT                 0x00u
    #define TimerTp_CTRL2_IRQ_SEL                      ((uint8)((uint8)0x01u << TimerTp_CTRL2_IRQ_SEL_SHIFT))

    #if (CY_PSOC5A)
        /* Use CFG1 Mode bits to set run mode */
        /* As defined by Verilog Implementation */
        #define TimerTp_CTRL_MODE_SHIFT                 0x01u
        #define TimerTp_CTRL_MODE_MASK                 ((uint8)((uint8)0x07u << TimerTp_CTRL_MODE_SHIFT))
    #endif /* (CY_PSOC5A) */
    #if (CY_PSOC3 || CY_PSOC5LP)
        /* Control3 Register Bit Locations */
        #define TimerTp_CTRL_RCOD_SHIFT        0x02u
        #define TimerTp_CTRL_ENBL_SHIFT        0x00u
        #define TimerTp_CTRL_MODE_SHIFT        0x00u

        /* Control3 Register Bit Masks */
        #define TimerTp_CTRL_RCOD_MASK  ((uint8)((uint8)0x03u << TimerTp_CTRL_RCOD_SHIFT)) /* ROD and COD bit masks */
        #define TimerTp_CTRL_ENBL_MASK  ((uint8)((uint8)0x80u << TimerTp_CTRL_ENBL_SHIFT)) /* HW_EN bit mask */
        #define TimerTp_CTRL_MODE_MASK  ((uint8)((uint8)0x03u << TimerTp_CTRL_MODE_SHIFT)) /* Run mode bit mask */

        #define TimerTp_CTRL_RCOD       ((uint8)((uint8)0x03u << TimerTp_CTRL_RCOD_SHIFT))
        #define TimerTp_CTRL_ENBL       ((uint8)((uint8)0x80u << TimerTp_CTRL_ENBL_SHIFT))
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */

    /*RT1 Synch Constants: Applicable for PSoC3 and PSoC5LP */
    #define TimerTp_RT1_SHIFT                       0x04u
    /* Sync TC and CMP bit masks */
    #define TimerTp_RT1_MASK                        ((uint8)((uint8)0x03u << TimerTp_RT1_SHIFT))
    #define TimerTp_SYNC                            ((uint8)((uint8)0x03u << TimerTp_RT1_SHIFT))
    #define TimerTp_SYNCDSI_SHIFT                   0x00u
    /* Sync all DSI inputs with Mask  */
    #define TimerTp_SYNCDSI_MASK                    ((uint8)((uint8)0x0Fu << TimerTp_SYNCDSI_SHIFT))
    /* Sync all DSI inputs */
    #define TimerTp_SYNCDSI_EN                      ((uint8)((uint8)0x0Fu << TimerTp_SYNCDSI_SHIFT))

    #define TimerTp_CTRL_MODE_PULSEWIDTH            ((uint8)((uint8)0x01u << TimerTp_CTRL_MODE_SHIFT))
    #define TimerTp_CTRL_MODE_PERIOD                ((uint8)((uint8)0x02u << TimerTp_CTRL_MODE_SHIFT))
    #define TimerTp_CTRL_MODE_CONTINUOUS            ((uint8)((uint8)0x00u << TimerTp_CTRL_MODE_SHIFT))

    /* Status Register Bit Locations */
    /* As defined in Register Map, part of TMRX_SR0 register */
    #define TimerTp_STATUS_TC_SHIFT                 0x07u
    /* As defined in Register Map, part of TMRX_SR0 register, Shared with Compare Status */
    #define TimerTp_STATUS_CAPTURE_SHIFT            0x06u
    /* As defined in Register Map, part of TMRX_SR0 register */
    #define TimerTp_STATUS_TC_INT_MASK_SHIFT        (TimerTp_STATUS_TC_SHIFT - 0x04u)
    /* As defined in Register Map, part of TMRX_SR0 register, Shared with Compare Status */
    #define TimerTp_STATUS_CAPTURE_INT_MASK_SHIFT   (TimerTp_STATUS_CAPTURE_SHIFT - 0x04u)

    /* Status Register Bit Masks */
    #define TimerTp_STATUS_TC                       ((uint8)((uint8)0x01u << TimerTp_STATUS_TC_SHIFT))
    #define TimerTp_STATUS_CAPTURE                  ((uint8)((uint8)0x01u << TimerTp_STATUS_CAPTURE_SHIFT))
    /* Interrupt Enable Bit-Mask for interrupt on TC */
    #define TimerTp_STATUS_TC_INT_MASK              ((uint8)((uint8)0x01u << TimerTp_STATUS_TC_INT_MASK_SHIFT))
    /* Interrupt Enable Bit-Mask for interrupt on Capture */
    #define TimerTp_STATUS_CAPTURE_INT_MASK         ((uint8)((uint8)0x01u << TimerTp_STATUS_CAPTURE_INT_MASK_SHIFT))

#else   /* UDB Registers and Register Constants */


    /***************************************
    *           UDB Registers
    ***************************************/

    #define TimerTp_STATUS              (* (reg8 *) TimerTp_TimerUDB_rstSts_stsreg__STATUS_REG )
    #define TimerTp_STATUS_MASK         (* (reg8 *) TimerTp_TimerUDB_rstSts_stsreg__MASK_REG)
    #define TimerTp_STATUS_AUX_CTRL     (* (reg8 *) TimerTp_TimerUDB_rstSts_stsreg__STATUS_AUX_CTL_REG)
    #define TimerTp_CONTROL             (* (reg8 *) TimerTp_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG )
    
    #if(TimerTp_Resolution <= 8u) /* 8-bit Timer */
        #define TimerTp_CAPTURE_LSB         (* (reg8 *) TimerTp_TimerUDB_sT16_timerdp_u0__F0_REG )
        #define TimerTp_CAPTURE_LSB_PTR       ((reg8 *) TimerTp_TimerUDB_sT16_timerdp_u0__F0_REG )
        #define TimerTp_PERIOD_LSB          (* (reg8 *) TimerTp_TimerUDB_sT16_timerdp_u0__D0_REG )
        #define TimerTp_PERIOD_LSB_PTR        ((reg8 *) TimerTp_TimerUDB_sT16_timerdp_u0__D0_REG )
        #define TimerTp_COUNTER_LSB         (* (reg8 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
        #define TimerTp_COUNTER_LSB_PTR       ((reg8 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
    #elif(TimerTp_Resolution <= 16u) /* 8-bit Timer */
        #if(CY_PSOC3) /* 8-bit addres space */
            #define TimerTp_CAPTURE_LSB         (* (reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__F0_REG )
            #define TimerTp_CAPTURE_LSB_PTR       ((reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__F0_REG )
            #define TimerTp_PERIOD_LSB          (* (reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__D0_REG )
            #define TimerTp_PERIOD_LSB_PTR        ((reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__D0_REG )
            #define TimerTp_COUNTER_LSB         (* (reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
            #define TimerTp_COUNTER_LSB_PTR       ((reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
        #else /* 16-bit address space */
            #define TimerTp_CAPTURE_LSB         (* (reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__16BIT_F0_REG )
            #define TimerTp_CAPTURE_LSB_PTR       ((reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__16BIT_F0_REG )
            #define TimerTp_PERIOD_LSB          (* (reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__16BIT_D0_REG )
            #define TimerTp_PERIOD_LSB_PTR        ((reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__16BIT_D0_REG )
            #define TimerTp_COUNTER_LSB         (* (reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__16BIT_A0_REG )
            #define TimerTp_COUNTER_LSB_PTR       ((reg16 *) TimerTp_TimerUDB_sT16_timerdp_u0__16BIT_A0_REG )
        #endif /* CY_PSOC3 */
    #elif(TimerTp_Resolution <= 24u)/* 24-bit Timer */
        #define TimerTp_CAPTURE_LSB         (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__F0_REG )
        #define TimerTp_CAPTURE_LSB_PTR       ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__F0_REG )
        #define TimerTp_PERIOD_LSB          (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__D0_REG )
        #define TimerTp_PERIOD_LSB_PTR        ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__D0_REG )
        #define TimerTp_COUNTER_LSB         (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
        #define TimerTp_COUNTER_LSB_PTR       ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
    #else /* 32-bit Timer */
        #if(CY_PSOC3 || CY_PSOC5) /* 8-bit address space */
            #define TimerTp_CAPTURE_LSB         (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__F0_REG )
            #define TimerTp_CAPTURE_LSB_PTR       ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__F0_REG )
            #define TimerTp_PERIOD_LSB          (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__D0_REG )
            #define TimerTp_PERIOD_LSB_PTR        ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__D0_REG )
            #define TimerTp_COUNTER_LSB         (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
            #define TimerTp_COUNTER_LSB_PTR       ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
        #else /* 32-bit address space */
            #define TimerTp_CAPTURE_LSB         (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__32BIT_F0_REG )
            #define TimerTp_CAPTURE_LSB_PTR       ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__32BIT_F0_REG )
            #define TimerTp_PERIOD_LSB          (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__32BIT_D0_REG )
            #define TimerTp_PERIOD_LSB_PTR        ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__32BIT_D0_REG )
            #define TimerTp_COUNTER_LSB         (* (reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__32BIT_A0_REG )
            #define TimerTp_COUNTER_LSB_PTR       ((reg32 *) TimerTp_TimerUDB_sT16_timerdp_u0__32BIT_A0_REG )
        #endif /* CY_PSOC3 || CY_PSOC5 */ 
    #endif

    #define TimerTp_COUNTER_LSB_PTR_8BIT       ((reg8 *) TimerTp_TimerUDB_sT16_timerdp_u0__A0_REG )
    
    #if (TimerTp_UsingHWCaptureCounter)
        #define TimerTp_CAP_COUNT              (*(reg8 *) TimerTp_TimerUDB_sCapCount_counter__PERIOD_REG )
        #define TimerTp_CAP_COUNT_PTR          ( (reg8 *) TimerTp_TimerUDB_sCapCount_counter__PERIOD_REG )
        #define TimerTp_CAPTURE_COUNT_CTRL     (*(reg8 *) TimerTp_TimerUDB_sCapCount_counter__CONTROL_AUX_CTL_REG )
        #define TimerTp_CAPTURE_COUNT_CTRL_PTR ( (reg8 *) TimerTp_TimerUDB_sCapCount_counter__CONTROL_AUX_CTL_REG )
    #endif /* (TimerTp_UsingHWCaptureCounter) */


    /***************************************
    *       Register Constants
    ***************************************/

    /* Control Register Bit Locations */
    #define TimerTp_CTRL_INTCNT_SHIFT              0x00u       /* As defined by Verilog Implementation */
    #define TimerTp_CTRL_TRIG_MODE_SHIFT           0x02u       /* As defined by Verilog Implementation */
    #define TimerTp_CTRL_TRIG_EN_SHIFT             0x04u       /* As defined by Verilog Implementation */
    #define TimerTp_CTRL_CAP_MODE_SHIFT            0x05u       /* As defined by Verilog Implementation */
    #define TimerTp_CTRL_ENABLE_SHIFT              0x07u       /* As defined by Verilog Implementation */

    /* Control Register Bit Masks */
    #define TimerTp_CTRL_INTCNT_MASK               ((uint8)((uint8)0x03u << TimerTp_CTRL_INTCNT_SHIFT))
    #define TimerTp_CTRL_TRIG_MODE_MASK            ((uint8)((uint8)0x03u << TimerTp_CTRL_TRIG_MODE_SHIFT))
    #define TimerTp_CTRL_TRIG_EN                   ((uint8)((uint8)0x01u << TimerTp_CTRL_TRIG_EN_SHIFT))
    #define TimerTp_CTRL_CAP_MODE_MASK             ((uint8)((uint8)0x03u << TimerTp_CTRL_CAP_MODE_SHIFT))
    #define TimerTp_CTRL_ENABLE                    ((uint8)((uint8)0x01u << TimerTp_CTRL_ENABLE_SHIFT))

    /* Bit Counter (7-bit) Control Register Bit Definitions */
    /* As defined by the Register map for the AUX Control Register */
    #define TimerTp_CNTR_ENABLE                    0x20u

    /* Status Register Bit Locations */
    #define TimerTp_STATUS_TC_SHIFT                0x00u  /* As defined by Verilog Implementation */
    #define TimerTp_STATUS_CAPTURE_SHIFT           0x01u  /* As defined by Verilog Implementation */
    #define TimerTp_STATUS_TC_INT_MASK_SHIFT       TimerTp_STATUS_TC_SHIFT
    #define TimerTp_STATUS_CAPTURE_INT_MASK_SHIFT  TimerTp_STATUS_CAPTURE_SHIFT
    #define TimerTp_STATUS_FIFOFULL_SHIFT          0x02u  /* As defined by Verilog Implementation */
    #define TimerTp_STATUS_FIFONEMP_SHIFT          0x03u  /* As defined by Verilog Implementation */
    #define TimerTp_STATUS_FIFOFULL_INT_MASK_SHIFT TimerTp_STATUS_FIFOFULL_SHIFT

    /* Status Register Bit Masks */
    /* Sticky TC Event Bit-Mask */
    #define TimerTp_STATUS_TC                      ((uint8)((uint8)0x01u << TimerTp_STATUS_TC_SHIFT))
    /* Sticky Capture Event Bit-Mask */
    #define TimerTp_STATUS_CAPTURE                 ((uint8)((uint8)0x01u << TimerTp_STATUS_CAPTURE_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerTp_STATUS_TC_INT_MASK             ((uint8)((uint8)0x01u << TimerTp_STATUS_TC_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerTp_STATUS_CAPTURE_INT_MASK        ((uint8)((uint8)0x01u << TimerTp_STATUS_CAPTURE_SHIFT))
    /* NOT-Sticky FIFO Full Bit-Mask */
    #define TimerTp_STATUS_FIFOFULL                ((uint8)((uint8)0x01u << TimerTp_STATUS_FIFOFULL_SHIFT))
    /* NOT-Sticky FIFO Not Empty Bit-Mask */
    #define TimerTp_STATUS_FIFONEMP                ((uint8)((uint8)0x01u << TimerTp_STATUS_FIFONEMP_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerTp_STATUS_FIFOFULL_INT_MASK       ((uint8)((uint8)0x01u << TimerTp_STATUS_FIFOFULL_SHIFT))

    #define TimerTp_STATUS_ACTL_INT_EN             0x10u   /* As defined for the ACTL Register */

    /* Datapath Auxillary Control Register definitions */
    #define TimerTp_AUX_CTRL_FIFO0_CLR             0x01u   /* As defined by Register map */
    #define TimerTp_AUX_CTRL_FIFO1_CLR             0x02u   /* As defined by Register map */
    #define TimerTp_AUX_CTRL_FIFO0_LVL             0x04u   /* As defined by Register map */
    #define TimerTp_AUX_CTRL_FIFO1_LVL             0x08u   /* As defined by Register map */
    #define TimerTp_STATUS_ACTL_INT_EN_MASK        0x10u   /* As defined for the ACTL Register */

#endif /* Implementation Specific Registers and Register Constants */

#endif  /* CY_TIMER_TimerTp_H */


/* [] END OF FILE */
