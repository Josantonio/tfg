/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

uint8 paso = 1;

CY_ISR (ZC_Handler){
    UART_PutString("Ha cambiado comp\n");
    isr_zc_Disable();
    ScanComp_Stop();
    isr_zc_ClearPending();
    isr_zc_Enable();
    ScanComp_Start();
}

int main(void)
{
    WaveDAC8_Start();
    PGA_Start();
    ScanComp_Start();
    UART_Start();
    
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    isr_zc_StartEx(ZC_Handler);

    for(;;)
    {

        /*
        CyDelayUs(1667); //1667 us -> 1.66ms*6 = 10 ms -> f = 100 Hz
        paso++;
        
        if (paso >= 7){
            paso = 1;
        }
        */
    }
}

