/*******************************************************************************
* File Name: TimerTp_PM.c
* Version 2.80
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "TimerTp.h"

static TimerTp_backupStruct TimerTp_backup;


/*******************************************************************************
* Function Name: TimerTp_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerTp_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void TimerTp_SaveConfig(void) 
{
    #if (!TimerTp_UsingFixedFunction)
        TimerTp_backup.TimerUdb = TimerTp_ReadCounter();
        TimerTp_backup.InterruptMaskValue = TimerTp_STATUS_MASK;
        #if (TimerTp_UsingHWCaptureCounter)
            TimerTp_backup.TimerCaptureCounter = TimerTp_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!TimerTp_UDB_CONTROL_REG_REMOVED)
            TimerTp_backup.TimerControlRegister = TimerTp_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: TimerTp_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerTp_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void TimerTp_RestoreConfig(void) 
{   
    #if (!TimerTp_UsingFixedFunction)

        TimerTp_WriteCounter(TimerTp_backup.TimerUdb);
        TimerTp_STATUS_MASK =TimerTp_backup.InterruptMaskValue;
        #if (TimerTp_UsingHWCaptureCounter)
            TimerTp_SetCaptureCount(TimerTp_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!TimerTp_UDB_CONTROL_REG_REMOVED)
            TimerTp_WriteControlRegister(TimerTp_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: TimerTp_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerTp_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void TimerTp_Sleep(void) 
{
    #if(!TimerTp_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(TimerTp_CTRL_ENABLE == (TimerTp_CONTROL & TimerTp_CTRL_ENABLE))
        {
            /* Timer is enabled */
            TimerTp_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            TimerTp_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    TimerTp_Stop();
    TimerTp_SaveConfig();
}


/*******************************************************************************
* Function Name: TimerTp_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerTp_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void TimerTp_Wakeup(void) 
{
    TimerTp_RestoreConfig();
    #if(!TimerTp_UDB_CONTROL_REG_REMOVED)
        if(TimerTp_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                TimerTp_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */
