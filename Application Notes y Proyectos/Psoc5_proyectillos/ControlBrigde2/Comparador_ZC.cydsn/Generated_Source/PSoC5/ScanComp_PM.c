/*******************************************************************************
* File Name: ScanComp_PM.c
* Version 1.10
*
* Description:
*  This file provides Sleep APIs for Scanning Comparator component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ScanComp.h"

static ScanComp_BACKUP_STRUCT ScanComp_backup;


/*******************************************************************************
* Function Name: ScanComp_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for low power mode operation.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ScanComp_Sleep(void) 
{
    if(0u != (ScanComp_CONTROL_REG & ScanComp_CONTROL_ENABLE))
    {
        ScanComp_backup.enableState = 1u;
        ScanComp_Stop();
    }
    else /* The component is disabled */
    {
        ScanComp_backup.enableState = 0u;
    }

    ScanComp_Comp_Sleep();

    #if (ScanComp_INTERNAL_REF_IS_USED)
        ScanComp_VDAC_Sleep();
    #endif /* ScanComp_INTERNAL_REF_IS_USED */
}


/*******************************************************************************
* Function Name: ScanComp_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component to the state when ScanComp_Sleep() was called.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ScanComp_Wakeup(void) 
{
    ScanComp_Comp_Wakeup();
    
    #if (ScanComp_INTERNAL_REF_IS_USED)
        ScanComp_VDAC_Start();

        #if (ScanComp_VDAC_PER_CHANNEL_MODE_USED)
            ScanComp_Init();
        #endif /* ScanComp_VDAC_PER_CHANNEL_MODE_USED */
    #endif /* ScanComp_INTERNAL_REF_IS_USED */

    if(0u != ScanComp_backup.enableState)
    {
        ScanComp_Enable();
    }
}


/* [] END OF FILE */
