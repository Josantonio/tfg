/*******************************************************************************
* File Name: Neutro.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Neutro_H) /* Pins Neutro_H */
#define CY_PINS_Neutro_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Neutro_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Neutro__PORT == 15 && ((Neutro__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Neutro_Write(uint8 value);
void    Neutro_SetDriveMode(uint8 mode);
uint8   Neutro_ReadDataReg(void);
uint8   Neutro_Read(void);
void    Neutro_SetInterruptMode(uint16 position, uint16 mode);
uint8   Neutro_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Neutro_SetDriveMode() function.
     *  @{
     */
        #define Neutro_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Neutro_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Neutro_DM_RES_UP          PIN_DM_RES_UP
        #define Neutro_DM_RES_DWN         PIN_DM_RES_DWN
        #define Neutro_DM_OD_LO           PIN_DM_OD_LO
        #define Neutro_DM_OD_HI           PIN_DM_OD_HI
        #define Neutro_DM_STRONG          PIN_DM_STRONG
        #define Neutro_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Neutro_MASK               Neutro__MASK
#define Neutro_SHIFT              Neutro__SHIFT
#define Neutro_WIDTH              1u

/* Interrupt constants */
#if defined(Neutro__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Neutro_SetInterruptMode() function.
     *  @{
     */
        #define Neutro_INTR_NONE      (uint16)(0x0000u)
        #define Neutro_INTR_RISING    (uint16)(0x0001u)
        #define Neutro_INTR_FALLING   (uint16)(0x0002u)
        #define Neutro_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Neutro_INTR_MASK      (0x01u) 
#endif /* (Neutro__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Neutro_PS                     (* (reg8 *) Neutro__PS)
/* Data Register */
#define Neutro_DR                     (* (reg8 *) Neutro__DR)
/* Port Number */
#define Neutro_PRT_NUM                (* (reg8 *) Neutro__PRT) 
/* Connect to Analog Globals */                                                  
#define Neutro_AG                     (* (reg8 *) Neutro__AG)                       
/* Analog MUX bux enable */
#define Neutro_AMUX                   (* (reg8 *) Neutro__AMUX) 
/* Bidirectional Enable */                                                        
#define Neutro_BIE                    (* (reg8 *) Neutro__BIE)
/* Bit-mask for Aliased Register Access */
#define Neutro_BIT_MASK               (* (reg8 *) Neutro__BIT_MASK)
/* Bypass Enable */
#define Neutro_BYP                    (* (reg8 *) Neutro__BYP)
/* Port wide control signals */                                                   
#define Neutro_CTL                    (* (reg8 *) Neutro__CTL)
/* Drive Modes */
#define Neutro_DM0                    (* (reg8 *) Neutro__DM0) 
#define Neutro_DM1                    (* (reg8 *) Neutro__DM1)
#define Neutro_DM2                    (* (reg8 *) Neutro__DM2) 
/* Input Buffer Disable Override */
#define Neutro_INP_DIS                (* (reg8 *) Neutro__INP_DIS)
/* LCD Common or Segment Drive */
#define Neutro_LCD_COM_SEG            (* (reg8 *) Neutro__LCD_COM_SEG)
/* Enable Segment LCD */
#define Neutro_LCD_EN                 (* (reg8 *) Neutro__LCD_EN)
/* Slew Rate Control */
#define Neutro_SLW                    (* (reg8 *) Neutro__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Neutro_PRTDSI__CAPS_SEL       (* (reg8 *) Neutro__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Neutro_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Neutro__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Neutro_PRTDSI__OE_SEL0        (* (reg8 *) Neutro__PRTDSI__OE_SEL0) 
#define Neutro_PRTDSI__OE_SEL1        (* (reg8 *) Neutro__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Neutro_PRTDSI__OUT_SEL0       (* (reg8 *) Neutro__PRTDSI__OUT_SEL0) 
#define Neutro_PRTDSI__OUT_SEL1       (* (reg8 *) Neutro__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Neutro_PRTDSI__SYNC_OUT       (* (reg8 *) Neutro__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Neutro__SIO_CFG)
    #define Neutro_SIO_HYST_EN        (* (reg8 *) Neutro__SIO_HYST_EN)
    #define Neutro_SIO_REG_HIFREQ     (* (reg8 *) Neutro__SIO_REG_HIFREQ)
    #define Neutro_SIO_CFG            (* (reg8 *) Neutro__SIO_CFG)
    #define Neutro_SIO_DIFF           (* (reg8 *) Neutro__SIO_DIFF)
#endif /* (Neutro__SIO_CFG) */

/* Interrupt Registers */
#if defined(Neutro__INTSTAT)
    #define Neutro_INTSTAT            (* (reg8 *) Neutro__INTSTAT)
    #define Neutro_SNAP               (* (reg8 *) Neutro__SNAP)
    
	#define Neutro_0_INTTYPE_REG 		(* (reg8 *) Neutro__0__INTTYPE)
#endif /* (Neutro__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Neutro_H */


/* [] END OF FILE */
