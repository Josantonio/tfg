/*******************************************************************************
* File Name: Signal100Hz.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Signal100Hz_H) /* Pins Signal100Hz_H */
#define CY_PINS_Signal100Hz_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Signal100Hz_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Signal100Hz__PORT == 15 && ((Signal100Hz__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Signal100Hz_Write(uint8 value);
void    Signal100Hz_SetDriveMode(uint8 mode);
uint8   Signal100Hz_ReadDataReg(void);
uint8   Signal100Hz_Read(void);
void    Signal100Hz_SetInterruptMode(uint16 position, uint16 mode);
uint8   Signal100Hz_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Signal100Hz_SetDriveMode() function.
     *  @{
     */
        #define Signal100Hz_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Signal100Hz_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Signal100Hz_DM_RES_UP          PIN_DM_RES_UP
        #define Signal100Hz_DM_RES_DWN         PIN_DM_RES_DWN
        #define Signal100Hz_DM_OD_LO           PIN_DM_OD_LO
        #define Signal100Hz_DM_OD_HI           PIN_DM_OD_HI
        #define Signal100Hz_DM_STRONG          PIN_DM_STRONG
        #define Signal100Hz_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Signal100Hz_MASK               Signal100Hz__MASK
#define Signal100Hz_SHIFT              Signal100Hz__SHIFT
#define Signal100Hz_WIDTH              1u

/* Interrupt constants */
#if defined(Signal100Hz__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Signal100Hz_SetInterruptMode() function.
     *  @{
     */
        #define Signal100Hz_INTR_NONE      (uint16)(0x0000u)
        #define Signal100Hz_INTR_RISING    (uint16)(0x0001u)
        #define Signal100Hz_INTR_FALLING   (uint16)(0x0002u)
        #define Signal100Hz_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Signal100Hz_INTR_MASK      (0x01u) 
#endif /* (Signal100Hz__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Signal100Hz_PS                     (* (reg8 *) Signal100Hz__PS)
/* Data Register */
#define Signal100Hz_DR                     (* (reg8 *) Signal100Hz__DR)
/* Port Number */
#define Signal100Hz_PRT_NUM                (* (reg8 *) Signal100Hz__PRT) 
/* Connect to Analog Globals */                                                  
#define Signal100Hz_AG                     (* (reg8 *) Signal100Hz__AG)                       
/* Analog MUX bux enable */
#define Signal100Hz_AMUX                   (* (reg8 *) Signal100Hz__AMUX) 
/* Bidirectional Enable */                                                        
#define Signal100Hz_BIE                    (* (reg8 *) Signal100Hz__BIE)
/* Bit-mask for Aliased Register Access */
#define Signal100Hz_BIT_MASK               (* (reg8 *) Signal100Hz__BIT_MASK)
/* Bypass Enable */
#define Signal100Hz_BYP                    (* (reg8 *) Signal100Hz__BYP)
/* Port wide control signals */                                                   
#define Signal100Hz_CTL                    (* (reg8 *) Signal100Hz__CTL)
/* Drive Modes */
#define Signal100Hz_DM0                    (* (reg8 *) Signal100Hz__DM0) 
#define Signal100Hz_DM1                    (* (reg8 *) Signal100Hz__DM1)
#define Signal100Hz_DM2                    (* (reg8 *) Signal100Hz__DM2) 
/* Input Buffer Disable Override */
#define Signal100Hz_INP_DIS                (* (reg8 *) Signal100Hz__INP_DIS)
/* LCD Common or Segment Drive */
#define Signal100Hz_LCD_COM_SEG            (* (reg8 *) Signal100Hz__LCD_COM_SEG)
/* Enable Segment LCD */
#define Signal100Hz_LCD_EN                 (* (reg8 *) Signal100Hz__LCD_EN)
/* Slew Rate Control */
#define Signal100Hz_SLW                    (* (reg8 *) Signal100Hz__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Signal100Hz_PRTDSI__CAPS_SEL       (* (reg8 *) Signal100Hz__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Signal100Hz_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Signal100Hz__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Signal100Hz_PRTDSI__OE_SEL0        (* (reg8 *) Signal100Hz__PRTDSI__OE_SEL0) 
#define Signal100Hz_PRTDSI__OE_SEL1        (* (reg8 *) Signal100Hz__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Signal100Hz_PRTDSI__OUT_SEL0       (* (reg8 *) Signal100Hz__PRTDSI__OUT_SEL0) 
#define Signal100Hz_PRTDSI__OUT_SEL1       (* (reg8 *) Signal100Hz__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Signal100Hz_PRTDSI__SYNC_OUT       (* (reg8 *) Signal100Hz__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Signal100Hz__SIO_CFG)
    #define Signal100Hz_SIO_HYST_EN        (* (reg8 *) Signal100Hz__SIO_HYST_EN)
    #define Signal100Hz_SIO_REG_HIFREQ     (* (reg8 *) Signal100Hz__SIO_REG_HIFREQ)
    #define Signal100Hz_SIO_CFG            (* (reg8 *) Signal100Hz__SIO_CFG)
    #define Signal100Hz_SIO_DIFF           (* (reg8 *) Signal100Hz__SIO_DIFF)
#endif /* (Signal100Hz__SIO_CFG) */

/* Interrupt Registers */
#if defined(Signal100Hz__INTSTAT)
    #define Signal100Hz_INTSTAT            (* (reg8 *) Signal100Hz__INTSTAT)
    #define Signal100Hz_SNAP               (* (reg8 *) Signal100Hz__SNAP)
    
	#define Signal100Hz_0_INTTYPE_REG 		(* (reg8 *) Signal100Hz__0__INTTYPE)
#endif /* (Signal100Hz__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Signal100Hz_H */


/* [] END OF FILE */
