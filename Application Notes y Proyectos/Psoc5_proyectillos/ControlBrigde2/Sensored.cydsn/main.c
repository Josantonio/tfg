/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "math.h"

typedef unsigned char bool;
#define FALSE 0
#define TRUE (!FALSE)

bool state_button = FALSE; //Empezamos en OFF

/*** VEL ANGULAR Y Nº POLOS ***/
#define OMEGA_MAX   4000    //rpm
#define OMEGA_MIN   600     //rpm
#define P_POLOS     4       //n pares de polos
#define VDD         12.0      // Volts

/*** CLOCKS ***/
#define CLK_HALL    12.0e6  // 12 MHz
#define CLK_SPEED   1.0e6   // 1 MHz

#define TP_MIN      (float)(10.0/(P_POLOS*OMEGA_MAX))     // segundos
#define TP_MAX      (float)(10.0/(P_POLOS*OMEGA_MIN))     // segundos

#define NBITS_MAX   (float)(TP_MAX*CLK_HALL)    //n bits máx, luego se redondeará
#define NBITS_MIN   (float)(TP_MIN*CLK_HALL)    //n bits min
#define ADC_HALL_MAX_VALUE   (pow(2,ADC_Hall_DEFAULT_RESOLUTION)) // 2^12 = 4096
#define ADC_VREF_MAX_VALUE   (pow(2, ADC_Vref_DEFAULT_RESOLUTION)) //2^12

/**** Parámetros para la medida de la velocidad ****/
#define TIMER_PERIOD (uint16)(pow(2, Timer_speed_Resolution)) // 2^12 = 65536 bits

bool first_turn = TRUE; //Se activa para no contabilizar la primera captura en el primer giro, no completo
bool first_time = TRUE;
uint16 periodos_count = 0; //Nº de veces que cuenta un período el timer, y no se ha dado una captura
// este parámetro incrementará indefinidamente mientras el motor esté parado
//llegará a valores altos a baja velocidad de rotación
uint16 T0,T1; //Variable para almacenar el valor de la primera captura
float delay; //Variable para almacenar el tiempo transcurrido desde la captura actual a la anterior
float w_real; //Velocidad del motor calculada por los sensores hall (rpm)
float w_ref; //Velocidad de referencia, leída por el potenciómetro, ajustada por el usuario
uint8 duty; //Duty calculado para escribir el PWM, n bits
float duty_perc; //Porcentaje de duty, salida del PID


CY_ISR(isr_button_handler){
    state_button = ~state_button; //Cambiamos el estado del botón cada vez que se pulsa
}

CY_ISR(isr_speed_handler){
    /* Hay que averiguar por qué se ha dado la interrupción */
    
    /* Problema -> si leemos el registro se borra, entonces lo vamos a almacenar        *
     * en un avariable intermedia de 8 bits para que aunque se borre, podemos comparar  *
     * su valor en el segundo if                                                        *
     */
    uint8 status_register = 0x00;
    
    status_register = Timer_speed_ReadStatusRegister(); //Se borra y se limpia la interrupción
    
    /*** INTERRUPCIÓN DEBIDA A TC ***/
    
    //Si la interrupción es porque ha llegado al final de la cuenta, y además el motor está en marcha
    // de esta forma evitamos que el timer cuente cuando esté el motor parado y la variable se desborde
     if ((status_register & Timer_speed_STATUS_TC) && state_button){
        periodos_count++;
      // UART_PutString("\n\nTC\n\n");
    }
  
    /*** INTERRUPCIÓN DEBIDA A CAPTURA ***/
    
    else if ((status_register & Timer_speed_STATUS_CAPTURE) && state_button){
      //   UART_PutString("\n\nCAPTURE\n\n");
        
        if (first_turn){ //Es la primera vez
            first_turn = FALSE; //Cambiamos variable para mostrar que la próxima vez ya no 
            //será la primera
            T0 = Timer_speed_ReadCapture(); //guardamos la captura, está en n*bits
        }else if (!first_turn){
            // Si se produce la segunda catura se calcula el tiempo transcurrido desde que se dió la primera
            T1 = Timer_speed_ReadCapture();
            delay = ((T0+periodos_count*TIMER_PERIOD-T1)/CLK_SPEED);
            T0 = T1; //Actualizamos T0
            periodos_count = 0; //Limpiamos variable que cuenta los períodos almacenados
            w_real = 60.0/(P_POLOS*delay); //Vel obtenida [rpm]        
        }
        
    }
}

    uint8 pasos[6] = {1,5,4,6,2,3};
    uint8 j = 0;
    uint8 PWM_write[21] = {22,23,24,25,26,28,29,30,31,32,33,35,36,37,38,39,41,42,43,44,45};
    uint16 retardo[21] = {37500,25862,19737,15957,13393,11538,10135,9036,8152,7426,6818,6303,5859,5474,5137,4839,4573,4335,4121,3927,3750};

void arranque (void){
 

    
    //Alignment
    Control_Reg_Write(0);
    Control_LUT_Write(pasos[5]);
    PWM_WriteCompare(22);
    CyDelay(1000);
    
    for (uint8 i = 0 ; i <=20 ; i++){
        Control_LUT_Write(pasos[j]);
        PWM_WriteCompare(PWM_write[i]);
        CyDelayUs(retardo[i]);
        j++;
        if(j == 6){ j = 0;}
    }
//    Control_LUT_Write(0x00);
//    Control_Reg_Write(1);
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /**** VARIABLES PARA LA SEÑAL SIMULADA DE HALL *****/
    uint16 adc_hall_value; //N de bits reibidos por el ADC
    uint16 pwm_hall_write;
    uint16 duty_hall_write;
    
    
    char str[100]; //Para representar datos vía serial
    
    /* INICIALIZACIÓN COMPONENTES*/
    PWM_Start();
    UART_Start();
    PWM_clock_Start();
    ADC_Hall_Start();
    ADC_Vref_Start();
    Timer_speed_Start();
    
    PWM_probe_Start();

    Control_Reg_Write(0);

    isr_button_StartEx(isr_button_handler); //Cabecera interrupción para el botón
    isr_speed_StartEx(isr_speed_handler); //Cabecera para la medición de la velocidad
    
    
    for(;;)
    {
        /************ CONTROL DEL USUARIO ***********/
        if(state_button){ //Si está en estado ON
            Pin_LED_Write(1);
            if(first_time){
                arranque();
                first_time = FALSE;
            }else if (!first_time){
                Control_Reg_Write(0);
                Control_LUT_Write(pasos[j]);
                CyDelayUs(retardo[20]);
                j++;
                if(j == 6){ j = 0;}            
            }
        }else if (!state_button){ //Si está en OFF
            
            /* RESETEO VARIABLES */
            Pin_LED_Write(0);
            Control_Reg_Write(0);
            first_turn = TRUE; //Volvemos a activar la variable que nos indica que es la primera vuelta
            first_time = TRUE;
        }
        
       
        // SIMULACIÓN SENSORES HALL 
        ADC_Hall_StartConvert();
        ADC_Hall_IsEndConversion(ADC_Hall_WAIT_FOR_RESULT);
        adc_hall_value = ADC_Hall_GetResult16();
        pwm_hall_write = round(NBITS_MAX-((ADC_HALL_MAX_VALUE-adc_hall_value)*(NBITS_MAX-NBITS_MIN)/ADC_HALL_MAX_VALUE));
        duty_hall_write = round(pwm_hall_write/2);
        
        // Asignamos los resultados al PWM que controla las señales Hall 
        PWM_clock_WriteCompare(duty_hall_write);
        PWM_clock_WritePeriod(pwm_hall_write);
        
       UART_PutString("HALL SIGNAL:\n");
       sprintf(str,"ADC Value: %d, PWM_write: %d, Duty: %d\n", adc_hall_value, pwm_hall_write, duty_hall_write);
       UART_PutString(str);
       
    
        /*
        // MEDIDA DE LA VELOCIDAD FÍSICA DEL MOTOR 
        
       UART_PutString("SPEED MEASUREMENT:\n");
       sprintf(str, "Velocidad mecanica: %0.f rpm, First turn: %d\n", w_real, first_turn);
       UART_PutString(str);

        */
        /**** MEDIDA DE LA VELOCIDAD DE REFERENCIA *********/
        ADC_Vref_StartConvert(); 
        ADC_Vref_IsEndConversion(ADC_Vref_WAIT_FOR_RESULT);
        w_ref = OMEGA_MIN+ADC_Vref_GetResult16()*(OMEGA_MAX-OMEGA_MIN)/ADC_VREF_MAX_VALUE;

        /*
        duty_perc = w_ref/(3.35*VDD);
        duty = duty_perc*ADC_VREF_MAX_VALUE/100;
        
        PWM_WriteCompare(duty);
        
        UART_PutString("SPEED REFERENCE:\n");
        sprintf(str, "Velocidad referencia: %0.f\n Duty perc: %0.2f %%, Duty: %d\n", w_ref, duty_perc, duty);
        UART_PutString(str);

        */

      //  UART_PutString("*********************\n");
      // CyDelay(1000);
    }
}

/* [] END OF FILE */
