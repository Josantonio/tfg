/*******************************************************************************
* File Name: PWM_Hall_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "PWM_Hall.h"

static PWM_Hall_backupStruct PWM_Hall_backup;


/*******************************************************************************
* Function Name: PWM_Hall_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_Hall_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void PWM_Hall_SaveConfig(void) 
{

    #if(!PWM_Hall_UsingFixedFunction)
        #if(!PWM_Hall_PWMModeIsCenterAligned)
            PWM_Hall_backup.PWMPeriod = PWM_Hall_ReadPeriod();
        #endif /* (!PWM_Hall_PWMModeIsCenterAligned) */
        PWM_Hall_backup.PWMUdb = PWM_Hall_ReadCounter();
        #if (PWM_Hall_UseStatus)
            PWM_Hall_backup.InterruptMaskValue = PWM_Hall_STATUS_MASK;
        #endif /* (PWM_Hall_UseStatus) */

        #if(PWM_Hall_DeadBandMode == PWM_Hall__B_PWM__DBM_256_CLOCKS || \
            PWM_Hall_DeadBandMode == PWM_Hall__B_PWM__DBM_2_4_CLOCKS)
            PWM_Hall_backup.PWMdeadBandValue = PWM_Hall_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(PWM_Hall_KillModeMinTime)
             PWM_Hall_backup.PWMKillCounterPeriod = PWM_Hall_ReadKillTime();
        #endif /* (PWM_Hall_KillModeMinTime) */

        #if(PWM_Hall_UseControl)
            PWM_Hall_backup.PWMControlRegister = PWM_Hall_ReadControlRegister();
        #endif /* (PWM_Hall_UseControl) */
    #endif  /* (!PWM_Hall_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PWM_Hall_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_Hall_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_Hall_RestoreConfig(void) 
{
        #if(!PWM_Hall_UsingFixedFunction)
            #if(!PWM_Hall_PWMModeIsCenterAligned)
                PWM_Hall_WritePeriod(PWM_Hall_backup.PWMPeriod);
            #endif /* (!PWM_Hall_PWMModeIsCenterAligned) */

            PWM_Hall_WriteCounter(PWM_Hall_backup.PWMUdb);

            #if (PWM_Hall_UseStatus)
                PWM_Hall_STATUS_MASK = PWM_Hall_backup.InterruptMaskValue;
            #endif /* (PWM_Hall_UseStatus) */

            #if(PWM_Hall_DeadBandMode == PWM_Hall__B_PWM__DBM_256_CLOCKS || \
                PWM_Hall_DeadBandMode == PWM_Hall__B_PWM__DBM_2_4_CLOCKS)
                PWM_Hall_WriteDeadTime(PWM_Hall_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(PWM_Hall_KillModeMinTime)
                PWM_Hall_WriteKillTime(PWM_Hall_backup.PWMKillCounterPeriod);
            #endif /* (PWM_Hall_KillModeMinTime) */

            #if(PWM_Hall_UseControl)
                PWM_Hall_WriteControlRegister(PWM_Hall_backup.PWMControlRegister);
            #endif /* (PWM_Hall_UseControl) */
        #endif  /* (!PWM_Hall_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: PWM_Hall_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_Hall_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void PWM_Hall_Sleep(void) 
{
    #if(PWM_Hall_UseControl)
        if(PWM_Hall_CTRL_ENABLE == (PWM_Hall_CONTROL & PWM_Hall_CTRL_ENABLE))
        {
            /*Component is enabled */
            PWM_Hall_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            PWM_Hall_backup.PWMEnableState = 0u;
        }
    #endif /* (PWM_Hall_UseControl) */

    /* Stop component */
    PWM_Hall_Stop();

    /* Save registers configuration */
    PWM_Hall_SaveConfig();
}


/*******************************************************************************
* Function Name: PWM_Hall_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_Hall_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_Hall_Wakeup(void) 
{
     /* Restore registers values */
    PWM_Hall_RestoreConfig();

    if(PWM_Hall_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        PWM_Hall_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
