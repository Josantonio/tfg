/*******************************************************************************
* File Name: Hall3.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Hall3_H) /* Pins Hall3_H */
#define CY_PINS_Hall3_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Hall3_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Hall3__PORT == 15 && ((Hall3__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Hall3_Write(uint8 value);
void    Hall3_SetDriveMode(uint8 mode);
uint8   Hall3_ReadDataReg(void);
uint8   Hall3_Read(void);
void    Hall3_SetInterruptMode(uint16 position, uint16 mode);
uint8   Hall3_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Hall3_SetDriveMode() function.
     *  @{
     */
        #define Hall3_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Hall3_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Hall3_DM_RES_UP          PIN_DM_RES_UP
        #define Hall3_DM_RES_DWN         PIN_DM_RES_DWN
        #define Hall3_DM_OD_LO           PIN_DM_OD_LO
        #define Hall3_DM_OD_HI           PIN_DM_OD_HI
        #define Hall3_DM_STRONG          PIN_DM_STRONG
        #define Hall3_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Hall3_MASK               Hall3__MASK
#define Hall3_SHIFT              Hall3__SHIFT
#define Hall3_WIDTH              1u

/* Interrupt constants */
#if defined(Hall3__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Hall3_SetInterruptMode() function.
     *  @{
     */
        #define Hall3_INTR_NONE      (uint16)(0x0000u)
        #define Hall3_INTR_RISING    (uint16)(0x0001u)
        #define Hall3_INTR_FALLING   (uint16)(0x0002u)
        #define Hall3_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Hall3_INTR_MASK      (0x01u) 
#endif /* (Hall3__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Hall3_PS                     (* (reg8 *) Hall3__PS)
/* Data Register */
#define Hall3_DR                     (* (reg8 *) Hall3__DR)
/* Port Number */
#define Hall3_PRT_NUM                (* (reg8 *) Hall3__PRT) 
/* Connect to Analog Globals */                                                  
#define Hall3_AG                     (* (reg8 *) Hall3__AG)                       
/* Analog MUX bux enable */
#define Hall3_AMUX                   (* (reg8 *) Hall3__AMUX) 
/* Bidirectional Enable */                                                        
#define Hall3_BIE                    (* (reg8 *) Hall3__BIE)
/* Bit-mask for Aliased Register Access */
#define Hall3_BIT_MASK               (* (reg8 *) Hall3__BIT_MASK)
/* Bypass Enable */
#define Hall3_BYP                    (* (reg8 *) Hall3__BYP)
/* Port wide control signals */                                                   
#define Hall3_CTL                    (* (reg8 *) Hall3__CTL)
/* Drive Modes */
#define Hall3_DM0                    (* (reg8 *) Hall3__DM0) 
#define Hall3_DM1                    (* (reg8 *) Hall3__DM1)
#define Hall3_DM2                    (* (reg8 *) Hall3__DM2) 
/* Input Buffer Disable Override */
#define Hall3_INP_DIS                (* (reg8 *) Hall3__INP_DIS)
/* LCD Common or Segment Drive */
#define Hall3_LCD_COM_SEG            (* (reg8 *) Hall3__LCD_COM_SEG)
/* Enable Segment LCD */
#define Hall3_LCD_EN                 (* (reg8 *) Hall3__LCD_EN)
/* Slew Rate Control */
#define Hall3_SLW                    (* (reg8 *) Hall3__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Hall3_PRTDSI__CAPS_SEL       (* (reg8 *) Hall3__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Hall3_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Hall3__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Hall3_PRTDSI__OE_SEL0        (* (reg8 *) Hall3__PRTDSI__OE_SEL0) 
#define Hall3_PRTDSI__OE_SEL1        (* (reg8 *) Hall3__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Hall3_PRTDSI__OUT_SEL0       (* (reg8 *) Hall3__PRTDSI__OUT_SEL0) 
#define Hall3_PRTDSI__OUT_SEL1       (* (reg8 *) Hall3__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Hall3_PRTDSI__SYNC_OUT       (* (reg8 *) Hall3__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Hall3__SIO_CFG)
    #define Hall3_SIO_HYST_EN        (* (reg8 *) Hall3__SIO_HYST_EN)
    #define Hall3_SIO_REG_HIFREQ     (* (reg8 *) Hall3__SIO_REG_HIFREQ)
    #define Hall3_SIO_CFG            (* (reg8 *) Hall3__SIO_CFG)
    #define Hall3_SIO_DIFF           (* (reg8 *) Hall3__SIO_DIFF)
#endif /* (Hall3__SIO_CFG) */

/* Interrupt Registers */
#if defined(Hall3__INTSTAT)
    #define Hall3_INTSTAT            (* (reg8 *) Hall3__INTSTAT)
    #define Hall3_SNAP               (* (reg8 *) Hall3__SNAP)
    
	#define Hall3_0_INTTYPE_REG 		(* (reg8 *) Hall3__0__INTTYPE)
#endif /* (Hall3__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Hall3_H */


/* [] END OF FILE */
