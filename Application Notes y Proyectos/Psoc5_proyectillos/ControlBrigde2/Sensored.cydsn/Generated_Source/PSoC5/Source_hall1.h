/*******************************************************************************
* File Name: Source_hall1.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Source_hall1_H) /* Pins Source_hall1_H */
#define CY_PINS_Source_hall1_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Source_hall1_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Source_hall1__PORT == 15 && ((Source_hall1__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Source_hall1_Write(uint8 value);
void    Source_hall1_SetDriveMode(uint8 mode);
uint8   Source_hall1_ReadDataReg(void);
uint8   Source_hall1_Read(void);
void    Source_hall1_SetInterruptMode(uint16 position, uint16 mode);
uint8   Source_hall1_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Source_hall1_SetDriveMode() function.
     *  @{
     */
        #define Source_hall1_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Source_hall1_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Source_hall1_DM_RES_UP          PIN_DM_RES_UP
        #define Source_hall1_DM_RES_DWN         PIN_DM_RES_DWN
        #define Source_hall1_DM_OD_LO           PIN_DM_OD_LO
        #define Source_hall1_DM_OD_HI           PIN_DM_OD_HI
        #define Source_hall1_DM_STRONG          PIN_DM_STRONG
        #define Source_hall1_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Source_hall1_MASK               Source_hall1__MASK
#define Source_hall1_SHIFT              Source_hall1__SHIFT
#define Source_hall1_WIDTH              1u

/* Interrupt constants */
#if defined(Source_hall1__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Source_hall1_SetInterruptMode() function.
     *  @{
     */
        #define Source_hall1_INTR_NONE      (uint16)(0x0000u)
        #define Source_hall1_INTR_RISING    (uint16)(0x0001u)
        #define Source_hall1_INTR_FALLING   (uint16)(0x0002u)
        #define Source_hall1_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Source_hall1_INTR_MASK      (0x01u) 
#endif /* (Source_hall1__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Source_hall1_PS                     (* (reg8 *) Source_hall1__PS)
/* Data Register */
#define Source_hall1_DR                     (* (reg8 *) Source_hall1__DR)
/* Port Number */
#define Source_hall1_PRT_NUM                (* (reg8 *) Source_hall1__PRT) 
/* Connect to Analog Globals */                                                  
#define Source_hall1_AG                     (* (reg8 *) Source_hall1__AG)                       
/* Analog MUX bux enable */
#define Source_hall1_AMUX                   (* (reg8 *) Source_hall1__AMUX) 
/* Bidirectional Enable */                                                        
#define Source_hall1_BIE                    (* (reg8 *) Source_hall1__BIE)
/* Bit-mask for Aliased Register Access */
#define Source_hall1_BIT_MASK               (* (reg8 *) Source_hall1__BIT_MASK)
/* Bypass Enable */
#define Source_hall1_BYP                    (* (reg8 *) Source_hall1__BYP)
/* Port wide control signals */                                                   
#define Source_hall1_CTL                    (* (reg8 *) Source_hall1__CTL)
/* Drive Modes */
#define Source_hall1_DM0                    (* (reg8 *) Source_hall1__DM0) 
#define Source_hall1_DM1                    (* (reg8 *) Source_hall1__DM1)
#define Source_hall1_DM2                    (* (reg8 *) Source_hall1__DM2) 
/* Input Buffer Disable Override */
#define Source_hall1_INP_DIS                (* (reg8 *) Source_hall1__INP_DIS)
/* LCD Common or Segment Drive */
#define Source_hall1_LCD_COM_SEG            (* (reg8 *) Source_hall1__LCD_COM_SEG)
/* Enable Segment LCD */
#define Source_hall1_LCD_EN                 (* (reg8 *) Source_hall1__LCD_EN)
/* Slew Rate Control */
#define Source_hall1_SLW                    (* (reg8 *) Source_hall1__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Source_hall1_PRTDSI__CAPS_SEL       (* (reg8 *) Source_hall1__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Source_hall1_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Source_hall1__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Source_hall1_PRTDSI__OE_SEL0        (* (reg8 *) Source_hall1__PRTDSI__OE_SEL0) 
#define Source_hall1_PRTDSI__OE_SEL1        (* (reg8 *) Source_hall1__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Source_hall1_PRTDSI__OUT_SEL0       (* (reg8 *) Source_hall1__PRTDSI__OUT_SEL0) 
#define Source_hall1_PRTDSI__OUT_SEL1       (* (reg8 *) Source_hall1__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Source_hall1_PRTDSI__SYNC_OUT       (* (reg8 *) Source_hall1__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Source_hall1__SIO_CFG)
    #define Source_hall1_SIO_HYST_EN        (* (reg8 *) Source_hall1__SIO_HYST_EN)
    #define Source_hall1_SIO_REG_HIFREQ     (* (reg8 *) Source_hall1__SIO_REG_HIFREQ)
    #define Source_hall1_SIO_CFG            (* (reg8 *) Source_hall1__SIO_CFG)
    #define Source_hall1_SIO_DIFF           (* (reg8 *) Source_hall1__SIO_DIFF)
#endif /* (Source_hall1__SIO_CFG) */

/* Interrupt Registers */
#if defined(Source_hall1__INTSTAT)
    #define Source_hall1_INTSTAT            (* (reg8 *) Source_hall1__INTSTAT)
    #define Source_hall1_SNAP               (* (reg8 *) Source_hall1__SNAP)
    
	#define Source_hall1_0_INTTYPE_REG 		(* (reg8 *) Source_hall1__0__INTTYPE)
#endif /* (Source_hall1__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Source_hall1_H */


/* [] END OF FILE */
