/*******************************************************************************
* File Name: PWM_probe_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "PWM_probe.h"

static PWM_probe_backupStruct PWM_probe_backup;


/*******************************************************************************
* Function Name: PWM_probe_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_probe_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void PWM_probe_SaveConfig(void) 
{

    #if(!PWM_probe_UsingFixedFunction)
        #if(!PWM_probe_PWMModeIsCenterAligned)
            PWM_probe_backup.PWMPeriod = PWM_probe_ReadPeriod();
        #endif /* (!PWM_probe_PWMModeIsCenterAligned) */
        PWM_probe_backup.PWMUdb = PWM_probe_ReadCounter();
        #if (PWM_probe_UseStatus)
            PWM_probe_backup.InterruptMaskValue = PWM_probe_STATUS_MASK;
        #endif /* (PWM_probe_UseStatus) */

        #if(PWM_probe_DeadBandMode == PWM_probe__B_PWM__DBM_256_CLOCKS || \
            PWM_probe_DeadBandMode == PWM_probe__B_PWM__DBM_2_4_CLOCKS)
            PWM_probe_backup.PWMdeadBandValue = PWM_probe_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(PWM_probe_KillModeMinTime)
             PWM_probe_backup.PWMKillCounterPeriod = PWM_probe_ReadKillTime();
        #endif /* (PWM_probe_KillModeMinTime) */

        #if(PWM_probe_UseControl)
            PWM_probe_backup.PWMControlRegister = PWM_probe_ReadControlRegister();
        #endif /* (PWM_probe_UseControl) */
    #endif  /* (!PWM_probe_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PWM_probe_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_probe_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_probe_RestoreConfig(void) 
{
        #if(!PWM_probe_UsingFixedFunction)
            #if(!PWM_probe_PWMModeIsCenterAligned)
                PWM_probe_WritePeriod(PWM_probe_backup.PWMPeriod);
            #endif /* (!PWM_probe_PWMModeIsCenterAligned) */

            PWM_probe_WriteCounter(PWM_probe_backup.PWMUdb);

            #if (PWM_probe_UseStatus)
                PWM_probe_STATUS_MASK = PWM_probe_backup.InterruptMaskValue;
            #endif /* (PWM_probe_UseStatus) */

            #if(PWM_probe_DeadBandMode == PWM_probe__B_PWM__DBM_256_CLOCKS || \
                PWM_probe_DeadBandMode == PWM_probe__B_PWM__DBM_2_4_CLOCKS)
                PWM_probe_WriteDeadTime(PWM_probe_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(PWM_probe_KillModeMinTime)
                PWM_probe_WriteKillTime(PWM_probe_backup.PWMKillCounterPeriod);
            #endif /* (PWM_probe_KillModeMinTime) */

            #if(PWM_probe_UseControl)
                PWM_probe_WriteControlRegister(PWM_probe_backup.PWMControlRegister);
            #endif /* (PWM_probe_UseControl) */
        #endif  /* (!PWM_probe_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: PWM_probe_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_probe_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void PWM_probe_Sleep(void) 
{
    #if(PWM_probe_UseControl)
        if(PWM_probe_CTRL_ENABLE == (PWM_probe_CONTROL & PWM_probe_CTRL_ENABLE))
        {
            /*Component is enabled */
            PWM_probe_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            PWM_probe_backup.PWMEnableState = 0u;
        }
    #endif /* (PWM_probe_UseControl) */

    /* Stop component */
    PWM_probe_Stop();

    /* Save registers configuration */
    PWM_probe_SaveConfig();
}


/*******************************************************************************
* Function Name: PWM_probe_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_probe_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_probe_Wakeup(void) 
{
     /* Restore registers values */
    PWM_probe_RestoreConfig();

    if(PWM_probe_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        PWM_probe_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
