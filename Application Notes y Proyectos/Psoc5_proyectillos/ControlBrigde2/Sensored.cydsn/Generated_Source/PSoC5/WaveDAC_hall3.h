/*******************************************************************************
* File Name: WaveDAC_hall3.h  
* Version 2.10
*
* Description:
*  This file contains the function prototypes and constants used in
*  the 8-bit Waveform DAC (WaveDAC8) Component.
*
********************************************************************************
* Copyright 2013, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_WaveDAC8_WaveDAC_hall3_H) 
#define CY_WaveDAC8_WaveDAC_hall3_H

#include "cytypes.h"
#include "cyfitter.h"
#include <WaveDAC_hall3_Wave1_DMA_dma.h>
#include <WaveDAC_hall3_Wave2_DMA_dma.h>
#include <WaveDAC_hall3_VDAC8.h>


/***************************************
*  Initial Parameter Constants
***************************************/

#define WaveDAC_hall3_WAVE1_TYPE     (1u)     /* Waveform for wave1 */
#define WaveDAC_hall3_WAVE2_TYPE     (2u)     /* Waveform for wave2 */
#define WaveDAC_hall3_SINE_WAVE      (0u)
#define WaveDAC_hall3_SQUARE_WAVE    (1u)
#define WaveDAC_hall3_TRIANGLE_WAVE  (2u)
#define WaveDAC_hall3_SAWTOOTH_WAVE  (3u)
#define WaveDAC_hall3_ARB_DRAW_WAVE  (10u) /* Arbitrary (draw) */
#define WaveDAC_hall3_ARB_FILE_WAVE  (11u) /* Arbitrary (from file) */

#define WaveDAC_hall3_WAVE1_LENGTH   (100u)   /* Length for wave1 */
#define WaveDAC_hall3_WAVE2_LENGTH   (100u)   /* Length for wave2 */
	
#define WaveDAC_hall3_DEFAULT_RANGE    (1u) /* Default DAC range */
#define WaveDAC_hall3_DAC_RANGE_1V     (0u)
#define WaveDAC_hall3_DAC_RANGE_1V_BUF (16u)
#define WaveDAC_hall3_DAC_RANGE_4V     (1u)
#define WaveDAC_hall3_DAC_RANGE_4V_BUF (17u)
#define WaveDAC_hall3_VOLT_MODE        (0u)
#define WaveDAC_hall3_CURRENT_MODE     (1u)
#define WaveDAC_hall3_DAC_MODE         (((WaveDAC_hall3_DEFAULT_RANGE == WaveDAC_hall3_DAC_RANGE_1V) || \
									  (WaveDAC_hall3_DEFAULT_RANGE == WaveDAC_hall3_DAC_RANGE_4V) || \
							  		  (WaveDAC_hall3_DEFAULT_RANGE == WaveDAC_hall3_DAC_RANGE_1V_BUF) || \
									  (WaveDAC_hall3_DEFAULT_RANGE == WaveDAC_hall3_DAC_RANGE_4V_BUF)) ? \
									   WaveDAC_hall3_VOLT_MODE : WaveDAC_hall3_CURRENT_MODE)

#define WaveDAC_hall3_DACMODE WaveDAC_hall3_DAC_MODE /* legacy definition for backward compatibility */

#define WaveDAC_hall3_DIRECT_MODE (0u)
#define WaveDAC_hall3_BUFFER_MODE (1u)
#define WaveDAC_hall3_OUT_MODE    (((WaveDAC_hall3_DEFAULT_RANGE == WaveDAC_hall3_DAC_RANGE_1V_BUF) || \
								 (WaveDAC_hall3_DEFAULT_RANGE == WaveDAC_hall3_DAC_RANGE_4V_BUF)) ? \
								  WaveDAC_hall3_BUFFER_MODE : WaveDAC_hall3_DIRECT_MODE)

#if(WaveDAC_hall3_OUT_MODE == WaveDAC_hall3_BUFFER_MODE)
    #include <WaveDAC_hall3_BuffAmp.h>
#endif /* WaveDAC_hall3_OUT_MODE == WaveDAC_hall3_BUFFER_MODE */

#define WaveDAC_hall3_CLOCK_INT      (1u)
#define WaveDAC_hall3_CLOCK_EXT      (0u)
#define WaveDAC_hall3_CLOCK_SRC      (0u)

#if(WaveDAC_hall3_CLOCK_SRC == WaveDAC_hall3_CLOCK_INT)  
	#include <WaveDAC_hall3_DacClk.h>
	#if defined(WaveDAC_hall3_DacClk_PHASE)
		#define WaveDAC_hall3_CLK_PHASE_0nS (1u)
	#endif /* defined(WaveDAC_hall3_DacClk_PHASE) */
#endif /* WaveDAC_hall3_CLOCK_SRC == WaveDAC_hall3_CLOCK_INT */

#if (CY_PSOC3)
	#define WaveDAC_hall3_HI16FLASHPTR   (0xFFu)
#endif /* CY_PSOC3 */

#define WaveDAC_hall3_Wave1_DMA_BYTES_PER_BURST      (1u)
#define WaveDAC_hall3_Wave1_DMA_REQUEST_PER_BURST    (1u)
#define WaveDAC_hall3_Wave2_DMA_BYTES_PER_BURST      (1u)
#define WaveDAC_hall3_Wave2_DMA_REQUEST_PER_BURST    (1u)


/***************************************
*   Data Struct Definition
***************************************/

/* Low power Mode API Support */
typedef struct
{
	uint8   enableState;
}WaveDAC_hall3_BACKUP_STRUCT;


/***************************************
*        Function Prototypes 
***************************************/

void WaveDAC_hall3_Start(void)             ;
void WaveDAC_hall3_StartEx(const uint8 * wavePtr1, uint16 sampleSize1, const uint8 * wavePtr2, uint16 sampleSize2)
                                        ;
void WaveDAC_hall3_Init(void)              ;
void WaveDAC_hall3_Enable(void)            ;
void WaveDAC_hall3_Stop(void)              ;

void WaveDAC_hall3_Wave1Setup(const uint8 * wavePtr, uint16 sampleSize)
                                        ;
void WaveDAC_hall3_Wave2Setup(const uint8 * wavePtr, uint16 sampleSize)
                                        ;

void WaveDAC_hall3_Sleep(void)             ;
void WaveDAC_hall3_Wakeup(void)            ;

#define WaveDAC_hall3_SetSpeed       WaveDAC_hall3_VDAC8_SetSpeed
#define WaveDAC_hall3_SetRange       WaveDAC_hall3_VDAC8_SetRange
#define WaveDAC_hall3_SetValue       WaveDAC_hall3_VDAC8_SetValue
#define WaveDAC_hall3_DacTrim        WaveDAC_hall3_VDAC8_DacTrim
#define WaveDAC_hall3_SaveConfig     WaveDAC_hall3_VDAC8_SaveConfig
#define WaveDAC_hall3_RestoreConfig  WaveDAC_hall3_VDAC8_RestoreConfig


/***************************************
*    Variable with external linkage 
***************************************/

extern uint8 WaveDAC_hall3_initVar;

extern const uint8 CYCODE WaveDAC_hall3_wave1[WaveDAC_hall3_WAVE1_LENGTH];
extern const uint8 CYCODE WaveDAC_hall3_wave2[WaveDAC_hall3_WAVE2_LENGTH];


/***************************************
*            API Constants
***************************************/

/* SetRange constants */
#if(WaveDAC_hall3_DAC_MODE == WaveDAC_hall3_VOLT_MODE)
    #define WaveDAC_hall3_RANGE_1V       (0x00u)
    #define WaveDAC_hall3_RANGE_4V       (0x04u)
#else /* current mode */
    #define WaveDAC_hall3_RANGE_32uA     (0x00u)
    #define WaveDAC_hall3_RANGE_255uA    (0x04u)
    #define WaveDAC_hall3_RANGE_2mA      (0x08u)
    #define WaveDAC_hall3_RANGE_2048uA   WaveDAC_hall3_RANGE_2mA
#endif /* WaveDAC_hall3_DAC_MODE == WaveDAC_hall3_VOLT_MODE */

/* Power setting for SetSpeed API */
#define WaveDAC_hall3_LOWSPEED       (0x00u)
#define WaveDAC_hall3_HIGHSPEED      (0x02u)


/***************************************
*              Registers        
***************************************/

#define WaveDAC_hall3_DAC8__D WaveDAC_hall3_VDAC8_viDAC8__D


/***************************************
*         Register Constants       
***************************************/

/* CR0 vDac Control Register 0 definitions */

/* Bit Field  DAC_HS_MODE */
#define WaveDAC_hall3_HS_MASK        (0x02u)
#define WaveDAC_hall3_HS_LOWPOWER    (0x00u)
#define WaveDAC_hall3_HS_HIGHSPEED   (0x02u)

/* Bit Field  DAC_MODE */
#define WaveDAC_hall3_MODE_MASK      (0x10u)
#define WaveDAC_hall3_MODE_V         (0x00u)
#define WaveDAC_hall3_MODE_I         (0x10u)

/* Bit Field  DAC_RANGE */
#define WaveDAC_hall3_RANGE_MASK     (0x0Cu)
#define WaveDAC_hall3_RANGE_0        (0x00u)
#define WaveDAC_hall3_RANGE_1        (0x04u)
#define WaveDAC_hall3_RANGE_2        (0x08u)
#define WaveDAC_hall3_RANGE_3        (0x0Cu)
#define WaveDAC_hall3_IDIR_MASK      (0x04u)

#define WaveDAC_hall3_DAC_RANGE      ((uint8)(1u << 2u) & WaveDAC_hall3_RANGE_MASK)
#define WaveDAC_hall3_DAC_POL        ((uint8)(1u >> 1u) & WaveDAC_hall3_IDIR_MASK)


#endif /* CY_WaveDAC8_WaveDAC_hall3_H  */

/* [] END OF FILE */
