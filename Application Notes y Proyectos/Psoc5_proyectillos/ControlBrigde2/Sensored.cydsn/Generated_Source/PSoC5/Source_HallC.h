/*******************************************************************************
* File Name: Source_HallC.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Source_HallC_H) /* Pins Source_HallC_H */
#define CY_PINS_Source_HallC_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Source_HallC_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Source_HallC__PORT == 15 && ((Source_HallC__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Source_HallC_Write(uint8 value);
void    Source_HallC_SetDriveMode(uint8 mode);
uint8   Source_HallC_ReadDataReg(void);
uint8   Source_HallC_Read(void);
void    Source_HallC_SetInterruptMode(uint16 position, uint16 mode);
uint8   Source_HallC_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Source_HallC_SetDriveMode() function.
     *  @{
     */
        #define Source_HallC_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Source_HallC_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Source_HallC_DM_RES_UP          PIN_DM_RES_UP
        #define Source_HallC_DM_RES_DWN         PIN_DM_RES_DWN
        #define Source_HallC_DM_OD_LO           PIN_DM_OD_LO
        #define Source_HallC_DM_OD_HI           PIN_DM_OD_HI
        #define Source_HallC_DM_STRONG          PIN_DM_STRONG
        #define Source_HallC_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Source_HallC_MASK               Source_HallC__MASK
#define Source_HallC_SHIFT              Source_HallC__SHIFT
#define Source_HallC_WIDTH              1u

/* Interrupt constants */
#if defined(Source_HallC__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Source_HallC_SetInterruptMode() function.
     *  @{
     */
        #define Source_HallC_INTR_NONE      (uint16)(0x0000u)
        #define Source_HallC_INTR_RISING    (uint16)(0x0001u)
        #define Source_HallC_INTR_FALLING   (uint16)(0x0002u)
        #define Source_HallC_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Source_HallC_INTR_MASK      (0x01u) 
#endif /* (Source_HallC__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Source_HallC_PS                     (* (reg8 *) Source_HallC__PS)
/* Data Register */
#define Source_HallC_DR                     (* (reg8 *) Source_HallC__DR)
/* Port Number */
#define Source_HallC_PRT_NUM                (* (reg8 *) Source_HallC__PRT) 
/* Connect to Analog Globals */                                                  
#define Source_HallC_AG                     (* (reg8 *) Source_HallC__AG)                       
/* Analog MUX bux enable */
#define Source_HallC_AMUX                   (* (reg8 *) Source_HallC__AMUX) 
/* Bidirectional Enable */                                                        
#define Source_HallC_BIE                    (* (reg8 *) Source_HallC__BIE)
/* Bit-mask for Aliased Register Access */
#define Source_HallC_BIT_MASK               (* (reg8 *) Source_HallC__BIT_MASK)
/* Bypass Enable */
#define Source_HallC_BYP                    (* (reg8 *) Source_HallC__BYP)
/* Port wide control signals */                                                   
#define Source_HallC_CTL                    (* (reg8 *) Source_HallC__CTL)
/* Drive Modes */
#define Source_HallC_DM0                    (* (reg8 *) Source_HallC__DM0) 
#define Source_HallC_DM1                    (* (reg8 *) Source_HallC__DM1)
#define Source_HallC_DM2                    (* (reg8 *) Source_HallC__DM2) 
/* Input Buffer Disable Override */
#define Source_HallC_INP_DIS                (* (reg8 *) Source_HallC__INP_DIS)
/* LCD Common or Segment Drive */
#define Source_HallC_LCD_COM_SEG            (* (reg8 *) Source_HallC__LCD_COM_SEG)
/* Enable Segment LCD */
#define Source_HallC_LCD_EN                 (* (reg8 *) Source_HallC__LCD_EN)
/* Slew Rate Control */
#define Source_HallC_SLW                    (* (reg8 *) Source_HallC__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Source_HallC_PRTDSI__CAPS_SEL       (* (reg8 *) Source_HallC__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Source_HallC_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Source_HallC__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Source_HallC_PRTDSI__OE_SEL0        (* (reg8 *) Source_HallC__PRTDSI__OE_SEL0) 
#define Source_HallC_PRTDSI__OE_SEL1        (* (reg8 *) Source_HallC__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Source_HallC_PRTDSI__OUT_SEL0       (* (reg8 *) Source_HallC__PRTDSI__OUT_SEL0) 
#define Source_HallC_PRTDSI__OUT_SEL1       (* (reg8 *) Source_HallC__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Source_HallC_PRTDSI__SYNC_OUT       (* (reg8 *) Source_HallC__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Source_HallC__SIO_CFG)
    #define Source_HallC_SIO_HYST_EN        (* (reg8 *) Source_HallC__SIO_HYST_EN)
    #define Source_HallC_SIO_REG_HIFREQ     (* (reg8 *) Source_HallC__SIO_REG_HIFREQ)
    #define Source_HallC_SIO_CFG            (* (reg8 *) Source_HallC__SIO_CFG)
    #define Source_HallC_SIO_DIFF           (* (reg8 *) Source_HallC__SIO_DIFF)
#endif /* (Source_HallC__SIO_CFG) */

/* Interrupt Registers */
#if defined(Source_HallC__INTSTAT)
    #define Source_HallC_INTSTAT            (* (reg8 *) Source_HallC__INTSTAT)
    #define Source_HallC_SNAP               (* (reg8 *) Source_HallC__SNAP)
    
	#define Source_HallC_0_INTTYPE_REG 		(* (reg8 *) Source_HallC__0__INTTYPE)
#endif /* (Source_HallC__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Source_HallC_H */


/* [] END OF FILE */
