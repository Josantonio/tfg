/*******************************************************************************
* File Name: BH.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_BH_H) /* Pins BH_H */
#define CY_PINS_BH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "BH_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 BH__PORT == 15 && ((BH__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    BH_Write(uint8 value);
void    BH_SetDriveMode(uint8 mode);
uint8   BH_ReadDataReg(void);
uint8   BH_Read(void);
void    BH_SetInterruptMode(uint16 position, uint16 mode);
uint8   BH_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the BH_SetDriveMode() function.
     *  @{
     */
        #define BH_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define BH_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define BH_DM_RES_UP          PIN_DM_RES_UP
        #define BH_DM_RES_DWN         PIN_DM_RES_DWN
        #define BH_DM_OD_LO           PIN_DM_OD_LO
        #define BH_DM_OD_HI           PIN_DM_OD_HI
        #define BH_DM_STRONG          PIN_DM_STRONG
        #define BH_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define BH_MASK               BH__MASK
#define BH_SHIFT              BH__SHIFT
#define BH_WIDTH              1u

/* Interrupt constants */
#if defined(BH__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in BH_SetInterruptMode() function.
     *  @{
     */
        #define BH_INTR_NONE      (uint16)(0x0000u)
        #define BH_INTR_RISING    (uint16)(0x0001u)
        #define BH_INTR_FALLING   (uint16)(0x0002u)
        #define BH_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define BH_INTR_MASK      (0x01u) 
#endif /* (BH__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define BH_PS                     (* (reg8 *) BH__PS)
/* Data Register */
#define BH_DR                     (* (reg8 *) BH__DR)
/* Port Number */
#define BH_PRT_NUM                (* (reg8 *) BH__PRT) 
/* Connect to Analog Globals */                                                  
#define BH_AG                     (* (reg8 *) BH__AG)                       
/* Analog MUX bux enable */
#define BH_AMUX                   (* (reg8 *) BH__AMUX) 
/* Bidirectional Enable */                                                        
#define BH_BIE                    (* (reg8 *) BH__BIE)
/* Bit-mask for Aliased Register Access */
#define BH_BIT_MASK               (* (reg8 *) BH__BIT_MASK)
/* Bypass Enable */
#define BH_BYP                    (* (reg8 *) BH__BYP)
/* Port wide control signals */                                                   
#define BH_CTL                    (* (reg8 *) BH__CTL)
/* Drive Modes */
#define BH_DM0                    (* (reg8 *) BH__DM0) 
#define BH_DM1                    (* (reg8 *) BH__DM1)
#define BH_DM2                    (* (reg8 *) BH__DM2) 
/* Input Buffer Disable Override */
#define BH_INP_DIS                (* (reg8 *) BH__INP_DIS)
/* LCD Common or Segment Drive */
#define BH_LCD_COM_SEG            (* (reg8 *) BH__LCD_COM_SEG)
/* Enable Segment LCD */
#define BH_LCD_EN                 (* (reg8 *) BH__LCD_EN)
/* Slew Rate Control */
#define BH_SLW                    (* (reg8 *) BH__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define BH_PRTDSI__CAPS_SEL       (* (reg8 *) BH__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define BH_PRTDSI__DBL_SYNC_IN    (* (reg8 *) BH__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define BH_PRTDSI__OE_SEL0        (* (reg8 *) BH__PRTDSI__OE_SEL0) 
#define BH_PRTDSI__OE_SEL1        (* (reg8 *) BH__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define BH_PRTDSI__OUT_SEL0       (* (reg8 *) BH__PRTDSI__OUT_SEL0) 
#define BH_PRTDSI__OUT_SEL1       (* (reg8 *) BH__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define BH_PRTDSI__SYNC_OUT       (* (reg8 *) BH__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(BH__SIO_CFG)
    #define BH_SIO_HYST_EN        (* (reg8 *) BH__SIO_HYST_EN)
    #define BH_SIO_REG_HIFREQ     (* (reg8 *) BH__SIO_REG_HIFREQ)
    #define BH_SIO_CFG            (* (reg8 *) BH__SIO_CFG)
    #define BH_SIO_DIFF           (* (reg8 *) BH__SIO_DIFF)
#endif /* (BH__SIO_CFG) */

/* Interrupt Registers */
#if defined(BH__INTSTAT)
    #define BH_INTSTAT            (* (reg8 *) BH__INTSTAT)
    #define BH_SNAP               (* (reg8 *) BH__SNAP)
    
	#define BH_0_INTTYPE_REG 		(* (reg8 *) BH__0__INTTYPE)
#endif /* (BH__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_BH_H */


/* [] END OF FILE */
