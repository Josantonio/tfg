/*******************************************************************************
* File Name: ADC_Vref_PM.c
* Version 3.10
*
* Description:
*  This file provides Sleep/WakeUp APIs functionality.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "ADC_Vref.h"


/***************************************
* Local data allocation
***************************************/

static ADC_Vref_BACKUP_STRUCT  ADC_Vref_backup =
{
    ADC_Vref_DISABLED
};


/*******************************************************************************
* Function Name: ADC_Vref_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_Vref_SaveConfig(void)
{
    /* All configuration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_Vref_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ADC_Vref_RestoreConfig(void)
{
    /* All congiguration registers are marked as [reset_all_retention] */
}


/*******************************************************************************
* Function Name: ADC_Vref_Sleep
********************************************************************************
*
* Summary:
*  This is the preferred routine to prepare the component for sleep.
*  The ADC_Vref_Sleep() routine saves the current component state,
*  then it calls the ADC_Stop() function.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_Vref_backup - The structure field 'enableState' is modified
*  depending on the enable state of the block before entering to sleep mode.
*
*******************************************************************************/
void ADC_Vref_Sleep(void)
{
    if((ADC_Vref_PWRMGR_SAR_REG  & ADC_Vref_ACT_PWR_SAR_EN) != 0u)
    {
        if((ADC_Vref_SAR_CSR0_REG & ADC_Vref_SAR_SOF_START_CONV) != 0u)
        {
            ADC_Vref_backup.enableState = ADC_Vref_ENABLED | ADC_Vref_STARTED;
        }
        else
        {
            ADC_Vref_backup.enableState = ADC_Vref_ENABLED;
        }
        ADC_Vref_Stop();
    }
    else
    {
        ADC_Vref_backup.enableState = ADC_Vref_DISABLED;
    }
}


/*******************************************************************************
* Function Name: ADC_Vref_Wakeup
********************************************************************************
*
* Summary:
*  This is the preferred routine to restore the component to the state when
*  ADC_Vref_Sleep() was called. If the component was enabled before the
*  ADC_Vref_Sleep() function was called, the
*  ADC_Vref_Wakeup() function also re-enables the component.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  ADC_Vref_backup - The structure field 'enableState' is used to
*  restore the enable state of block after wakeup from sleep mode.
*
*******************************************************************************/
void ADC_Vref_Wakeup(void)
{
    if(ADC_Vref_backup.enableState != ADC_Vref_DISABLED)
    {
        ADC_Vref_Enable();
        #if(ADC_Vref_DEFAULT_CONV_MODE != ADC_Vref__HARDWARE_TRIGGER)
            if((ADC_Vref_backup.enableState & ADC_Vref_STARTED) != 0u)
            {
                ADC_Vref_StartConvert();
            }
        #endif /* End ADC_Vref_DEFAULT_CONV_MODE != ADC_Vref__HARDWARE_TRIGGER */
    }
}


/* [] END OF FILE */
