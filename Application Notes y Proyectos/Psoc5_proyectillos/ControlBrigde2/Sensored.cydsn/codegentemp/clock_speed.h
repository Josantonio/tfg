/*******************************************************************************
* File Name: clock_speed.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_clock_speed_H)
#define CY_CLOCK_clock_speed_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void clock_speed_Start(void) ;
void clock_speed_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void clock_speed_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void clock_speed_StandbyPower(uint8 state) ;
void clock_speed_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 clock_speed_GetDividerRegister(void) ;
void clock_speed_SetModeRegister(uint8 modeBitMask) ;
void clock_speed_ClearModeRegister(uint8 modeBitMask) ;
uint8 clock_speed_GetModeRegister(void) ;
void clock_speed_SetSourceRegister(uint8 clkSource) ;
uint8 clock_speed_GetSourceRegister(void) ;
#if defined(clock_speed__CFG3)
void clock_speed_SetPhaseRegister(uint8 clkPhase) ;
uint8 clock_speed_GetPhaseRegister(void) ;
#endif /* defined(clock_speed__CFG3) */

#define clock_speed_Enable()                       clock_speed_Start()
#define clock_speed_Disable()                      clock_speed_Stop()
#define clock_speed_SetDivider(clkDivider)         clock_speed_SetDividerRegister(clkDivider, 1u)
#define clock_speed_SetDividerValue(clkDivider)    clock_speed_SetDividerRegister((clkDivider) - 1u, 1u)
#define clock_speed_SetMode(clkMode)               clock_speed_SetModeRegister(clkMode)
#define clock_speed_SetSource(clkSource)           clock_speed_SetSourceRegister(clkSource)
#if defined(clock_speed__CFG3)
#define clock_speed_SetPhase(clkPhase)             clock_speed_SetPhaseRegister(clkPhase)
#define clock_speed_SetPhaseValue(clkPhase)        clock_speed_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(clock_speed__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define clock_speed_CLKEN              (* (reg8 *) clock_speed__PM_ACT_CFG)
#define clock_speed_CLKEN_PTR          ((reg8 *) clock_speed__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define clock_speed_CLKSTBY            (* (reg8 *) clock_speed__PM_STBY_CFG)
#define clock_speed_CLKSTBY_PTR        ((reg8 *) clock_speed__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define clock_speed_DIV_LSB            (* (reg8 *) clock_speed__CFG0)
#define clock_speed_DIV_LSB_PTR        ((reg8 *) clock_speed__CFG0)
#define clock_speed_DIV_PTR            ((reg16 *) clock_speed__CFG0)

/* Clock MSB divider configuration register. */
#define clock_speed_DIV_MSB            (* (reg8 *) clock_speed__CFG1)
#define clock_speed_DIV_MSB_PTR        ((reg8 *) clock_speed__CFG1)

/* Mode and source configuration register */
#define clock_speed_MOD_SRC            (* (reg8 *) clock_speed__CFG2)
#define clock_speed_MOD_SRC_PTR        ((reg8 *) clock_speed__CFG2)

#if defined(clock_speed__CFG3)
/* Analog clock phase configuration register */
#define clock_speed_PHASE              (* (reg8 *) clock_speed__CFG3)
#define clock_speed_PHASE_PTR          ((reg8 *) clock_speed__CFG3)
#endif /* defined(clock_speed__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define clock_speed_CLKEN_MASK         clock_speed__PM_ACT_MSK
#define clock_speed_CLKSTBY_MASK       clock_speed__PM_STBY_MSK

/* CFG2 field masks */
#define clock_speed_SRC_SEL_MSK        clock_speed__CFG2_SRC_SEL_MASK
#define clock_speed_MODE_MASK          (~(clock_speed_SRC_SEL_MSK))

#if defined(clock_speed__CFG3)
/* CFG3 phase mask */
#define clock_speed_PHASE_MASK         clock_speed__CFG3_PHASE_DLY_MASK
#endif /* defined(clock_speed__CFG3) */

#endif /* CY_CLOCK_clock_speed_H */


/* [] END OF FILE */
