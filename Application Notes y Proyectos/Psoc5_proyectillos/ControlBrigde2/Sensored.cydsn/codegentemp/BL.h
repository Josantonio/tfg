/*******************************************************************************
* File Name: BL.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_BL_H) /* Pins BL_H */
#define CY_PINS_BL_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "BL_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 BL__PORT == 15 && ((BL__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    BL_Write(uint8 value);
void    BL_SetDriveMode(uint8 mode);
uint8   BL_ReadDataReg(void);
uint8   BL_Read(void);
void    BL_SetInterruptMode(uint16 position, uint16 mode);
uint8   BL_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the BL_SetDriveMode() function.
     *  @{
     */
        #define BL_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define BL_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define BL_DM_RES_UP          PIN_DM_RES_UP
        #define BL_DM_RES_DWN         PIN_DM_RES_DWN
        #define BL_DM_OD_LO           PIN_DM_OD_LO
        #define BL_DM_OD_HI           PIN_DM_OD_HI
        #define BL_DM_STRONG          PIN_DM_STRONG
        #define BL_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define BL_MASK               BL__MASK
#define BL_SHIFT              BL__SHIFT
#define BL_WIDTH              1u

/* Interrupt constants */
#if defined(BL__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in BL_SetInterruptMode() function.
     *  @{
     */
        #define BL_INTR_NONE      (uint16)(0x0000u)
        #define BL_INTR_RISING    (uint16)(0x0001u)
        #define BL_INTR_FALLING   (uint16)(0x0002u)
        #define BL_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define BL_INTR_MASK      (0x01u) 
#endif /* (BL__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define BL_PS                     (* (reg8 *) BL__PS)
/* Data Register */
#define BL_DR                     (* (reg8 *) BL__DR)
/* Port Number */
#define BL_PRT_NUM                (* (reg8 *) BL__PRT) 
/* Connect to Analog Globals */                                                  
#define BL_AG                     (* (reg8 *) BL__AG)                       
/* Analog MUX bux enable */
#define BL_AMUX                   (* (reg8 *) BL__AMUX) 
/* Bidirectional Enable */                                                        
#define BL_BIE                    (* (reg8 *) BL__BIE)
/* Bit-mask for Aliased Register Access */
#define BL_BIT_MASK               (* (reg8 *) BL__BIT_MASK)
/* Bypass Enable */
#define BL_BYP                    (* (reg8 *) BL__BYP)
/* Port wide control signals */                                                   
#define BL_CTL                    (* (reg8 *) BL__CTL)
/* Drive Modes */
#define BL_DM0                    (* (reg8 *) BL__DM0) 
#define BL_DM1                    (* (reg8 *) BL__DM1)
#define BL_DM2                    (* (reg8 *) BL__DM2) 
/* Input Buffer Disable Override */
#define BL_INP_DIS                (* (reg8 *) BL__INP_DIS)
/* LCD Common or Segment Drive */
#define BL_LCD_COM_SEG            (* (reg8 *) BL__LCD_COM_SEG)
/* Enable Segment LCD */
#define BL_LCD_EN                 (* (reg8 *) BL__LCD_EN)
/* Slew Rate Control */
#define BL_SLW                    (* (reg8 *) BL__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define BL_PRTDSI__CAPS_SEL       (* (reg8 *) BL__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define BL_PRTDSI__DBL_SYNC_IN    (* (reg8 *) BL__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define BL_PRTDSI__OE_SEL0        (* (reg8 *) BL__PRTDSI__OE_SEL0) 
#define BL_PRTDSI__OE_SEL1        (* (reg8 *) BL__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define BL_PRTDSI__OUT_SEL0       (* (reg8 *) BL__PRTDSI__OUT_SEL0) 
#define BL_PRTDSI__OUT_SEL1       (* (reg8 *) BL__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define BL_PRTDSI__SYNC_OUT       (* (reg8 *) BL__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(BL__SIO_CFG)
    #define BL_SIO_HYST_EN        (* (reg8 *) BL__SIO_HYST_EN)
    #define BL_SIO_REG_HIFREQ     (* (reg8 *) BL__SIO_REG_HIFREQ)
    #define BL_SIO_CFG            (* (reg8 *) BL__SIO_CFG)
    #define BL_SIO_DIFF           (* (reg8 *) BL__SIO_DIFF)
#endif /* (BL__SIO_CFG) */

/* Interrupt Registers */
#if defined(BL__INTSTAT)
    #define BL_INTSTAT            (* (reg8 *) BL__INTSTAT)
    #define BL_SNAP               (* (reg8 *) BL__SNAP)
    
	#define BL_0_INTTYPE_REG 		(* (reg8 *) BL__0__INTTYPE)
#endif /* (BL__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_BL_H */


/* [] END OF FILE */
