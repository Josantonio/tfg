/*******************************************************************************
* File Name: PinPotHall.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinPotHall_H) /* Pins PinPotHall_H */
#define CY_PINS_PinPotHall_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinPotHall_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinPotHall__PORT == 15 && ((PinPotHall__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinPotHall_Write(uint8 value);
void    PinPotHall_SetDriveMode(uint8 mode);
uint8   PinPotHall_ReadDataReg(void);
uint8   PinPotHall_Read(void);
void    PinPotHall_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinPotHall_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinPotHall_SetDriveMode() function.
     *  @{
     */
        #define PinPotHall_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinPotHall_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinPotHall_DM_RES_UP          PIN_DM_RES_UP
        #define PinPotHall_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinPotHall_DM_OD_LO           PIN_DM_OD_LO
        #define PinPotHall_DM_OD_HI           PIN_DM_OD_HI
        #define PinPotHall_DM_STRONG          PIN_DM_STRONG
        #define PinPotHall_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinPotHall_MASK               PinPotHall__MASK
#define PinPotHall_SHIFT              PinPotHall__SHIFT
#define PinPotHall_WIDTH              1u

/* Interrupt constants */
#if defined(PinPotHall__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinPotHall_SetInterruptMode() function.
     *  @{
     */
        #define PinPotHall_INTR_NONE      (uint16)(0x0000u)
        #define PinPotHall_INTR_RISING    (uint16)(0x0001u)
        #define PinPotHall_INTR_FALLING   (uint16)(0x0002u)
        #define PinPotHall_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinPotHall_INTR_MASK      (0x01u) 
#endif /* (PinPotHall__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinPotHall_PS                     (* (reg8 *) PinPotHall__PS)
/* Data Register */
#define PinPotHall_DR                     (* (reg8 *) PinPotHall__DR)
/* Port Number */
#define PinPotHall_PRT_NUM                (* (reg8 *) PinPotHall__PRT) 
/* Connect to Analog Globals */                                                  
#define PinPotHall_AG                     (* (reg8 *) PinPotHall__AG)                       
/* Analog MUX bux enable */
#define PinPotHall_AMUX                   (* (reg8 *) PinPotHall__AMUX) 
/* Bidirectional Enable */                                                        
#define PinPotHall_BIE                    (* (reg8 *) PinPotHall__BIE)
/* Bit-mask for Aliased Register Access */
#define PinPotHall_BIT_MASK               (* (reg8 *) PinPotHall__BIT_MASK)
/* Bypass Enable */
#define PinPotHall_BYP                    (* (reg8 *) PinPotHall__BYP)
/* Port wide control signals */                                                   
#define PinPotHall_CTL                    (* (reg8 *) PinPotHall__CTL)
/* Drive Modes */
#define PinPotHall_DM0                    (* (reg8 *) PinPotHall__DM0) 
#define PinPotHall_DM1                    (* (reg8 *) PinPotHall__DM1)
#define PinPotHall_DM2                    (* (reg8 *) PinPotHall__DM2) 
/* Input Buffer Disable Override */
#define PinPotHall_INP_DIS                (* (reg8 *) PinPotHall__INP_DIS)
/* LCD Common or Segment Drive */
#define PinPotHall_LCD_COM_SEG            (* (reg8 *) PinPotHall__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinPotHall_LCD_EN                 (* (reg8 *) PinPotHall__LCD_EN)
/* Slew Rate Control */
#define PinPotHall_SLW                    (* (reg8 *) PinPotHall__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinPotHall_PRTDSI__CAPS_SEL       (* (reg8 *) PinPotHall__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinPotHall_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinPotHall__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinPotHall_PRTDSI__OE_SEL0        (* (reg8 *) PinPotHall__PRTDSI__OE_SEL0) 
#define PinPotHall_PRTDSI__OE_SEL1        (* (reg8 *) PinPotHall__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinPotHall_PRTDSI__OUT_SEL0       (* (reg8 *) PinPotHall__PRTDSI__OUT_SEL0) 
#define PinPotHall_PRTDSI__OUT_SEL1       (* (reg8 *) PinPotHall__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinPotHall_PRTDSI__SYNC_OUT       (* (reg8 *) PinPotHall__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinPotHall__SIO_CFG)
    #define PinPotHall_SIO_HYST_EN        (* (reg8 *) PinPotHall__SIO_HYST_EN)
    #define PinPotHall_SIO_REG_HIFREQ     (* (reg8 *) PinPotHall__SIO_REG_HIFREQ)
    #define PinPotHall_SIO_CFG            (* (reg8 *) PinPotHall__SIO_CFG)
    #define PinPotHall_SIO_DIFF           (* (reg8 *) PinPotHall__SIO_DIFF)
#endif /* (PinPotHall__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinPotHall__INTSTAT)
    #define PinPotHall_INTSTAT            (* (reg8 *) PinPotHall__INTSTAT)
    #define PinPotHall_SNAP               (* (reg8 *) PinPotHall__SNAP)
    
	#define PinPotHall_0_INTTYPE_REG 		(* (reg8 *) PinPotHall__0__INTTYPE)
#endif /* (PinPotHall__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinPotHall_H */


/* [] END OF FILE */
