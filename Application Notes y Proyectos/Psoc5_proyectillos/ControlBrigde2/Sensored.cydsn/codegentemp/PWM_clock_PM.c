/*******************************************************************************
* File Name: PWM_clock_PM.c
* Version 3.30
*
* Description:
*  This file provides the power management source code to API for the
*  PWM.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "PWM_clock.h"

static PWM_clock_backupStruct PWM_clock_backup;


/*******************************************************************************
* Function Name: PWM_clock_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_clock_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void PWM_clock_SaveConfig(void) 
{

    #if(!PWM_clock_UsingFixedFunction)
        #if(!PWM_clock_PWMModeIsCenterAligned)
            PWM_clock_backup.PWMPeriod = PWM_clock_ReadPeriod();
        #endif /* (!PWM_clock_PWMModeIsCenterAligned) */
        PWM_clock_backup.PWMUdb = PWM_clock_ReadCounter();
        #if (PWM_clock_UseStatus)
            PWM_clock_backup.InterruptMaskValue = PWM_clock_STATUS_MASK;
        #endif /* (PWM_clock_UseStatus) */

        #if(PWM_clock_DeadBandMode == PWM_clock__B_PWM__DBM_256_CLOCKS || \
            PWM_clock_DeadBandMode == PWM_clock__B_PWM__DBM_2_4_CLOCKS)
            PWM_clock_backup.PWMdeadBandValue = PWM_clock_ReadDeadTime();
        #endif /*  deadband count is either 2-4 clocks or 256 clocks */

        #if(PWM_clock_KillModeMinTime)
             PWM_clock_backup.PWMKillCounterPeriod = PWM_clock_ReadKillTime();
        #endif /* (PWM_clock_KillModeMinTime) */

        #if(PWM_clock_UseControl)
            PWM_clock_backup.PWMControlRegister = PWM_clock_ReadControlRegister();
        #endif /* (PWM_clock_UseControl) */
    #endif  /* (!PWM_clock_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PWM_clock_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration of the component.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_clock_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_clock_RestoreConfig(void) 
{
        #if(!PWM_clock_UsingFixedFunction)
            #if(!PWM_clock_PWMModeIsCenterAligned)
                PWM_clock_WritePeriod(PWM_clock_backup.PWMPeriod);
            #endif /* (!PWM_clock_PWMModeIsCenterAligned) */

            PWM_clock_WriteCounter(PWM_clock_backup.PWMUdb);

            #if (PWM_clock_UseStatus)
                PWM_clock_STATUS_MASK = PWM_clock_backup.InterruptMaskValue;
            #endif /* (PWM_clock_UseStatus) */

            #if(PWM_clock_DeadBandMode == PWM_clock__B_PWM__DBM_256_CLOCKS || \
                PWM_clock_DeadBandMode == PWM_clock__B_PWM__DBM_2_4_CLOCKS)
                PWM_clock_WriteDeadTime(PWM_clock_backup.PWMdeadBandValue);
            #endif /* deadband count is either 2-4 clocks or 256 clocks */

            #if(PWM_clock_KillModeMinTime)
                PWM_clock_WriteKillTime(PWM_clock_backup.PWMKillCounterPeriod);
            #endif /* (PWM_clock_KillModeMinTime) */

            #if(PWM_clock_UseControl)
                PWM_clock_WriteControlRegister(PWM_clock_backup.PWMControlRegister);
            #endif /* (PWM_clock_UseControl) */
        #endif  /* (!PWM_clock_UsingFixedFunction) */
    }


/*******************************************************************************
* Function Name: PWM_clock_Sleep
********************************************************************************
*
* Summary:
*  Disables block's operation and saves the user configuration. Should be called
*  just prior to entering sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_clock_backup.PWMEnableState:  Is modified depending on the enable
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void PWM_clock_Sleep(void) 
{
    #if(PWM_clock_UseControl)
        if(PWM_clock_CTRL_ENABLE == (PWM_clock_CONTROL & PWM_clock_CTRL_ENABLE))
        {
            /*Component is enabled */
            PWM_clock_backup.PWMEnableState = 1u;
        }
        else
        {
            /* Component is disabled */
            PWM_clock_backup.PWMEnableState = 0u;
        }
    #endif /* (PWM_clock_UseControl) */

    /* Stop component */
    PWM_clock_Stop();

    /* Save registers configuration */
    PWM_clock_SaveConfig();
}


/*******************************************************************************
* Function Name: PWM_clock_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration. Should be called just after
*  awaking from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
* Global variables:
*  PWM_clock_backup.pwmEnable:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void PWM_clock_Wakeup(void) 
{
     /* Restore registers values */
    PWM_clock_RestoreConfig();

    if(PWM_clock_backup.PWMEnableState != 0u)
    {
        /* Enable component's operation */
        PWM_clock_Enable();
    } /* Do nothing if component's block was disabled before */

}


/* [] END OF FILE */
