/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "math.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define NUM_STEP (6) //Numero totales de pasos a dar
#define R_DSon (9.8e-3) //Resistencia de los MOSFET en conducción (ohmios)
#define R_wire (100e-3) //Resistencia de los cables
#define pwm_frec (10e3) //Frecuencia del PWM, tiene que ser mayor o igual a 10*frec_motor

/*************** CARACTERÍSTICAS DEL MOTOR ************************************************/

struct motor {
    uint8 Potencia;     //Potencia [w]
    uint16 SP_MAX;       //Vel máxima [rpm]
    uint16 SP_MIN;      //Vel minim [rpm]
    uint8 p_polos;        //Nº pares de polos
    float K_tau;        //Constante mecánica [Nm/A]
    float Ke;           //Constante eléctrica [V/rpm]
    float R_line;      //Resistencia fase-fase [Ohm]
    float L_line;      //Impedancia fase-fase [mH]
    uint16 I_max;       //Corriente máxima [mA] 
};

struct motor BLY172S = { 53, 4000, 4000/8, 4, 0.0410276162, 0.00335, 0.8, 1.2, 2208};

#define VDD (24.0) //Tensión de alimentación

/******************************************************************************************/

/*********************************** START ***********************************************/

#define t1 (1) // t1 es el tiempo de alineación, se aplica un pwm constante y se activa una posición,
                //se espera el tiempo t1 para que al motor le de tiempo a alinearse
#define SP1 (BLY172S.SP_MAX/60.0) //Velocidad a la que se empezará a girar a partir de t1 (rpm)
#define PWM1 ((BLY172S.R_line+2.0*R_DSon+R_wire)*BLY172S.I_max/VDD) //pwm que se aplica al principio para controlar la corriente que pasa por el motor
#define SP2 (BLY172S.SP_MAX/6.0) //Velocidad estimada suficiente para empezar a medir el BEMF (rpm)
#define PWM2 (((BLY172S.Ke*SP2+(BLY172S.R_line+2.0*R_DSon+R_wire)*BLY172S.I_max))/VDD) // pwm al que se pretende llegar progresivamente en lazo abierto

// Recta de arranque pwm(Sp) = c0+c1*Sp
#define c1 ((PWM2-PWM1)/(SP2-SP1)) 
#define c0 (PWM1-c1*SP1)

//Recta de la velocidad en función de n (tiempo muestreado) Sp(n) = a+b*n
#define n_max (20) //Número de pasos que da en el arranque
#define a (SP1)
#define b ((SP2-SP1)/n_max) //Pendiente de la recta

#define ADC_RANGE (2048)
#define V_MAX (ADC_RANGE*(330+10000)/(10000+2*330)) //Lectura máxima posible en el potenciómetro
#define V_MIN (ADC_RANGE*330/(10000+2*330)) //Lectura mínima en el potnciómetro

/*

#define F_MAX ((P_POLOS*RPM_MAX)/60) //Frecuencia eléctrica máxima
#define F_MIN ((P_POLOS*RPM_MIN)/60) //Frecuencia eléctrica mínima
#define INITIAL_PERIOD ((1.0/((F_MAX-F_MIN)/2.0))*1.0e6) //Período inicial en microsegundos
*/
uint8 fase = 1; //Varible para conmutar las fases, empezamos en el 1 terminamos en el 6
uint8 state_button = 0; //Botón a OFF como inicial
uint8 primer_arranque = 1; //Variable que establece si se ha producido o no la aceleración a la vel_min para la detección del BEMF
uint8 detect_BEMF = 0;
float frecuencia = 0.0;
uint16 tabla_arranque [n_max][2];
uint16 delay; //Retardo, en el período dividido entre 6

uint16 Vel_read;

CY_ISR(Interrupt_State_Button){
    state_button = ~state_button; //Cmabiamos el estado del botón cada vez que se pulsa
}

CY_ISR(Int_ZC){
//    delay = round(Timer_ZC_SoftwareCapture()*1.0e6);
}

int Vel_ref(void){ //Función que lee el valor del potenciómetro y lo transforma en un valor 
    //de velocidad deseada (rpm)
    uint16 SP_ref = 0; //Velocidad deseada, a través del potenciómetro
    
    ADC_StartConvert();
    ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);
    SP_ref = (ADC_GetResult16()-V_MIN)*(BLY172S.SP_MAX-BLY172S.SP_MIN)/(V_MAX-V_MIN)+BLY172S.SP_MIN;

    if (SP_ref < BLY172S.SP_MIN){ SP_ref = BLY172S.SP_MIN;}
    if (SP_ref > BLY172S.SP_MAX){ SP_ref = BLY172S.SP_MAX;}
    
    return SP_ref;
}

struct paramPID {
    float Kp; //Constante proporcional Kp = 1.2*T/L
    float Ti; //Parámetro integral ki = 2*L
    float Td; //Parámetro derivativo Kd = 0.5*L
    uint8 Tn; //Tiempo de muestreo del controlador en ms
    
};
struct paramPID PID = {15.61, 4.84e-3, 1.21e-3, 10};

int controlPID(void){
    int duty = 0;
    float eT; //Señal de error eT = Vel_ref - Vel_read
    float iT, iT0 = 0.0; //Señal integrativa iT = b*eT+iT0
    float dT, dT0 = 0.0; //Señal derivativa dT = c*(eT-eT0)
    float b = (PID.Kp*PID.Tn)/PID.Ti;
    float c = (PID.Kp*PID.Td)/PID.Tn;
    
    //Señal de error
   // eT = Vel_ref() - Vel_real;
    //Señal integrativa
    iT = b*eT+iT0;
    iT0 = iT;
    //Señal derivativa
    dT = c*(eT-dT0);
    dT0 = dT;
    //Señal de salida de control
    duty = PID.Kp*eT+iT+dT;
    return duty;
}



int creacion_tabla (void)
{
    uint8 pwm = 0; //Este PWM tiene que estar entre 0 y 255
    uint16 Sp;
    
    for(uint8 i=0; i<=n_max; i++){
        
        Sp = a+b*i; //Calculamos la velocidad, rpm
        pwm = round((c0+c1*Sp)*255); //Caclulamos el PWM para estar en condiciones seguras (control de corriente)
        frecuencia = BLY172S.p_polos*Sp/60.0; //Calculamos la frecuencia eléctrica
        delay = round((1.0/(6.0*frecuencia))*1.0e6); //Tiempo para el paso siguiente en us
        
        tabla_arranque [i][0] = pwm;
        tabla_arranque [i][1] = delay;
    } 
    return 0;
}

/*
int sig_paso(void){
    fase++;
    if (fase > 6){
        fase = 1;
    }
    if (detect_BEMF){
        Timer_ZC_Start();
    }
    return 0;
}
*/

int start_motor(void){
    Control_Write(fase);
    PWM_WriteCompare(tabla_arranque [0][0]);//pwm inicial, controlando la corriente
    CyDelay(1000); //Esperamos 1 segundo a que se alinee
    /******* RAMPA **********/
    for (uint8 i=1; i<=n_max; i++){
        fase++; //Conmutación de las bobinas
        if (fase > 6){
           fase = 1;
        }
        Control_Write(fase);
        PWM_WriteCompare(tabla_arranque [i][0]); //PWM
        CyDelayUs(tabla_arranque [i][1]); //Vamos incrementando también la frecuencia
    }
    return 0;
}

/*
int PID(int duty){ //Llemr a esta función en el loop principal cada T segundos (muestreo)
    // Leer velocidad referencia 
    ADC_StartConvert();
    ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);
    Vel_ref = ADC_GetResult8()*(RPM_MAX-SP2)/255+SP2;
    // Leer Velocidad real
    
    //Calcular error
    return duty;
}
*/

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    Control_Write(0); //Motor parado
    //Inicializamos componentes
    PWM_Start();
    ADC_Start();
    UART_Start();
    Timer_ZC_Enable(); //Para calcular el tiempo que pasa desde la última conmutación hasta que hace el paso por cero
    
    //Cabeceras para las interrupciones
    isr_button_StartEx(Interrupt_State_Button);
    isr_zcrossing_StartEx(Int_ZC);
    
    for(;;){
      if(state_button){ //ON
        if(primer_arranque){ //Si es la primera vez que se va a arrancar, hay que hacerlo progresivamente
            creacion_tabla(); //Se crea la tabla de arranque, la tabla de arranque nos da el pwm y delay que hay que ir aplicando
            start_motor(); //Aplicamos la rampa de arranque al motor, **El delay quizás haya que ponerlo fuera de la función **
            primer_arranque = 0; //Ya s eha hecho el control en lazo abierto
        }
        else{ //Ya tiene una vel minima para detectar el BEMF
            
            
        }
      }else{ //OFF
        Control_Write(0); //Motor STOP
        primer_arranque = 1; //S eprepara para la próxima vez que arranque
      }
    }
/*    
    char string [100]; //Declaramos variable para mostrar los datos 
    UART_PutString("\nPrograma de control Motores Brushless\n");
    sprintf(string, "Sp1: %f\t Sp2: %f\n",Sp1,Sp2); //n paso        
    UART_PutString(string);        
    sprintf(string, "pwm1: %f\t pwm2: %f\n",pwm1,pwm2); //n paso        
    UART_PutString(string); 
    sprintf(string, "c0: %f\t c1: %f\n",c0,c1); //n paso        
    UART_PutString(string);
        sprintf(string, "a: %f\t b: %f\n",a,b); //n paso        
    UART_PutString(string);
    UART_PutString("\nPaso     pwm       delay\n");
*/ //muestra tabla arranque
    
    /*
    for(uint8 i=0; i<=n_max; i++){
        sprintf(string, "%d\t",i); //n paso        
        UART_PutString(string);        
        sprintf(string, "%d\t",tabla_arranque [i][0]); //pwm
        UART_PutString(string);
        sprintf(string, "%d\n", tabla_arranque [i][1]); //delay
        UART_PutString(string);
    }
    */ //Muestra tabla arranque
}


