/*******************************************************************************
* File Name: CL.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_CL_H) /* Pins CL_H */
#define CY_PINS_CL_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "CL_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 CL__PORT == 15 && ((CL__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    CL_Write(uint8 value);
void    CL_SetDriveMode(uint8 mode);
uint8   CL_ReadDataReg(void);
uint8   CL_Read(void);
void    CL_SetInterruptMode(uint16 position, uint16 mode);
uint8   CL_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the CL_SetDriveMode() function.
     *  @{
     */
        #define CL_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define CL_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define CL_DM_RES_UP          PIN_DM_RES_UP
        #define CL_DM_RES_DWN         PIN_DM_RES_DWN
        #define CL_DM_OD_LO           PIN_DM_OD_LO
        #define CL_DM_OD_HI           PIN_DM_OD_HI
        #define CL_DM_STRONG          PIN_DM_STRONG
        #define CL_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define CL_MASK               CL__MASK
#define CL_SHIFT              CL__SHIFT
#define CL_WIDTH              1u

/* Interrupt constants */
#if defined(CL__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in CL_SetInterruptMode() function.
     *  @{
     */
        #define CL_INTR_NONE      (uint16)(0x0000u)
        #define CL_INTR_RISING    (uint16)(0x0001u)
        #define CL_INTR_FALLING   (uint16)(0x0002u)
        #define CL_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define CL_INTR_MASK      (0x01u) 
#endif /* (CL__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define CL_PS                     (* (reg8 *) CL__PS)
/* Data Register */
#define CL_DR                     (* (reg8 *) CL__DR)
/* Port Number */
#define CL_PRT_NUM                (* (reg8 *) CL__PRT) 
/* Connect to Analog Globals */                                                  
#define CL_AG                     (* (reg8 *) CL__AG)                       
/* Analog MUX bux enable */
#define CL_AMUX                   (* (reg8 *) CL__AMUX) 
/* Bidirectional Enable */                                                        
#define CL_BIE                    (* (reg8 *) CL__BIE)
/* Bit-mask for Aliased Register Access */
#define CL_BIT_MASK               (* (reg8 *) CL__BIT_MASK)
/* Bypass Enable */
#define CL_BYP                    (* (reg8 *) CL__BYP)
/* Port wide control signals */                                                   
#define CL_CTL                    (* (reg8 *) CL__CTL)
/* Drive Modes */
#define CL_DM0                    (* (reg8 *) CL__DM0) 
#define CL_DM1                    (* (reg8 *) CL__DM1)
#define CL_DM2                    (* (reg8 *) CL__DM2) 
/* Input Buffer Disable Override */
#define CL_INP_DIS                (* (reg8 *) CL__INP_DIS)
/* LCD Common or Segment Drive */
#define CL_LCD_COM_SEG            (* (reg8 *) CL__LCD_COM_SEG)
/* Enable Segment LCD */
#define CL_LCD_EN                 (* (reg8 *) CL__LCD_EN)
/* Slew Rate Control */
#define CL_SLW                    (* (reg8 *) CL__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define CL_PRTDSI__CAPS_SEL       (* (reg8 *) CL__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define CL_PRTDSI__DBL_SYNC_IN    (* (reg8 *) CL__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define CL_PRTDSI__OE_SEL0        (* (reg8 *) CL__PRTDSI__OE_SEL0) 
#define CL_PRTDSI__OE_SEL1        (* (reg8 *) CL__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define CL_PRTDSI__OUT_SEL0       (* (reg8 *) CL__PRTDSI__OUT_SEL0) 
#define CL_PRTDSI__OUT_SEL1       (* (reg8 *) CL__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define CL_PRTDSI__SYNC_OUT       (* (reg8 *) CL__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(CL__SIO_CFG)
    #define CL_SIO_HYST_EN        (* (reg8 *) CL__SIO_HYST_EN)
    #define CL_SIO_REG_HIFREQ     (* (reg8 *) CL__SIO_REG_HIFREQ)
    #define CL_SIO_CFG            (* (reg8 *) CL__SIO_CFG)
    #define CL_SIO_DIFF           (* (reg8 *) CL__SIO_DIFF)
#endif /* (CL__SIO_CFG) */

/* Interrupt Registers */
#if defined(CL__INTSTAT)
    #define CL_INTSTAT            (* (reg8 *) CL__INTSTAT)
    #define CL_SNAP               (* (reg8 *) CL__SNAP)
    
	#define CL_0_INTTYPE_REG 		(* (reg8 *) CL__0__INTTYPE)
#endif /* (CL__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_CL_H */


/* [] END OF FILE */
