/*******************************************************************************
* File Name: conmutacion.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_conmutacion_H) /* Pins conmutacion_H */
#define CY_PINS_conmutacion_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "conmutacion_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 conmutacion__PORT == 15 && ((conmutacion__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    conmutacion_Write(uint8 value);
void    conmutacion_SetDriveMode(uint8 mode);
uint8   conmutacion_ReadDataReg(void);
uint8   conmutacion_Read(void);
void    conmutacion_SetInterruptMode(uint16 position, uint16 mode);
uint8   conmutacion_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the conmutacion_SetDriveMode() function.
     *  @{
     */
        #define conmutacion_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define conmutacion_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define conmutacion_DM_RES_UP          PIN_DM_RES_UP
        #define conmutacion_DM_RES_DWN         PIN_DM_RES_DWN
        #define conmutacion_DM_OD_LO           PIN_DM_OD_LO
        #define conmutacion_DM_OD_HI           PIN_DM_OD_HI
        #define conmutacion_DM_STRONG          PIN_DM_STRONG
        #define conmutacion_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define conmutacion_MASK               conmutacion__MASK
#define conmutacion_SHIFT              conmutacion__SHIFT
#define conmutacion_WIDTH              1u

/* Interrupt constants */
#if defined(conmutacion__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in conmutacion_SetInterruptMode() function.
     *  @{
     */
        #define conmutacion_INTR_NONE      (uint16)(0x0000u)
        #define conmutacion_INTR_RISING    (uint16)(0x0001u)
        #define conmutacion_INTR_FALLING   (uint16)(0x0002u)
        #define conmutacion_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define conmutacion_INTR_MASK      (0x01u) 
#endif /* (conmutacion__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define conmutacion_PS                     (* (reg8 *) conmutacion__PS)
/* Data Register */
#define conmutacion_DR                     (* (reg8 *) conmutacion__DR)
/* Port Number */
#define conmutacion_PRT_NUM                (* (reg8 *) conmutacion__PRT) 
/* Connect to Analog Globals */                                                  
#define conmutacion_AG                     (* (reg8 *) conmutacion__AG)                       
/* Analog MUX bux enable */
#define conmutacion_AMUX                   (* (reg8 *) conmutacion__AMUX) 
/* Bidirectional Enable */                                                        
#define conmutacion_BIE                    (* (reg8 *) conmutacion__BIE)
/* Bit-mask for Aliased Register Access */
#define conmutacion_BIT_MASK               (* (reg8 *) conmutacion__BIT_MASK)
/* Bypass Enable */
#define conmutacion_BYP                    (* (reg8 *) conmutacion__BYP)
/* Port wide control signals */                                                   
#define conmutacion_CTL                    (* (reg8 *) conmutacion__CTL)
/* Drive Modes */
#define conmutacion_DM0                    (* (reg8 *) conmutacion__DM0) 
#define conmutacion_DM1                    (* (reg8 *) conmutacion__DM1)
#define conmutacion_DM2                    (* (reg8 *) conmutacion__DM2) 
/* Input Buffer Disable Override */
#define conmutacion_INP_DIS                (* (reg8 *) conmutacion__INP_DIS)
/* LCD Common or Segment Drive */
#define conmutacion_LCD_COM_SEG            (* (reg8 *) conmutacion__LCD_COM_SEG)
/* Enable Segment LCD */
#define conmutacion_LCD_EN                 (* (reg8 *) conmutacion__LCD_EN)
/* Slew Rate Control */
#define conmutacion_SLW                    (* (reg8 *) conmutacion__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define conmutacion_PRTDSI__CAPS_SEL       (* (reg8 *) conmutacion__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define conmutacion_PRTDSI__DBL_SYNC_IN    (* (reg8 *) conmutacion__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define conmutacion_PRTDSI__OE_SEL0        (* (reg8 *) conmutacion__PRTDSI__OE_SEL0) 
#define conmutacion_PRTDSI__OE_SEL1        (* (reg8 *) conmutacion__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define conmutacion_PRTDSI__OUT_SEL0       (* (reg8 *) conmutacion__PRTDSI__OUT_SEL0) 
#define conmutacion_PRTDSI__OUT_SEL1       (* (reg8 *) conmutacion__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define conmutacion_PRTDSI__SYNC_OUT       (* (reg8 *) conmutacion__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(conmutacion__SIO_CFG)
    #define conmutacion_SIO_HYST_EN        (* (reg8 *) conmutacion__SIO_HYST_EN)
    #define conmutacion_SIO_REG_HIFREQ     (* (reg8 *) conmutacion__SIO_REG_HIFREQ)
    #define conmutacion_SIO_CFG            (* (reg8 *) conmutacion__SIO_CFG)
    #define conmutacion_SIO_DIFF           (* (reg8 *) conmutacion__SIO_DIFF)
#endif /* (conmutacion__SIO_CFG) */

/* Interrupt Registers */
#if defined(conmutacion__INTSTAT)
    #define conmutacion_INTSTAT            (* (reg8 *) conmutacion__INTSTAT)
    #define conmutacion_SNAP               (* (reg8 *) conmutacion__SNAP)
    
	#define conmutacion_0_INTTYPE_REG 		(* (reg8 *) conmutacion__0__INTTYPE)
#endif /* (conmutacion__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_conmutacion_H */


/* [] END OF FILE */
