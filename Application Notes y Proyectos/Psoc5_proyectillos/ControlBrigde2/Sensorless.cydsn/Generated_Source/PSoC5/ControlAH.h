/*******************************************************************************
* File Name: ControlAH.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_ControlAH_H) /* Pins ControlAH_H */
#define CY_PINS_ControlAH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "ControlAH_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 ControlAH__PORT == 15 && ((ControlAH__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    ControlAH_Write(uint8 value);
void    ControlAH_SetDriveMode(uint8 mode);
uint8   ControlAH_ReadDataReg(void);
uint8   ControlAH_Read(void);
void    ControlAH_SetInterruptMode(uint16 position, uint16 mode);
uint8   ControlAH_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the ControlAH_SetDriveMode() function.
     *  @{
     */
        #define ControlAH_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define ControlAH_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define ControlAH_DM_RES_UP          PIN_DM_RES_UP
        #define ControlAH_DM_RES_DWN         PIN_DM_RES_DWN
        #define ControlAH_DM_OD_LO           PIN_DM_OD_LO
        #define ControlAH_DM_OD_HI           PIN_DM_OD_HI
        #define ControlAH_DM_STRONG          PIN_DM_STRONG
        #define ControlAH_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define ControlAH_MASK               ControlAH__MASK
#define ControlAH_SHIFT              ControlAH__SHIFT
#define ControlAH_WIDTH              1u

/* Interrupt constants */
#if defined(ControlAH__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in ControlAH_SetInterruptMode() function.
     *  @{
     */
        #define ControlAH_INTR_NONE      (uint16)(0x0000u)
        #define ControlAH_INTR_RISING    (uint16)(0x0001u)
        #define ControlAH_INTR_FALLING   (uint16)(0x0002u)
        #define ControlAH_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define ControlAH_INTR_MASK      (0x01u) 
#endif /* (ControlAH__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define ControlAH_PS                     (* (reg8 *) ControlAH__PS)
/* Data Register */
#define ControlAH_DR                     (* (reg8 *) ControlAH__DR)
/* Port Number */
#define ControlAH_PRT_NUM                (* (reg8 *) ControlAH__PRT) 
/* Connect to Analog Globals */                                                  
#define ControlAH_AG                     (* (reg8 *) ControlAH__AG)                       
/* Analog MUX bux enable */
#define ControlAH_AMUX                   (* (reg8 *) ControlAH__AMUX) 
/* Bidirectional Enable */                                                        
#define ControlAH_BIE                    (* (reg8 *) ControlAH__BIE)
/* Bit-mask for Aliased Register Access */
#define ControlAH_BIT_MASK               (* (reg8 *) ControlAH__BIT_MASK)
/* Bypass Enable */
#define ControlAH_BYP                    (* (reg8 *) ControlAH__BYP)
/* Port wide control signals */                                                   
#define ControlAH_CTL                    (* (reg8 *) ControlAH__CTL)
/* Drive Modes */
#define ControlAH_DM0                    (* (reg8 *) ControlAH__DM0) 
#define ControlAH_DM1                    (* (reg8 *) ControlAH__DM1)
#define ControlAH_DM2                    (* (reg8 *) ControlAH__DM2) 
/* Input Buffer Disable Override */
#define ControlAH_INP_DIS                (* (reg8 *) ControlAH__INP_DIS)
/* LCD Common or Segment Drive */
#define ControlAH_LCD_COM_SEG            (* (reg8 *) ControlAH__LCD_COM_SEG)
/* Enable Segment LCD */
#define ControlAH_LCD_EN                 (* (reg8 *) ControlAH__LCD_EN)
/* Slew Rate Control */
#define ControlAH_SLW                    (* (reg8 *) ControlAH__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define ControlAH_PRTDSI__CAPS_SEL       (* (reg8 *) ControlAH__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define ControlAH_PRTDSI__DBL_SYNC_IN    (* (reg8 *) ControlAH__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define ControlAH_PRTDSI__OE_SEL0        (* (reg8 *) ControlAH__PRTDSI__OE_SEL0) 
#define ControlAH_PRTDSI__OE_SEL1        (* (reg8 *) ControlAH__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define ControlAH_PRTDSI__OUT_SEL0       (* (reg8 *) ControlAH__PRTDSI__OUT_SEL0) 
#define ControlAH_PRTDSI__OUT_SEL1       (* (reg8 *) ControlAH__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define ControlAH_PRTDSI__SYNC_OUT       (* (reg8 *) ControlAH__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(ControlAH__SIO_CFG)
    #define ControlAH_SIO_HYST_EN        (* (reg8 *) ControlAH__SIO_HYST_EN)
    #define ControlAH_SIO_REG_HIFREQ     (* (reg8 *) ControlAH__SIO_REG_HIFREQ)
    #define ControlAH_SIO_CFG            (* (reg8 *) ControlAH__SIO_CFG)
    #define ControlAH_SIO_DIFF           (* (reg8 *) ControlAH__SIO_DIFF)
#endif /* (ControlAH__SIO_CFG) */

/* Interrupt Registers */
#if defined(ControlAH__INTSTAT)
    #define ControlAH_INTSTAT            (* (reg8 *) ControlAH__INTSTAT)
    #define ControlAH_SNAP               (* (reg8 *) ControlAH__SNAP)
    
	#define ControlAH_0_INTTYPE_REG 		(* (reg8 *) ControlAH__0__INTTYPE)
#endif /* (ControlAH__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_ControlAH_H */


/* [] END OF FILE */
