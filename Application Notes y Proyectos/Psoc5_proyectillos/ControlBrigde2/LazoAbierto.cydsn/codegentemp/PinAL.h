/*******************************************************************************
* File Name: PinAL.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinAL_H) /* Pins PinAL_H */
#define CY_PINS_PinAL_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinAL_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinAL__PORT == 15 && ((PinAL__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinAL_Write(uint8 value);
void    PinAL_SetDriveMode(uint8 mode);
uint8   PinAL_ReadDataReg(void);
uint8   PinAL_Read(void);
void    PinAL_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinAL_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinAL_SetDriveMode() function.
     *  @{
     */
        #define PinAL_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinAL_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinAL_DM_RES_UP          PIN_DM_RES_UP
        #define PinAL_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinAL_DM_OD_LO           PIN_DM_OD_LO
        #define PinAL_DM_OD_HI           PIN_DM_OD_HI
        #define PinAL_DM_STRONG          PIN_DM_STRONG
        #define PinAL_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinAL_MASK               PinAL__MASK
#define PinAL_SHIFT              PinAL__SHIFT
#define PinAL_WIDTH              1u

/* Interrupt constants */
#if defined(PinAL__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinAL_SetInterruptMode() function.
     *  @{
     */
        #define PinAL_INTR_NONE      (uint16)(0x0000u)
        #define PinAL_INTR_RISING    (uint16)(0x0001u)
        #define PinAL_INTR_FALLING   (uint16)(0x0002u)
        #define PinAL_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinAL_INTR_MASK      (0x01u) 
#endif /* (PinAL__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinAL_PS                     (* (reg8 *) PinAL__PS)
/* Data Register */
#define PinAL_DR                     (* (reg8 *) PinAL__DR)
/* Port Number */
#define PinAL_PRT_NUM                (* (reg8 *) PinAL__PRT) 
/* Connect to Analog Globals */                                                  
#define PinAL_AG                     (* (reg8 *) PinAL__AG)                       
/* Analog MUX bux enable */
#define PinAL_AMUX                   (* (reg8 *) PinAL__AMUX) 
/* Bidirectional Enable */                                                        
#define PinAL_BIE                    (* (reg8 *) PinAL__BIE)
/* Bit-mask for Aliased Register Access */
#define PinAL_BIT_MASK               (* (reg8 *) PinAL__BIT_MASK)
/* Bypass Enable */
#define PinAL_BYP                    (* (reg8 *) PinAL__BYP)
/* Port wide control signals */                                                   
#define PinAL_CTL                    (* (reg8 *) PinAL__CTL)
/* Drive Modes */
#define PinAL_DM0                    (* (reg8 *) PinAL__DM0) 
#define PinAL_DM1                    (* (reg8 *) PinAL__DM1)
#define PinAL_DM2                    (* (reg8 *) PinAL__DM2) 
/* Input Buffer Disable Override */
#define PinAL_INP_DIS                (* (reg8 *) PinAL__INP_DIS)
/* LCD Common or Segment Drive */
#define PinAL_LCD_COM_SEG            (* (reg8 *) PinAL__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinAL_LCD_EN                 (* (reg8 *) PinAL__LCD_EN)
/* Slew Rate Control */
#define PinAL_SLW                    (* (reg8 *) PinAL__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinAL_PRTDSI__CAPS_SEL       (* (reg8 *) PinAL__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinAL_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinAL__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinAL_PRTDSI__OE_SEL0        (* (reg8 *) PinAL__PRTDSI__OE_SEL0) 
#define PinAL_PRTDSI__OE_SEL1        (* (reg8 *) PinAL__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinAL_PRTDSI__OUT_SEL0       (* (reg8 *) PinAL__PRTDSI__OUT_SEL0) 
#define PinAL_PRTDSI__OUT_SEL1       (* (reg8 *) PinAL__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinAL_PRTDSI__SYNC_OUT       (* (reg8 *) PinAL__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinAL__SIO_CFG)
    #define PinAL_SIO_HYST_EN        (* (reg8 *) PinAL__SIO_HYST_EN)
    #define PinAL_SIO_REG_HIFREQ     (* (reg8 *) PinAL__SIO_REG_HIFREQ)
    #define PinAL_SIO_CFG            (* (reg8 *) PinAL__SIO_CFG)
    #define PinAL_SIO_DIFF           (* (reg8 *) PinAL__SIO_DIFF)
#endif /* (PinAL__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinAL__INTSTAT)
    #define PinAL_INTSTAT            (* (reg8 *) PinAL__INTSTAT)
    #define PinAL_SNAP               (* (reg8 *) PinAL__SNAP)
    
	#define PinAL_0_INTTYPE_REG 		(* (reg8 *) PinAL__0__INTTYPE)
#endif /* (PinAL__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinAL_H */


/* [] END OF FILE */
