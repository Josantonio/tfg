/*******************************************************************************
* File Name: PinCH.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinCH_H) /* Pins PinCH_H */
#define CY_PINS_PinCH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinCH_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinCH__PORT == 15 && ((PinCH__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinCH_Write(uint8 value);
void    PinCH_SetDriveMode(uint8 mode);
uint8   PinCH_ReadDataReg(void);
uint8   PinCH_Read(void);
void    PinCH_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinCH_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinCH_SetDriveMode() function.
     *  @{
     */
        #define PinCH_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinCH_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinCH_DM_RES_UP          PIN_DM_RES_UP
        #define PinCH_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinCH_DM_OD_LO           PIN_DM_OD_LO
        #define PinCH_DM_OD_HI           PIN_DM_OD_HI
        #define PinCH_DM_STRONG          PIN_DM_STRONG
        #define PinCH_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinCH_MASK               PinCH__MASK
#define PinCH_SHIFT              PinCH__SHIFT
#define PinCH_WIDTH              1u

/* Interrupt constants */
#if defined(PinCH__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinCH_SetInterruptMode() function.
     *  @{
     */
        #define PinCH_INTR_NONE      (uint16)(0x0000u)
        #define PinCH_INTR_RISING    (uint16)(0x0001u)
        #define PinCH_INTR_FALLING   (uint16)(0x0002u)
        #define PinCH_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinCH_INTR_MASK      (0x01u) 
#endif /* (PinCH__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinCH_PS                     (* (reg8 *) PinCH__PS)
/* Data Register */
#define PinCH_DR                     (* (reg8 *) PinCH__DR)
/* Port Number */
#define PinCH_PRT_NUM                (* (reg8 *) PinCH__PRT) 
/* Connect to Analog Globals */                                                  
#define PinCH_AG                     (* (reg8 *) PinCH__AG)                       
/* Analog MUX bux enable */
#define PinCH_AMUX                   (* (reg8 *) PinCH__AMUX) 
/* Bidirectional Enable */                                                        
#define PinCH_BIE                    (* (reg8 *) PinCH__BIE)
/* Bit-mask for Aliased Register Access */
#define PinCH_BIT_MASK               (* (reg8 *) PinCH__BIT_MASK)
/* Bypass Enable */
#define PinCH_BYP                    (* (reg8 *) PinCH__BYP)
/* Port wide control signals */                                                   
#define PinCH_CTL                    (* (reg8 *) PinCH__CTL)
/* Drive Modes */
#define PinCH_DM0                    (* (reg8 *) PinCH__DM0) 
#define PinCH_DM1                    (* (reg8 *) PinCH__DM1)
#define PinCH_DM2                    (* (reg8 *) PinCH__DM2) 
/* Input Buffer Disable Override */
#define PinCH_INP_DIS                (* (reg8 *) PinCH__INP_DIS)
/* LCD Common or Segment Drive */
#define PinCH_LCD_COM_SEG            (* (reg8 *) PinCH__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinCH_LCD_EN                 (* (reg8 *) PinCH__LCD_EN)
/* Slew Rate Control */
#define PinCH_SLW                    (* (reg8 *) PinCH__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinCH_PRTDSI__CAPS_SEL       (* (reg8 *) PinCH__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinCH_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinCH__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinCH_PRTDSI__OE_SEL0        (* (reg8 *) PinCH__PRTDSI__OE_SEL0) 
#define PinCH_PRTDSI__OE_SEL1        (* (reg8 *) PinCH__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinCH_PRTDSI__OUT_SEL0       (* (reg8 *) PinCH__PRTDSI__OUT_SEL0) 
#define PinCH_PRTDSI__OUT_SEL1       (* (reg8 *) PinCH__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinCH_PRTDSI__SYNC_OUT       (* (reg8 *) PinCH__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinCH__SIO_CFG)
    #define PinCH_SIO_HYST_EN        (* (reg8 *) PinCH__SIO_HYST_EN)
    #define PinCH_SIO_REG_HIFREQ     (* (reg8 *) PinCH__SIO_REG_HIFREQ)
    #define PinCH_SIO_CFG            (* (reg8 *) PinCH__SIO_CFG)
    #define PinCH_SIO_DIFF           (* (reg8 *) PinCH__SIO_DIFF)
#endif /* (PinCH__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinCH__INTSTAT)
    #define PinCH_INTSTAT            (* (reg8 *) PinCH__INTSTAT)
    #define PinCH_SNAP               (* (reg8 *) PinCH__SNAP)
    
	#define PinCH_0_INTTYPE_REG 		(* (reg8 *) PinCH__0__INTTYPE)
#endif /* (PinCH__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinCH_H */


/* [] END OF FILE */
