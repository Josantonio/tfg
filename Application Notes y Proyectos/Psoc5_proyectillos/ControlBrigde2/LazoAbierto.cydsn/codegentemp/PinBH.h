/*******************************************************************************
* File Name: PinBH.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinBH_H) /* Pins PinBH_H */
#define CY_PINS_PinBH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinBH_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinBH__PORT == 15 && ((PinBH__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinBH_Write(uint8 value);
void    PinBH_SetDriveMode(uint8 mode);
uint8   PinBH_ReadDataReg(void);
uint8   PinBH_Read(void);
void    PinBH_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinBH_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinBH_SetDriveMode() function.
     *  @{
     */
        #define PinBH_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinBH_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinBH_DM_RES_UP          PIN_DM_RES_UP
        #define PinBH_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinBH_DM_OD_LO           PIN_DM_OD_LO
        #define PinBH_DM_OD_HI           PIN_DM_OD_HI
        #define PinBH_DM_STRONG          PIN_DM_STRONG
        #define PinBH_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinBH_MASK               PinBH__MASK
#define PinBH_SHIFT              PinBH__SHIFT
#define PinBH_WIDTH              1u

/* Interrupt constants */
#if defined(PinBH__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinBH_SetInterruptMode() function.
     *  @{
     */
        #define PinBH_INTR_NONE      (uint16)(0x0000u)
        #define PinBH_INTR_RISING    (uint16)(0x0001u)
        #define PinBH_INTR_FALLING   (uint16)(0x0002u)
        #define PinBH_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinBH_INTR_MASK      (0x01u) 
#endif /* (PinBH__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinBH_PS                     (* (reg8 *) PinBH__PS)
/* Data Register */
#define PinBH_DR                     (* (reg8 *) PinBH__DR)
/* Port Number */
#define PinBH_PRT_NUM                (* (reg8 *) PinBH__PRT) 
/* Connect to Analog Globals */                                                  
#define PinBH_AG                     (* (reg8 *) PinBH__AG)                       
/* Analog MUX bux enable */
#define PinBH_AMUX                   (* (reg8 *) PinBH__AMUX) 
/* Bidirectional Enable */                                                        
#define PinBH_BIE                    (* (reg8 *) PinBH__BIE)
/* Bit-mask for Aliased Register Access */
#define PinBH_BIT_MASK               (* (reg8 *) PinBH__BIT_MASK)
/* Bypass Enable */
#define PinBH_BYP                    (* (reg8 *) PinBH__BYP)
/* Port wide control signals */                                                   
#define PinBH_CTL                    (* (reg8 *) PinBH__CTL)
/* Drive Modes */
#define PinBH_DM0                    (* (reg8 *) PinBH__DM0) 
#define PinBH_DM1                    (* (reg8 *) PinBH__DM1)
#define PinBH_DM2                    (* (reg8 *) PinBH__DM2) 
/* Input Buffer Disable Override */
#define PinBH_INP_DIS                (* (reg8 *) PinBH__INP_DIS)
/* LCD Common or Segment Drive */
#define PinBH_LCD_COM_SEG            (* (reg8 *) PinBH__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinBH_LCD_EN                 (* (reg8 *) PinBH__LCD_EN)
/* Slew Rate Control */
#define PinBH_SLW                    (* (reg8 *) PinBH__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinBH_PRTDSI__CAPS_SEL       (* (reg8 *) PinBH__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinBH_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinBH__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinBH_PRTDSI__OE_SEL0        (* (reg8 *) PinBH__PRTDSI__OE_SEL0) 
#define PinBH_PRTDSI__OE_SEL1        (* (reg8 *) PinBH__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinBH_PRTDSI__OUT_SEL0       (* (reg8 *) PinBH__PRTDSI__OUT_SEL0) 
#define PinBH_PRTDSI__OUT_SEL1       (* (reg8 *) PinBH__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinBH_PRTDSI__SYNC_OUT       (* (reg8 *) PinBH__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinBH__SIO_CFG)
    #define PinBH_SIO_HYST_EN        (* (reg8 *) PinBH__SIO_HYST_EN)
    #define PinBH_SIO_REG_HIFREQ     (* (reg8 *) PinBH__SIO_REG_HIFREQ)
    #define PinBH_SIO_CFG            (* (reg8 *) PinBH__SIO_CFG)
    #define PinBH_SIO_DIFF           (* (reg8 *) PinBH__SIO_DIFF)
#endif /* (PinBH__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinBH__INTSTAT)
    #define PinBH_INTSTAT            (* (reg8 *) PinBH__INTSTAT)
    #define PinBH_SNAP               (* (reg8 *) PinBH__SNAP)
    
	#define PinBH_0_INTTYPE_REG 		(* (reg8 *) PinBH__0__INTTYPE)
#endif /* (PinBH__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinBH_H */


/* [] END OF FILE */
