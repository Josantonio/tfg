/*******************************************************************************
* File Name: PinCL.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinCL_H) /* Pins PinCL_H */
#define CY_PINS_PinCL_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinCL_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinCL__PORT == 15 && ((PinCL__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinCL_Write(uint8 value);
void    PinCL_SetDriveMode(uint8 mode);
uint8   PinCL_ReadDataReg(void);
uint8   PinCL_Read(void);
void    PinCL_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinCL_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinCL_SetDriveMode() function.
     *  @{
     */
        #define PinCL_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinCL_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinCL_DM_RES_UP          PIN_DM_RES_UP
        #define PinCL_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinCL_DM_OD_LO           PIN_DM_OD_LO
        #define PinCL_DM_OD_HI           PIN_DM_OD_HI
        #define PinCL_DM_STRONG          PIN_DM_STRONG
        #define PinCL_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinCL_MASK               PinCL__MASK
#define PinCL_SHIFT              PinCL__SHIFT
#define PinCL_WIDTH              1u

/* Interrupt constants */
#if defined(PinCL__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinCL_SetInterruptMode() function.
     *  @{
     */
        #define PinCL_INTR_NONE      (uint16)(0x0000u)
        #define PinCL_INTR_RISING    (uint16)(0x0001u)
        #define PinCL_INTR_FALLING   (uint16)(0x0002u)
        #define PinCL_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinCL_INTR_MASK      (0x01u) 
#endif /* (PinCL__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinCL_PS                     (* (reg8 *) PinCL__PS)
/* Data Register */
#define PinCL_DR                     (* (reg8 *) PinCL__DR)
/* Port Number */
#define PinCL_PRT_NUM                (* (reg8 *) PinCL__PRT) 
/* Connect to Analog Globals */                                                  
#define PinCL_AG                     (* (reg8 *) PinCL__AG)                       
/* Analog MUX bux enable */
#define PinCL_AMUX                   (* (reg8 *) PinCL__AMUX) 
/* Bidirectional Enable */                                                        
#define PinCL_BIE                    (* (reg8 *) PinCL__BIE)
/* Bit-mask for Aliased Register Access */
#define PinCL_BIT_MASK               (* (reg8 *) PinCL__BIT_MASK)
/* Bypass Enable */
#define PinCL_BYP                    (* (reg8 *) PinCL__BYP)
/* Port wide control signals */                                                   
#define PinCL_CTL                    (* (reg8 *) PinCL__CTL)
/* Drive Modes */
#define PinCL_DM0                    (* (reg8 *) PinCL__DM0) 
#define PinCL_DM1                    (* (reg8 *) PinCL__DM1)
#define PinCL_DM2                    (* (reg8 *) PinCL__DM2) 
/* Input Buffer Disable Override */
#define PinCL_INP_DIS                (* (reg8 *) PinCL__INP_DIS)
/* LCD Common or Segment Drive */
#define PinCL_LCD_COM_SEG            (* (reg8 *) PinCL__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinCL_LCD_EN                 (* (reg8 *) PinCL__LCD_EN)
/* Slew Rate Control */
#define PinCL_SLW                    (* (reg8 *) PinCL__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinCL_PRTDSI__CAPS_SEL       (* (reg8 *) PinCL__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinCL_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinCL__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinCL_PRTDSI__OE_SEL0        (* (reg8 *) PinCL__PRTDSI__OE_SEL0) 
#define PinCL_PRTDSI__OE_SEL1        (* (reg8 *) PinCL__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinCL_PRTDSI__OUT_SEL0       (* (reg8 *) PinCL__PRTDSI__OUT_SEL0) 
#define PinCL_PRTDSI__OUT_SEL1       (* (reg8 *) PinCL__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinCL_PRTDSI__SYNC_OUT       (* (reg8 *) PinCL__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinCL__SIO_CFG)
    #define PinCL_SIO_HYST_EN        (* (reg8 *) PinCL__SIO_HYST_EN)
    #define PinCL_SIO_REG_HIFREQ     (* (reg8 *) PinCL__SIO_REG_HIFREQ)
    #define PinCL_SIO_CFG            (* (reg8 *) PinCL__SIO_CFG)
    #define PinCL_SIO_DIFF           (* (reg8 *) PinCL__SIO_DIFF)
#endif /* (PinCL__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinCL__INTSTAT)
    #define PinCL_INTSTAT            (* (reg8 *) PinCL__INTSTAT)
    #define PinCL_SNAP               (* (reg8 *) PinCL__SNAP)
    
	#define PinCL_0_INTTYPE_REG 		(* (reg8 *) PinCL__0__INTTYPE)
#endif /* (PinCL__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinCL_H */


/* [] END OF FILE */
