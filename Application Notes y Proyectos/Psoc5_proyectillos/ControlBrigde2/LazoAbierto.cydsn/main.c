/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

typedef unsigned char bool;
#define FALSE 0
#define TRUE (!FALSE)

bool state_button = FALSE;
bool arranque = TRUE;

uint8 pasos[6] = {1,5,4,6,2,3};
uint8 j = 0;
uint8 i = 0;
uint8 PWM_write[21] = {22,23,24,25,26,28,29,30,31,32,33,35,36,37,38,39,41,42,43,44,45};
uint16 retardo[21] = {37500,25862,19737,15957,13393,11538,10135,9036,8152,7426,6818,6303,5859,5474,5137,4839,4573,4335,4121,3927,3750};

CY_ISR ( isr_button_handler){
    state_button = ~state_button;
}

void rampa (void){
    
    //Alignment
    Control_Reg_Write(pasos[5]);
    PWM_WriteCompare(22);
    CyDelay(1000);
    
    for (uint8 i = 0 ; i <=20 ; i++){
        Control_Reg_Write(pasos[j]);
        PWM_WriteCompare(PWM_write[i]);
        CyDelayUs(retardo[i]);
        j++;
        if(j == 6){ j = 0;}
    }
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    PWM_Start();
    UART_Start();
    
    isr_button_StartEx(isr_button_handler);
    
    for(;;)
    {
        if (state_button){
            Pin_LED_Write(1);
            if (arranque){ rampa(); arranque = FALSE;}
            Control_Reg_Write(pasos[j]);
            j++;
            if(j == 6){ j = 0;}   

        }
        else if(!state_button){
            Control_Reg_Write(0);
            Pin_LED_Write(0);
            arranque = TRUE;
            j = 0;
            i = 0;
        }
       CyDelayUs(retardo[i]);
    }
}

/* [] END OF FILE */
