/*******************************************************************************
* File Name: PinAH.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinAH_H) /* Pins PinAH_H */
#define CY_PINS_PinAH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinAH_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinAH__PORT == 15 && ((PinAH__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinAH_Write(uint8 value);
void    PinAH_SetDriveMode(uint8 mode);
uint8   PinAH_ReadDataReg(void);
uint8   PinAH_Read(void);
void    PinAH_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinAH_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinAH_SetDriveMode() function.
     *  @{
     */
        #define PinAH_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinAH_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinAH_DM_RES_UP          PIN_DM_RES_UP
        #define PinAH_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinAH_DM_OD_LO           PIN_DM_OD_LO
        #define PinAH_DM_OD_HI           PIN_DM_OD_HI
        #define PinAH_DM_STRONG          PIN_DM_STRONG
        #define PinAH_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinAH_MASK               PinAH__MASK
#define PinAH_SHIFT              PinAH__SHIFT
#define PinAH_WIDTH              1u

/* Interrupt constants */
#if defined(PinAH__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinAH_SetInterruptMode() function.
     *  @{
     */
        #define PinAH_INTR_NONE      (uint16)(0x0000u)
        #define PinAH_INTR_RISING    (uint16)(0x0001u)
        #define PinAH_INTR_FALLING   (uint16)(0x0002u)
        #define PinAH_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinAH_INTR_MASK      (0x01u) 
#endif /* (PinAH__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinAH_PS                     (* (reg8 *) PinAH__PS)
/* Data Register */
#define PinAH_DR                     (* (reg8 *) PinAH__DR)
/* Port Number */
#define PinAH_PRT_NUM                (* (reg8 *) PinAH__PRT) 
/* Connect to Analog Globals */                                                  
#define PinAH_AG                     (* (reg8 *) PinAH__AG)                       
/* Analog MUX bux enable */
#define PinAH_AMUX                   (* (reg8 *) PinAH__AMUX) 
/* Bidirectional Enable */                                                        
#define PinAH_BIE                    (* (reg8 *) PinAH__BIE)
/* Bit-mask for Aliased Register Access */
#define PinAH_BIT_MASK               (* (reg8 *) PinAH__BIT_MASK)
/* Bypass Enable */
#define PinAH_BYP                    (* (reg8 *) PinAH__BYP)
/* Port wide control signals */                                                   
#define PinAH_CTL                    (* (reg8 *) PinAH__CTL)
/* Drive Modes */
#define PinAH_DM0                    (* (reg8 *) PinAH__DM0) 
#define PinAH_DM1                    (* (reg8 *) PinAH__DM1)
#define PinAH_DM2                    (* (reg8 *) PinAH__DM2) 
/* Input Buffer Disable Override */
#define PinAH_INP_DIS                (* (reg8 *) PinAH__INP_DIS)
/* LCD Common or Segment Drive */
#define PinAH_LCD_COM_SEG            (* (reg8 *) PinAH__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinAH_LCD_EN                 (* (reg8 *) PinAH__LCD_EN)
/* Slew Rate Control */
#define PinAH_SLW                    (* (reg8 *) PinAH__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinAH_PRTDSI__CAPS_SEL       (* (reg8 *) PinAH__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinAH_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinAH__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinAH_PRTDSI__OE_SEL0        (* (reg8 *) PinAH__PRTDSI__OE_SEL0) 
#define PinAH_PRTDSI__OE_SEL1        (* (reg8 *) PinAH__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinAH_PRTDSI__OUT_SEL0       (* (reg8 *) PinAH__PRTDSI__OUT_SEL0) 
#define PinAH_PRTDSI__OUT_SEL1       (* (reg8 *) PinAH__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinAH_PRTDSI__SYNC_OUT       (* (reg8 *) PinAH__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinAH__SIO_CFG)
    #define PinAH_SIO_HYST_EN        (* (reg8 *) PinAH__SIO_HYST_EN)
    #define PinAH_SIO_REG_HIFREQ     (* (reg8 *) PinAH__SIO_REG_HIFREQ)
    #define PinAH_SIO_CFG            (* (reg8 *) PinAH__SIO_CFG)
    #define PinAH_SIO_DIFF           (* (reg8 *) PinAH__SIO_DIFF)
#endif /* (PinAH__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinAH__INTSTAT)
    #define PinAH_INTSTAT            (* (reg8 *) PinAH__INTSTAT)
    #define PinAH_SNAP               (* (reg8 *) PinAH__SNAP)
    
	#define PinAH_0_INTTYPE_REG 		(* (reg8 *) PinAH__0__INTTYPE)
#endif /* (PinAH__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinAH_H */


/* [] END OF FILE */
