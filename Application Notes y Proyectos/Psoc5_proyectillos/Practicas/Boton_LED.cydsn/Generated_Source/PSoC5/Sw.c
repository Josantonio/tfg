/*******************************************************************************
* File Name: Sw.c  
* Version 2.10
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "Sw.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 Sw__PORT == 15 && ((Sw__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: Sw_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void Sw_Write(uint8 value) 
{
    uint8 staticBits = (Sw_DR & (uint8)(~Sw_MASK));
    Sw_DR = staticBits | ((uint8)(value << Sw_SHIFT) & Sw_MASK);
}


/*******************************************************************************
* Function Name: Sw_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to one of the following drive modes.
*
*  Sw_DM_STRONG     Strong Drive 
*  Sw_DM_OD_HI      Open Drain, Drives High 
*  Sw_DM_OD_LO      Open Drain, Drives Low 
*  Sw_DM_RES_UP     Resistive Pull Up 
*  Sw_DM_RES_DWN    Resistive Pull Down 
*  Sw_DM_RES_UPDWN  Resistive Pull Up/Down 
*  Sw_DM_DIG_HIZ    High Impedance Digital 
*  Sw_DM_ALG_HIZ    High Impedance Analog 
*
* Return: 
*  None
*
*******************************************************************************/
void Sw_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(Sw_0, mode);
}


/*******************************************************************************
* Function Name: Sw_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro Sw_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 Sw_Read(void) 
{
    return (Sw_PS & Sw_MASK) >> Sw_SHIFT;
}


/*******************************************************************************
* Function Name: Sw_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 Sw_ReadDataReg(void) 
{
    return (Sw_DR & Sw_MASK) >> Sw_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(Sw_INTSTAT) 

    /*******************************************************************************
    * Function Name: Sw_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 Sw_ClearInterrupt(void) 
    {
        return (Sw_INTSTAT & Sw_MASK) >> Sw_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
