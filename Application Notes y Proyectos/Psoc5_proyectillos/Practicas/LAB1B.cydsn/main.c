
#include <project.h>
/*Variables globales*/
uint8 intep ;

void toggleLed( void ) {
    //Fijamos el valor del LED a su complementario
    LED_pin_Write (LED_pin_Read( ) ^ 1U) ;
}
void toggleLed2( void ) {
    //Fijamos el valor del LED2 a su complementario
    LED2_pin_Write (LED2_pin_Read( ) ^ 1U) ;
}
CY_ISR(Interrupt){
    intep = 1;
}
int main ( ) {
 /*Inicializamos los componentes en el orden correcto*/
    // 1. Interrupciones
    isr_RelojLED_StartEx(Interrupt);
    //Fijamos que esta interrupción sea la única del sistema
    isr_RelojLED_SetVector(&Interrupt);
    // 2. Fuentes de interrupción (El reloj se habilita en el hardware)
    // 3. Habilitamos interrupciones globales
    CyGlobalIntEnable; //Macro para habilitarlas
    //Inicializamos el valor de interrupción
    intep = 0;
    
    for(;;){
        toggleLed2();
        if(intep == 1){
            toggleLed();
            intep = 0;
        }
    }
}
