/*******************************************************************************
* File Name: isrT.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_isrT_H)
#define CY_ISR_isrT_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void isrT_Start(void);
void isrT_StartEx(cyisraddress address);
void isrT_Stop(void);

CY_ISR_PROTO(isrT_Interrupt);

void isrT_SetVector(cyisraddress address);
cyisraddress isrT_GetVector(void);

void isrT_SetPriority(uint8 priority);
uint8 isrT_GetPriority(void);

void isrT_Enable(void);
uint8 isrT_GetState(void);
void isrT_Disable(void);

void isrT_SetPending(void);
void isrT_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the isrT ISR. */
#define isrT_INTC_VECTOR            ((reg32 *) isrT__INTC_VECT)

/* Address of the isrT ISR priority. */
#define isrT_INTC_PRIOR             ((reg8 *) isrT__INTC_PRIOR_REG)

/* Priority of the isrT interrupt. */
#define isrT_INTC_PRIOR_NUMBER      isrT__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable isrT interrupt. */
#define isrT_INTC_SET_EN            ((reg32 *) isrT__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the isrT interrupt. */
#define isrT_INTC_CLR_EN            ((reg32 *) isrT__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the isrT interrupt state to pending. */
#define isrT_INTC_SET_PD            ((reg32 *) isrT__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the isrT interrupt. */
#define isrT_INTC_CLR_PD            ((reg32 *) isrT__INTC_CLR_PD_REG)


#endif /* CY_ISR_isrT_H */


/* [] END OF FILE */
