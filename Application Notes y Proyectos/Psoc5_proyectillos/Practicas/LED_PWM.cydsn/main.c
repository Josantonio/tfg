/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
//Variables globales
uint8 intep ;
uint8 mili ;
int timeT = 0; //Para llevar la cuenta en ms

//Definimos lo que pasa en cada interrupción
CY_ISR(InterruptPWM){
    intep = 1; //Se activa la interrupción que hace que funcione el LED controlado por PWM
}
CY_ISR(InterruptT){
    mili = 1; //Se activa cada vez que pasa 1 ms
    timeT = timeT + 1; //Incrementamos el contador de ms
}
void toggleLED(){
}
void count(){//Para llevar la cuenta del tiempo
    timeT = 0;
}
int main()
{
    CyGlobalIntEnable; /* Uncomment this line to enable global interrupts. */
    
    isrPWM_StartEx(InterruptPWM);
    isrT_StartEx(InterruptT);
    
    //Inicializamos el valor de las interrupciones
    intep = 0;
    mili = 0;
    
    for(;;)
    {
       if(intep == 1){ //Se activa la interrupción por el PWM, LED se va a activar
        if(Button_Read()){ //Boton pulsado f = 0.5 Hz
        }
        else{ //Botón sin pulsar f = 5 Hz
        }
    }
}

/* [] END OF FILE */
