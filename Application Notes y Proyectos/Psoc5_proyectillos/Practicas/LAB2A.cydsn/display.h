/* ========================================
*Pequeña librería para el manejo del Display
 * ========================================
*/
void DisplayWelcome ( void ) ;
void DisplayTitle ( void ) ;
void DisplayCount ( uint8 count ) ;
/* [] END OF FILE */
