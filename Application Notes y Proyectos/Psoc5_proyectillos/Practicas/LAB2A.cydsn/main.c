/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include <display.h>

int main()
{
    uint8 contador = 0u;
    CharLCD_Start(); //Inicializamos la LCD
    DisplayWelcome(); //Mensaje de saludo
    DisplayTitle(); //Mensaje de título (son llamadas a funciones dentro de display.c

    /* CyGlobalIntEnable; */ /* Uncomment this line to enable global interrupts. */
    for(;;)
    {
        DisplayCount(contador);
        contador++;
        if (contador > 99){
            contador = 0; //Cuenta hasta 99 y vuelve a 0
        }
    }
}
/* [] END OF FILE */
