/* ========================================
 * Pequeña librería para el control del display
 * ========================================
*/
#include <project.h>
#include <display.h>

void DisplayWelcome(void){
    CharLCD_ClearDisplay();
    CharLCD_Position(0U, 0U); // Fila, columna
    CharLCD_PrintString("PSoC Mola!");
}
void DisplayTitle(void){
    CharLCD_Position(1U, 0U);
    CharLCD_PrintString("Contador: ");
}
void DisplayCount(uint8 count){
    CharLCD_Position(1U, 12U);
    CharLCD_PrintNumber((uint16) count);
    CharLCD_PrintString(" ");
}
/* [] END OF FILE */
