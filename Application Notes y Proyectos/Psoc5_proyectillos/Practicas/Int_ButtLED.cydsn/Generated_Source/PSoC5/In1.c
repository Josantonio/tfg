/*******************************************************************************
* File Name: In1.c  
* Version 2.10
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2014, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "In1.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 In1__PORT == 15 && ((In1__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: In1_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void In1_Write(uint8 value) 
{
    uint8 staticBits = (In1_DR & (uint8)(~In1_MASK));
    In1_DR = staticBits | ((uint8)(value << In1_SHIFT) & In1_MASK);
}


/*******************************************************************************
* Function Name: In1_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to one of the following drive modes.
*
*  In1_DM_STRONG     Strong Drive 
*  In1_DM_OD_HI      Open Drain, Drives High 
*  In1_DM_OD_LO      Open Drain, Drives Low 
*  In1_DM_RES_UP     Resistive Pull Up 
*  In1_DM_RES_DWN    Resistive Pull Down 
*  In1_DM_RES_UPDWN  Resistive Pull Up/Down 
*  In1_DM_DIG_HIZ    High Impedance Digital 
*  In1_DM_ALG_HIZ    High Impedance Analog 
*
* Return: 
*  None
*
*******************************************************************************/
void In1_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(In1_0, mode);
}


/*******************************************************************************
* Function Name: In1_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro In1_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 In1_Read(void) 
{
    return (In1_PS & In1_MASK) >> In1_SHIFT;
}


/*******************************************************************************
* Function Name: In1_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 In1_ReadDataReg(void) 
{
    return (In1_DR & In1_MASK) >> In1_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(In1_INTSTAT) 

    /*******************************************************************************
    * Function Name: In1_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 In1_ClearInterrupt(void) 
    {
        return (In1_INTSTAT & In1_MASK) >> In1_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
