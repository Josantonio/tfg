/* ========================================
*Manejo de INTERRUPCIONES
* Al pulsar 4 veces un botón queremos que se active una interrupción
* que activará el Timer y mantendrá encendido un LED durante un tiempo
 * ========================================
*/
#include <project.h>

CY_ISR(InterruptC){ //Cuando se ha pulsado el botón 4 veces se activa esta interrupción
    LED_Write(1); //Encendemos LED
    Timer_Start(); //Activamos el Timer
}

CY_ISR(InterruptT){ //Cuando acaba el Timer
    LED_Write(0); //Apagamos el LED
    Counter_Stop(); //Paramos contador
    Timer_Stop(); //Apagamos Timer
    
   // Counter_WritePeriod(4);//Escribimos el periodo del contador
   // Timer_WritePeriod(48000000);//Escribimos el período del Timer
    
     Counter_Start(); //Volvemos a encender el contador, que es el que inicializa el programa
}

int main()
{
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    CyGlobalIntEnable; /* Uncomment this line to enable global interrupts. */
    
   // Clock1_Start(); //Iniciamos relojes
    //BUS_CLK_Start();
    
    Counter_Start(); //Encendemos contador
    isrC_StartEx(InterruptC);
    isrT_StartEx(InterruptT);
    
    for(;;)
    {
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
