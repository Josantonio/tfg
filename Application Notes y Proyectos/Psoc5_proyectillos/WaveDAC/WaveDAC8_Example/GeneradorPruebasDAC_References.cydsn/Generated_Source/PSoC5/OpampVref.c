/*******************************************************************************
* File Name: OpampVref.c
* Version 1.90
*
* Description:
*  This file provides the source code to the API for OpAmp (Analog Buffer) 
*  Component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "OpampVref.h"

uint8 OpampVref_initVar = 0u;


/*******************************************************************************   
* Function Name: OpampVref_Init
********************************************************************************
*
* Summary:
*  Initialize component's parameters to the parameters set by user in the 
*  customizer of the component placed onto schematic. Usually called in 
*  OpampVref_Start().
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void OpampVref_Init(void) 
{
    OpampVref_SetPower(OpampVref_DEFAULT_POWER);
}


/*******************************************************************************   
* Function Name: OpampVref_Enable
********************************************************************************
*
* Summary:
*  Enables the OpAmp block operation
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void OpampVref_Enable(void) 
{
    /* Enable negative charge pumps in ANIF */
    OpampVref_PUMP_CR1_REG  |= (OpampVref_PUMP_CR1_CLKSEL | OpampVref_PUMP_CR1_FORCE);

    /* Enable power to buffer in active mode */
    OpampVref_PM_ACT_CFG_REG |= OpampVref_ACT_PWR_EN;

    /* Enable power to buffer in alternative active mode */
    OpampVref_PM_STBY_CFG_REG |= OpampVref_STBY_PWR_EN;
}


/*******************************************************************************
* Function Name:   OpampVref_Start
********************************************************************************
*
* Summary:
*  The start function initializes the Analog Buffer with the default values and 
*  sets the power to the given level. A power level of 0, is same as 
*  executing the stop function.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  OpampVref_initVar: Used to check the initial configuration, modified 
*  when this function is called for the first time.
*
*******************************************************************************/
void OpampVref_Start(void) 
{
    if(OpampVref_initVar == 0u)
    {
        OpampVref_initVar = 1u;
        OpampVref_Init();
    }

    OpampVref_Enable();
}


/*******************************************************************************
* Function Name: OpampVref_Stop
********************************************************************************
*
* Summary:
*  Powers down amplifier to lowest power state.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void OpampVref_Stop(void) 
{
    /* Disable power to buffer in active mode template */
    OpampVref_PM_ACT_CFG_REG &= (uint8)(~OpampVref_ACT_PWR_EN);

    /* Disable power to buffer in alternative active mode template */
    OpampVref_PM_STBY_CFG_REG &= (uint8)(~OpampVref_STBY_PWR_EN);
    
    /* Disable negative charge pumps for ANIF only if all ABuf is turned OFF */
    if(OpampVref_PM_ACT_CFG_REG == 0u)
    {
        OpampVref_PUMP_CR1_REG &= (uint8)(~(OpampVref_PUMP_CR1_CLKSEL | OpampVref_PUMP_CR1_FORCE));
    }
}


/*******************************************************************************
* Function Name: OpampVref_SetPower
********************************************************************************
*
* Summary:
*  Sets power level of Analog buffer.
*
* Parameters: 
*  power: PSoC3: Sets power level between low (1) and high power (3).
*         PSoC5: Sets power level High (0)
*
* Return:
*  void
*
**********************************************************************************/
void OpampVref_SetPower(uint8 power) 
{
    #if (CY_PSOC3 || CY_PSOC5LP)
        OpampVref_CR_REG &= (uint8)(~OpampVref_PWR_MASK);
        OpampVref_CR_REG |= power & OpampVref_PWR_MASK;      /* Set device power */
    #else
        CYASSERT(OpampVref_HIGHPOWER == power);
    #endif /* CY_PSOC3 || CY_PSOC5LP */
}


/* [] END OF FILE */
