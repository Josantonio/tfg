/*******************************************************************************
* File Name: PinRef.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinRef_H) /* Pins PinRef_H */
#define CY_PINS_PinRef_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinRef_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinRef__PORT == 15 && ((PinRef__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinRef_Write(uint8 value);
void    PinRef_SetDriveMode(uint8 mode);
uint8   PinRef_ReadDataReg(void);
uint8   PinRef_Read(void);
void    PinRef_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinRef_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinRef_SetDriveMode() function.
     *  @{
     */
        #define PinRef_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinRef_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinRef_DM_RES_UP          PIN_DM_RES_UP
        #define PinRef_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinRef_DM_OD_LO           PIN_DM_OD_LO
        #define PinRef_DM_OD_HI           PIN_DM_OD_HI
        #define PinRef_DM_STRONG          PIN_DM_STRONG
        #define PinRef_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinRef_MASK               PinRef__MASK
#define PinRef_SHIFT              PinRef__SHIFT
#define PinRef_WIDTH              1u

/* Interrupt constants */
#if defined(PinRef__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinRef_SetInterruptMode() function.
     *  @{
     */
        #define PinRef_INTR_NONE      (uint16)(0x0000u)
        #define PinRef_INTR_RISING    (uint16)(0x0001u)
        #define PinRef_INTR_FALLING   (uint16)(0x0002u)
        #define PinRef_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinRef_INTR_MASK      (0x01u) 
#endif /* (PinRef__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinRef_PS                     (* (reg8 *) PinRef__PS)
/* Data Register */
#define PinRef_DR                     (* (reg8 *) PinRef__DR)
/* Port Number */
#define PinRef_PRT_NUM                (* (reg8 *) PinRef__PRT) 
/* Connect to Analog Globals */                                                  
#define PinRef_AG                     (* (reg8 *) PinRef__AG)                       
/* Analog MUX bux enable */
#define PinRef_AMUX                   (* (reg8 *) PinRef__AMUX) 
/* Bidirectional Enable */                                                        
#define PinRef_BIE                    (* (reg8 *) PinRef__BIE)
/* Bit-mask for Aliased Register Access */
#define PinRef_BIT_MASK               (* (reg8 *) PinRef__BIT_MASK)
/* Bypass Enable */
#define PinRef_BYP                    (* (reg8 *) PinRef__BYP)
/* Port wide control signals */                                                   
#define PinRef_CTL                    (* (reg8 *) PinRef__CTL)
/* Drive Modes */
#define PinRef_DM0                    (* (reg8 *) PinRef__DM0) 
#define PinRef_DM1                    (* (reg8 *) PinRef__DM1)
#define PinRef_DM2                    (* (reg8 *) PinRef__DM2) 
/* Input Buffer Disable Override */
#define PinRef_INP_DIS                (* (reg8 *) PinRef__INP_DIS)
/* LCD Common or Segment Drive */
#define PinRef_LCD_COM_SEG            (* (reg8 *) PinRef__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinRef_LCD_EN                 (* (reg8 *) PinRef__LCD_EN)
/* Slew Rate Control */
#define PinRef_SLW                    (* (reg8 *) PinRef__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinRef_PRTDSI__CAPS_SEL       (* (reg8 *) PinRef__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinRef_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinRef__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinRef_PRTDSI__OE_SEL0        (* (reg8 *) PinRef__PRTDSI__OE_SEL0) 
#define PinRef_PRTDSI__OE_SEL1        (* (reg8 *) PinRef__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinRef_PRTDSI__OUT_SEL0       (* (reg8 *) PinRef__PRTDSI__OUT_SEL0) 
#define PinRef_PRTDSI__OUT_SEL1       (* (reg8 *) PinRef__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinRef_PRTDSI__SYNC_OUT       (* (reg8 *) PinRef__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinRef__SIO_CFG)
    #define PinRef_SIO_HYST_EN        (* (reg8 *) PinRef__SIO_HYST_EN)
    #define PinRef_SIO_REG_HIFREQ     (* (reg8 *) PinRef__SIO_REG_HIFREQ)
    #define PinRef_SIO_CFG            (* (reg8 *) PinRef__SIO_CFG)
    #define PinRef_SIO_DIFF           (* (reg8 *) PinRef__SIO_DIFF)
#endif /* (PinRef__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinRef__INTSTAT)
    #define PinRef_INTSTAT            (* (reg8 *) PinRef__INTSTAT)
    #define PinRef_SNAP               (* (reg8 *) PinRef__SNAP)
    
	#define PinRef_0_INTTYPE_REG 		(* (reg8 *) PinRef__0__INTTYPE)
#endif /* (PinRef__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinRef_H */


/* [] END OF FILE */
