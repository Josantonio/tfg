/*******************************************************************************
* File Name: PinVref.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinVref_H) /* Pins PinVref_H */
#define CY_PINS_PinVref_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinVref_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinVref__PORT == 15 && ((PinVref__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinVref_Write(uint8 value);
void    PinVref_SetDriveMode(uint8 mode);
uint8   PinVref_ReadDataReg(void);
uint8   PinVref_Read(void);
void    PinVref_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinVref_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinVref_SetDriveMode() function.
     *  @{
     */
        #define PinVref_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinVref_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinVref_DM_RES_UP          PIN_DM_RES_UP
        #define PinVref_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinVref_DM_OD_LO           PIN_DM_OD_LO
        #define PinVref_DM_OD_HI           PIN_DM_OD_HI
        #define PinVref_DM_STRONG          PIN_DM_STRONG
        #define PinVref_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinVref_MASK               PinVref__MASK
#define PinVref_SHIFT              PinVref__SHIFT
#define PinVref_WIDTH              1u

/* Interrupt constants */
#if defined(PinVref__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinVref_SetInterruptMode() function.
     *  @{
     */
        #define PinVref_INTR_NONE      (uint16)(0x0000u)
        #define PinVref_INTR_RISING    (uint16)(0x0001u)
        #define PinVref_INTR_FALLING   (uint16)(0x0002u)
        #define PinVref_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinVref_INTR_MASK      (0x01u) 
#endif /* (PinVref__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinVref_PS                     (* (reg8 *) PinVref__PS)
/* Data Register */
#define PinVref_DR                     (* (reg8 *) PinVref__DR)
/* Port Number */
#define PinVref_PRT_NUM                (* (reg8 *) PinVref__PRT) 
/* Connect to Analog Globals */                                                  
#define PinVref_AG                     (* (reg8 *) PinVref__AG)                       
/* Analog MUX bux enable */
#define PinVref_AMUX                   (* (reg8 *) PinVref__AMUX) 
/* Bidirectional Enable */                                                        
#define PinVref_BIE                    (* (reg8 *) PinVref__BIE)
/* Bit-mask for Aliased Register Access */
#define PinVref_BIT_MASK               (* (reg8 *) PinVref__BIT_MASK)
/* Bypass Enable */
#define PinVref_BYP                    (* (reg8 *) PinVref__BYP)
/* Port wide control signals */                                                   
#define PinVref_CTL                    (* (reg8 *) PinVref__CTL)
/* Drive Modes */
#define PinVref_DM0                    (* (reg8 *) PinVref__DM0) 
#define PinVref_DM1                    (* (reg8 *) PinVref__DM1)
#define PinVref_DM2                    (* (reg8 *) PinVref__DM2) 
/* Input Buffer Disable Override */
#define PinVref_INP_DIS                (* (reg8 *) PinVref__INP_DIS)
/* LCD Common or Segment Drive */
#define PinVref_LCD_COM_SEG            (* (reg8 *) PinVref__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinVref_LCD_EN                 (* (reg8 *) PinVref__LCD_EN)
/* Slew Rate Control */
#define PinVref_SLW                    (* (reg8 *) PinVref__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinVref_PRTDSI__CAPS_SEL       (* (reg8 *) PinVref__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinVref_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinVref__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinVref_PRTDSI__OE_SEL0        (* (reg8 *) PinVref__PRTDSI__OE_SEL0) 
#define PinVref_PRTDSI__OE_SEL1        (* (reg8 *) PinVref__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinVref_PRTDSI__OUT_SEL0       (* (reg8 *) PinVref__PRTDSI__OUT_SEL0) 
#define PinVref_PRTDSI__OUT_SEL1       (* (reg8 *) PinVref__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinVref_PRTDSI__SYNC_OUT       (* (reg8 *) PinVref__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinVref__SIO_CFG)
    #define PinVref_SIO_HYST_EN        (* (reg8 *) PinVref__SIO_HYST_EN)
    #define PinVref_SIO_REG_HIFREQ     (* (reg8 *) PinVref__SIO_REG_HIFREQ)
    #define PinVref_SIO_CFG            (* (reg8 *) PinVref__SIO_CFG)
    #define PinVref_SIO_DIFF           (* (reg8 *) PinVref__SIO_DIFF)
#endif /* (PinVref__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinVref__INTSTAT)
    #define PinVref_INTSTAT            (* (reg8 *) PinVref__INTSTAT)
    #define PinVref_SNAP               (* (reg8 *) PinVref__SNAP)
    
	#define PinVref_0_INTTYPE_REG 		(* (reg8 *) PinVref__0__INTTYPE)
#endif /* (PinVref__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinVref_H */


/* [] END OF FILE */
