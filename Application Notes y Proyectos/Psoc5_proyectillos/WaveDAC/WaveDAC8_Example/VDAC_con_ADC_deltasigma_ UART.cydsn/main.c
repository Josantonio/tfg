/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h" //Para usar la función sprintf
#include "math.h" //Operaciones


/*
typedef unsigned char bool;
#define FALSE 0
#define TRUE (!FALSE)
bool signo = FALSE;
*/

int vdac = 0; //Variable que selecciona el voltaje del VDAC (entre 0 y 255)

int32 adc_result; //Entero que recibimos del ADC
uint16 adc_SAR_result = 0x0000; //Valor para almacenar el resultado del ADC SAR
char str[12]; //Cadena de caracteres para representar los resultados vía serie
uint16 ADCCountsCorrected = 0; //Ajustar el resultado en función de Vref
float Vin = 0.0e-12; //Para convertir el entero en un valor de voltaje
float VinSar = 0.0;
float input_range = 5.0-0.35; //Rango de entrada en función del buffer

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    VDAC8_Start();
    UART_Start();
    
    ADC_DelSig_Start();
//    ADC_SAR_Start();
    
    for(;;)
    {
        /************************ ADC sigma delta ***************************************/

        /********* LEEMOS LA CONVERSIÓN REALIZADA *****/
        ADC_DelSig_StartConvert(); //Empezar la conversión
        ADC_DelSig_IsEndConversion(ADC_DelSig_WAIT_FOR_RESULT); //Si se ha terminado la conversión
    
        adc_result = ADC_DelSig_GetResult32(); //Obtenemos el valor en binario

        /*** MOSTRAMOS VALOR DEL NÚMERO BINARIO ****/
        sprintf(str, "ADC result delta sigma: %ld\n", adc_result); //Signed long
        UART_PutString(str);

        /*** CONVERSIÓN PARA OBTENER EL VOLTAJE, TENIENDO EN CUENTA EL RANGO DE ENTRADA Y RESOLUCIÓN ***/
       // Vin = input_range/(pow(2,ADC_DelSig_CFG1_RESOLUTION))*adc_result; //Convertimos a voltios
        Vin = ADC_DelSig_CountsTo_Volts(adc_result);
        //Resolucion = 4.434585571e-6 V/bit
        // ¿Se pierde un bit de resolución al ser signed?? ---> NO
        /* El programa lo tiene en cuenta y aumenta la resolución 1 bit, de esta forma no se pierde *
         * resolución, el rango que sobra es ignorado                                               *
         * Datasheet pag 21: Note that when you configure the ADC for 16-bit single-ended mode,     *
         * the output doesn’t stop at 0x7FFF, but goes all the way up to 0xFFFF.                    *
         * As an example, a 16-bit single-ended ADC is implemented by considering a 17-bit          *
         * differential ADC and ignoring half of the range (although the internal implementation    *
         * is slightly different from that).                                                        *
          */
         sprintf(str, "Conversion Value: %0.5fV\n", Vin);

        UART_PutString(str);
      
        /****************************** ADC SAR *******************************************/
     /*   
        ADC_SAR_StartConvert(); //Empezar la conversión
        ADC_SAR_IsEndConversion(ADC_SAR_WAIT_FOR_RESULT); //Si se ha terminado la conversión  
    
        adc_SAR_result = ADC_SAR_GetResult16(); //Obtenemos el valor en binario
        sprintf(str, "ADC result SAR: %d\n", adc_SAR_result);
        UART_PutString(str);
        
        if(adc_SAR_result > 0x0800){ //Tenemos un valor negativo, lo que nos indica que la tensión
        //es muy cercana a 0 por debajo
            adc_SAR_result = 0x0000;
        }
        Vin = (5.0/2048.0)*adc_SAR_result; //Convertimos a voltios
        
        sprintf(str, "Conversion Value: %0.3fV\n", Vin); //Imprimir la tensión con 3 dígitos de precisión
        UART_PutString(str);
        */
        UART_PutString("*******************************\n");
        CyDelay(3000);
    }
}

/* [] END OF FILE */
