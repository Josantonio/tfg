/*******************************************************************************
* File Name: VinSar.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_VinSar_ALIASES_H) /* Pins VinSar_ALIASES_H */
#define CY_PINS_VinSar_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define VinSar_0			(VinSar__0__PC)
#define VinSar_0_INTR	((uint16)((uint16)0x0001u << VinSar__0__SHIFT))

#define VinSar_INTR_ALL	 ((uint16)(VinSar_0_INTR))

#endif /* End Pins VinSar_ALIASES_H */


/* [] END OF FILE */
