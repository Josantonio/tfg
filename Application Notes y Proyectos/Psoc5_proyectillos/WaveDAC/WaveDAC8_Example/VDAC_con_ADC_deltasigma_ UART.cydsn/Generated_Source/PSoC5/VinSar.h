/*******************************************************************************
* File Name: VinSar.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_VinSar_H) /* Pins VinSar_H */
#define CY_PINS_VinSar_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "VinSar_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 VinSar__PORT == 15 && ((VinSar__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    VinSar_Write(uint8 value);
void    VinSar_SetDriveMode(uint8 mode);
uint8   VinSar_ReadDataReg(void);
uint8   VinSar_Read(void);
void    VinSar_SetInterruptMode(uint16 position, uint16 mode);
uint8   VinSar_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the VinSar_SetDriveMode() function.
     *  @{
     */
        #define VinSar_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define VinSar_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define VinSar_DM_RES_UP          PIN_DM_RES_UP
        #define VinSar_DM_RES_DWN         PIN_DM_RES_DWN
        #define VinSar_DM_OD_LO           PIN_DM_OD_LO
        #define VinSar_DM_OD_HI           PIN_DM_OD_HI
        #define VinSar_DM_STRONG          PIN_DM_STRONG
        #define VinSar_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define VinSar_MASK               VinSar__MASK
#define VinSar_SHIFT              VinSar__SHIFT
#define VinSar_WIDTH              1u

/* Interrupt constants */
#if defined(VinSar__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in VinSar_SetInterruptMode() function.
     *  @{
     */
        #define VinSar_INTR_NONE      (uint16)(0x0000u)
        #define VinSar_INTR_RISING    (uint16)(0x0001u)
        #define VinSar_INTR_FALLING   (uint16)(0x0002u)
        #define VinSar_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define VinSar_INTR_MASK      (0x01u) 
#endif /* (VinSar__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define VinSar_PS                     (* (reg8 *) VinSar__PS)
/* Data Register */
#define VinSar_DR                     (* (reg8 *) VinSar__DR)
/* Port Number */
#define VinSar_PRT_NUM                (* (reg8 *) VinSar__PRT) 
/* Connect to Analog Globals */                                                  
#define VinSar_AG                     (* (reg8 *) VinSar__AG)                       
/* Analog MUX bux enable */
#define VinSar_AMUX                   (* (reg8 *) VinSar__AMUX) 
/* Bidirectional Enable */                                                        
#define VinSar_BIE                    (* (reg8 *) VinSar__BIE)
/* Bit-mask for Aliased Register Access */
#define VinSar_BIT_MASK               (* (reg8 *) VinSar__BIT_MASK)
/* Bypass Enable */
#define VinSar_BYP                    (* (reg8 *) VinSar__BYP)
/* Port wide control signals */                                                   
#define VinSar_CTL                    (* (reg8 *) VinSar__CTL)
/* Drive Modes */
#define VinSar_DM0                    (* (reg8 *) VinSar__DM0) 
#define VinSar_DM1                    (* (reg8 *) VinSar__DM1)
#define VinSar_DM2                    (* (reg8 *) VinSar__DM2) 
/* Input Buffer Disable Override */
#define VinSar_INP_DIS                (* (reg8 *) VinSar__INP_DIS)
/* LCD Common or Segment Drive */
#define VinSar_LCD_COM_SEG            (* (reg8 *) VinSar__LCD_COM_SEG)
/* Enable Segment LCD */
#define VinSar_LCD_EN                 (* (reg8 *) VinSar__LCD_EN)
/* Slew Rate Control */
#define VinSar_SLW                    (* (reg8 *) VinSar__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define VinSar_PRTDSI__CAPS_SEL       (* (reg8 *) VinSar__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define VinSar_PRTDSI__DBL_SYNC_IN    (* (reg8 *) VinSar__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define VinSar_PRTDSI__OE_SEL0        (* (reg8 *) VinSar__PRTDSI__OE_SEL0) 
#define VinSar_PRTDSI__OE_SEL1        (* (reg8 *) VinSar__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define VinSar_PRTDSI__OUT_SEL0       (* (reg8 *) VinSar__PRTDSI__OUT_SEL0) 
#define VinSar_PRTDSI__OUT_SEL1       (* (reg8 *) VinSar__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define VinSar_PRTDSI__SYNC_OUT       (* (reg8 *) VinSar__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(VinSar__SIO_CFG)
    #define VinSar_SIO_HYST_EN        (* (reg8 *) VinSar__SIO_HYST_EN)
    #define VinSar_SIO_REG_HIFREQ     (* (reg8 *) VinSar__SIO_REG_HIFREQ)
    #define VinSar_SIO_CFG            (* (reg8 *) VinSar__SIO_CFG)
    #define VinSar_SIO_DIFF           (* (reg8 *) VinSar__SIO_DIFF)
#endif /* (VinSar__SIO_CFG) */

/* Interrupt Registers */
#if defined(VinSar__INTSTAT)
    #define VinSar_INTSTAT            (* (reg8 *) VinSar__INTSTAT)
    #define VinSar_SNAP               (* (reg8 *) VinSar__SNAP)
    
	#define VinSar_0_INTTYPE_REG 		(* (reg8 *) VinSar__0__INTTYPE)
#endif /* (VinSar__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_VinSar_H */


/* [] END OF FILE */
