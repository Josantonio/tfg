/*******************************************************************************
* File Name: Voltage.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Voltage_H) /* Pins Voltage_H */
#define CY_PINS_Voltage_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Voltage_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Voltage__PORT == 15 && ((Voltage__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Voltage_Write(uint8 value);
void    Voltage_SetDriveMode(uint8 mode);
uint8   Voltage_ReadDataReg(void);
uint8   Voltage_Read(void);
void    Voltage_SetInterruptMode(uint16 position, uint16 mode);
uint8   Voltage_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Voltage_SetDriveMode() function.
     *  @{
     */
        #define Voltage_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Voltage_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Voltage_DM_RES_UP          PIN_DM_RES_UP
        #define Voltage_DM_RES_DWN         PIN_DM_RES_DWN
        #define Voltage_DM_OD_LO           PIN_DM_OD_LO
        #define Voltage_DM_OD_HI           PIN_DM_OD_HI
        #define Voltage_DM_STRONG          PIN_DM_STRONG
        #define Voltage_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Voltage_MASK               Voltage__MASK
#define Voltage_SHIFT              Voltage__SHIFT
#define Voltage_WIDTH              1u

/* Interrupt constants */
#if defined(Voltage__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Voltage_SetInterruptMode() function.
     *  @{
     */
        #define Voltage_INTR_NONE      (uint16)(0x0000u)
        #define Voltage_INTR_RISING    (uint16)(0x0001u)
        #define Voltage_INTR_FALLING   (uint16)(0x0002u)
        #define Voltage_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Voltage_INTR_MASK      (0x01u) 
#endif /* (Voltage__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Voltage_PS                     (* (reg8 *) Voltage__PS)
/* Data Register */
#define Voltage_DR                     (* (reg8 *) Voltage__DR)
/* Port Number */
#define Voltage_PRT_NUM                (* (reg8 *) Voltage__PRT) 
/* Connect to Analog Globals */                                                  
#define Voltage_AG                     (* (reg8 *) Voltage__AG)                       
/* Analog MUX bux enable */
#define Voltage_AMUX                   (* (reg8 *) Voltage__AMUX) 
/* Bidirectional Enable */                                                        
#define Voltage_BIE                    (* (reg8 *) Voltage__BIE)
/* Bit-mask for Aliased Register Access */
#define Voltage_BIT_MASK               (* (reg8 *) Voltage__BIT_MASK)
/* Bypass Enable */
#define Voltage_BYP                    (* (reg8 *) Voltage__BYP)
/* Port wide control signals */                                                   
#define Voltage_CTL                    (* (reg8 *) Voltage__CTL)
/* Drive Modes */
#define Voltage_DM0                    (* (reg8 *) Voltage__DM0) 
#define Voltage_DM1                    (* (reg8 *) Voltage__DM1)
#define Voltage_DM2                    (* (reg8 *) Voltage__DM2) 
/* Input Buffer Disable Override */
#define Voltage_INP_DIS                (* (reg8 *) Voltage__INP_DIS)
/* LCD Common or Segment Drive */
#define Voltage_LCD_COM_SEG            (* (reg8 *) Voltage__LCD_COM_SEG)
/* Enable Segment LCD */
#define Voltage_LCD_EN                 (* (reg8 *) Voltage__LCD_EN)
/* Slew Rate Control */
#define Voltage_SLW                    (* (reg8 *) Voltage__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Voltage_PRTDSI__CAPS_SEL       (* (reg8 *) Voltage__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Voltage_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Voltage__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Voltage_PRTDSI__OE_SEL0        (* (reg8 *) Voltage__PRTDSI__OE_SEL0) 
#define Voltage_PRTDSI__OE_SEL1        (* (reg8 *) Voltage__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Voltage_PRTDSI__OUT_SEL0       (* (reg8 *) Voltage__PRTDSI__OUT_SEL0) 
#define Voltage_PRTDSI__OUT_SEL1       (* (reg8 *) Voltage__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Voltage_PRTDSI__SYNC_OUT       (* (reg8 *) Voltage__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Voltage__SIO_CFG)
    #define Voltage_SIO_HYST_EN        (* (reg8 *) Voltage__SIO_HYST_EN)
    #define Voltage_SIO_REG_HIFREQ     (* (reg8 *) Voltage__SIO_REG_HIFREQ)
    #define Voltage_SIO_CFG            (* (reg8 *) Voltage__SIO_CFG)
    #define Voltage_SIO_DIFF           (* (reg8 *) Voltage__SIO_DIFF)
#endif /* (Voltage__SIO_CFG) */

/* Interrupt Registers */
#if defined(Voltage__INTSTAT)
    #define Voltage_INTSTAT            (* (reg8 *) Voltage__INTSTAT)
    #define Voltage_SNAP               (* (reg8 *) Voltage__SNAP)
    
	#define Voltage_0_INTTYPE_REG 		(* (reg8 *) Voltage__0__INTTYPE)
#endif /* (Voltage__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Voltage_H */


/* [] END OF FILE */
