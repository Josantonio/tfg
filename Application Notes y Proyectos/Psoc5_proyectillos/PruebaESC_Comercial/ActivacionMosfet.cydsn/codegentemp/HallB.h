/*******************************************************************************
* File Name: HallB.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_HallB_H) /* Pins HallB_H */
#define CY_PINS_HallB_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "HallB_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 HallB__PORT == 15 && ((HallB__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    HallB_Write(uint8 value);
void    HallB_SetDriveMode(uint8 mode);
uint8   HallB_ReadDataReg(void);
uint8   HallB_Read(void);
void    HallB_SetInterruptMode(uint16 position, uint16 mode);
uint8   HallB_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the HallB_SetDriveMode() function.
     *  @{
     */
        #define HallB_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define HallB_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define HallB_DM_RES_UP          PIN_DM_RES_UP
        #define HallB_DM_RES_DWN         PIN_DM_RES_DWN
        #define HallB_DM_OD_LO           PIN_DM_OD_LO
        #define HallB_DM_OD_HI           PIN_DM_OD_HI
        #define HallB_DM_STRONG          PIN_DM_STRONG
        #define HallB_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define HallB_MASK               HallB__MASK
#define HallB_SHIFT              HallB__SHIFT
#define HallB_WIDTH              1u

/* Interrupt constants */
#if defined(HallB__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in HallB_SetInterruptMode() function.
     *  @{
     */
        #define HallB_INTR_NONE      (uint16)(0x0000u)
        #define HallB_INTR_RISING    (uint16)(0x0001u)
        #define HallB_INTR_FALLING   (uint16)(0x0002u)
        #define HallB_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define HallB_INTR_MASK      (0x01u) 
#endif /* (HallB__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define HallB_PS                     (* (reg8 *) HallB__PS)
/* Data Register */
#define HallB_DR                     (* (reg8 *) HallB__DR)
/* Port Number */
#define HallB_PRT_NUM                (* (reg8 *) HallB__PRT) 
/* Connect to Analog Globals */                                                  
#define HallB_AG                     (* (reg8 *) HallB__AG)                       
/* Analog MUX bux enable */
#define HallB_AMUX                   (* (reg8 *) HallB__AMUX) 
/* Bidirectional Enable */                                                        
#define HallB_BIE                    (* (reg8 *) HallB__BIE)
/* Bit-mask for Aliased Register Access */
#define HallB_BIT_MASK               (* (reg8 *) HallB__BIT_MASK)
/* Bypass Enable */
#define HallB_BYP                    (* (reg8 *) HallB__BYP)
/* Port wide control signals */                                                   
#define HallB_CTL                    (* (reg8 *) HallB__CTL)
/* Drive Modes */
#define HallB_DM0                    (* (reg8 *) HallB__DM0) 
#define HallB_DM1                    (* (reg8 *) HallB__DM1)
#define HallB_DM2                    (* (reg8 *) HallB__DM2) 
/* Input Buffer Disable Override */
#define HallB_INP_DIS                (* (reg8 *) HallB__INP_DIS)
/* LCD Common or Segment Drive */
#define HallB_LCD_COM_SEG            (* (reg8 *) HallB__LCD_COM_SEG)
/* Enable Segment LCD */
#define HallB_LCD_EN                 (* (reg8 *) HallB__LCD_EN)
/* Slew Rate Control */
#define HallB_SLW                    (* (reg8 *) HallB__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define HallB_PRTDSI__CAPS_SEL       (* (reg8 *) HallB__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define HallB_PRTDSI__DBL_SYNC_IN    (* (reg8 *) HallB__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define HallB_PRTDSI__OE_SEL0        (* (reg8 *) HallB__PRTDSI__OE_SEL0) 
#define HallB_PRTDSI__OE_SEL1        (* (reg8 *) HallB__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define HallB_PRTDSI__OUT_SEL0       (* (reg8 *) HallB__PRTDSI__OUT_SEL0) 
#define HallB_PRTDSI__OUT_SEL1       (* (reg8 *) HallB__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define HallB_PRTDSI__SYNC_OUT       (* (reg8 *) HallB__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(HallB__SIO_CFG)
    #define HallB_SIO_HYST_EN        (* (reg8 *) HallB__SIO_HYST_EN)
    #define HallB_SIO_REG_HIFREQ     (* (reg8 *) HallB__SIO_REG_HIFREQ)
    #define HallB_SIO_CFG            (* (reg8 *) HallB__SIO_CFG)
    #define HallB_SIO_DIFF           (* (reg8 *) HallB__SIO_DIFF)
#endif /* (HallB__SIO_CFG) */

/* Interrupt Registers */
#if defined(HallB__INTSTAT)
    #define HallB_INTSTAT            (* (reg8 *) HallB__INTSTAT)
    #define HallB_SNAP               (* (reg8 *) HallB__SNAP)
    
	#define HallB_0_INTTYPE_REG 		(* (reg8 *) HallB__0__INTTYPE)
#endif /* (HallB__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_HallB_H */


/* [] END OF FILE */
