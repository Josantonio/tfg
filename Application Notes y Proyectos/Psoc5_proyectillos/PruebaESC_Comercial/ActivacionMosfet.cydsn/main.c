/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "math.h"

typedef unsigned char bool;
#define FALSE 0
#define TRUE (!FALSE)

bool state_button = FALSE; //Empezamos en OFF

/**** Parámetros para la medida de la velocidad ****/
#define TIMER_PERIOD (uint16)(pow(2, Timer_Speed_Resolution)) // 2^12 = 65536 bits

bool first_turn = TRUE; //Se activa para no contabilizar la primera captura en el primer giro, no completo
bool first_time = TRUE;
uint16 periodos_count = 0; //Nº de veces que cuenta un período el timer, y no se ha dado una captura
// este parámetro incrementará indefinidamente mientras el motor esté parado
//llegará a valores altos a baja velocidad de rotación
uint16 T0,T1; //Variable para almacenar el valor de la primera captura
float delay; //Variable para almacenar el tiempo transcurrido desde la captura actual a la anterior
float w_real; //Velocidad del motor calculada por los sensores hall (rpm)
float w_ref; //Velocidad de referencia, leída por el potenciómetro, ajustada por el usuario
uint8 duty; //Duty calculado para escribir el PWM, n bits
float duty_perc; //Porcentaje de duty, salida del PID
#define CLK_SPEED   1.0e6   // 1 MHz
#define P_POLOS     4       //n pares de polos


CY_ISR (isr_button_handler){
   // Pin_Out_Write(Pin_Out_Read()^1U);
    state_button = !state_button;

}

CY_ISR (isr_speed_handler){
    /* Hay que averiguar por qué se ha dado la interrupción */
    
    /* Problema -> si leemos el registro se borra, entonces lo vamos a almacenar        *
     * en un avariable intermedia de 8 bits para que aunque se borre, podemos comparar  *
     * su valor en el segundo if                                                        *
     */
    uint8 status_register = 0x00;
    
    status_register = Timer_Speed_ReadStatusRegister(); //Se borra y se limpia la interrupción
    
    /*** INTERRUPCIÓN DEBIDA A TC ***/
    
    //Si la interrupción es porque ha llegado al final de la cuenta, y además el motor está en marcha
    // de esta forma evitamos que el timer cuente cuando esté el motor parado y la variable se desborde
     if ((status_register & Timer_Speed_STATUS_TC) && state_button){
        periodos_count++;
      // UART_PutString("\n\nTC\n\n");
    }
  
    /*** INTERRUPCIÓN DEBIDA A CAPTURA ***/
    
    else if ((status_register & Timer_Speed_STATUS_CAPTURE) && state_button){
      //   UART_PutString("\n\nCAPTURE\n\n");
        
        if (first_turn){ //Es la primera vez
            first_turn = FALSE; //Cambiamos variable para mostrar que la próxima vez ya no 
            //será la primera
            T0 = Timer_Speed_ReadCapture(); //guardamos la captura, está en n*bits
        }else if (!first_turn){
            // Si se produce la segunda catura se calcula el tiempo transcurrido desde que se dió la primera
            T1 = Timer_Speed_ReadCapture();
            delay = ((T0+periodos_count*TIMER_PERIOD-T1)/CLK_SPEED);
            T0 = T1; //Actualizamos T0
            periodos_count = 0; //Limpiamos variable que cuenta los períodos almacenados
            w_real = 60.0/(P_POLOS*delay); //Vel obtenida [rpm]        
        }
        
    }
}

int main(void)
{
    char msg[100];
    
    CyGlobalIntEnable; /* Enable global interrupts. */

    PWM_probe_Start();
    PWM_Driver_Start();
    Timer_Speed_Start();
    UART_Start();
    
    isr_speed_StartEx(isr_speed_handler);
    isr_button_StartEx(isr_button_handler);
            
    Control_Reg_Write(0x00);
    
    for(;;)
    {
        if (state_button){
            Control_Reg_Write(0x03);
            Pin_LED_Write(1);
            sprintf(msg, "Velocidad angular: %0.2f rpm\n",w_real);
            UART_PutString(msg);  
        }if (state_button == 0){
            Control_Reg_Write(0x00);
            Pin_LED_Write(0);       
        }
    }
}

/* [] END OF FILE */
