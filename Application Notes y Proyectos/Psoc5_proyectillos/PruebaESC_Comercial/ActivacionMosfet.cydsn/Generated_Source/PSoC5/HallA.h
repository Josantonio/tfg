/*******************************************************************************
* File Name: HallA.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_HallA_H) /* Pins HallA_H */
#define CY_PINS_HallA_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "HallA_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 HallA__PORT == 15 && ((HallA__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    HallA_Write(uint8 value);
void    HallA_SetDriveMode(uint8 mode);
uint8   HallA_ReadDataReg(void);
uint8   HallA_Read(void);
void    HallA_SetInterruptMode(uint16 position, uint16 mode);
uint8   HallA_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the HallA_SetDriveMode() function.
     *  @{
     */
        #define HallA_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define HallA_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define HallA_DM_RES_UP          PIN_DM_RES_UP
        #define HallA_DM_RES_DWN         PIN_DM_RES_DWN
        #define HallA_DM_OD_LO           PIN_DM_OD_LO
        #define HallA_DM_OD_HI           PIN_DM_OD_HI
        #define HallA_DM_STRONG          PIN_DM_STRONG
        #define HallA_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define HallA_MASK               HallA__MASK
#define HallA_SHIFT              HallA__SHIFT
#define HallA_WIDTH              1u

/* Interrupt constants */
#if defined(HallA__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in HallA_SetInterruptMode() function.
     *  @{
     */
        #define HallA_INTR_NONE      (uint16)(0x0000u)
        #define HallA_INTR_RISING    (uint16)(0x0001u)
        #define HallA_INTR_FALLING   (uint16)(0x0002u)
        #define HallA_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define HallA_INTR_MASK      (0x01u) 
#endif /* (HallA__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define HallA_PS                     (* (reg8 *) HallA__PS)
/* Data Register */
#define HallA_DR                     (* (reg8 *) HallA__DR)
/* Port Number */
#define HallA_PRT_NUM                (* (reg8 *) HallA__PRT) 
/* Connect to Analog Globals */                                                  
#define HallA_AG                     (* (reg8 *) HallA__AG)                       
/* Analog MUX bux enable */
#define HallA_AMUX                   (* (reg8 *) HallA__AMUX) 
/* Bidirectional Enable */                                                        
#define HallA_BIE                    (* (reg8 *) HallA__BIE)
/* Bit-mask for Aliased Register Access */
#define HallA_BIT_MASK               (* (reg8 *) HallA__BIT_MASK)
/* Bypass Enable */
#define HallA_BYP                    (* (reg8 *) HallA__BYP)
/* Port wide control signals */                                                   
#define HallA_CTL                    (* (reg8 *) HallA__CTL)
/* Drive Modes */
#define HallA_DM0                    (* (reg8 *) HallA__DM0) 
#define HallA_DM1                    (* (reg8 *) HallA__DM1)
#define HallA_DM2                    (* (reg8 *) HallA__DM2) 
/* Input Buffer Disable Override */
#define HallA_INP_DIS                (* (reg8 *) HallA__INP_DIS)
/* LCD Common or Segment Drive */
#define HallA_LCD_COM_SEG            (* (reg8 *) HallA__LCD_COM_SEG)
/* Enable Segment LCD */
#define HallA_LCD_EN                 (* (reg8 *) HallA__LCD_EN)
/* Slew Rate Control */
#define HallA_SLW                    (* (reg8 *) HallA__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define HallA_PRTDSI__CAPS_SEL       (* (reg8 *) HallA__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define HallA_PRTDSI__DBL_SYNC_IN    (* (reg8 *) HallA__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define HallA_PRTDSI__OE_SEL0        (* (reg8 *) HallA__PRTDSI__OE_SEL0) 
#define HallA_PRTDSI__OE_SEL1        (* (reg8 *) HallA__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define HallA_PRTDSI__OUT_SEL0       (* (reg8 *) HallA__PRTDSI__OUT_SEL0) 
#define HallA_PRTDSI__OUT_SEL1       (* (reg8 *) HallA__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define HallA_PRTDSI__SYNC_OUT       (* (reg8 *) HallA__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(HallA__SIO_CFG)
    #define HallA_SIO_HYST_EN        (* (reg8 *) HallA__SIO_HYST_EN)
    #define HallA_SIO_REG_HIFREQ     (* (reg8 *) HallA__SIO_REG_HIFREQ)
    #define HallA_SIO_CFG            (* (reg8 *) HallA__SIO_CFG)
    #define HallA_SIO_DIFF           (* (reg8 *) HallA__SIO_DIFF)
#endif /* (HallA__SIO_CFG) */

/* Interrupt Registers */
#if defined(HallA__INTSTAT)
    #define HallA_INTSTAT            (* (reg8 *) HallA__INTSTAT)
    #define HallA_SNAP               (* (reg8 *) HallA__SNAP)
    
	#define HallA_0_INTTYPE_REG 		(* (reg8 *) HallA__0__INTTYPE)
#endif /* (HallA__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_HallA_H */


/* [] END OF FILE */
