/*******************************************************************************
* File Name: HallC.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_HallC_H) /* Pins HallC_H */
#define CY_PINS_HallC_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "HallC_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 HallC__PORT == 15 && ((HallC__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    HallC_Write(uint8 value);
void    HallC_SetDriveMode(uint8 mode);
uint8   HallC_ReadDataReg(void);
uint8   HallC_Read(void);
void    HallC_SetInterruptMode(uint16 position, uint16 mode);
uint8   HallC_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the HallC_SetDriveMode() function.
     *  @{
     */
        #define HallC_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define HallC_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define HallC_DM_RES_UP          PIN_DM_RES_UP
        #define HallC_DM_RES_DWN         PIN_DM_RES_DWN
        #define HallC_DM_OD_LO           PIN_DM_OD_LO
        #define HallC_DM_OD_HI           PIN_DM_OD_HI
        #define HallC_DM_STRONG          PIN_DM_STRONG
        #define HallC_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define HallC_MASK               HallC__MASK
#define HallC_SHIFT              HallC__SHIFT
#define HallC_WIDTH              1u

/* Interrupt constants */
#if defined(HallC__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in HallC_SetInterruptMode() function.
     *  @{
     */
        #define HallC_INTR_NONE      (uint16)(0x0000u)
        #define HallC_INTR_RISING    (uint16)(0x0001u)
        #define HallC_INTR_FALLING   (uint16)(0x0002u)
        #define HallC_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define HallC_INTR_MASK      (0x01u) 
#endif /* (HallC__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define HallC_PS                     (* (reg8 *) HallC__PS)
/* Data Register */
#define HallC_DR                     (* (reg8 *) HallC__DR)
/* Port Number */
#define HallC_PRT_NUM                (* (reg8 *) HallC__PRT) 
/* Connect to Analog Globals */                                                  
#define HallC_AG                     (* (reg8 *) HallC__AG)                       
/* Analog MUX bux enable */
#define HallC_AMUX                   (* (reg8 *) HallC__AMUX) 
/* Bidirectional Enable */                                                        
#define HallC_BIE                    (* (reg8 *) HallC__BIE)
/* Bit-mask for Aliased Register Access */
#define HallC_BIT_MASK               (* (reg8 *) HallC__BIT_MASK)
/* Bypass Enable */
#define HallC_BYP                    (* (reg8 *) HallC__BYP)
/* Port wide control signals */                                                   
#define HallC_CTL                    (* (reg8 *) HallC__CTL)
/* Drive Modes */
#define HallC_DM0                    (* (reg8 *) HallC__DM0) 
#define HallC_DM1                    (* (reg8 *) HallC__DM1)
#define HallC_DM2                    (* (reg8 *) HallC__DM2) 
/* Input Buffer Disable Override */
#define HallC_INP_DIS                (* (reg8 *) HallC__INP_DIS)
/* LCD Common or Segment Drive */
#define HallC_LCD_COM_SEG            (* (reg8 *) HallC__LCD_COM_SEG)
/* Enable Segment LCD */
#define HallC_LCD_EN                 (* (reg8 *) HallC__LCD_EN)
/* Slew Rate Control */
#define HallC_SLW                    (* (reg8 *) HallC__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define HallC_PRTDSI__CAPS_SEL       (* (reg8 *) HallC__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define HallC_PRTDSI__DBL_SYNC_IN    (* (reg8 *) HallC__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define HallC_PRTDSI__OE_SEL0        (* (reg8 *) HallC__PRTDSI__OE_SEL0) 
#define HallC_PRTDSI__OE_SEL1        (* (reg8 *) HallC__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define HallC_PRTDSI__OUT_SEL0       (* (reg8 *) HallC__PRTDSI__OUT_SEL0) 
#define HallC_PRTDSI__OUT_SEL1       (* (reg8 *) HallC__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define HallC_PRTDSI__SYNC_OUT       (* (reg8 *) HallC__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(HallC__SIO_CFG)
    #define HallC_SIO_HYST_EN        (* (reg8 *) HallC__SIO_HYST_EN)
    #define HallC_SIO_REG_HIFREQ     (* (reg8 *) HallC__SIO_REG_HIFREQ)
    #define HallC_SIO_CFG            (* (reg8 *) HallC__SIO_CFG)
    #define HallC_SIO_DIFF           (* (reg8 *) HallC__SIO_DIFF)
#endif /* (HallC__SIO_CFG) */

/* Interrupt Registers */
#if defined(HallC__INTSTAT)
    #define HallC_INTSTAT            (* (reg8 *) HallC__INTSTAT)
    #define HallC_SNAP               (* (reg8 *) HallC__SNAP)
    
	#define HallC_0_INTTYPE_REG 		(* (reg8 *) HallC__0__INTTYPE)
#endif /* (HallC__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_HallC_H */


/* [] END OF FILE */
