/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "math.h"

typedef unsigned char bool;
#define FALSE 0
#define TRUE (!FALSE)

#define TIMER_SPEED_PERIOD (pow(2, TimerSpeed_Resolution)) //Max nbits  65536
#define FREC_CLOCK 24e6 //Frecuencia reloj para medir la velocidad

bool first_capture = TRUE; //Variable para evitar contabilizar la primera captura
bool ventilador_on = TRUE; 
bool get_params = FALSE;

#define MAX_DUTY    100 // %
#define MIN_DUTY    50 // %
#define MAX_PWM     (pow(2, PWM_Resolution)-1) // 255
#define ADC_MAX_VALUE   (pow(2, ADC_DEFAULT_RESOLUTION)) //4096


uint16 w_real; //Velocidad en rpm medida
uint8 num_period = 0; //Variable para almacenar el número de períodos que transcurren entre dos TC
//Este número será mayor cuánto más lento vaya el ventilador, y si está parado será infinito
uint16 T0,T1; //Variable para almacenar el nº bits que se leen de la captura, el máximo es 2^16
uint32 delay; //Tiempo, nº bits entre una captura y la siguiente. ¡Cuidado! sabiendo la
//frecuencia mínima dle motor debería calcularse el valor máximo de esta variable para que no desborde
   
 
char str[100];
    
CY_ISR (isr_speed_measure_handler){

    /* Determinamos el origen de la interrupción */
    
    uint8 status_register = 0x00;
    status_register = TimerSpeed_ReadStatusRegister();
    //Leemos el registro y lo asignamos a una variable
    
    if ((status_register & TimerSpeed_STATUS_TC) && ventilador_on){
        /* La interrupción se ha producido por llegar al final de la cuenta */
        num_period++;
        /*
        UART_PutString("\nTC\n");
        sprintf(str, "numperiod %d\n", num_period);
        UART_PutString(str);
        */
    }
    else if ((status_register & TimerSpeed_STATUS_CAPTURE) && ventilador_on){
        /* La interrupción se ha producido por darse una captura */
        if (first_capture){
            first_capture = FALSE;
            T0 = TimerSpeed_ReadCapture();
            /*
            sprintf(str, "T0 %d\n", T0);
            UART_PutString(str);
            */
        }
        else if (!first_capture){
            /*
           UART_PutString("\nCAPTURE\n");
            */
            T1 = TimerSpeed_ReadCapture(); //Leemos el valor del timer en la segunda captura
         //   sprintf(str, "T1 %d, T0: %d\n", T1, T0);
          //  UART_PutString(str);
            delay = (T0+num_period*TIMER_SPEED_PERIOD-T1); //Calculamos el n ciclos con respecto a la anterior
            T0 = T1; //Guardamos la actual para usarla para la próxima
            num_period = 0; //Reseteamos el contador de periodo
           /* sprintf(str, "delay %lu\n", delay);
            UART_PutString(str); */
            w_real = ((60*FREC_CLOCK)/(2*delay)); //Calculamos la velocidad en rpm
        }
    }

}

CY_ISR ( isr_button_handler){
    get_params = ~get_params;
    UART_PutString("INTERRUPT\n\n");
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    uint16 duty = 0;
    uint16 lecturas[10]; //Array de 10 elementos para hacer un promediado
    uint8 i = 0; //índice para recorrer el array
    /*
        for (i=0; 1<10; i++){
            lecturas[i] = 0x0000;
        }
        */
    uint32 suma = 0x0000;
    uint16 adc_result; //La resolucion del ADC es de 12 bits, 
    float Kss1; //Kss teniendo en cuenta la tensión
    float Kss2; //Kss teniendo en cuenta el duty_cycle
    
    TimerSpeed_Start();
    ADC_Start();
    PWM_Start();
    UART_Start();
    
    isr_speed_StartEx(isr_speed_measure_handler);
    isr_button_StartEx(isr_button_handler);

    for(;;)
    {
        /*
        if(get_params){
                PWM_WriteCompare(255);
                CyDelay(1000); //Esperamos que se estabilice
                Kss1 = w_real/12;
                Kss2 = w_real/255;
                
               // PWM_WriteCompare(0);
              //  PWM_WriteCompare(255);
                
                sprintf(str, "Omega: %d, Kss1: %f, Kss2: %f\n", w_real, Kss1, Kss2);
                UART_PutString(str);
        }if(!get_params){
            PWM_WriteCompare(0);
        }
        */
        ADC_StartConvert();
        ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);
        lecturas[i] = (ADC_GetResult16() & 0x0FFF); //Leemos un nuevo valor, hacemos una AND con su valor máximo
        //para evitar valores negativos debidos al ruido
        suma = suma + lecturas[i]; //Lo añadimos al total
        i++; //Se incrementa el contador
        
        if (i >= 10 ){ //Ya tenemos 10 valores nuevos
            adc_result = (suma/10);
            i = 0; //Reseteamos índice
            suma = 0x0000;
        }
      
        duty = round((PWM_ReadPeriod()+1)*adc_result/ADC_MAX_VALUE);
        PWM_WriteCompare(duty);
            
        sprintf(str, "Omega: %d, Delay: %lu, ADC: %d, duty: %d\n", w_real, delay, adc_result, duty);
        UART_PutString(str);
        //CyDelay(10);
    }
}

/* [] END OF FILE */
