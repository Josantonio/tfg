/*******************************************************************************
* File Name: TimerSpeed_PM.c
* Version 2.80
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "TimerSpeed.h"

static TimerSpeed_backupStruct TimerSpeed_backup;


/*******************************************************************************
* Function Name: TimerSpeed_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerSpeed_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void TimerSpeed_SaveConfig(void) 
{
    #if (!TimerSpeed_UsingFixedFunction)
        TimerSpeed_backup.TimerUdb = TimerSpeed_ReadCounter();
        TimerSpeed_backup.InterruptMaskValue = TimerSpeed_STATUS_MASK;
        #if (TimerSpeed_UsingHWCaptureCounter)
            TimerSpeed_backup.TimerCaptureCounter = TimerSpeed_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!TimerSpeed_UDB_CONTROL_REG_REMOVED)
            TimerSpeed_backup.TimerControlRegister = TimerSpeed_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: TimerSpeed_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerSpeed_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void TimerSpeed_RestoreConfig(void) 
{   
    #if (!TimerSpeed_UsingFixedFunction)

        TimerSpeed_WriteCounter(TimerSpeed_backup.TimerUdb);
        TimerSpeed_STATUS_MASK =TimerSpeed_backup.InterruptMaskValue;
        #if (TimerSpeed_UsingHWCaptureCounter)
            TimerSpeed_SetCaptureCount(TimerSpeed_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!TimerSpeed_UDB_CONTROL_REG_REMOVED)
            TimerSpeed_WriteControlRegister(TimerSpeed_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: TimerSpeed_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerSpeed_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void TimerSpeed_Sleep(void) 
{
    #if(!TimerSpeed_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(TimerSpeed_CTRL_ENABLE == (TimerSpeed_CONTROL & TimerSpeed_CTRL_ENABLE))
        {
            /* Timer is enabled */
            TimerSpeed_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            TimerSpeed_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    TimerSpeed_Stop();
    TimerSpeed_SaveConfig();
}


/*******************************************************************************
* Function Name: TimerSpeed_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerSpeed_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void TimerSpeed_Wakeup(void) 
{
    TimerSpeed_RestoreConfig();
    #if(!TimerSpeed_UDB_CONTROL_REG_REMOVED)
        if(TimerSpeed_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                TimerSpeed_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */
