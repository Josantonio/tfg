/*******************************************************************************
* File Name: PinHall.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinHall_H) /* Pins PinHall_H */
#define CY_PINS_PinHall_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinHall_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinHall__PORT == 15 && ((PinHall__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinHall_Write(uint8 value);
void    PinHall_SetDriveMode(uint8 mode);
uint8   PinHall_ReadDataReg(void);
uint8   PinHall_Read(void);
void    PinHall_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinHall_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinHall_SetDriveMode() function.
     *  @{
     */
        #define PinHall_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinHall_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinHall_DM_RES_UP          PIN_DM_RES_UP
        #define PinHall_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinHall_DM_OD_LO           PIN_DM_OD_LO
        #define PinHall_DM_OD_HI           PIN_DM_OD_HI
        #define PinHall_DM_STRONG          PIN_DM_STRONG
        #define PinHall_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinHall_MASK               PinHall__MASK
#define PinHall_SHIFT              PinHall__SHIFT
#define PinHall_WIDTH              1u

/* Interrupt constants */
#if defined(PinHall__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinHall_SetInterruptMode() function.
     *  @{
     */
        #define PinHall_INTR_NONE      (uint16)(0x0000u)
        #define PinHall_INTR_RISING    (uint16)(0x0001u)
        #define PinHall_INTR_FALLING   (uint16)(0x0002u)
        #define PinHall_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinHall_INTR_MASK      (0x01u) 
#endif /* (PinHall__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinHall_PS                     (* (reg8 *) PinHall__PS)
/* Data Register */
#define PinHall_DR                     (* (reg8 *) PinHall__DR)
/* Port Number */
#define PinHall_PRT_NUM                (* (reg8 *) PinHall__PRT) 
/* Connect to Analog Globals */                                                  
#define PinHall_AG                     (* (reg8 *) PinHall__AG)                       
/* Analog MUX bux enable */
#define PinHall_AMUX                   (* (reg8 *) PinHall__AMUX) 
/* Bidirectional Enable */                                                        
#define PinHall_BIE                    (* (reg8 *) PinHall__BIE)
/* Bit-mask for Aliased Register Access */
#define PinHall_BIT_MASK               (* (reg8 *) PinHall__BIT_MASK)
/* Bypass Enable */
#define PinHall_BYP                    (* (reg8 *) PinHall__BYP)
/* Port wide control signals */                                                   
#define PinHall_CTL                    (* (reg8 *) PinHall__CTL)
/* Drive Modes */
#define PinHall_DM0                    (* (reg8 *) PinHall__DM0) 
#define PinHall_DM1                    (* (reg8 *) PinHall__DM1)
#define PinHall_DM2                    (* (reg8 *) PinHall__DM2) 
/* Input Buffer Disable Override */
#define PinHall_INP_DIS                (* (reg8 *) PinHall__INP_DIS)
/* LCD Common or Segment Drive */
#define PinHall_LCD_COM_SEG            (* (reg8 *) PinHall__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinHall_LCD_EN                 (* (reg8 *) PinHall__LCD_EN)
/* Slew Rate Control */
#define PinHall_SLW                    (* (reg8 *) PinHall__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinHall_PRTDSI__CAPS_SEL       (* (reg8 *) PinHall__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinHall_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinHall__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinHall_PRTDSI__OE_SEL0        (* (reg8 *) PinHall__PRTDSI__OE_SEL0) 
#define PinHall_PRTDSI__OE_SEL1        (* (reg8 *) PinHall__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinHall_PRTDSI__OUT_SEL0       (* (reg8 *) PinHall__PRTDSI__OUT_SEL0) 
#define PinHall_PRTDSI__OUT_SEL1       (* (reg8 *) PinHall__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinHall_PRTDSI__SYNC_OUT       (* (reg8 *) PinHall__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinHall__SIO_CFG)
    #define PinHall_SIO_HYST_EN        (* (reg8 *) PinHall__SIO_HYST_EN)
    #define PinHall_SIO_REG_HIFREQ     (* (reg8 *) PinHall__SIO_REG_HIFREQ)
    #define PinHall_SIO_CFG            (* (reg8 *) PinHall__SIO_CFG)
    #define PinHall_SIO_DIFF           (* (reg8 *) PinHall__SIO_DIFF)
#endif /* (PinHall__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinHall__INTSTAT)
    #define PinHall_INTSTAT            (* (reg8 *) PinHall__INTSTAT)
    #define PinHall_SNAP               (* (reg8 *) PinHall__SNAP)
    
	#define PinHall_0_INTTYPE_REG 		(* (reg8 *) PinHall__0__INTTYPE)
#endif /* (PinHall__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinHall_H */


/* [] END OF FILE */
