/*******************************************************************************
* File Name: ClockSpeed.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_ClockSpeed_H)
#define CY_CLOCK_ClockSpeed_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void ClockSpeed_Start(void) ;
void ClockSpeed_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void ClockSpeed_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void ClockSpeed_StandbyPower(uint8 state) ;
void ClockSpeed_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 ClockSpeed_GetDividerRegister(void) ;
void ClockSpeed_SetModeRegister(uint8 modeBitMask) ;
void ClockSpeed_ClearModeRegister(uint8 modeBitMask) ;
uint8 ClockSpeed_GetModeRegister(void) ;
void ClockSpeed_SetSourceRegister(uint8 clkSource) ;
uint8 ClockSpeed_GetSourceRegister(void) ;
#if defined(ClockSpeed__CFG3)
void ClockSpeed_SetPhaseRegister(uint8 clkPhase) ;
uint8 ClockSpeed_GetPhaseRegister(void) ;
#endif /* defined(ClockSpeed__CFG3) */

#define ClockSpeed_Enable()                       ClockSpeed_Start()
#define ClockSpeed_Disable()                      ClockSpeed_Stop()
#define ClockSpeed_SetDivider(clkDivider)         ClockSpeed_SetDividerRegister(clkDivider, 1u)
#define ClockSpeed_SetDividerValue(clkDivider)    ClockSpeed_SetDividerRegister((clkDivider) - 1u, 1u)
#define ClockSpeed_SetMode(clkMode)               ClockSpeed_SetModeRegister(clkMode)
#define ClockSpeed_SetSource(clkSource)           ClockSpeed_SetSourceRegister(clkSource)
#if defined(ClockSpeed__CFG3)
#define ClockSpeed_SetPhase(clkPhase)             ClockSpeed_SetPhaseRegister(clkPhase)
#define ClockSpeed_SetPhaseValue(clkPhase)        ClockSpeed_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(ClockSpeed__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define ClockSpeed_CLKEN              (* (reg8 *) ClockSpeed__PM_ACT_CFG)
#define ClockSpeed_CLKEN_PTR          ((reg8 *) ClockSpeed__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define ClockSpeed_CLKSTBY            (* (reg8 *) ClockSpeed__PM_STBY_CFG)
#define ClockSpeed_CLKSTBY_PTR        ((reg8 *) ClockSpeed__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define ClockSpeed_DIV_LSB            (* (reg8 *) ClockSpeed__CFG0)
#define ClockSpeed_DIV_LSB_PTR        ((reg8 *) ClockSpeed__CFG0)
#define ClockSpeed_DIV_PTR            ((reg16 *) ClockSpeed__CFG0)

/* Clock MSB divider configuration register. */
#define ClockSpeed_DIV_MSB            (* (reg8 *) ClockSpeed__CFG1)
#define ClockSpeed_DIV_MSB_PTR        ((reg8 *) ClockSpeed__CFG1)

/* Mode and source configuration register */
#define ClockSpeed_MOD_SRC            (* (reg8 *) ClockSpeed__CFG2)
#define ClockSpeed_MOD_SRC_PTR        ((reg8 *) ClockSpeed__CFG2)

#if defined(ClockSpeed__CFG3)
/* Analog clock phase configuration register */
#define ClockSpeed_PHASE              (* (reg8 *) ClockSpeed__CFG3)
#define ClockSpeed_PHASE_PTR          ((reg8 *) ClockSpeed__CFG3)
#endif /* defined(ClockSpeed__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define ClockSpeed_CLKEN_MASK         ClockSpeed__PM_ACT_MSK
#define ClockSpeed_CLKSTBY_MASK       ClockSpeed__PM_STBY_MSK

/* CFG2 field masks */
#define ClockSpeed_SRC_SEL_MSK        ClockSpeed__CFG2_SRC_SEL_MASK
#define ClockSpeed_MODE_MASK          (~(ClockSpeed_SRC_SEL_MSK))

#if defined(ClockSpeed__CFG3)
/* CFG3 phase mask */
#define ClockSpeed_PHASE_MASK         ClockSpeed__CFG3_PHASE_DLY_MASK
#endif /* defined(ClockSpeed__CFG3) */

#endif /* CY_CLOCK_ClockSpeed_H */


/* [] END OF FILE */
