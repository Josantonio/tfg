/*******************************************************************************
* File Name: PinBC.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinBC_H) /* Pins PinBC_H */
#define CY_PINS_PinBC_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinBC_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinBC__PORT == 15 && ((PinBC__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinBC_Write(uint8 value);
void    PinBC_SetDriveMode(uint8 mode);
uint8   PinBC_ReadDataReg(void);
uint8   PinBC_Read(void);
void    PinBC_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinBC_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinBC_SetDriveMode() function.
     *  @{
     */
        #define PinBC_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinBC_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinBC_DM_RES_UP          PIN_DM_RES_UP
        #define PinBC_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinBC_DM_OD_LO           PIN_DM_OD_LO
        #define PinBC_DM_OD_HI           PIN_DM_OD_HI
        #define PinBC_DM_STRONG          PIN_DM_STRONG
        #define PinBC_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinBC_MASK               PinBC__MASK
#define PinBC_SHIFT              PinBC__SHIFT
#define PinBC_WIDTH              1u

/* Interrupt constants */
#if defined(PinBC__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinBC_SetInterruptMode() function.
     *  @{
     */
        #define PinBC_INTR_NONE      (uint16)(0x0000u)
        #define PinBC_INTR_RISING    (uint16)(0x0001u)
        #define PinBC_INTR_FALLING   (uint16)(0x0002u)
        #define PinBC_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinBC_INTR_MASK      (0x01u) 
#endif /* (PinBC__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinBC_PS                     (* (reg8 *) PinBC__PS)
/* Data Register */
#define PinBC_DR                     (* (reg8 *) PinBC__DR)
/* Port Number */
#define PinBC_PRT_NUM                (* (reg8 *) PinBC__PRT) 
/* Connect to Analog Globals */                                                  
#define PinBC_AG                     (* (reg8 *) PinBC__AG)                       
/* Analog MUX bux enable */
#define PinBC_AMUX                   (* (reg8 *) PinBC__AMUX) 
/* Bidirectional Enable */                                                        
#define PinBC_BIE                    (* (reg8 *) PinBC__BIE)
/* Bit-mask for Aliased Register Access */
#define PinBC_BIT_MASK               (* (reg8 *) PinBC__BIT_MASK)
/* Bypass Enable */
#define PinBC_BYP                    (* (reg8 *) PinBC__BYP)
/* Port wide control signals */                                                   
#define PinBC_CTL                    (* (reg8 *) PinBC__CTL)
/* Drive Modes */
#define PinBC_DM0                    (* (reg8 *) PinBC__DM0) 
#define PinBC_DM1                    (* (reg8 *) PinBC__DM1)
#define PinBC_DM2                    (* (reg8 *) PinBC__DM2) 
/* Input Buffer Disable Override */
#define PinBC_INP_DIS                (* (reg8 *) PinBC__INP_DIS)
/* LCD Common or Segment Drive */
#define PinBC_LCD_COM_SEG            (* (reg8 *) PinBC__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinBC_LCD_EN                 (* (reg8 *) PinBC__LCD_EN)
/* Slew Rate Control */
#define PinBC_SLW                    (* (reg8 *) PinBC__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinBC_PRTDSI__CAPS_SEL       (* (reg8 *) PinBC__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinBC_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinBC__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinBC_PRTDSI__OE_SEL0        (* (reg8 *) PinBC__PRTDSI__OE_SEL0) 
#define PinBC_PRTDSI__OE_SEL1        (* (reg8 *) PinBC__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinBC_PRTDSI__OUT_SEL0       (* (reg8 *) PinBC__PRTDSI__OUT_SEL0) 
#define PinBC_PRTDSI__OUT_SEL1       (* (reg8 *) PinBC__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinBC_PRTDSI__SYNC_OUT       (* (reg8 *) PinBC__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinBC__SIO_CFG)
    #define PinBC_SIO_HYST_EN        (* (reg8 *) PinBC__SIO_HYST_EN)
    #define PinBC_SIO_REG_HIFREQ     (* (reg8 *) PinBC__SIO_REG_HIFREQ)
    #define PinBC_SIO_CFG            (* (reg8 *) PinBC__SIO_CFG)
    #define PinBC_SIO_DIFF           (* (reg8 *) PinBC__SIO_DIFF)
#endif /* (PinBC__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinBC__INTSTAT)
    #define PinBC_INTSTAT            (* (reg8 *) PinBC__INTSTAT)
    #define PinBC_SNAP               (* (reg8 *) PinBC__SNAP)
    
	#define PinBC_0_INTTYPE_REG 		(* (reg8 *) PinBC__0__INTTYPE)
#endif /* (PinBC__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinBC_H */


/* [] END OF FILE */
