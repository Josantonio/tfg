/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h"
#include "math.h"

int32 adc_result; //Lectura del ADC
float v1 = 0.0e-9; //Valor de tensión 1
float v2 = 0.0e-9; //Valor de tensión 2
float temp; //Variable para almacenar la temperatura

char str[100]; //String para representar los datos

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    IDAC8_Start();
    ADC_Start();
    UART_Start();
    
    for(;;)
    {
        IDAC8_SetValue(10); //Ponemos la fuente de corriente a 10uA
        CyDelay(600);
        ADC_StartConvert(); //Leemos el valor
        ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);
        adc_result = ADC_GetResult32(); //Resultado en binario
        v1 = ADC_CountsTo_Volts(adc_result); //Resultado en voltios
        
        sprintf(str, "ADC1: %ld bits, Volts1: %f\n", adc_result, v1);
        UART_PutString(str);
        
        IDAC8_SetValue(100); //Ponemos la fuente de corriente a 100uA  
        CyDelay(600);
        ADC_StartConvert(); //Leemos el valor de tensión
        ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);
        adc_result = ADC_GetResult32(); //Resultado en binario
        v2 = ADC_CountsTo_Volts(adc_result); //Resultado en voltios
        
        sprintf(str, "ADC2: %ld bits, Volts2: %f\n", adc_result, v2);
        UART_PutString(str);
        
        temp = ((v2-v1)*5039.55)-273; //Valor de la temperatura en grados celsius
        
        sprintf(str, "Temperatura: %0.2f\n", temp);
        UART_PutString(str);
        UART_PutString("***********************\n");
        CyDelay(2000);
    }
}

/* [] END OF FILE */
