-- ======================================================================
-- AN58827_Termometro_PSoC5_RoutingConsiderations.ctl generated from AN58827_Termometro_PSoC5_RoutingConsiderations
-- 04/01/2020 at 21:00
-- This file is auto generated. ANY EDITS YOU MAKE MAY BE LOST WHEN THIS FILE IS REGENERATED!!!
-- ======================================================================

-- PSoC Clock Editor
-- Directives Editor
-- Analog Device Editor
csattribute placement_force of \ADC:DSM\ : label is "F(DSM,0)";
csattribute placement_force of \IDAC8:viDAC8\ : label is "F(VIDAC,0)";
csattribute locked_route of Net_6 : signal is "p0_6,dsm_0_vplus,vidac_0_iout,agl0_x_vidac_0_iout,agl0,agl6_x_p0_6,p0_6_x_vidac_0_iout,agl6_x_dsm_0_vplus,agl6";
csattribute locked_route of Net_7 : signal is "dsm_0_vminus,agl7_x_dsm_0_vminus,agl7,agl7_x_p0_7,p0_7";
