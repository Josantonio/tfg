/*******************************************************************************
* File Name: PinE.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinE_H) /* Pins PinE_H */
#define CY_PINS_PinE_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinE_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinE__PORT == 15 && ((PinE__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinE_Write(uint8 value);
void    PinE_SetDriveMode(uint8 mode);
uint8   PinE_ReadDataReg(void);
uint8   PinE_Read(void);
void    PinE_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinE_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinE_SetDriveMode() function.
     *  @{
     */
        #define PinE_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinE_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinE_DM_RES_UP          PIN_DM_RES_UP
        #define PinE_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinE_DM_OD_LO           PIN_DM_OD_LO
        #define PinE_DM_OD_HI           PIN_DM_OD_HI
        #define PinE_DM_STRONG          PIN_DM_STRONG
        #define PinE_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinE_MASK               PinE__MASK
#define PinE_SHIFT              PinE__SHIFT
#define PinE_WIDTH              1u

/* Interrupt constants */
#if defined(PinE__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinE_SetInterruptMode() function.
     *  @{
     */
        #define PinE_INTR_NONE      (uint16)(0x0000u)
        #define PinE_INTR_RISING    (uint16)(0x0001u)
        #define PinE_INTR_FALLING   (uint16)(0x0002u)
        #define PinE_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinE_INTR_MASK      (0x01u) 
#endif /* (PinE__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinE_PS                     (* (reg8 *) PinE__PS)
/* Data Register */
#define PinE_DR                     (* (reg8 *) PinE__DR)
/* Port Number */
#define PinE_PRT_NUM                (* (reg8 *) PinE__PRT) 
/* Connect to Analog Globals */                                                  
#define PinE_AG                     (* (reg8 *) PinE__AG)                       
/* Analog MUX bux enable */
#define PinE_AMUX                   (* (reg8 *) PinE__AMUX) 
/* Bidirectional Enable */                                                        
#define PinE_BIE                    (* (reg8 *) PinE__BIE)
/* Bit-mask for Aliased Register Access */
#define PinE_BIT_MASK               (* (reg8 *) PinE__BIT_MASK)
/* Bypass Enable */
#define PinE_BYP                    (* (reg8 *) PinE__BYP)
/* Port wide control signals */                                                   
#define PinE_CTL                    (* (reg8 *) PinE__CTL)
/* Drive Modes */
#define PinE_DM0                    (* (reg8 *) PinE__DM0) 
#define PinE_DM1                    (* (reg8 *) PinE__DM1)
#define PinE_DM2                    (* (reg8 *) PinE__DM2) 
/* Input Buffer Disable Override */
#define PinE_INP_DIS                (* (reg8 *) PinE__INP_DIS)
/* LCD Common or Segment Drive */
#define PinE_LCD_COM_SEG            (* (reg8 *) PinE__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinE_LCD_EN                 (* (reg8 *) PinE__LCD_EN)
/* Slew Rate Control */
#define PinE_SLW                    (* (reg8 *) PinE__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinE_PRTDSI__CAPS_SEL       (* (reg8 *) PinE__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinE_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinE__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinE_PRTDSI__OE_SEL0        (* (reg8 *) PinE__PRTDSI__OE_SEL0) 
#define PinE_PRTDSI__OE_SEL1        (* (reg8 *) PinE__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinE_PRTDSI__OUT_SEL0       (* (reg8 *) PinE__PRTDSI__OUT_SEL0) 
#define PinE_PRTDSI__OUT_SEL1       (* (reg8 *) PinE__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinE_PRTDSI__SYNC_OUT       (* (reg8 *) PinE__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinE__SIO_CFG)
    #define PinE_SIO_HYST_EN        (* (reg8 *) PinE__SIO_HYST_EN)
    #define PinE_SIO_REG_HIFREQ     (* (reg8 *) PinE__SIO_REG_HIFREQ)
    #define PinE_SIO_CFG            (* (reg8 *) PinE__SIO_CFG)
    #define PinE_SIO_DIFF           (* (reg8 *) PinE__SIO_DIFF)
#endif /* (PinE__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinE__INTSTAT)
    #define PinE_INTSTAT            (* (reg8 *) PinE__INTSTAT)
    #define PinE_SNAP               (* (reg8 *) PinE__SNAP)
    
	#define PinE_0_INTTYPE_REG 		(* (reg8 *) PinE__0__INTTYPE)
#endif /* (PinE__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinE_H */


/* [] END OF FILE */
