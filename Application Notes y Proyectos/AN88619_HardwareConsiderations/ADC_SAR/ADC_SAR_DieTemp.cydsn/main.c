/******************************************************************************
* File Name: main.c
*
* Version 3.0
*
* Description: 
*  This example demonstrates two channels measurements by sequencing the 
*  SAR ADC and transferring the results to the UART. 
*
* Related Document: 
*  CE195275_ADC_SAR_Seq_DieTemp_PSoC4.pdf
*
* Hardware Dependency: 
*  See CE195275_ADC_SAR_Seq_DieTemp_PSoC4.pdf
*
*******************************************************************************
* Copyright (2018), Cypress Semiconductor Corporation. All rights reserved.
*******************************************************************************
* This software, including source code, documentation and related materials
* ("Software"), is owned by Cypress Semiconductor Corporation or one of its
* subsidiaries ("Cypress") and is protected by and subject to worldwide patent
* protection (United States and foreign), United States copyright laws and
* international treaty provisions. Therefore, you may use this Software only
* as provided in the license agreement accompanying the software package from
* which you obtained this Software ("EULA").
*
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
* non-transferable license to copy, modify, and compile the Software source
* code solely for use in connection with Cypress’s integrated circuit products.
* Any reproduction, modification, translation, compilation, or representation
* of this Software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, 
* EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress 
* reserves the right to make changes to the Software without notice. Cypress 
* does not assume any liability arising out of the application or use of the 
* Software or any product or circuit described in the Software. Cypress does 
* not authorize its products for use in any products where a malfunction or 
* failure of the Cypress product may reasonably be expected to result in 
* significant property damage, injury or death ("High Risk Product"). By 
* including Cypress’s product in a High Risk Product, the manufacturer of such 
* system or application assumes all risk of such use and in doing so agrees to 
* indemnify Cypress against all liability. 
*******************************************************************************/

#include "project.h"
#include <stdio.h>

/* Temperature measurement channel */
#define ADC_CH_TEMP         (0x02u) //Por el canal 2 del ADC lee la temperatura

#define ADC_INJ_PERIOD      (5u) //Cada 5 segundos (5 unsigned) se mide la temperatura


/******************************************************************************
* The DieTemp Component designed to work with 1.024 reference. If the value 
* of Vref in the ADC SAR Sequencer settings is different from the proposed, 
* the value of ADC counts that is passed to DieTemp_CountsTo_Celsius API 
* should be adjusted.
* For details, see Section "Functional Description" of the PSoC 4 DieTemp  
* datasheet.
******************************************************************************/
#define DIETEMP_VREF_MV_VALUE   (1024) //1.024 V es la referencia

#define UART_BUFFLEN        (100u) //Tamaño del buffer para representar la salida por puerto serie


/******************************************************************************
* Function Name: Isr_Timer_Handler
*******************************************************************************
*
* Summary:
*  Handles the Interrupt Service Routine for the Timer Component.
*
******************************************************************************/
CY_ISR(Isr_Timer_Handler) //Cada segundo se activa la conversión en el ADC Sar
{
    static uint32 injTimer = 0u; //Se pone el injtimer a 0
    
    injTimer++; //se aumenta uno por cada vez que el timer hace una interrupción
    
    if(injTimer >= ADC_INJ_PERIOD) //si es mayor a 5, significa que llevamos 5 segundos, leemos temperatura
    {
        injTimer = 0u; //se vuelve a 0
        /* Enable the injection channel for the next scan */
        ADC_SAR_Seq_EnableInjection(); 
    }
    //cada segundo se lee la otra entrada, que da un valor de voltage
    /* Start the ADC conversion */
    ADC_SAR_Seq_StartConvert();

    Timer_ClearInterrupt(Timer_INTR_MASK_TC); //Limpiamos la interrupción en el timer
}
/* El cana injection sirve para medir una entrada a una frecuencia inferior que las demás */

/******************************************************************************
* Function Name: main
*******************************************************************************
*
* Summary:
*  The main function performs these functions:
*  1. Initializes the UART, ADC, and Timer.
*  2. Starts the ADC conversion.
*  3. Gets the converted results.
*  4. Calculates temperature and voltage values. 
*  5. Prints the calculated values into the UART.
*
******************************************************************************/
int main()
{
    int16 adcResult[ADC_SAR_Seq_TOTAL_CHANNELS_NUM] = {0};
    /*This constant represents the total number of input channels including the injection channel.*/
    int16 voltage[ADC_SAR_Seq_SEQUENCED_CHANNELS_NUM] = {0};
    /*This constant represents the amount of input sequencing channels available for scanning.*/
    
    int32 temperature = 0;
    int16 ADCCountsCorrected = 0; //Corrección en caso de que la Vref no sea 1.024V
    uint16 i = 0;
    char  uartBuff[UART_BUFFLEN];
    
    /* Enable the global interrupts */
    CyGlobalIntEnable; 
    
    /* Start the components */
    UART_Start();
    ADC_SAR_Seq_Start();
    Timer_Start();
    Isr_Timer_StartEx(Isr_Timer_Handler);
    
    UART_PutString("Starting measurements...\r\n\r\n");
    
    for(;;)
    {
        /* When conversion of sequencing channels has completed */
        if(0u != ADC_SAR_Seq_IsEndConversion(ADC_SAR_Seq_RETURN_STATUS))
        {
            /*Leemos cada canal */
            for(i = 0; i < ADC_SAR_Seq_SEQUENCED_CHANNELS_NUM; i++)
            {
                /* Save and convert the ADC result value */
                adcResult[i] = ADC_SAR_Seq_GetResult16(i); //Se guarda en un array de dos elementos
                voltage[i] = ADC_SAR_Seq_CountsTo_mVolts(i, adcResult[i]);
                                                        //channel, Result from de ADC conversion
                /* Print the voltage value into the UART */
                (void)snprintf(uartBuff, UART_BUFFLEN, //Valor de voltaje de las dos entradas 
                               "ADC voltage CH%u = %imV \r\n", i,  voltage[i]);
                UART_PutString(uartBuff);
            }
            
            UART_PutCRLF();//Salto de línea con retorno de carro
        }
        
        /* When conversion of the injection channel has completed */
        if(0u != ADC_SAR_Seq_IsEndConversion(ADC_SAR_Seq_RETURN_STATUS_INJ))
        {
            adcResult[ADC_CH_TEMP] = ADC_SAR_Seq_GetResult16(ADC_CH_TEMP);
            //Lee el resultado sólo del canal de la temperatura
            
            /*****************************************************************
            * Adjust data from ADC with respect to ADC Vref value
            * For details, see Section "Functional Description"
            * of the DieTemp P4 datasheet.
            *****************************************************************/
            ADCCountsCorrected = 
            (int16)(((int32)adcResult[ADC_CH_TEMP] * ADC_SAR_Seq_DEFAULT_VREF_MV_VALUE) / 
                    DIETEMP_VREF_MV_VALUE);
            
            /* Calculate temperature value */
            temperature = DieTemp_CountsTo_Celsius(ADCCountsCorrected);
            
            /* Print the temperature value into the UART */
            (void)snprintf(uartBuff, UART_BUFFLEN, 
                           "Temperature value: %liC \r\n\r\n", temperature);
            UART_PutString(uartBuff);
        }
    }
}


/* [] END OF FILE */
