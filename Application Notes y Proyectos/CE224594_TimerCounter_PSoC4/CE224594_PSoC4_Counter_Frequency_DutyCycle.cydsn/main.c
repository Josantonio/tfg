/******************************************************************************
* File Name: main.c
*
* Version: 1.00
*
* Description: This example demonstrates measuring frequency and duty cycle of
*              an input waveform using counter mode, result is printed over UART.
*
* Related Document: CE224594_TCPWM_CounterTimer.pdf 
*
* Hardware Dependency: CY8CKIT-042 PSoC 4 PIONEER KIT
*
******************************************************************************
* Copyright (2018), Cypress Semiconductor Corporation.
******************************************************************************
* This software, including source code, documentation and related materials
* ("Software") is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and 
* foreign), United States copyright laws and international treaty provisions. 
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the 
* Cypress source code and derivative works for the sole purpose of creating 
* custom software in support of licensee product, such licensee product to be
* used only in conjunction with Cypress's integrated circuit as specified in the
* applicable agreement. Any reproduction, modification, translation, compilation,
* or representation of this Software except as specified above is prohibited 
* without the express written permission of Cypress.
* 
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, 
* EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes to the Software without notice. 
* Cypress does not assume any liability arising out of the application or use
* of Software or any product or circuit described in the Software. Cypress does
* not authorize its products for use as critical components in any products 
* where a malfunction or failure may reasonably be expected to result in 
* significant injury or death ("ACTIVE Risk Product"). By including Cypress's 
* product in a ACTIVE Risk Product, the manufacturer of such system or application
* assumes all risk of such use and in doing so indemnifies Cypress against all
* liability. Use of this Software may be limited by and subject to the applicable
* Cypress software license agreement.
*****************************************************************************/

#include <project.h>
#include <stdio.h>


/*******************************************************************************
* Function Name: main
********************************************************************************
*
*  The main function performs the following actions:
*   1. Timer, PWM, and UART is started.
*   2. Every five seconds the period and duty cycle is calculated and printed
*
*******************************************************************************/
int main(void)
{
    /* Values used for calculation and captures are initialized */
    uint32 highCaptureCount = 0;
    uint32 lowCaptureCount = 0;
    uint32 period = 0;
    uint32 frequency = 0;
    uint32 dutyCycle = 0;
    uint32 clockSpeed = 12000000;
    char snum[32]; /* for string conversion */

    /* Start timers */
    Timer_1_Start();
    Timer_2_Start();
    
    /* Start PWM and UART */
    PWM_Start();
    UART_Start();
    UART_UartPutString("UART has successfully started\r\n");
    
    /* Enable global interrupts */
	CyGlobalIntEnable; 
    
    for(;;)
    {
        /* Delay for stability and slower repeated prints to terminal */
        CyDelay(5000);
        
        /* Capture of both counts of high and low parts of waveform */
        highCaptureCount = Timer_1_ReadCapture();
        lowCaptureCount = Timer_2_ReadCapture();
        
        /* Period is calculated from high + low time of the waveform */
        period = highCaptureCount + lowCaptureCount;
        
        /* Frequency is calculated */
        frequency = clockSpeed/period;
        
        /* Duty Cycle is calculated with rounding */
        dutyCycle = (highCaptureCount*100 + (period/2))/(period);
        
        /* Frequency and Duty Cycle is printed */
        sprintf(snum, "\r\nCurrent Frequency: %lu", frequency);
        UART_UartPutString(snum);
        sprintf(snum, " | Current Duty Cycle: %lu ", dutyCycle);
        UART_UartPutString(snum);
    }
}

/* [] END OF FILE */
