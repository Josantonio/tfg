/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h" //Para sprintf

uint8 num_period = 0; //Variable para almacenar el número de períodos que ocurren entre que pulsamos el botón
//puede no darse el caso
uint32 total_count; //Variable para almacenar el numero de períodos totales
float tiempo;

CY_ISR(isr_TC_handler){ //Cada vez que llegemos al final del período
    num_period++; //Se incrementa el número de períodos contabilizados
    /*
    PinLED_Write(1);
    CyDelay(50);
    PinLED_Write(0);
    */
    Timer_ReadStatusRegister(); //Leemos el estado del registro para que se borre la interrupción
}

int main(void)
{
    char snum[100]; /* Buffer for string conversion */
    uint32 prevCount = 0;
    uint32 currentCount;
    
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    UART_Start();
    UART_PutString("Comunicacion Serie Establecida\n");
    Timer_Start();
    SwClock_Start();
    timer_clock_Start();
    
    isr_TC_StartEx(isr_TC_handler);
    isr_TC_ClearPending();
    
    /* Enable global interrupts. */
	CyGlobalIntEnable;

    for(;;)
    {
        //Cada vez que se pulsa el botón hace una captura
        currentCount = Timer_ReadCapture(); //Leemos el valor de la captura

        
        //Si se ha pulsado el botón, la captura ha tenido que cambiar
        if(currentCount != prevCount){
            PinLED_Write(1); //Encendemos el LED
            
            /* Calculamos la cuenta de períodos totales */
            total_count = currentCount+num_period*Timer_ReadPeriod()-prevCount; 
            num_period = 0; //Se resetea el contador que cuenta los períodos transcurridos
            tiempo = total_count*0.001;
            
            sprintf(snum,"Total Count: %lu Tiempo transcurrido: %f s\n", total_count, tiempo);
            UART_PutString(snum);
            
            CyDelay(50); //Esperamos 50 ms
            PinLED_Write(0); //Apagamos el LED
            prevCount = currentCount; //Actualizamos el valor de la cuenta
        }     
    }
}

/* [] END OF FILE */
