/*******************************************************************************
* File Name: PinButton.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinButton_H) /* Pins PinButton_H */
#define CY_PINS_PinButton_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinButton_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinButton__PORT == 15 && ((PinButton__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinButton_Write(uint8 value);
void    PinButton_SetDriveMode(uint8 mode);
uint8   PinButton_ReadDataReg(void);
uint8   PinButton_Read(void);
void    PinButton_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinButton_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinButton_SetDriveMode() function.
     *  @{
     */
        #define PinButton_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinButton_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinButton_DM_RES_UP          PIN_DM_RES_UP
        #define PinButton_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinButton_DM_OD_LO           PIN_DM_OD_LO
        #define PinButton_DM_OD_HI           PIN_DM_OD_HI
        #define PinButton_DM_STRONG          PIN_DM_STRONG
        #define PinButton_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinButton_MASK               PinButton__MASK
#define PinButton_SHIFT              PinButton__SHIFT
#define PinButton_WIDTH              1u

/* Interrupt constants */
#if defined(PinButton__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinButton_SetInterruptMode() function.
     *  @{
     */
        #define PinButton_INTR_NONE      (uint16)(0x0000u)
        #define PinButton_INTR_RISING    (uint16)(0x0001u)
        #define PinButton_INTR_FALLING   (uint16)(0x0002u)
        #define PinButton_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinButton_INTR_MASK      (0x01u) 
#endif /* (PinButton__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinButton_PS                     (* (reg8 *) PinButton__PS)
/* Data Register */
#define PinButton_DR                     (* (reg8 *) PinButton__DR)
/* Port Number */
#define PinButton_PRT_NUM                (* (reg8 *) PinButton__PRT) 
/* Connect to Analog Globals */                                                  
#define PinButton_AG                     (* (reg8 *) PinButton__AG)                       
/* Analog MUX bux enable */
#define PinButton_AMUX                   (* (reg8 *) PinButton__AMUX) 
/* Bidirectional Enable */                                                        
#define PinButton_BIE                    (* (reg8 *) PinButton__BIE)
/* Bit-mask for Aliased Register Access */
#define PinButton_BIT_MASK               (* (reg8 *) PinButton__BIT_MASK)
/* Bypass Enable */
#define PinButton_BYP                    (* (reg8 *) PinButton__BYP)
/* Port wide control signals */                                                   
#define PinButton_CTL                    (* (reg8 *) PinButton__CTL)
/* Drive Modes */
#define PinButton_DM0                    (* (reg8 *) PinButton__DM0) 
#define PinButton_DM1                    (* (reg8 *) PinButton__DM1)
#define PinButton_DM2                    (* (reg8 *) PinButton__DM2) 
/* Input Buffer Disable Override */
#define PinButton_INP_DIS                (* (reg8 *) PinButton__INP_DIS)
/* LCD Common or Segment Drive */
#define PinButton_LCD_COM_SEG            (* (reg8 *) PinButton__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinButton_LCD_EN                 (* (reg8 *) PinButton__LCD_EN)
/* Slew Rate Control */
#define PinButton_SLW                    (* (reg8 *) PinButton__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinButton_PRTDSI__CAPS_SEL       (* (reg8 *) PinButton__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinButton_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinButton__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinButton_PRTDSI__OE_SEL0        (* (reg8 *) PinButton__PRTDSI__OE_SEL0) 
#define PinButton_PRTDSI__OE_SEL1        (* (reg8 *) PinButton__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinButton_PRTDSI__OUT_SEL0       (* (reg8 *) PinButton__PRTDSI__OUT_SEL0) 
#define PinButton_PRTDSI__OUT_SEL1       (* (reg8 *) PinButton__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinButton_PRTDSI__SYNC_OUT       (* (reg8 *) PinButton__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinButton__SIO_CFG)
    #define PinButton_SIO_HYST_EN        (* (reg8 *) PinButton__SIO_HYST_EN)
    #define PinButton_SIO_REG_HIFREQ     (* (reg8 *) PinButton__SIO_REG_HIFREQ)
    #define PinButton_SIO_CFG            (* (reg8 *) PinButton__SIO_CFG)
    #define PinButton_SIO_DIFF           (* (reg8 *) PinButton__SIO_DIFF)
#endif /* (PinButton__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinButton__INTSTAT)
    #define PinButton_INTSTAT            (* (reg8 *) PinButton__INTSTAT)
    #define PinButton_SNAP               (* (reg8 *) PinButton__SNAP)
    
	#define PinButton_0_INTTYPE_REG 		(* (reg8 *) PinButton__0__INTTYPE)
#endif /* (PinButton__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinButton_H */


/* [] END OF FILE */
