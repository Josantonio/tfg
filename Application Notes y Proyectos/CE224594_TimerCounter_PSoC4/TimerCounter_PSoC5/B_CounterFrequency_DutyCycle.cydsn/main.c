/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "stdio.h" //Para sprintf
#include "math.h" //Hay que incluirla tb en el linker
//Creator -> Project -> Build Settings -> Linker  the "Additional Libraries" to "m" 

#define MAX_PWM_PERIOD (uint16)(pow(2,PWM_Resolution)-1) //Máximo n bits del período del PWM
#define MAX_POT_LECTURE (uint16)(pow(2,ADC_SAR_DEFAULT_RESOLUTION)-1) //Máximo n bits de lectura en los potenciómetros
#define PERIOD_CLOCK (float)(1/24e6) //Período de la señal de reloj principal

float dutyPWM; //Duty cycle, de 0% a 100%
uint32 periodPWM; //Period del PWM, de 0 a 255
uint16 freqPWM; //Frecuencia, de 0-65535Hz

int16 data[ADC_NUMBER_OF_CHANNELS];
float volts[ADC_NUMBER_OF_CHANNELS];

char str[100]; //String para la representación

uint16 countH; //Cuenta realizada por el contador que mide el estado en alto
uint16 countL; //Cuenta realizada por el contador que mide el estado en bajo

float period_calc; //Variable para almacenar el período calculado
float freq; //Frecuencia calculada
float duty; //Variable para almacenar el tiempo en alto
float duty_perc; //Variable para convertir el duty en porcentaje

int main(void)
{    
    CyGlobalIntEnable; /* Enable global interrupts. */
 
    /* INICIALIZACIÓN COMPONENTES */
    UART_Start();
    ADC_Start();
    PWM_Start();
    CounterH_Start();
    CounterL_Start();
    
    for(;;)
    {
        /**************** LEER POTENCIÓMETROS Y GENERAR SEÑAL *****************/
        
            ADC_StartConvert(); //Empieza la conversión del ADC
            ADC_IsEndConversion(ADC_WAIT_FOR_RESULT); //Espera a que se termine
            for (uint i = 0; i < ADC_NUMBER_OF_CHANNELS ; i++){ //Leemos los canales del ADC
                data[i] = ADC_GetResult16(i); //Almacenamos cada uno en la variable data
                sprintf(str, "Data%d: %d bits\n",i,data[i]);  //Creamos un string para representarlos
                UART_PutString(str); //Representamos el string
                volts[i] = ADC_CountsTo_Volts(data[i]); //Lo mismo hacemos pero convirtiendo el nbits a voltios
                sprintf(str, "Volts%d: %0.02f V\n",i,volts[i]);
                UART_PutString(str);
            }
            /* Necesitamos mostrar el duty como un %, ya que se debe aplicar al período, para que 
             * nunca se dé el caso de que el duty sea mayor que el período */
            periodPWM = round(MAX_PWM_PERIOD-MAX_PWM_PERIOD*(MAX_POT_LECTURE-data[0])/MAX_POT_LECTURE);
            dutyPWM = data[1]*100/MAX_POT_LECTURE; //Duty en tanto por ciento %
            sprintf(str, "Period: %lu, Duty: %0.2f%% PWM max: %d, Pot Max: %d\n\n",periodPWM,dutyPWM, MAX_PWM_PERIOD, MAX_POT_LECTURE);
            UART_PutString(str);

            /* Modificamos el PWM en función de la lectura de los potenciómetros */
            PWM_WritePeriod(periodPWM); //Escribimos el período
            PWM_WriteCompare(round(periodPWM*dutyPWM/100)); //Escribimos el nuevo duty cycle
            
            
       /********************** CALCULAR PERÍODO Y DUTY CYCLE ***********************************/
            
            //Leemos el valor de las capturas, nos dará un número del 0 al 65536
            countH = CounterH_ReadCapture();
            countL = CounterL_ReadCapture();
            
            //El período será la suma de los dos, y el duty sólo el de high
            period_calc = (countH+countL)*2*PERIOD_CLOCK;
            freq = 1/period_calc;
            /* Cada bit equivale a un período de Count, que equivale a su vez a dos ciclos  *
             * de reloj global de 24 Mhz.                                                   *
             * count tiene una frecuencia de la mitad, 12 Mhz                               *
             */
            duty = countH*2*PERIOD_CLOCK; //Tiempo que está en alto en us
            duty_perc = duty*100/period_calc;
            
            UART_PutString("MEDICIONES\n");
            sprintf(str, "Periodo calc: %0.2f us, Duty: %0.3fus \nFrecuencia: %0.2fkHz, Duty perc: %0.2f%%\n",period_calc*1e6,duty*1e6,freq*1e-3, duty_perc);
            UART_PutString(str);
            UART_PutString("******************************\n");
            CyDelay(3000);          
    }
}

/* [] END OF FILE */
