/*******************************************************************************
* File Name: TimerH_PM.c
* Version 2.80
*
*  Description:
*     This file provides the power management source code to API for the
*     Timer.
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#include "TimerH.h"

static TimerH_backupStruct TimerH_backup;


/*******************************************************************************
* Function Name: TimerH_SaveConfig
********************************************************************************
*
* Summary:
*     Save the current user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerH_backup:  Variables of this global structure are modified to
*  store the values of non retention configuration registers when Sleep() API is
*  called.
*
*******************************************************************************/
void TimerH_SaveConfig(void) 
{
    #if (!TimerH_UsingFixedFunction)
        TimerH_backup.TimerUdb = TimerH_ReadCounter();
        TimerH_backup.InterruptMaskValue = TimerH_STATUS_MASK;
        #if (TimerH_UsingHWCaptureCounter)
            TimerH_backup.TimerCaptureCounter = TimerH_ReadCaptureCount();
        #endif /* Back Up capture counter register  */

        #if(!TimerH_UDB_CONTROL_REG_REMOVED)
            TimerH_backup.TimerControlRegister = TimerH_ReadControlRegister();
        #endif /* Backup the enable state of the Timer component */
    #endif /* Backup non retention registers in UDB implementation. All fixed function registers are retention */
}


/*******************************************************************************
* Function Name: TimerH_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerH_backup:  Variables of this global structure are used to
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void TimerH_RestoreConfig(void) 
{   
    #if (!TimerH_UsingFixedFunction)

        TimerH_WriteCounter(TimerH_backup.TimerUdb);
        TimerH_STATUS_MASK =TimerH_backup.InterruptMaskValue;
        #if (TimerH_UsingHWCaptureCounter)
            TimerH_SetCaptureCount(TimerH_backup.TimerCaptureCounter);
        #endif /* Restore Capture counter register*/

        #if(!TimerH_UDB_CONTROL_REG_REMOVED)
            TimerH_WriteControlRegister(TimerH_backup.TimerControlRegister);
        #endif /* Restore the enable state of the Timer component */
    #endif /* Restore non retention registers in the UDB implementation only */
}


/*******************************************************************************
* Function Name: TimerH_Sleep
********************************************************************************
*
* Summary:
*     Stop and Save the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerH_backup.TimerEnableState:  Is modified depending on the
*  enable state of the block before entering sleep mode.
*
*******************************************************************************/
void TimerH_Sleep(void) 
{
    #if(!TimerH_UDB_CONTROL_REG_REMOVED)
        /* Save Counter's enable state */
        if(TimerH_CTRL_ENABLE == (TimerH_CONTROL & TimerH_CTRL_ENABLE))
        {
            /* Timer is enabled */
            TimerH_backup.TimerEnableState = 1u;
        }
        else
        {
            /* Timer is disabled */
            TimerH_backup.TimerEnableState = 0u;
        }
    #endif /* Back up enable state from the Timer control register */
    TimerH_Stop();
    TimerH_SaveConfig();
}


/*******************************************************************************
* Function Name: TimerH_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  TimerH_backup.enableState:  Is used to restore the enable state of
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void TimerH_Wakeup(void) 
{
    TimerH_RestoreConfig();
    #if(!TimerH_UDB_CONTROL_REG_REMOVED)
        if(TimerH_backup.TimerEnableState == 1u)
        {     /* Enable Timer's operation */
                TimerH_Enable();
        } /* Do nothing if Timer was disabled before */
    #endif /* Remove this code section if Control register is removed */
}


/* [] END OF FILE */
