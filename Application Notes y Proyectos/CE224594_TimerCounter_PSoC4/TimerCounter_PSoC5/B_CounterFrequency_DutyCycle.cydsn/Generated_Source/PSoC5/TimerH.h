/*******************************************************************************
* File Name: TimerH.h
* Version 2.80
*
*  Description:
*     Contains the function prototypes and constants available to the timer
*     user module.
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_TIMER_TimerH_H)
#define CY_TIMER_TimerH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 TimerH_initVar;

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component Timer_v2_80 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */


/**************************************
*           Parameter Defaults
**************************************/

#define TimerH_Resolution                 16u
#define TimerH_UsingFixedFunction         0u
#define TimerH_UsingHWCaptureCounter      1u
#define TimerH_SoftwareCaptureMode        0u
#define TimerH_SoftwareTriggerMode        0u
#define TimerH_UsingHWEnable              0u
#define TimerH_EnableTriggerMode          0u
#define TimerH_InterruptOnCaptureCount    1u
#define TimerH_RunModeUsed                0u
#define TimerH_ControlRegRemoved          0u

#if defined(TimerH_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG)
    #define TimerH_UDB_CONTROL_REG_REMOVED            (0u)
#elif  (TimerH_UsingFixedFunction)
    #define TimerH_UDB_CONTROL_REG_REMOVED            (0u)
#else 
    #define TimerH_UDB_CONTROL_REG_REMOVED            (1u)
#endif /* End TimerH_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG */


/***************************************
*       Type defines
***************************************/


/**************************************************************************
 * Sleep Wakeup Backup structure for Timer Component
 *************************************************************************/
typedef struct
{
    uint8 TimerEnableState;
    #if(!TimerH_UsingFixedFunction)

        uint16 TimerUdb;
        uint8 InterruptMaskValue;
        #if (TimerH_UsingHWCaptureCounter)
            uint8 TimerCaptureCounter;
        #endif /* variable declarations for backing up non retention registers in CY_UDB_V1 */

        #if (!TimerH_UDB_CONTROL_REG_REMOVED)
            uint8 TimerControlRegister;
        #endif /* variable declaration for backing up enable state of the Timer */
    #endif /* define backup variables only for UDB implementation. Fixed function registers are all retention */

}TimerH_backupStruct;


/***************************************
*       Function Prototypes
***************************************/

void    TimerH_Start(void) ;
void    TimerH_Stop(void) ;

void    TimerH_SetInterruptMode(uint8 interruptMode) ;
uint8   TimerH_ReadStatusRegister(void) ;
/* Deprecated function. Do not use this in future. Retained for backward compatibility */
#define TimerH_GetInterruptSource() TimerH_ReadStatusRegister()

#if(!TimerH_UDB_CONTROL_REG_REMOVED)
    uint8   TimerH_ReadControlRegister(void) ;
    void    TimerH_WriteControlRegister(uint8 control) ;
#endif /* (!TimerH_UDB_CONTROL_REG_REMOVED) */

uint16  TimerH_ReadPeriod(void) ;
void    TimerH_WritePeriod(uint16 period) ;
uint16  TimerH_ReadCounter(void) ;
void    TimerH_WriteCounter(uint16 counter) ;
uint16  TimerH_ReadCapture(void) ;
void    TimerH_SoftwareCapture(void) ;

#if(!TimerH_UsingFixedFunction) /* UDB Prototypes */
    #if (TimerH_SoftwareCaptureMode)
        void    TimerH_SetCaptureMode(uint8 captureMode) ;
    #endif /* (!TimerH_UsingFixedFunction) */

    #if (TimerH_SoftwareTriggerMode)
        void    TimerH_SetTriggerMode(uint8 triggerMode) ;
    #endif /* (TimerH_SoftwareTriggerMode) */

    #if (TimerH_EnableTriggerMode)
        void    TimerH_EnableTrigger(void) ;
        void    TimerH_DisableTrigger(void) ;
    #endif /* (TimerH_EnableTriggerMode) */


    #if(TimerH_InterruptOnCaptureCount)
        void    TimerH_SetInterruptCount(uint8 interruptCount) ;
    #endif /* (TimerH_InterruptOnCaptureCount) */

    #if (TimerH_UsingHWCaptureCounter)
        void    TimerH_SetCaptureCount(uint8 captureCount) ;
        uint8   TimerH_ReadCaptureCount(void) ;
    #endif /* (TimerH_UsingHWCaptureCounter) */

    void TimerH_ClearFIFO(void) ;
#endif /* UDB Prototypes */

/* Sleep Retention APIs */
void TimerH_Init(void)          ;
void TimerH_Enable(void)        ;
void TimerH_SaveConfig(void)    ;
void TimerH_RestoreConfig(void) ;
void TimerH_Sleep(void)         ;
void TimerH_Wakeup(void)        ;


/***************************************
*   Enumerated Types and Parameters
***************************************/

/* Enumerated Type B_Timer__CaptureModes, Used in Capture Mode */
#define TimerH__B_TIMER__CM_NONE 0
#define TimerH__B_TIMER__CM_RISINGEDGE 1
#define TimerH__B_TIMER__CM_FALLINGEDGE 2
#define TimerH__B_TIMER__CM_EITHEREDGE 3
#define TimerH__B_TIMER__CM_SOFTWARE 4



/* Enumerated Type B_Timer__TriggerModes, Used in Trigger Mode */
#define TimerH__B_TIMER__TM_NONE 0x00u
#define TimerH__B_TIMER__TM_RISINGEDGE 0x04u
#define TimerH__B_TIMER__TM_FALLINGEDGE 0x08u
#define TimerH__B_TIMER__TM_EITHEREDGE 0x0Cu
#define TimerH__B_TIMER__TM_SOFTWARE 0x10u


/***************************************
*    Initialial Parameter Constants
***************************************/

#define TimerH_INIT_PERIOD             65535u
#define TimerH_INIT_CAPTURE_MODE       ((uint8)((uint8)3u << TimerH_CTRL_CAP_MODE_SHIFT))
#define TimerH_INIT_TRIGGER_MODE       ((uint8)((uint8)0u << TimerH_CTRL_TRIG_MODE_SHIFT))
#if (TimerH_UsingFixedFunction)
    #define TimerH_INIT_INTERRUPT_MODE (((uint8)((uint8)1u << TimerH_STATUS_TC_INT_MASK_SHIFT)) | \
                                                  ((uint8)((uint8)1 << TimerH_STATUS_CAPTURE_INT_MASK_SHIFT)))
#else
    #define TimerH_INIT_INTERRUPT_MODE (((uint8)((uint8)1u << TimerH_STATUS_TC_INT_MASK_SHIFT)) | \
                                                 ((uint8)((uint8)1 << TimerH_STATUS_CAPTURE_INT_MASK_SHIFT)) | \
                                                 ((uint8)((uint8)0 << TimerH_STATUS_FIFOFULL_INT_MASK_SHIFT)))
#endif /* (TimerH_UsingFixedFunction) */
#define TimerH_INIT_CAPTURE_COUNT      (2u)
#define TimerH_INIT_INT_CAPTURE_COUNT  ((uint8)((uint8)(2u - 1u) << TimerH_CTRL_INTCNT_SHIFT))


/***************************************
*           Registers
***************************************/

#if (TimerH_UsingFixedFunction) /* Implementation Specific Registers and Register Constants */


    /***************************************
    *    Fixed Function Registers
    ***************************************/

    #define TimerH_STATUS         (*(reg8 *) TimerH_TimerHW__SR0 )
    /* In Fixed Function Block Status and Mask are the same register */
    #define TimerH_STATUS_MASK    (*(reg8 *) TimerH_TimerHW__SR0 )
    #define TimerH_CONTROL        (*(reg8 *) TimerH_TimerHW__CFG0)
    #define TimerH_CONTROL2       (*(reg8 *) TimerH_TimerHW__CFG1)
    #define TimerH_CONTROL2_PTR   ( (reg8 *) TimerH_TimerHW__CFG1)
    #define TimerH_RT1            (*(reg8 *) TimerH_TimerHW__RT1)
    #define TimerH_RT1_PTR        ( (reg8 *) TimerH_TimerHW__RT1)

    #if (CY_PSOC3 || CY_PSOC5LP)
        #define TimerH_CONTROL3       (*(reg8 *) TimerH_TimerHW__CFG2)
        #define TimerH_CONTROL3_PTR   ( (reg8 *) TimerH_TimerHW__CFG2)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    #define TimerH_GLOBAL_ENABLE  (*(reg8 *) TimerH_TimerHW__PM_ACT_CFG)
    #define TimerH_GLOBAL_STBY_ENABLE  (*(reg8 *) TimerH_TimerHW__PM_STBY_CFG)

    #define TimerH_CAPTURE_LSB         (* (reg16 *) TimerH_TimerHW__CAP0 )
    #define TimerH_CAPTURE_LSB_PTR       ((reg16 *) TimerH_TimerHW__CAP0 )
    #define TimerH_PERIOD_LSB          (* (reg16 *) TimerH_TimerHW__PER0 )
    #define TimerH_PERIOD_LSB_PTR        ((reg16 *) TimerH_TimerHW__PER0 )
    #define TimerH_COUNTER_LSB         (* (reg16 *) TimerH_TimerHW__CNT_CMP0 )
    #define TimerH_COUNTER_LSB_PTR       ((reg16 *) TimerH_TimerHW__CNT_CMP0 )


    /***************************************
    *    Register Constants
    ***************************************/

    /* Fixed Function Block Chosen */
    #define TimerH_BLOCK_EN_MASK                     TimerH_TimerHW__PM_ACT_MSK
    #define TimerH_BLOCK_STBY_EN_MASK                TimerH_TimerHW__PM_STBY_MSK

    /* Control Register Bit Locations */
    /* Interrupt Count - Not valid for Fixed Function Block */
    #define TimerH_CTRL_INTCNT_SHIFT                  0x00u
    /* Trigger Polarity - Not valid for Fixed Function Block */
    #define TimerH_CTRL_TRIG_MODE_SHIFT               0x00u
    /* Trigger Enable - Not valid for Fixed Function Block */
    #define TimerH_CTRL_TRIG_EN_SHIFT                 0x00u
    /* Capture Polarity - Not valid for Fixed Function Block */
    #define TimerH_CTRL_CAP_MODE_SHIFT                0x00u
    /* Timer Enable - As defined in Register Map, part of TMRX_CFG0 register */
    #define TimerH_CTRL_ENABLE_SHIFT                  0x00u

    /* Control Register Bit Masks */
    #define TimerH_CTRL_ENABLE                        ((uint8)((uint8)0x01u << TimerH_CTRL_ENABLE_SHIFT))

    /* Control2 Register Bit Masks */
    /* As defined in Register Map, Part of the TMRX_CFG1 register */
    #define TimerH_CTRL2_IRQ_SEL_SHIFT                 0x00u
    #define TimerH_CTRL2_IRQ_SEL                      ((uint8)((uint8)0x01u << TimerH_CTRL2_IRQ_SEL_SHIFT))

    #if (CY_PSOC5A)
        /* Use CFG1 Mode bits to set run mode */
        /* As defined by Verilog Implementation */
        #define TimerH_CTRL_MODE_SHIFT                 0x01u
        #define TimerH_CTRL_MODE_MASK                 ((uint8)((uint8)0x07u << TimerH_CTRL_MODE_SHIFT))
    #endif /* (CY_PSOC5A) */
    #if (CY_PSOC3 || CY_PSOC5LP)
        /* Control3 Register Bit Locations */
        #define TimerH_CTRL_RCOD_SHIFT        0x02u
        #define TimerH_CTRL_ENBL_SHIFT        0x00u
        #define TimerH_CTRL_MODE_SHIFT        0x00u

        /* Control3 Register Bit Masks */
        #define TimerH_CTRL_RCOD_MASK  ((uint8)((uint8)0x03u << TimerH_CTRL_RCOD_SHIFT)) /* ROD and COD bit masks */
        #define TimerH_CTRL_ENBL_MASK  ((uint8)((uint8)0x80u << TimerH_CTRL_ENBL_SHIFT)) /* HW_EN bit mask */
        #define TimerH_CTRL_MODE_MASK  ((uint8)((uint8)0x03u << TimerH_CTRL_MODE_SHIFT)) /* Run mode bit mask */

        #define TimerH_CTRL_RCOD       ((uint8)((uint8)0x03u << TimerH_CTRL_RCOD_SHIFT))
        #define TimerH_CTRL_ENBL       ((uint8)((uint8)0x80u << TimerH_CTRL_ENBL_SHIFT))
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */

    /*RT1 Synch Constants: Applicable for PSoC3 and PSoC5LP */
    #define TimerH_RT1_SHIFT                       0x04u
    /* Sync TC and CMP bit masks */
    #define TimerH_RT1_MASK                        ((uint8)((uint8)0x03u << TimerH_RT1_SHIFT))
    #define TimerH_SYNC                            ((uint8)((uint8)0x03u << TimerH_RT1_SHIFT))
    #define TimerH_SYNCDSI_SHIFT                   0x00u
    /* Sync all DSI inputs with Mask  */
    #define TimerH_SYNCDSI_MASK                    ((uint8)((uint8)0x0Fu << TimerH_SYNCDSI_SHIFT))
    /* Sync all DSI inputs */
    #define TimerH_SYNCDSI_EN                      ((uint8)((uint8)0x0Fu << TimerH_SYNCDSI_SHIFT))

    #define TimerH_CTRL_MODE_PULSEWIDTH            ((uint8)((uint8)0x01u << TimerH_CTRL_MODE_SHIFT))
    #define TimerH_CTRL_MODE_PERIOD                ((uint8)((uint8)0x02u << TimerH_CTRL_MODE_SHIFT))
    #define TimerH_CTRL_MODE_CONTINUOUS            ((uint8)((uint8)0x00u << TimerH_CTRL_MODE_SHIFT))

    /* Status Register Bit Locations */
    /* As defined in Register Map, part of TMRX_SR0 register */
    #define TimerH_STATUS_TC_SHIFT                 0x07u
    /* As defined in Register Map, part of TMRX_SR0 register, Shared with Compare Status */
    #define TimerH_STATUS_CAPTURE_SHIFT            0x06u
    /* As defined in Register Map, part of TMRX_SR0 register */
    #define TimerH_STATUS_TC_INT_MASK_SHIFT        (TimerH_STATUS_TC_SHIFT - 0x04u)
    /* As defined in Register Map, part of TMRX_SR0 register, Shared with Compare Status */
    #define TimerH_STATUS_CAPTURE_INT_MASK_SHIFT   (TimerH_STATUS_CAPTURE_SHIFT - 0x04u)

    /* Status Register Bit Masks */
    #define TimerH_STATUS_TC                       ((uint8)((uint8)0x01u << TimerH_STATUS_TC_SHIFT))
    #define TimerH_STATUS_CAPTURE                  ((uint8)((uint8)0x01u << TimerH_STATUS_CAPTURE_SHIFT))
    /* Interrupt Enable Bit-Mask for interrupt on TC */
    #define TimerH_STATUS_TC_INT_MASK              ((uint8)((uint8)0x01u << TimerH_STATUS_TC_INT_MASK_SHIFT))
    /* Interrupt Enable Bit-Mask for interrupt on Capture */
    #define TimerH_STATUS_CAPTURE_INT_MASK         ((uint8)((uint8)0x01u << TimerH_STATUS_CAPTURE_INT_MASK_SHIFT))

#else   /* UDB Registers and Register Constants */


    /***************************************
    *           UDB Registers
    ***************************************/

    #define TimerH_STATUS              (* (reg8 *) TimerH_TimerUDB_rstSts_stsreg__STATUS_REG )
    #define TimerH_STATUS_MASK         (* (reg8 *) TimerH_TimerUDB_rstSts_stsreg__MASK_REG)
    #define TimerH_STATUS_AUX_CTRL     (* (reg8 *) TimerH_TimerUDB_rstSts_stsreg__STATUS_AUX_CTL_REG)
    #define TimerH_CONTROL             (* (reg8 *) TimerH_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG )
    
    #if(TimerH_Resolution <= 8u) /* 8-bit Timer */
        #define TimerH_CAPTURE_LSB         (* (reg8 *) TimerH_TimerUDB_sT16_timerdp_u0__F0_REG )
        #define TimerH_CAPTURE_LSB_PTR       ((reg8 *) TimerH_TimerUDB_sT16_timerdp_u0__F0_REG )
        #define TimerH_PERIOD_LSB          (* (reg8 *) TimerH_TimerUDB_sT16_timerdp_u0__D0_REG )
        #define TimerH_PERIOD_LSB_PTR        ((reg8 *) TimerH_TimerUDB_sT16_timerdp_u0__D0_REG )
        #define TimerH_COUNTER_LSB         (* (reg8 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
        #define TimerH_COUNTER_LSB_PTR       ((reg8 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
    #elif(TimerH_Resolution <= 16u) /* 8-bit Timer */
        #if(CY_PSOC3) /* 8-bit addres space */
            #define TimerH_CAPTURE_LSB         (* (reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__F0_REG )
            #define TimerH_CAPTURE_LSB_PTR       ((reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__F0_REG )
            #define TimerH_PERIOD_LSB          (* (reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__D0_REG )
            #define TimerH_PERIOD_LSB_PTR        ((reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__D0_REG )
            #define TimerH_COUNTER_LSB         (* (reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
            #define TimerH_COUNTER_LSB_PTR       ((reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
        #else /* 16-bit address space */
            #define TimerH_CAPTURE_LSB         (* (reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__16BIT_F0_REG )
            #define TimerH_CAPTURE_LSB_PTR       ((reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__16BIT_F0_REG )
            #define TimerH_PERIOD_LSB          (* (reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__16BIT_D0_REG )
            #define TimerH_PERIOD_LSB_PTR        ((reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__16BIT_D0_REG )
            #define TimerH_COUNTER_LSB         (* (reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__16BIT_A0_REG )
            #define TimerH_COUNTER_LSB_PTR       ((reg16 *) TimerH_TimerUDB_sT16_timerdp_u0__16BIT_A0_REG )
        #endif /* CY_PSOC3 */
    #elif(TimerH_Resolution <= 24u)/* 24-bit Timer */
        #define TimerH_CAPTURE_LSB         (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__F0_REG )
        #define TimerH_CAPTURE_LSB_PTR       ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__F0_REG )
        #define TimerH_PERIOD_LSB          (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__D0_REG )
        #define TimerH_PERIOD_LSB_PTR        ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__D0_REG )
        #define TimerH_COUNTER_LSB         (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
        #define TimerH_COUNTER_LSB_PTR       ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
    #else /* 32-bit Timer */
        #if(CY_PSOC3 || CY_PSOC5) /* 8-bit address space */
            #define TimerH_CAPTURE_LSB         (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__F0_REG )
            #define TimerH_CAPTURE_LSB_PTR       ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__F0_REG )
            #define TimerH_PERIOD_LSB          (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__D0_REG )
            #define TimerH_PERIOD_LSB_PTR        ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__D0_REG )
            #define TimerH_COUNTER_LSB         (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
            #define TimerH_COUNTER_LSB_PTR       ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
        #else /* 32-bit address space */
            #define TimerH_CAPTURE_LSB         (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__32BIT_F0_REG )
            #define TimerH_CAPTURE_LSB_PTR       ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__32BIT_F0_REG )
            #define TimerH_PERIOD_LSB          (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__32BIT_D0_REG )
            #define TimerH_PERIOD_LSB_PTR        ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__32BIT_D0_REG )
            #define TimerH_COUNTER_LSB         (* (reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__32BIT_A0_REG )
            #define TimerH_COUNTER_LSB_PTR       ((reg32 *) TimerH_TimerUDB_sT16_timerdp_u0__32BIT_A0_REG )
        #endif /* CY_PSOC3 || CY_PSOC5 */ 
    #endif

    #define TimerH_COUNTER_LSB_PTR_8BIT       ((reg8 *) TimerH_TimerUDB_sT16_timerdp_u0__A0_REG )
    
    #if (TimerH_UsingHWCaptureCounter)
        #define TimerH_CAP_COUNT              (*(reg8 *) TimerH_TimerUDB_sCapCount_counter__PERIOD_REG )
        #define TimerH_CAP_COUNT_PTR          ( (reg8 *) TimerH_TimerUDB_sCapCount_counter__PERIOD_REG )
        #define TimerH_CAPTURE_COUNT_CTRL     (*(reg8 *) TimerH_TimerUDB_sCapCount_counter__CONTROL_AUX_CTL_REG )
        #define TimerH_CAPTURE_COUNT_CTRL_PTR ( (reg8 *) TimerH_TimerUDB_sCapCount_counter__CONTROL_AUX_CTL_REG )
    #endif /* (TimerH_UsingHWCaptureCounter) */


    /***************************************
    *       Register Constants
    ***************************************/

    /* Control Register Bit Locations */
    #define TimerH_CTRL_INTCNT_SHIFT              0x00u       /* As defined by Verilog Implementation */
    #define TimerH_CTRL_TRIG_MODE_SHIFT           0x02u       /* As defined by Verilog Implementation */
    #define TimerH_CTRL_TRIG_EN_SHIFT             0x04u       /* As defined by Verilog Implementation */
    #define TimerH_CTRL_CAP_MODE_SHIFT            0x05u       /* As defined by Verilog Implementation */
    #define TimerH_CTRL_ENABLE_SHIFT              0x07u       /* As defined by Verilog Implementation */

    /* Control Register Bit Masks */
    #define TimerH_CTRL_INTCNT_MASK               ((uint8)((uint8)0x03u << TimerH_CTRL_INTCNT_SHIFT))
    #define TimerH_CTRL_TRIG_MODE_MASK            ((uint8)((uint8)0x03u << TimerH_CTRL_TRIG_MODE_SHIFT))
    #define TimerH_CTRL_TRIG_EN                   ((uint8)((uint8)0x01u << TimerH_CTRL_TRIG_EN_SHIFT))
    #define TimerH_CTRL_CAP_MODE_MASK             ((uint8)((uint8)0x03u << TimerH_CTRL_CAP_MODE_SHIFT))
    #define TimerH_CTRL_ENABLE                    ((uint8)((uint8)0x01u << TimerH_CTRL_ENABLE_SHIFT))

    /* Bit Counter (7-bit) Control Register Bit Definitions */
    /* As defined by the Register map for the AUX Control Register */
    #define TimerH_CNTR_ENABLE                    0x20u

    /* Status Register Bit Locations */
    #define TimerH_STATUS_TC_SHIFT                0x00u  /* As defined by Verilog Implementation */
    #define TimerH_STATUS_CAPTURE_SHIFT           0x01u  /* As defined by Verilog Implementation */
    #define TimerH_STATUS_TC_INT_MASK_SHIFT       TimerH_STATUS_TC_SHIFT
    #define TimerH_STATUS_CAPTURE_INT_MASK_SHIFT  TimerH_STATUS_CAPTURE_SHIFT
    #define TimerH_STATUS_FIFOFULL_SHIFT          0x02u  /* As defined by Verilog Implementation */
    #define TimerH_STATUS_FIFONEMP_SHIFT          0x03u  /* As defined by Verilog Implementation */
    #define TimerH_STATUS_FIFOFULL_INT_MASK_SHIFT TimerH_STATUS_FIFOFULL_SHIFT

    /* Status Register Bit Masks */
    /* Sticky TC Event Bit-Mask */
    #define TimerH_STATUS_TC                      ((uint8)((uint8)0x01u << TimerH_STATUS_TC_SHIFT))
    /* Sticky Capture Event Bit-Mask */
    #define TimerH_STATUS_CAPTURE                 ((uint8)((uint8)0x01u << TimerH_STATUS_CAPTURE_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerH_STATUS_TC_INT_MASK             ((uint8)((uint8)0x01u << TimerH_STATUS_TC_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerH_STATUS_CAPTURE_INT_MASK        ((uint8)((uint8)0x01u << TimerH_STATUS_CAPTURE_SHIFT))
    /* NOT-Sticky FIFO Full Bit-Mask */
    #define TimerH_STATUS_FIFOFULL                ((uint8)((uint8)0x01u << TimerH_STATUS_FIFOFULL_SHIFT))
    /* NOT-Sticky FIFO Not Empty Bit-Mask */
    #define TimerH_STATUS_FIFONEMP                ((uint8)((uint8)0x01u << TimerH_STATUS_FIFONEMP_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerH_STATUS_FIFOFULL_INT_MASK       ((uint8)((uint8)0x01u << TimerH_STATUS_FIFOFULL_SHIFT))

    #define TimerH_STATUS_ACTL_INT_EN             0x10u   /* As defined for the ACTL Register */

    /* Datapath Auxillary Control Register definitions */
    #define TimerH_AUX_CTRL_FIFO0_CLR             0x01u   /* As defined by Register map */
    #define TimerH_AUX_CTRL_FIFO1_CLR             0x02u   /* As defined by Register map */
    #define TimerH_AUX_CTRL_FIFO0_LVL             0x04u   /* As defined by Register map */
    #define TimerH_AUX_CTRL_FIFO1_LVL             0x08u   /* As defined by Register map */
    #define TimerH_STATUS_ACTL_INT_EN_MASK        0x10u   /* As defined for the ACTL Register */

#endif /* Implementation Specific Registers and Register Constants */

#endif  /* CY_TIMER_TimerH_H */


/* [] END OF FILE */
