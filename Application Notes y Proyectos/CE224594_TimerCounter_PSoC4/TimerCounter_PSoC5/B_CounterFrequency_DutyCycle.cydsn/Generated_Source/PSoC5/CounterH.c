/*******************************************************************************
* File Name: CounterH.c  
* Version 3.0
*
*  Description:
*     The Counter component consists of a 8, 16, 24 or 32-bit counter with
*     a selectable period between 2 and 2^Width - 1.  
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "CounterH.h"

uint8 CounterH_initVar = 0u;


/*******************************************************************************
* Function Name: CounterH_Init
********************************************************************************
* Summary:
*     Initialize to the schematic state
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void CounterH_Init(void) 
{
        #if (!CounterH_UsingFixedFunction && !CounterH_ControlRegRemoved)
            uint8 ctrl;
        #endif /* (!CounterH_UsingFixedFunction && !CounterH_ControlRegRemoved) */
        
        #if(!CounterH_UsingFixedFunction) 
            /* Interrupt State Backup for Critical Region*/
            uint8 CounterH_interruptState;
        #endif /* (!CounterH_UsingFixedFunction) */
        
        #if (CounterH_UsingFixedFunction)
            /* Clear all bits but the enable bit (if it's already set for Timer operation */
            CounterH_CONTROL &= CounterH_CTRL_ENABLE;
            
            /* Clear the mode bits for continuous run mode */
            #if (CY_PSOC5A)
                CounterH_CONTROL2 &= ((uint8)(~CounterH_CTRL_MODE_MASK));
            #endif /* (CY_PSOC5A) */
            #if (CY_PSOC3 || CY_PSOC5LP)
                CounterH_CONTROL3 &= ((uint8)(~CounterH_CTRL_MODE_MASK));                
            #endif /* (CY_PSOC3 || CY_PSOC5LP) */
            /* Check if One Shot mode is enabled i.e. RunMode !=0*/
            #if (CounterH_RunModeUsed != 0x0u)
                /* Set 3rd bit of Control register to enable one shot mode */
                CounterH_CONTROL |= CounterH_ONESHOT;
            #endif /* (CounterH_RunModeUsed != 0x0u) */
            
            /* Set the IRQ to use the status register interrupts */
            CounterH_CONTROL2 |= CounterH_CTRL2_IRQ_SEL;
            
            /* Clear and Set SYNCTC and SYNCCMP bits of RT1 register */
            CounterH_RT1 &= ((uint8)(~CounterH_RT1_MASK));
            CounterH_RT1 |= CounterH_SYNC;     
                    
            /*Enable DSI Sync all all inputs of the Timer*/
            CounterH_RT1 &= ((uint8)(~CounterH_SYNCDSI_MASK));
            CounterH_RT1 |= CounterH_SYNCDSI_EN;

        #else
            #if(!CounterH_ControlRegRemoved)
            /* Set the default compare mode defined in the parameter */
            ctrl = CounterH_CONTROL & ((uint8)(~CounterH_CTRL_CMPMODE_MASK));
            CounterH_CONTROL = ctrl | CounterH_DEFAULT_COMPARE_MODE;
            
            /* Set the default capture mode defined in the parameter */
            ctrl = CounterH_CONTROL & ((uint8)(~CounterH_CTRL_CAPMODE_MASK));
            
            #if( 0 != CounterH_CAPTURE_MODE_CONF)
                CounterH_CONTROL = ctrl | CounterH_DEFAULT_CAPTURE_MODE;
            #else
                CounterH_CONTROL = ctrl;
            #endif /* 0 != CounterH_CAPTURE_MODE */ 
            
            #endif /* (!CounterH_ControlRegRemoved) */
        #endif /* (CounterH_UsingFixedFunction) */
        
        /* Clear all data in the FIFO's */
        #if (!CounterH_UsingFixedFunction)
            CounterH_ClearFIFO();
        #endif /* (!CounterH_UsingFixedFunction) */
        
        /* Set Initial values from Configuration */
        CounterH_WritePeriod(CounterH_INIT_PERIOD_VALUE);
        #if (!(CounterH_UsingFixedFunction && (CY_PSOC5A)))
            CounterH_WriteCounter(CounterH_INIT_COUNTER_VALUE);
        #endif /* (!(CounterH_UsingFixedFunction && (CY_PSOC5A))) */
        CounterH_SetInterruptMode(CounterH_INIT_INTERRUPTS_MASK);
        
        #if (!CounterH_UsingFixedFunction)
            /* Read the status register to clear the unwanted interrupts */
            (void)CounterH_ReadStatusRegister();
            /* Set the compare value (only available to non-fixed function implementation */
            CounterH_WriteCompare(CounterH_INIT_COMPARE_VALUE);
            /* Use the interrupt output of the status register for IRQ output */
            
            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            CounterH_interruptState = CyEnterCriticalSection();
            
            CounterH_STATUS_AUX_CTRL |= CounterH_STATUS_ACTL_INT_EN_MASK;
            
            /* Exit Critical Region*/
            CyExitCriticalSection(CounterH_interruptState);
            
        #endif /* (!CounterH_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: CounterH_Enable
********************************************************************************
* Summary:
*     Enable the Counter
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: 
*   If the Enable mode is set to Hardware only then this function has no effect 
*   on the operation of the counter.
*
*******************************************************************************/
void CounterH_Enable(void) 
{
    /* Globally Enable the Fixed Function Block chosen */
    #if (CounterH_UsingFixedFunction)
        CounterH_GLOBAL_ENABLE |= CounterH_BLOCK_EN_MASK;
        CounterH_GLOBAL_STBY_ENABLE |= CounterH_BLOCK_STBY_EN_MASK;
    #endif /* (CounterH_UsingFixedFunction) */  
        
    /* Enable the counter from the control register  */
    /* If Fixed Function then make sure Mode is set correctly */
    /* else make sure reset is clear */
    #if(!CounterH_ControlRegRemoved || CounterH_UsingFixedFunction)
        CounterH_CONTROL |= CounterH_CTRL_ENABLE;                
    #endif /* (!CounterH_ControlRegRemoved || CounterH_UsingFixedFunction) */
    
}


/*******************************************************************************
* Function Name: CounterH_Start
********************************************************************************
* Summary:
*  Enables the counter for operation 
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Global variables:
*  CounterH_initVar: Is modified when this function is called for the  
*   first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void CounterH_Start(void) 
{
    if(CounterH_initVar == 0u)
    {
        CounterH_Init();
        
        CounterH_initVar = 1u; /* Clear this bit for Initialization */        
    }
    
    /* Enable the Counter */
    CounterH_Enable();        
}


/*******************************************************************************
* Function Name: CounterH_Stop
********************************************************************************
* Summary:
* Halts the counter, but does not change any modes or disable interrupts.
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: If the Enable mode is set to Hardware only then this function
*               has no effect on the operation of the counter.
*
*******************************************************************************/
void CounterH_Stop(void) 
{
    /* Disable Counter */
    #if(!CounterH_ControlRegRemoved || CounterH_UsingFixedFunction)
        CounterH_CONTROL &= ((uint8)(~CounterH_CTRL_ENABLE));        
    #endif /* (!CounterH_ControlRegRemoved || CounterH_UsingFixedFunction) */
    
    /* Globally disable the Fixed Function Block chosen */
    #if (CounterH_UsingFixedFunction)
        CounterH_GLOBAL_ENABLE &= ((uint8)(~CounterH_BLOCK_EN_MASK));
        CounterH_GLOBAL_STBY_ENABLE &= ((uint8)(~CounterH_BLOCK_STBY_EN_MASK));
    #endif /* (CounterH_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: CounterH_SetInterruptMode
********************************************************************************
* Summary:
* Configures which interrupt sources are enabled to generate the final interrupt
*
* Parameters:  
*  InterruptsMask: This parameter is an or'd collection of the status bits
*                   which will be allowed to generate the counters interrupt.   
*
* Return: 
*  void
*
*******************************************************************************/
void CounterH_SetInterruptMode(uint8 interruptsMask) 
{
    CounterH_STATUS_MASK = interruptsMask;
}


/*******************************************************************************
* Function Name: CounterH_ReadStatusRegister
********************************************************************************
* Summary:
*   Reads the status register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the status register
*
* Side Effects:
*   Status register bits may be clear on read. 
*
*******************************************************************************/
uint8   CounterH_ReadStatusRegister(void) 
{
    return CounterH_STATUS;
}


#if(!CounterH_ControlRegRemoved)
/*******************************************************************************
* Function Name: CounterH_ReadControlRegister
********************************************************************************
* Summary:
*   Reads the control register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
uint8   CounterH_ReadControlRegister(void) 
{
    return CounterH_CONTROL;
}


/*******************************************************************************
* Function Name: CounterH_WriteControlRegister
********************************************************************************
* Summary:
*   Sets the bit-field of the control register.  This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
void    CounterH_WriteControlRegister(uint8 control) 
{
    CounterH_CONTROL = control;
}

#endif  /* (!CounterH_ControlRegRemoved) */


#if (!(CounterH_UsingFixedFunction && (CY_PSOC5A)))
/*******************************************************************************
* Function Name: CounterH_WriteCounter
********************************************************************************
* Summary:
*   This funtion is used to set the counter to a specific value
*
* Parameters:  
*  counter:  New counter value. 
*
* Return: 
*  void 
*
*******************************************************************************/
void CounterH_WriteCounter(uint16 counter) \
                                   
{
    #if(CounterH_UsingFixedFunction)
        /* assert if block is already enabled */
        CYASSERT (0u == (CounterH_GLOBAL_ENABLE & CounterH_BLOCK_EN_MASK));
        /* If block is disabled, enable it and then write the counter */
        CounterH_GLOBAL_ENABLE |= CounterH_BLOCK_EN_MASK;
        CY_SET_REG16(CounterH_COUNTER_LSB_PTR, (uint16)counter);
        CounterH_GLOBAL_ENABLE &= ((uint8)(~CounterH_BLOCK_EN_MASK));
    #else
        CY_SET_REG16(CounterH_COUNTER_LSB_PTR, counter);
    #endif /* (CounterH_UsingFixedFunction) */
}
#endif /* (!(CounterH_UsingFixedFunction && (CY_PSOC5A))) */


/*******************************************************************************
* Function Name: CounterH_ReadCounter
********************************************************************************
* Summary:
* Returns the current value of the counter.  It doesn't matter
* if the counter is enabled or running.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) The present value of the counter.
*
*******************************************************************************/
uint16 CounterH_ReadCounter(void) 
{
    /* Force capture by reading Accumulator */
    /* Must first do a software capture to be able to read the counter */
    /* It is up to the user code to make sure there isn't already captured data in the FIFO */
    #if(CounterH_UsingFixedFunction)
		(void)CY_GET_REG16(CounterH_COUNTER_LSB_PTR);
	#else
		(void)CY_GET_REG8(CounterH_COUNTER_LSB_PTR_8BIT);
	#endif/* (CounterH_UsingFixedFunction) */
    
    /* Read the data from the FIFO (or capture register for Fixed Function)*/
    #if(CounterH_UsingFixedFunction)
        return ((uint16)CY_GET_REG16(CounterH_STATICCOUNT_LSB_PTR));
    #else
        return (CY_GET_REG16(CounterH_STATICCOUNT_LSB_PTR));
    #endif /* (CounterH_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: CounterH_ReadCapture
********************************************************************************
* Summary:
*   This function returns the last value captured.
*
* Parameters:  
*  void
*
* Return: 
*  (uint16) Present Capture value.
*
*******************************************************************************/
uint16 CounterH_ReadCapture(void) 
{
    #if(CounterH_UsingFixedFunction)
        return ((uint16)CY_GET_REG16(CounterH_STATICCOUNT_LSB_PTR));
    #else
        return (CY_GET_REG16(CounterH_STATICCOUNT_LSB_PTR));
    #endif /* (CounterH_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: CounterH_WritePeriod
********************************************************************************
* Summary:
* Changes the period of the counter.  The new period 
* will be loaded the next time terminal count is detected.
*
* Parameters:  
*  period: (uint16) A value of 0 will result in
*         the counter remaining at zero.  
*
* Return: 
*  void
*
*******************************************************************************/
void CounterH_WritePeriod(uint16 period) 
{
    #if(CounterH_UsingFixedFunction)
        CY_SET_REG16(CounterH_PERIOD_LSB_PTR,(uint16)period);
    #else
        CY_SET_REG16(CounterH_PERIOD_LSB_PTR, period);
    #endif /* (CounterH_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: CounterH_ReadPeriod
********************************************************************************
* Summary:
* Reads the current period value without affecting counter operation.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) Present period value.
*
*******************************************************************************/
uint16 CounterH_ReadPeriod(void) 
{
    #if(CounterH_UsingFixedFunction)
        return ((uint16)CY_GET_REG16(CounterH_PERIOD_LSB_PTR));
    #else
        return (CY_GET_REG16(CounterH_PERIOD_LSB_PTR));
    #endif /* (CounterH_UsingFixedFunction) */
}


#if (!CounterH_UsingFixedFunction)
/*******************************************************************************
* Function Name: CounterH_WriteCompare
********************************************************************************
* Summary:
* Changes the compare value.  The compare output will 
* reflect the new value on the next UDB clock.  The compare output will be 
* driven high when the present counter value compares true based on the 
* configured compare mode setting. 
*
* Parameters:  
*  Compare:  New compare value. 
*
* Return: 
*  void
*
*******************************************************************************/
void CounterH_WriteCompare(uint16 compare) \
                                   
{
    #if(CounterH_UsingFixedFunction)
        CY_SET_REG16(CounterH_COMPARE_LSB_PTR, (uint16)compare);
    #else
        CY_SET_REG16(CounterH_COMPARE_LSB_PTR, compare);
    #endif /* (CounterH_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: CounterH_ReadCompare
********************************************************************************
* Summary:
* Returns the compare value.
*
* Parameters:  
*  void:
*
* Return: 
*  (uint16) Present compare value.
*
*******************************************************************************/
uint16 CounterH_ReadCompare(void) 
{
    return (CY_GET_REG16(CounterH_COMPARE_LSB_PTR));
}


#if (CounterH_COMPARE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: CounterH_SetCompareMode
********************************************************************************
* Summary:
*  Sets the software controlled Compare Mode.
*
* Parameters:
*  compareMode:  Compare Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void CounterH_SetCompareMode(uint8 compareMode) 
{
    /* Clear the compare mode bits in the control register */
    CounterH_CONTROL &= ((uint8)(~CounterH_CTRL_CMPMODE_MASK));
    
    /* Write the new setting */
    CounterH_CONTROL |= compareMode;
}
#endif  /* (CounterH_COMPARE_MODE_SOFTWARE) */


#if (CounterH_CAPTURE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: CounterH_SetCaptureMode
********************************************************************************
* Summary:
*  Sets the software controlled Capture Mode.
*
* Parameters:
*  captureMode:  Capture Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void CounterH_SetCaptureMode(uint8 captureMode) 
{
    /* Clear the capture mode bits in the control register */
    CounterH_CONTROL &= ((uint8)(~CounterH_CTRL_CAPMODE_MASK));
    
    /* Write the new setting */
    CounterH_CONTROL |= ((uint8)((uint8)captureMode << CounterH_CTRL_CAPMODE0_SHIFT));
}
#endif  /* (CounterH_CAPTURE_MODE_SOFTWARE) */


/*******************************************************************************
* Function Name: CounterH_ClearFIFO
********************************************************************************
* Summary:
*   This function clears all capture data from the capture FIFO
*
* Parameters:  
*  void:
*
* Return: 
*  None
*
*******************************************************************************/
void CounterH_ClearFIFO(void) 
{

    while(0u != (CounterH_ReadStatusRegister() & CounterH_STATUS_FIFONEMP))
    {
        (void)CounterH_ReadCapture();
    }

}
#endif  /* (!CounterH_UsingFixedFunction) */


/* [] END OF FILE */

