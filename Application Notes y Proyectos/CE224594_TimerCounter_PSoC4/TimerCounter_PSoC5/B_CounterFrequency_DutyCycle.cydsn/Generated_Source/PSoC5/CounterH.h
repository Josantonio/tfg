/*******************************************************************************
* File Name: CounterH.h  
* Version 3.0
*
*  Description:
*   Contains the function prototypes and constants available to the counter
*   user module.
*
*   Note:
*    None
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
    
#if !defined(CY_COUNTER_CounterH_H)
#define CY_COUNTER_CounterH_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 CounterH_initVar;

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component Counter_v3_0 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */

/* Error message for removed CounterH_CounterUDB_sCTRLReg_ctrlreg through optimization */
#ifdef CounterH_CounterUDB_sCTRLReg_ctrlreg__REMOVED
    #error Counter_v3_0 detected with a constant 0 for the enable, a \
                                constant 0 for the count or constant 1 for \
                                the reset. This will prevent the component from\
                                operating.
#endif /* CounterH_CounterUDB_sCTRLReg_ctrlreg__REMOVED */


/**************************************
*           Parameter Defaults        
**************************************/

#define CounterH_Resolution            16u
#define CounterH_UsingFixedFunction    0u
#define CounterH_ControlRegRemoved     1u
#define CounterH_COMPARE_MODE_SOFTWARE 0u
#define CounterH_CAPTURE_MODE_SOFTWARE 0u
#define CounterH_RunModeUsed           0u


/***************************************
*       Type defines
***************************************/


/**************************************************************************
 * Sleep Mode API Support
 * Backup structure for Sleep Wake up operations
 *************************************************************************/
typedef struct
{
    uint8 CounterEnableState; 
    uint16 CounterUdb;         /* Current Counter Value */

    #if (!CounterH_ControlRegRemoved)
        uint8 CounterControlRegister;               /* Counter Control Register */
    #endif /* (!CounterH_ControlRegRemoved) */

}CounterH_backupStruct;


/**************************************
 *  Function Prototypes
 *************************************/
void    CounterH_Start(void) ;
void    CounterH_Stop(void) ;
void    CounterH_SetInterruptMode(uint8 interruptsMask) ;
uint8   CounterH_ReadStatusRegister(void) ;
#define CounterH_GetInterruptSource() CounterH_ReadStatusRegister()
#if(!CounterH_ControlRegRemoved)
    uint8   CounterH_ReadControlRegister(void) ;
    void    CounterH_WriteControlRegister(uint8 control) \
        ;
#endif /* (!CounterH_ControlRegRemoved) */
#if (!(CounterH_UsingFixedFunction && (CY_PSOC5A)))
    void    CounterH_WriteCounter(uint16 counter) \
            ; 
#endif /* (!(CounterH_UsingFixedFunction && (CY_PSOC5A))) */
uint16  CounterH_ReadCounter(void) ;
uint16  CounterH_ReadCapture(void) ;
void    CounterH_WritePeriod(uint16 period) \
    ;
uint16  CounterH_ReadPeriod( void ) ;
#if (!CounterH_UsingFixedFunction)
    void    CounterH_WriteCompare(uint16 compare) \
        ;
    uint16  CounterH_ReadCompare( void ) \
        ;
#endif /* (!CounterH_UsingFixedFunction) */

#if (CounterH_COMPARE_MODE_SOFTWARE)
    void    CounterH_SetCompareMode(uint8 compareMode) ;
#endif /* (CounterH_COMPARE_MODE_SOFTWARE) */
#if (CounterH_CAPTURE_MODE_SOFTWARE)
    void    CounterH_SetCaptureMode(uint8 captureMode) ;
#endif /* (CounterH_CAPTURE_MODE_SOFTWARE) */
void CounterH_ClearFIFO(void)     ;
void CounterH_Init(void)          ;
void CounterH_Enable(void)        ;
void CounterH_SaveConfig(void)    ;
void CounterH_RestoreConfig(void) ;
void CounterH_Sleep(void)         ;
void CounterH_Wakeup(void)        ;


/***************************************
*   Enumerated Types and Parameters
***************************************/

/* Enumerated Type B_Counter__CompareModes, Used in Compare Mode retained for backward compatibility of tests*/
#define CounterH__B_COUNTER__LESS_THAN 1
#define CounterH__B_COUNTER__LESS_THAN_OR_EQUAL 2
#define CounterH__B_COUNTER__EQUAL 0
#define CounterH__B_COUNTER__GREATER_THAN 3
#define CounterH__B_COUNTER__GREATER_THAN_OR_EQUAL 4
#define CounterH__B_COUNTER__SOFTWARE 5

/* Enumerated Type Counter_CompareModes */
#define CounterH_CMP_MODE_LT 1u
#define CounterH_CMP_MODE_LTE 2u
#define CounterH_CMP_MODE_EQ 0u
#define CounterH_CMP_MODE_GT 3u
#define CounterH_CMP_MODE_GTE 4u
#define CounterH_CMP_MODE_SOFTWARE_CONTROLLED 5u

/* Enumerated Type B_Counter__CaptureModes, Used in Capture Mode retained for backward compatibility of tests*/
#define CounterH__B_COUNTER__NONE 0
#define CounterH__B_COUNTER__RISING_EDGE 1
#define CounterH__B_COUNTER__FALLING_EDGE 2
#define CounterH__B_COUNTER__EITHER_EDGE 3
#define CounterH__B_COUNTER__SOFTWARE_CONTROL 4

/* Enumerated Type Counter_CompareModes */
#define CounterH_CAP_MODE_NONE 0u
#define CounterH_CAP_MODE_RISE 1u
#define CounterH_CAP_MODE_FALL 2u
#define CounterH_CAP_MODE_BOTH 3u
#define CounterH_CAP_MODE_SOFTWARE_CONTROLLED 4u


/***************************************
 *  Initialization Values
 **************************************/
#define CounterH_CAPTURE_MODE_CONF       2u
#define CounterH_INIT_PERIOD_VALUE       65534u
#define CounterH_INIT_COUNTER_VALUE      0u
#if (CounterH_UsingFixedFunction)
#define CounterH_INIT_INTERRUPTS_MASK    ((uint8)((uint8)0u << CounterH_STATUS_ZERO_INT_EN_MASK_SHIFT))
#else
#define CounterH_INIT_COMPARE_VALUE      128u
#define CounterH_INIT_INTERRUPTS_MASK ((uint8)((uint8)0u << CounterH_STATUS_ZERO_INT_EN_MASK_SHIFT) | \
        ((uint8)((uint8)0u << CounterH_STATUS_CAPTURE_INT_EN_MASK_SHIFT)) | \
        ((uint8)((uint8)0u << CounterH_STATUS_CMP_INT_EN_MASK_SHIFT)) | \
        ((uint8)((uint8)0u << CounterH_STATUS_OVERFLOW_INT_EN_MASK_SHIFT)) | \
        ((uint8)((uint8)0u << CounterH_STATUS_UNDERFLOW_INT_EN_MASK_SHIFT)))
#define CounterH_DEFAULT_COMPARE_MODE    1u

#if( 0 != CounterH_CAPTURE_MODE_CONF)
    #define CounterH_DEFAULT_CAPTURE_MODE    ((uint8)((uint8)2u << CounterH_CTRL_CAPMODE0_SHIFT))
#else    
    #define CounterH_DEFAULT_CAPTURE_MODE    (2u )
#endif /* ( 0 != CounterH_CAPTURE_MODE_CONF) */

#endif /* (CounterH_UsingFixedFunction) */


/**************************************
 *  Registers
 *************************************/
#if (CounterH_UsingFixedFunction)
    #define CounterH_STATICCOUNT_LSB     (*(reg16 *) CounterH_CounterHW__CAP0 )
    #define CounterH_STATICCOUNT_LSB_PTR ( (reg16 *) CounterH_CounterHW__CAP0 )
    #define CounterH_PERIOD_LSB          (*(reg16 *) CounterH_CounterHW__PER0 )
    #define CounterH_PERIOD_LSB_PTR      ( (reg16 *) CounterH_CounterHW__PER0 )
    /* MODE must be set to 1 to set the compare value */
    #define CounterH_COMPARE_LSB         (*(reg16 *) CounterH_CounterHW__CNT_CMP0 )
    #define CounterH_COMPARE_LSB_PTR     ( (reg16 *) CounterH_CounterHW__CNT_CMP0 )
    /* MODE must be set to 0 to get the count */
    #define CounterH_COUNTER_LSB         (*(reg16 *) CounterH_CounterHW__CNT_CMP0 )
    #define CounterH_COUNTER_LSB_PTR     ( (reg16 *) CounterH_CounterHW__CNT_CMP0 )
    #define CounterH_RT1                 (*(reg8 *) CounterH_CounterHW__RT1)
    #define CounterH_RT1_PTR             ( (reg8 *) CounterH_CounterHW__RT1)
#else
    
    #if (CounterH_Resolution <= 8u) /* 8-bit Counter */
    
        #define CounterH_STATICCOUNT_LSB     (*(reg8 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__F0_REG )
        #define CounterH_STATICCOUNT_LSB_PTR ( (reg8 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__F0_REG )
        #define CounterH_PERIOD_LSB          (*(reg8 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__D0_REG )
        #define CounterH_PERIOD_LSB_PTR      ( (reg8 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__D0_REG )
        #define CounterH_COMPARE_LSB         (*(reg8 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__D1_REG )
        #define CounterH_COMPARE_LSB_PTR     ( (reg8 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__D1_REG )
        #define CounterH_COUNTER_LSB         (*(reg8 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )  
        #define CounterH_COUNTER_LSB_PTR     ( (reg8 *)\
            CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )
    
    #elif(CounterH_Resolution <= 16u) /* 16-bit Counter */
        #if(CY_PSOC3) /* 8-bit address space */ 
            #define CounterH_STATICCOUNT_LSB     (*(reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__F0_REG )
            #define CounterH_STATICCOUNT_LSB_PTR ( (reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__F0_REG )
            #define CounterH_PERIOD_LSB          (*(reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__D0_REG )
            #define CounterH_PERIOD_LSB_PTR      ( (reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__D0_REG )
            #define CounterH_COMPARE_LSB         (*(reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__D1_REG )
            #define CounterH_COMPARE_LSB_PTR     ( (reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__D1_REG )
            #define CounterH_COUNTER_LSB         (*(reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )  
            #define CounterH_COUNTER_LSB_PTR     ( (reg16 *)\
                CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )
        #else /* 16-bit address space */
            #define CounterH_STATICCOUNT_LSB     (*(reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__16BIT_F0_REG )
            #define CounterH_STATICCOUNT_LSB_PTR ( (reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__16BIT_F0_REG )
            #define CounterH_PERIOD_LSB          (*(reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__16BIT_D0_REG )
            #define CounterH_PERIOD_LSB_PTR      ( (reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__16BIT_D0_REG )
            #define CounterH_COMPARE_LSB         (*(reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__16BIT_D1_REG )
            #define CounterH_COMPARE_LSB_PTR     ( (reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__16BIT_D1_REG )
            #define CounterH_COUNTER_LSB         (*(reg16 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__16BIT_A0_REG )  
            #define CounterH_COUNTER_LSB_PTR     ( (reg16 *)\
                CounterH_CounterUDB_sC16_counterdp_u0__16BIT_A0_REG )
        #endif /* CY_PSOC3 */   
    #elif(CounterH_Resolution <= 24u) /* 24-bit Counter */
        
        #define CounterH_STATICCOUNT_LSB     (*(reg32 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__F0_REG )
        #define CounterH_STATICCOUNT_LSB_PTR ( (reg32 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__F0_REG )
        #define CounterH_PERIOD_LSB          (*(reg32 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__D0_REG )
        #define CounterH_PERIOD_LSB_PTR      ( (reg32 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__D0_REG )
        #define CounterH_COMPARE_LSB         (*(reg32 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__D1_REG )
        #define CounterH_COMPARE_LSB_PTR     ( (reg32 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__D1_REG )
        #define CounterH_COUNTER_LSB         (*(reg32 *) \
            CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )  
        #define CounterH_COUNTER_LSB_PTR     ( (reg32 *)\
            CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )
    
    #else /* 32-bit Counter */
        #if(CY_PSOC3 || CY_PSOC5) /* 8-bit address space */
            #define CounterH_STATICCOUNT_LSB     (*(reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__F0_REG )
            #define CounterH_STATICCOUNT_LSB_PTR ( (reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__F0_REG )
            #define CounterH_PERIOD_LSB          (*(reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__D0_REG )
            #define CounterH_PERIOD_LSB_PTR      ( (reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__D0_REG )
            #define CounterH_COMPARE_LSB         (*(reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__D1_REG )
            #define CounterH_COMPARE_LSB_PTR     ( (reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__D1_REG )
            #define CounterH_COUNTER_LSB         (*(reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )  
            #define CounterH_COUNTER_LSB_PTR     ( (reg32 *)\
                CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )
        #else /* 32-bit address space */
            #define CounterH_STATICCOUNT_LSB     (*(reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__32BIT_F0_REG )
            #define CounterH_STATICCOUNT_LSB_PTR ( (reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__32BIT_F0_REG )
            #define CounterH_PERIOD_LSB          (*(reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__32BIT_D0_REG )
            #define CounterH_PERIOD_LSB_PTR      ( (reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__32BIT_D0_REG )
            #define CounterH_COMPARE_LSB         (*(reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__32BIT_D1_REG )
            #define CounterH_COMPARE_LSB_PTR     ( (reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__32BIT_D1_REG )
            #define CounterH_COUNTER_LSB         (*(reg32 *) \
                CounterH_CounterUDB_sC16_counterdp_u0__32BIT_A0_REG )  
            #define CounterH_COUNTER_LSB_PTR     ( (reg32 *)\
                CounterH_CounterUDB_sC16_counterdp_u0__32BIT_A0_REG )
        #endif /* CY_PSOC3 || CY_PSOC5 */   
    #endif

	#define CounterH_COUNTER_LSB_PTR_8BIT     ( (reg8 *)\
                CounterH_CounterUDB_sC16_counterdp_u0__A0_REG )
				
    #define CounterH_AUX_CONTROLDP0 \
        (*(reg8 *) CounterH_CounterUDB_sC16_counterdp_u0__DP_AUX_CTL_REG)
    
    #define CounterH_AUX_CONTROLDP0_PTR \
        ( (reg8 *) CounterH_CounterUDB_sC16_counterdp_u0__DP_AUX_CTL_REG)
    
    #if (CounterH_Resolution == 16 || CounterH_Resolution == 24 || CounterH_Resolution == 32)
       #define CounterH_AUX_CONTROLDP1 \
           (*(reg8 *) CounterH_CounterUDB_sC16_counterdp_u1__DP_AUX_CTL_REG)
       #define CounterH_AUX_CONTROLDP1_PTR \
           ( (reg8 *) CounterH_CounterUDB_sC16_counterdp_u1__DP_AUX_CTL_REG)
    #endif /* (CounterH_Resolution == 16 || CounterH_Resolution == 24 || CounterH_Resolution == 32) */
    
    #if (CounterH_Resolution == 24 || CounterH_Resolution == 32)
       #define CounterH_AUX_CONTROLDP2 \
           (*(reg8 *) CounterH_CounterUDB_sC16_counterdp_u2__DP_AUX_CTL_REG)
       #define CounterH_AUX_CONTROLDP2_PTR \
           ( (reg8 *) CounterH_CounterUDB_sC16_counterdp_u2__DP_AUX_CTL_REG)
    #endif /* (CounterH_Resolution == 24 || CounterH_Resolution == 32) */
    
    #if (CounterH_Resolution == 32)
       #define CounterH_AUX_CONTROLDP3 \
           (*(reg8 *) CounterH_CounterUDB_sC16_counterdp_u3__DP_AUX_CTL_REG)
       #define CounterH_AUX_CONTROLDP3_PTR \
           ( (reg8 *) CounterH_CounterUDB_sC16_counterdp_u3__DP_AUX_CTL_REG)
    #endif /* (CounterH_Resolution == 32) */

#endif  /* (CounterH_UsingFixedFunction) */

#if (CounterH_UsingFixedFunction)
    #define CounterH_STATUS         (*(reg8 *) CounterH_CounterHW__SR0 )
    /* In Fixed Function Block Status and Mask are the same register */
    #define CounterH_STATUS_MASK             (*(reg8 *) CounterH_CounterHW__SR0 )
    #define CounterH_STATUS_MASK_PTR         ( (reg8 *) CounterH_CounterHW__SR0 )
    #define CounterH_CONTROL                 (*(reg8 *) CounterH_CounterHW__CFG0)
    #define CounterH_CONTROL_PTR             ( (reg8 *) CounterH_CounterHW__CFG0)
    #define CounterH_CONTROL2                (*(reg8 *) CounterH_CounterHW__CFG1)
    #define CounterH_CONTROL2_PTR            ( (reg8 *) CounterH_CounterHW__CFG1)
    #if (CY_PSOC3 || CY_PSOC5LP)
        #define CounterH_CONTROL3       (*(reg8 *) CounterH_CounterHW__CFG2)
        #define CounterH_CONTROL3_PTR   ( (reg8 *) CounterH_CounterHW__CFG2)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    #define CounterH_GLOBAL_ENABLE           (*(reg8 *) CounterH_CounterHW__PM_ACT_CFG)
    #define CounterH_GLOBAL_ENABLE_PTR       ( (reg8 *) CounterH_CounterHW__PM_ACT_CFG)
    #define CounterH_GLOBAL_STBY_ENABLE      (*(reg8 *) CounterH_CounterHW__PM_STBY_CFG)
    #define CounterH_GLOBAL_STBY_ENABLE_PTR  ( (reg8 *) CounterH_CounterHW__PM_STBY_CFG)
    

    /********************************
    *    Constants
    ********************************/

    /* Fixed Function Block Chosen */
    #define CounterH_BLOCK_EN_MASK          CounterH_CounterHW__PM_ACT_MSK
    #define CounterH_BLOCK_STBY_EN_MASK     CounterH_CounterHW__PM_STBY_MSK 
    
    /* Control Register Bit Locations */    
    /* As defined in Register Map, part of TMRX_CFG0 register */
    #define CounterH_CTRL_ENABLE_SHIFT      0x00u
    #define CounterH_ONESHOT_SHIFT          0x02u
    /* Control Register Bit Masks */
    #define CounterH_CTRL_ENABLE            ((uint8)((uint8)0x01u << CounterH_CTRL_ENABLE_SHIFT))         
    #define CounterH_ONESHOT                ((uint8)((uint8)0x01u << CounterH_ONESHOT_SHIFT))

    /* Control2 Register Bit Masks */
    /* Set the mask for run mode */
    #if (CY_PSOC5A)
        /* Use CFG1 Mode bits to set run mode */
        #define CounterH_CTRL_MODE_SHIFT        0x01u    
        #define CounterH_CTRL_MODE_MASK         ((uint8)((uint8)0x07u << CounterH_CTRL_MODE_SHIFT))
    #endif /* (CY_PSOC5A) */
    #if (CY_PSOC3 || CY_PSOC5LP)
        /* Use CFG2 Mode bits to set run mode */
        #define CounterH_CTRL_MODE_SHIFT        0x00u    
        #define CounterH_CTRL_MODE_MASK         ((uint8)((uint8)0x03u << CounterH_CTRL_MODE_SHIFT))
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    /* Set the mask for interrupt (raw/status register) */
    #define CounterH_CTRL2_IRQ_SEL_SHIFT     0x00u
    #define CounterH_CTRL2_IRQ_SEL          ((uint8)((uint8)0x01u << CounterH_CTRL2_IRQ_SEL_SHIFT))     
    
    /* Status Register Bit Locations */
    #define CounterH_STATUS_ZERO_SHIFT      0x07u  /* As defined in Register Map, part of TMRX_SR0 register */ 

    /* Status Register Interrupt Enable Bit Locations */
    #define CounterH_STATUS_ZERO_INT_EN_MASK_SHIFT      (CounterH_STATUS_ZERO_SHIFT - 0x04u)

    /* Status Register Bit Masks */                           
    #define CounterH_STATUS_ZERO            ((uint8)((uint8)0x01u << CounterH_STATUS_ZERO_SHIFT))

    /* Status Register Interrupt Bit Masks*/
    #define CounterH_STATUS_ZERO_INT_EN_MASK       ((uint8)((uint8)CounterH_STATUS_ZERO >> 0x04u))
    
    /*RT1 Synch Constants: Applicable for PSoC3 and PSoC5LP */
    #define CounterH_RT1_SHIFT            0x04u
    #define CounterH_RT1_MASK             ((uint8)((uint8)0x03u << CounterH_RT1_SHIFT))  /* Sync TC and CMP bit masks */
    #define CounterH_SYNC                 ((uint8)((uint8)0x03u << CounterH_RT1_SHIFT))
    #define CounterH_SYNCDSI_SHIFT        0x00u
    #define CounterH_SYNCDSI_MASK         ((uint8)((uint8)0x0Fu << CounterH_SYNCDSI_SHIFT)) /* Sync all DSI inputs */
    #define CounterH_SYNCDSI_EN           ((uint8)((uint8)0x0Fu << CounterH_SYNCDSI_SHIFT)) /* Sync all DSI inputs */
    
#else /* !CounterH_UsingFixedFunction */
    #define CounterH_STATUS               (* (reg8 *) CounterH_CounterUDB_sSTSReg_stsreg__STATUS_REG )
    #define CounterH_STATUS_PTR           (  (reg8 *) CounterH_CounterUDB_sSTSReg_stsreg__STATUS_REG )
    #define CounterH_STATUS_MASK          (* (reg8 *) CounterH_CounterUDB_sSTSReg_stsreg__MASK_REG )
    #define CounterH_STATUS_MASK_PTR      (  (reg8 *) CounterH_CounterUDB_sSTSReg_stsreg__MASK_REG )
    #define CounterH_STATUS_AUX_CTRL      (*(reg8 *) CounterH_CounterUDB_sSTSReg_stsreg__STATUS_AUX_CTL_REG)
    #define CounterH_STATUS_AUX_CTRL_PTR  ( (reg8 *) CounterH_CounterUDB_sSTSReg_stsreg__STATUS_AUX_CTL_REG)
    #define CounterH_CONTROL              (* (reg8 *) CounterH_CounterUDB_sCTRLReg_ctrlreg__CONTROL_REG )
    #define CounterH_CONTROL_PTR          (  (reg8 *) CounterH_CounterUDB_sCTRLReg_ctrlreg__CONTROL_REG )


    /********************************
    *    Constants
    ********************************/
    /* Control Register Bit Locations */
    #define CounterH_CTRL_CAPMODE0_SHIFT    0x03u       /* As defined by Verilog Implementation */
    #define CounterH_CTRL_RESET_SHIFT       0x06u       /* As defined by Verilog Implementation */
    #define CounterH_CTRL_ENABLE_SHIFT      0x07u       /* As defined by Verilog Implementation */
    /* Control Register Bit Masks */
    #define CounterH_CTRL_CMPMODE_MASK      0x07u 
    #define CounterH_CTRL_CAPMODE_MASK      0x03u  
    #define CounterH_CTRL_RESET             ((uint8)((uint8)0x01u << CounterH_CTRL_RESET_SHIFT))  
    #define CounterH_CTRL_ENABLE            ((uint8)((uint8)0x01u << CounterH_CTRL_ENABLE_SHIFT)) 

    /* Status Register Bit Locations */
    #define CounterH_STATUS_CMP_SHIFT       0x00u       /* As defined by Verilog Implementation */
    #define CounterH_STATUS_ZERO_SHIFT      0x01u       /* As defined by Verilog Implementation */
    #define CounterH_STATUS_OVERFLOW_SHIFT  0x02u       /* As defined by Verilog Implementation */
    #define CounterH_STATUS_UNDERFLOW_SHIFT 0x03u       /* As defined by Verilog Implementation */
    #define CounterH_STATUS_CAPTURE_SHIFT   0x04u       /* As defined by Verilog Implementation */
    #define CounterH_STATUS_FIFOFULL_SHIFT  0x05u       /* As defined by Verilog Implementation */
    #define CounterH_STATUS_FIFONEMP_SHIFT  0x06u       /* As defined by Verilog Implementation */
    /* Status Register Interrupt Enable Bit Locations - UDB Status Interrupt Mask match Status Bit Locations*/
    #define CounterH_STATUS_CMP_INT_EN_MASK_SHIFT       CounterH_STATUS_CMP_SHIFT       
    #define CounterH_STATUS_ZERO_INT_EN_MASK_SHIFT      CounterH_STATUS_ZERO_SHIFT      
    #define CounterH_STATUS_OVERFLOW_INT_EN_MASK_SHIFT  CounterH_STATUS_OVERFLOW_SHIFT  
    #define CounterH_STATUS_UNDERFLOW_INT_EN_MASK_SHIFT CounterH_STATUS_UNDERFLOW_SHIFT 
    #define CounterH_STATUS_CAPTURE_INT_EN_MASK_SHIFT   CounterH_STATUS_CAPTURE_SHIFT   
    #define CounterH_STATUS_FIFOFULL_INT_EN_MASK_SHIFT  CounterH_STATUS_FIFOFULL_SHIFT  
    #define CounterH_STATUS_FIFONEMP_INT_EN_MASK_SHIFT  CounterH_STATUS_FIFONEMP_SHIFT  
    /* Status Register Bit Masks */                
    #define CounterH_STATUS_CMP             ((uint8)((uint8)0x01u << CounterH_STATUS_CMP_SHIFT))  
    #define CounterH_STATUS_ZERO            ((uint8)((uint8)0x01u << CounterH_STATUS_ZERO_SHIFT)) 
    #define CounterH_STATUS_OVERFLOW        ((uint8)((uint8)0x01u << CounterH_STATUS_OVERFLOW_SHIFT)) 
    #define CounterH_STATUS_UNDERFLOW       ((uint8)((uint8)0x01u << CounterH_STATUS_UNDERFLOW_SHIFT)) 
    #define CounterH_STATUS_CAPTURE         ((uint8)((uint8)0x01u << CounterH_STATUS_CAPTURE_SHIFT)) 
    #define CounterH_STATUS_FIFOFULL        ((uint8)((uint8)0x01u << CounterH_STATUS_FIFOFULL_SHIFT))
    #define CounterH_STATUS_FIFONEMP        ((uint8)((uint8)0x01u << CounterH_STATUS_FIFONEMP_SHIFT))
    /* Status Register Interrupt Bit Masks  - UDB Status Interrupt Mask match Status Bit Locations */
    #define CounterH_STATUS_CMP_INT_EN_MASK            CounterH_STATUS_CMP                    
    #define CounterH_STATUS_ZERO_INT_EN_MASK           CounterH_STATUS_ZERO            
    #define CounterH_STATUS_OVERFLOW_INT_EN_MASK       CounterH_STATUS_OVERFLOW        
    #define CounterH_STATUS_UNDERFLOW_INT_EN_MASK      CounterH_STATUS_UNDERFLOW       
    #define CounterH_STATUS_CAPTURE_INT_EN_MASK        CounterH_STATUS_CAPTURE         
    #define CounterH_STATUS_FIFOFULL_INT_EN_MASK       CounterH_STATUS_FIFOFULL        
    #define CounterH_STATUS_FIFONEMP_INT_EN_MASK       CounterH_STATUS_FIFONEMP         
    

    /* StatusI Interrupt Enable bit Location in the Auxilliary Control Register */
    #define CounterH_STATUS_ACTL_INT_EN     0x10u /* As defined for the ACTL Register */
    
    /* Datapath Auxillary Control Register definitions */
    #define CounterH_AUX_CTRL_FIFO0_CLR         0x01u   /* As defined by Register map */
    #define CounterH_AUX_CTRL_FIFO1_CLR         0x02u   /* As defined by Register map */
    #define CounterH_AUX_CTRL_FIFO0_LVL         0x04u   /* As defined by Register map */
    #define CounterH_AUX_CTRL_FIFO1_LVL         0x08u   /* As defined by Register map */
    #define CounterH_STATUS_ACTL_INT_EN_MASK    0x10u   /* As defined for the ACTL Register */
    
#endif /* CounterH_UsingFixedFunction */

#endif  /* CY_COUNTER_CounterH_H */


/* [] END OF FILE */

