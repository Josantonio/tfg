/*******************************************************************************
* File Name: PotDuty.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PotDuty_H) /* Pins PotDuty_H */
#define CY_PINS_PotDuty_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PotDuty_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PotDuty__PORT == 15 && ((PotDuty__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PotDuty_Write(uint8 value);
void    PotDuty_SetDriveMode(uint8 mode);
uint8   PotDuty_ReadDataReg(void);
uint8   PotDuty_Read(void);
void    PotDuty_SetInterruptMode(uint16 position, uint16 mode);
uint8   PotDuty_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PotDuty_SetDriveMode() function.
     *  @{
     */
        #define PotDuty_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PotDuty_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PotDuty_DM_RES_UP          PIN_DM_RES_UP
        #define PotDuty_DM_RES_DWN         PIN_DM_RES_DWN
        #define PotDuty_DM_OD_LO           PIN_DM_OD_LO
        #define PotDuty_DM_OD_HI           PIN_DM_OD_HI
        #define PotDuty_DM_STRONG          PIN_DM_STRONG
        #define PotDuty_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PotDuty_MASK               PotDuty__MASK
#define PotDuty_SHIFT              PotDuty__SHIFT
#define PotDuty_WIDTH              1u

/* Interrupt constants */
#if defined(PotDuty__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PotDuty_SetInterruptMode() function.
     *  @{
     */
        #define PotDuty_INTR_NONE      (uint16)(0x0000u)
        #define PotDuty_INTR_RISING    (uint16)(0x0001u)
        #define PotDuty_INTR_FALLING   (uint16)(0x0002u)
        #define PotDuty_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PotDuty_INTR_MASK      (0x01u) 
#endif /* (PotDuty__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PotDuty_PS                     (* (reg8 *) PotDuty__PS)
/* Data Register */
#define PotDuty_DR                     (* (reg8 *) PotDuty__DR)
/* Port Number */
#define PotDuty_PRT_NUM                (* (reg8 *) PotDuty__PRT) 
/* Connect to Analog Globals */                                                  
#define PotDuty_AG                     (* (reg8 *) PotDuty__AG)                       
/* Analog MUX bux enable */
#define PotDuty_AMUX                   (* (reg8 *) PotDuty__AMUX) 
/* Bidirectional Enable */                                                        
#define PotDuty_BIE                    (* (reg8 *) PotDuty__BIE)
/* Bit-mask for Aliased Register Access */
#define PotDuty_BIT_MASK               (* (reg8 *) PotDuty__BIT_MASK)
/* Bypass Enable */
#define PotDuty_BYP                    (* (reg8 *) PotDuty__BYP)
/* Port wide control signals */                                                   
#define PotDuty_CTL                    (* (reg8 *) PotDuty__CTL)
/* Drive Modes */
#define PotDuty_DM0                    (* (reg8 *) PotDuty__DM0) 
#define PotDuty_DM1                    (* (reg8 *) PotDuty__DM1)
#define PotDuty_DM2                    (* (reg8 *) PotDuty__DM2) 
/* Input Buffer Disable Override */
#define PotDuty_INP_DIS                (* (reg8 *) PotDuty__INP_DIS)
/* LCD Common or Segment Drive */
#define PotDuty_LCD_COM_SEG            (* (reg8 *) PotDuty__LCD_COM_SEG)
/* Enable Segment LCD */
#define PotDuty_LCD_EN                 (* (reg8 *) PotDuty__LCD_EN)
/* Slew Rate Control */
#define PotDuty_SLW                    (* (reg8 *) PotDuty__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PotDuty_PRTDSI__CAPS_SEL       (* (reg8 *) PotDuty__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PotDuty_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PotDuty__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PotDuty_PRTDSI__OE_SEL0        (* (reg8 *) PotDuty__PRTDSI__OE_SEL0) 
#define PotDuty_PRTDSI__OE_SEL1        (* (reg8 *) PotDuty__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PotDuty_PRTDSI__OUT_SEL0       (* (reg8 *) PotDuty__PRTDSI__OUT_SEL0) 
#define PotDuty_PRTDSI__OUT_SEL1       (* (reg8 *) PotDuty__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PotDuty_PRTDSI__SYNC_OUT       (* (reg8 *) PotDuty__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PotDuty__SIO_CFG)
    #define PotDuty_SIO_HYST_EN        (* (reg8 *) PotDuty__SIO_HYST_EN)
    #define PotDuty_SIO_REG_HIFREQ     (* (reg8 *) PotDuty__SIO_REG_HIFREQ)
    #define PotDuty_SIO_CFG            (* (reg8 *) PotDuty__SIO_CFG)
    #define PotDuty_SIO_DIFF           (* (reg8 *) PotDuty__SIO_DIFF)
#endif /* (PotDuty__SIO_CFG) */

/* Interrupt Registers */
#if defined(PotDuty__INTSTAT)
    #define PotDuty_INTSTAT            (* (reg8 *) PotDuty__INTSTAT)
    #define PotDuty_SNAP               (* (reg8 *) PotDuty__SNAP)
    
	#define PotDuty_0_INTTYPE_REG 		(* (reg8 *) PotDuty__0__INTTYPE)
#endif /* (PotDuty__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PotDuty_H */


/* [] END OF FILE */
