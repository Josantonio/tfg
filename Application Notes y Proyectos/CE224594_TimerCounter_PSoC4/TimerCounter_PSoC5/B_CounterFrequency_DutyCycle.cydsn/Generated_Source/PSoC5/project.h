/*******************************************************************************
* File Name: project.h
* 
* PSoC Creator  4.2
*
* Description:
* It contains references to all generated header files and should not be modified.
* This file is automatically generated by PSoC Creator.
*
********************************************************************************
* Copyright (c) 2007-2018 Cypress Semiconductor.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cyfitter_cfg.h"
#include "cydevice.h"
#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cydisabledsheets.h"
#include "UART.h"
#include "UART_PVT.h"
#include "PinSignal_aliases.h"
#include "PinSignal.h"
#include "CounterL.h"
#include "PotDuty_aliases.h"
#include "PotDuty.h"
#include "ADC.h"
#include "PotFreq_aliases.h"
#include "PotFreq.h"
#include "PWM.h"
#include "Square_signal_aliases.h"
#include "Square_signal.h"
#include "CounterH.h"
#include "ADC_SAR.h"
#include "ADC_IntClock.h"
#include "ADC_TempBuf_dma.h"
#include "ADC_FinalBuf_dma.h"
#include "ADC_IRQ.h"
#include "cy_em_eeprom.h"
#include "core_cm3_psoc5.h"
#include "CyDmac.h"
#include "CyFlash.h"
#include "CyLib.h"
#include "cypins.h"
#include "cyPm.h"
#include "CySpc.h"
#include "cytypes.h"

/*[]*/

