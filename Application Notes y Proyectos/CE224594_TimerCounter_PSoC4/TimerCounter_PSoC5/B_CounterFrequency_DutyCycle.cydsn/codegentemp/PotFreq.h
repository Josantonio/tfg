/*******************************************************************************
* File Name: PotFreq.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PotFreq_H) /* Pins PotFreq_H */
#define CY_PINS_PotFreq_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PotFreq_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PotFreq__PORT == 15 && ((PotFreq__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PotFreq_Write(uint8 value);
void    PotFreq_SetDriveMode(uint8 mode);
uint8   PotFreq_ReadDataReg(void);
uint8   PotFreq_Read(void);
void    PotFreq_SetInterruptMode(uint16 position, uint16 mode);
uint8   PotFreq_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PotFreq_SetDriveMode() function.
     *  @{
     */
        #define PotFreq_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PotFreq_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PotFreq_DM_RES_UP          PIN_DM_RES_UP
        #define PotFreq_DM_RES_DWN         PIN_DM_RES_DWN
        #define PotFreq_DM_OD_LO           PIN_DM_OD_LO
        #define PotFreq_DM_OD_HI           PIN_DM_OD_HI
        #define PotFreq_DM_STRONG          PIN_DM_STRONG
        #define PotFreq_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PotFreq_MASK               PotFreq__MASK
#define PotFreq_SHIFT              PotFreq__SHIFT
#define PotFreq_WIDTH              1u

/* Interrupt constants */
#if defined(PotFreq__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PotFreq_SetInterruptMode() function.
     *  @{
     */
        #define PotFreq_INTR_NONE      (uint16)(0x0000u)
        #define PotFreq_INTR_RISING    (uint16)(0x0001u)
        #define PotFreq_INTR_FALLING   (uint16)(0x0002u)
        #define PotFreq_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PotFreq_INTR_MASK      (0x01u) 
#endif /* (PotFreq__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PotFreq_PS                     (* (reg8 *) PotFreq__PS)
/* Data Register */
#define PotFreq_DR                     (* (reg8 *) PotFreq__DR)
/* Port Number */
#define PotFreq_PRT_NUM                (* (reg8 *) PotFreq__PRT) 
/* Connect to Analog Globals */                                                  
#define PotFreq_AG                     (* (reg8 *) PotFreq__AG)                       
/* Analog MUX bux enable */
#define PotFreq_AMUX                   (* (reg8 *) PotFreq__AMUX) 
/* Bidirectional Enable */                                                        
#define PotFreq_BIE                    (* (reg8 *) PotFreq__BIE)
/* Bit-mask for Aliased Register Access */
#define PotFreq_BIT_MASK               (* (reg8 *) PotFreq__BIT_MASK)
/* Bypass Enable */
#define PotFreq_BYP                    (* (reg8 *) PotFreq__BYP)
/* Port wide control signals */                                                   
#define PotFreq_CTL                    (* (reg8 *) PotFreq__CTL)
/* Drive Modes */
#define PotFreq_DM0                    (* (reg8 *) PotFreq__DM0) 
#define PotFreq_DM1                    (* (reg8 *) PotFreq__DM1)
#define PotFreq_DM2                    (* (reg8 *) PotFreq__DM2) 
/* Input Buffer Disable Override */
#define PotFreq_INP_DIS                (* (reg8 *) PotFreq__INP_DIS)
/* LCD Common or Segment Drive */
#define PotFreq_LCD_COM_SEG            (* (reg8 *) PotFreq__LCD_COM_SEG)
/* Enable Segment LCD */
#define PotFreq_LCD_EN                 (* (reg8 *) PotFreq__LCD_EN)
/* Slew Rate Control */
#define PotFreq_SLW                    (* (reg8 *) PotFreq__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PotFreq_PRTDSI__CAPS_SEL       (* (reg8 *) PotFreq__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PotFreq_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PotFreq__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PotFreq_PRTDSI__OE_SEL0        (* (reg8 *) PotFreq__PRTDSI__OE_SEL0) 
#define PotFreq_PRTDSI__OE_SEL1        (* (reg8 *) PotFreq__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PotFreq_PRTDSI__OUT_SEL0       (* (reg8 *) PotFreq__PRTDSI__OUT_SEL0) 
#define PotFreq_PRTDSI__OUT_SEL1       (* (reg8 *) PotFreq__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PotFreq_PRTDSI__SYNC_OUT       (* (reg8 *) PotFreq__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PotFreq__SIO_CFG)
    #define PotFreq_SIO_HYST_EN        (* (reg8 *) PotFreq__SIO_HYST_EN)
    #define PotFreq_SIO_REG_HIFREQ     (* (reg8 *) PotFreq__SIO_REG_HIFREQ)
    #define PotFreq_SIO_CFG            (* (reg8 *) PotFreq__SIO_CFG)
    #define PotFreq_SIO_DIFF           (* (reg8 *) PotFreq__SIO_DIFF)
#endif /* (PotFreq__SIO_CFG) */

/* Interrupt Registers */
#if defined(PotFreq__INTSTAT)
    #define PotFreq_INTSTAT            (* (reg8 *) PotFreq__INTSTAT)
    #define PotFreq_SNAP               (* (reg8 *) PotFreq__SNAP)
    
	#define PotFreq_0_INTTYPE_REG 		(* (reg8 *) PotFreq__0__INTTYPE)
#endif /* (PotFreq__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PotFreq_H */


/* [] END OF FILE */
