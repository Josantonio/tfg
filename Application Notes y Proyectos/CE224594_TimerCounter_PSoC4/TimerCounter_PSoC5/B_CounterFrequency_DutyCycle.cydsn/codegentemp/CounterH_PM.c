/*******************************************************************************
* File Name: CounterH_PM.c  
* Version 3.0
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "CounterH.h"

static CounterH_backupStruct CounterH_backup;


/*******************************************************************************
* Function Name: CounterH_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  CounterH_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void CounterH_SaveConfig(void) 
{
    #if (!CounterH_UsingFixedFunction)

        CounterH_backup.CounterUdb = CounterH_ReadCounter();

        #if(!CounterH_ControlRegRemoved)
            CounterH_backup.CounterControlRegister = CounterH_ReadControlRegister();
        #endif /* (!CounterH_ControlRegRemoved) */

    #endif /* (!CounterH_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: CounterH_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  CounterH_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void CounterH_RestoreConfig(void) 
{      
    #if (!CounterH_UsingFixedFunction)

       CounterH_WriteCounter(CounterH_backup.CounterUdb);

        #if(!CounterH_ControlRegRemoved)
            CounterH_WriteControlRegister(CounterH_backup.CounterControlRegister);
        #endif /* (!CounterH_ControlRegRemoved) */

    #endif /* (!CounterH_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: CounterH_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  CounterH_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void CounterH_Sleep(void) 
{
    #if(!CounterH_ControlRegRemoved)
        /* Save Counter's enable state */
        if(CounterH_CTRL_ENABLE == (CounterH_CONTROL & CounterH_CTRL_ENABLE))
        {
            /* Counter is enabled */
            CounterH_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            CounterH_backup.CounterEnableState = 0u;
        }
    #else
        CounterH_backup.CounterEnableState = 1u;
        if(CounterH_backup.CounterEnableState != 0u)
        {
            CounterH_backup.CounterEnableState = 0u;
        }
    #endif /* (!CounterH_ControlRegRemoved) */
    
    CounterH_Stop();
    CounterH_SaveConfig();
}


/*******************************************************************************
* Function Name: CounterH_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  CounterH_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void CounterH_Wakeup(void) 
{
    CounterH_RestoreConfig();
    #if(!CounterH_ControlRegRemoved)
        if(CounterH_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            CounterH_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!CounterH_ControlRegRemoved) */
    
}


/* [] END OF FILE */
