/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

CY_ISR (TC_isr_handler){
    PinLED_Write(~PinLED_Read()); //Cambiamos el estado del LED
    Timer_ReadStatusRegister(); //Limpiamos la interrupción leyendo el registro
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* INITIALIZATION */
    Timer_Start();
    TC_interrupt_StartEx(TC_isr_handler); //Declaramos cabecera de interrupción

    for(;;)
    {
    }
}

/* [] END OF FILE */
