/*******************************************************************************
* File Name: PinLED.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_PinLED_H) /* Pins PinLED_H */
#define CY_PINS_PinLED_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "PinLED_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 PinLED__PORT == 15 && ((PinLED__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    PinLED_Write(uint8 value);
void    PinLED_SetDriveMode(uint8 mode);
uint8   PinLED_ReadDataReg(void);
uint8   PinLED_Read(void);
void    PinLED_SetInterruptMode(uint16 position, uint16 mode);
uint8   PinLED_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the PinLED_SetDriveMode() function.
     *  @{
     */
        #define PinLED_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define PinLED_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define PinLED_DM_RES_UP          PIN_DM_RES_UP
        #define PinLED_DM_RES_DWN         PIN_DM_RES_DWN
        #define PinLED_DM_OD_LO           PIN_DM_OD_LO
        #define PinLED_DM_OD_HI           PIN_DM_OD_HI
        #define PinLED_DM_STRONG          PIN_DM_STRONG
        #define PinLED_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define PinLED_MASK               PinLED__MASK
#define PinLED_SHIFT              PinLED__SHIFT
#define PinLED_WIDTH              1u

/* Interrupt constants */
#if defined(PinLED__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in PinLED_SetInterruptMode() function.
     *  @{
     */
        #define PinLED_INTR_NONE      (uint16)(0x0000u)
        #define PinLED_INTR_RISING    (uint16)(0x0001u)
        #define PinLED_INTR_FALLING   (uint16)(0x0002u)
        #define PinLED_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define PinLED_INTR_MASK      (0x01u) 
#endif /* (PinLED__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define PinLED_PS                     (* (reg8 *) PinLED__PS)
/* Data Register */
#define PinLED_DR                     (* (reg8 *) PinLED__DR)
/* Port Number */
#define PinLED_PRT_NUM                (* (reg8 *) PinLED__PRT) 
/* Connect to Analog Globals */                                                  
#define PinLED_AG                     (* (reg8 *) PinLED__AG)                       
/* Analog MUX bux enable */
#define PinLED_AMUX                   (* (reg8 *) PinLED__AMUX) 
/* Bidirectional Enable */                                                        
#define PinLED_BIE                    (* (reg8 *) PinLED__BIE)
/* Bit-mask for Aliased Register Access */
#define PinLED_BIT_MASK               (* (reg8 *) PinLED__BIT_MASK)
/* Bypass Enable */
#define PinLED_BYP                    (* (reg8 *) PinLED__BYP)
/* Port wide control signals */                                                   
#define PinLED_CTL                    (* (reg8 *) PinLED__CTL)
/* Drive Modes */
#define PinLED_DM0                    (* (reg8 *) PinLED__DM0) 
#define PinLED_DM1                    (* (reg8 *) PinLED__DM1)
#define PinLED_DM2                    (* (reg8 *) PinLED__DM2) 
/* Input Buffer Disable Override */
#define PinLED_INP_DIS                (* (reg8 *) PinLED__INP_DIS)
/* LCD Common or Segment Drive */
#define PinLED_LCD_COM_SEG            (* (reg8 *) PinLED__LCD_COM_SEG)
/* Enable Segment LCD */
#define PinLED_LCD_EN                 (* (reg8 *) PinLED__LCD_EN)
/* Slew Rate Control */
#define PinLED_SLW                    (* (reg8 *) PinLED__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define PinLED_PRTDSI__CAPS_SEL       (* (reg8 *) PinLED__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define PinLED_PRTDSI__DBL_SYNC_IN    (* (reg8 *) PinLED__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define PinLED_PRTDSI__OE_SEL0        (* (reg8 *) PinLED__PRTDSI__OE_SEL0) 
#define PinLED_PRTDSI__OE_SEL1        (* (reg8 *) PinLED__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define PinLED_PRTDSI__OUT_SEL0       (* (reg8 *) PinLED__PRTDSI__OUT_SEL0) 
#define PinLED_PRTDSI__OUT_SEL1       (* (reg8 *) PinLED__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define PinLED_PRTDSI__SYNC_OUT       (* (reg8 *) PinLED__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(PinLED__SIO_CFG)
    #define PinLED_SIO_HYST_EN        (* (reg8 *) PinLED__SIO_HYST_EN)
    #define PinLED_SIO_REG_HIFREQ     (* (reg8 *) PinLED__SIO_REG_HIFREQ)
    #define PinLED_SIO_CFG            (* (reg8 *) PinLED__SIO_CFG)
    #define PinLED_SIO_DIFF           (* (reg8 *) PinLED__SIO_DIFF)
#endif /* (PinLED__SIO_CFG) */

/* Interrupt Registers */
#if defined(PinLED__INTSTAT)
    #define PinLED_INTSTAT            (* (reg8 *) PinLED__INTSTAT)
    #define PinLED_SNAP               (* (reg8 *) PinLED__SNAP)
    
	#define PinLED_0_INTTYPE_REG 		(* (reg8 *) PinLED__0__INTTYPE)
#endif /* (PinLED__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_PinLED_H */


/* [] END OF FILE */
