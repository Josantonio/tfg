/******************************************************************************
* File Name: main.c
*
* Version: 1.00
*
* Description: This example demonstrates the use of count on the TCPWM Timer.
*              The count is incremented through the use of button presses.
*              Number of button presses is shown and LED is lit when button is
*              pressed
*
* Related Document: CE224594_TCPWM_CounterTimer.pdf 
*
* Hardware Dependency: CY8CKIT-042 PSoC 4 PIONEER KIT
*
******************************************************************************
* Copyright (2018), Cypress Semiconductor Corporation.
******************************************************************************
* This software, including source code, documentation and related materials
* ("Software") is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and 
* foreign), United States copyright laws and international treaty provisions. 
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the 
* Cypress source code and derivative works for the sole purpose of creating 
* custom software in support of licensee product, such licensee product to be
* used only in conjunction with Cypress's integrated circuit as specified in the
* applicable agreement. Any reproduction, modification, translation, compilation,
* or representation of this Software except as specified above is prohibited 
* without the express written permission of Cypress.
* 
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, 
* EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes to the Software without notice. 
* Cypress does not assume any liability arising out of the application or use
* of Software or any product or circuit described in the Software. Cypress does
* not authorize its products for use as critical components in any products 
* where a malfunction or failure may reasonably be expected to result in 
* significant injury or death ("ACTIVE Risk Product"). By including Cypress's 
* product in a ACTIVE Risk Product, the manufacturer of such system or application
* assumes all risk of such use and in doing so indemnifies Cypress against all
* liability. Use of this Software may be limited by and subject to the applicable
* Cypress software license agreement.
*****************************************************************************/

#include <project.h>
#include <stdio.h>

/*******************************************************************************
* Function Name: main
********************************************************************************
*
*  The main function performs the following actions:
*   1. Sets up the Timer and UART components.
*   2. Checks for changes in the current count. The counter increments each time the kit button SW2 is pressed. 
*       I.  LED is flashed to show that count has changed
*       II. The current count is printed via UART to terminal.
*
*******************************************************************************/
int main(void)
{
    /* Buffer for string conversion */
    char snum[32];

    /* Initializing currentCount*/
    uint32 prevCount = 0xFFFF;

    /* Enable global interrupts. */
	CyGlobalIntEnable; 
    
    /* Start the Timer component */
    Timer_Start();
    
    /* Starts the UART component */
    UART_Start();
    UART_UartPutString("UART Successfully Started! \r\n");
    
    for(;;)
    {
        uint32 currentCount = Timer_ReadCounter();
        /* When the count changes in the counter */
        if(currentCount != prevCount)
        {
            /* LED is turned on to indicate a count */
            LED_RED_Write(0);
            
            /* Sending current count to UART to be printed in terminal */
            UART_UartPutString("Current Count: ");
            sprintf(snum, "%lu", currentCount);
            UART_UartPutString(snum);
            UART_UartPutString("\r\n");
            
            /* Delay to show LED in ON state before going to OFF state */
            CyDelay(50);
            LED_RED_Write(1);
            
            /* Update previous count */
            prevCount = currentCount;
        }
    }
}

/* [] END OF FILE */
