# Control de motores brushless con PSoC

**Trabajo Fin de Grado**  
_Ingeniería Electrónica Industrial_  
Universidad de Granada

## Descripción 📖

Este repositorio contiene todo el material utilizado para la realización del TFG.   
El TFG consiste en realizar un programa con la PSoC para el control de motores sin escobillas (brushless), en este caso, se ha implementado un programa para control de motores con sensores hall, el ajuste de la velocidad se hace mediante un potenciómetro y se consigue implementando un controlador PID.


### Archivos de interés 📋

El archivo que contiene el **programa principal** que se está desarrollando se puede encontrar en la dirección:<br/> **[CY8CKIT-037_Documentacion > CONTROLBRUSHLESS > Sensored](https://gitlab.com/Josantonio/tfg/-/tree/master/CY8CKIT-037_Documentacion/CONTROLBRUSHLESS/Sensored/BLDCmotorControl)**

El archivo con la **memoria** que describe el TFG está en la dirección:  
**[Memoria > Memoria.pdf](https://gitlab.com/Josantonio/tfg/-/blob/master/Memoria/Memoria.pdf)**


### Algoritmos del programa 🔧

Algoritmo del programa principal: [_main.c_](CY8CKIT-037_Documentacion/CONTROLBRUSHLESS/Sensored/BLDCmotorControl/Sensored.cydsn/main.c)

<img src="Imagenes/Algoritmos/main.svg"  width="300"  class="left">
<br/>
<img src="Imagenes/Algoritmos/SensoredCloseLoop.png" width="520" class="right">



## Pruebas de funcionamiento ⚙️

En el siguiente gif se puede ver el funcionamiento correcto del motor:

![](Imagenes/pruebas/FuncionamientoNormal.gif)
<br/><br/><br/>


En esta otra imagen animada se puede observar cómo cuando se desconecta la tensión Vin, la placa para el motor e indica que se ha dado un error de voltaje mediante el parpadeo de un LED rojo:

![](Imagenes/pruebas/VinError.gif)
<br/><br/><br/>


Finalmente, si el error se produce por las señales hall (se han desconectado), también indica el error mediante el parpadeo del LED rojo, pero esta vez, con una frecuencia superior, indicando otro tipo de error:

![](Imagenes/pruebas/HallError.gif)


## Descripción de las demás carpetas y documentos 📌

* [**Application Notes y Proyectos:**](Application%20Notes%20y%20Proyectos/) Documentación técnica de fabricantes y diversos programas realizados con la PSoC para la familiarización con esta tecnología.

* [**Artículos:**](Artículos/) Artículos del **IEEE* utilizados para la realización del trabajo.
* [**CY8CKIT-037_Documentacion:**](CY8CKIT-037_Documentacion/) Contiene el programa final y documentación acerca d ela shield, como el esquemático, PCB, la guía de usuario.
* [**CY8CKIT-042 Code Examples:**](CY8CKIT-042%20Code%20Examples/) Documentación acerca de la PSoC 4 que se ha usado, y algunos programas de ejemplo.
* [**Esquemas:**](esquemas/) Código en LaTex para la creación de figuras que se utilizarán en el programa de PSoC creator y esclarecer su funcionamiento.
* [**Fixed-Point PI controller:**](Fised-Point%20PI%20Controller/) Artículo acerca del control PI y cómo implementarlo sin hacer uso de variables tipo _float_.
* [**Gráficas:**](graficas/) Código en LaTex para la creación de gráficos y figuras que se utilizan en la memoria.
* [**Imagenes**](Imagenes/) 
* [**Memoria:**](Memoria/) Carpeta que contiene el código en LaTex para la realización de la memoria del TFG. También contiene una carpeta de imágenes que se incluyen en e PDF.


## Autores ✒️

* **José Antonio Negro Espejo** - *Alumno* 
* **Diego Pedro Morales Santos** - *Tutor* - Dpto Electrónica y Tecnología de Computadores
* **Noel Rodríguez Santiago** - *Tutor* - Dpto Electrónica y Tecnología de Computadores
