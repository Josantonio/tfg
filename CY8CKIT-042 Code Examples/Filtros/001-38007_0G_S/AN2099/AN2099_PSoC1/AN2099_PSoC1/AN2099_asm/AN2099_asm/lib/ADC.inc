;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: ADC.inc
;;   Version: 1.0, Updated on 2012/9/21 at 11:58:50
;;
;;  DESCRIPTION: Assembler declarations for the Delta-Sigma A/D Converter
;;               with a 2nd-order modulator and based on a type 2 decimator.
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2012. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "m8c.inc"

;--------------------------------------------------
; Constants for ADC API's.
;--------------------------------------------------
ADC_INT_REG:                       equ  0e0h
ADC_INT_MASK:                      equ  02h    ; Interrupt mask

ADC_OFF:                           equ  0
ADC_LOWPOWER:                      equ  1
ADC_MEDPOWER:                      equ  2
ADC_HIGHPOWER:                     equ  3

ADC_DATA_READY_BIT:                equ  10h
ADC_2S_COMPLEMENT:                 equ  0

;--------------------------------------------------
; Register Address Constants for ADC
;--------------------------------------------------
ADC_AtoD1cr0:                      equ 0x80    ; SC Block 1 Control Reg 0
ADC_AtoD1cr1:                      equ 0x81    ; SC Block 1 Control Reg 1
ADC_AtoD1cr2:                      equ 0x82    ; SC Block 1 Control Reg 2
ADC_AtoD1cr3:                      equ 0x83    ; SC Block 1 Control Reg 3
ADC_AtoD2cr0:                      equ 0x90    ; SC Block 2 Control Reg 0
ADC_AtoD2cr1:                      equ 0x91    ; SC Block 2 Control Reg 1
ADC_AtoD2cr2:                      equ 0x92    ; SC Block 2 Control Reg 2
ADC_AtoD2cr3:                      equ 0x93    ; SC Block 2 Control Reg 3

ADC_DEC_DL:                                  equ 0xa1                                  ; Decimator Low Data Reg
ADC_DEC_DH:                                  equ 0xa0                                  ; Decimator High Data Reg

; end of file ADC.inc
