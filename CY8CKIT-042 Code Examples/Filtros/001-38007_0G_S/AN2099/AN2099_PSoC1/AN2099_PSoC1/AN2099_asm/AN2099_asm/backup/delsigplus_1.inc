;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: DelSigPlus_1.inc
;;   Version: 1.0, Updated on 2010/12/27 at 15:27:23
;;
;;  DESCRIPTION: Assembler declarations for the Delta-Sigma A/D Converter
;;               with a 2nd-order modulator and based on a type 2 decimator.
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2010. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "m8c.inc"

;--------------------------------------------------
; Constants for DelSigPlus_1 API's.
;--------------------------------------------------
DelSigPlus_1_INT_REG:              equ  0e0h
DelSigPlus_1_INT_MASK:             equ  02h    ; Interrupt mask

DelSigPlus_1_OFF:                  equ  0
DelSigPlus_1_LOWPOWER:             equ  1
DelSigPlus_1_MEDPOWER:             equ  2
DelSigPlus_1_HIGHPOWER:            equ  3

DelSigPlus_1_DATA_READY_BIT:       equ  10h
DelSigPlus_1_2S_COMPLEMENT:        equ  0

;--------------------------------------------------
; Register Address Constants for DelSigPlus_1
;--------------------------------------------------
DelSigPlus_1_AtoD1cr0:             equ 0x80    ; SC Block 1 Control Reg 0
DelSigPlus_1_AtoD1cr1:             equ 0x81    ; SC Block 1 Control Reg 1
DelSigPlus_1_AtoD1cr2:             equ 0x82    ; SC Block 1 Control Reg 2
DelSigPlus_1_AtoD1cr3:             equ 0x83    ; SC Block 1 Control Reg 3
DelSigPlus_1_AtoD2cr0:             equ 0x90    ; SC Block 2 Control Reg 0
DelSigPlus_1_AtoD2cr1:             equ 0x91    ; SC Block 2 Control Reg 1
DelSigPlus_1_AtoD2cr2:             equ 0x92    ; SC Block 2 Control Reg 2
DelSigPlus_1_AtoD2cr3:             equ 0x93    ; SC Block 2 Control Reg 3

DelSigPlus_1_DEC_DL:                         equ 0xa1                                  ; Decimator Low Data Reg
DelSigPlus_1_DEC_DH:                         equ 0xa0                                  ; Decimator High Data Reg

; end of file DelSigPlus_1.inc
