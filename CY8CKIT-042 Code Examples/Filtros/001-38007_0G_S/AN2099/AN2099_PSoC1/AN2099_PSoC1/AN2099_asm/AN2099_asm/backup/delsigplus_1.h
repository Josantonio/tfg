//*****************************************************************************
//*****************************************************************************
//  FILENAME: DelSigPlus_1.h
//   Version: 1.0, Updated on 2010/12/27 at 15:27:23
//
//  DESCRIPTION:  C declarations for the DelSig User Module with a 2nd-order
//                modulator based on a type-2 dcecimator
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2010. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef DelSigPlus_1_INCLUDE
#define DelSigPlus_1_INCLUDE

#include <m8c.h>

#pragma fastcall16 DelSigPlus_1_Start
#pragma fastcall16 DelSigPlus_1_SetPower
#pragma fastcall16 DelSigPlus_1_StartAD
#pragma fastcall16 DelSigPlus_1_StopAD
#pragma fastcall16 DelSigPlus_1_Stop
#pragma fastcall16 DelSigPlus_1_fIsDataAvailable
#pragma fastcall16 DelSigPlus_1_iGetData
#pragma fastcall16 DelSigPlus_1_wGetData
#pragma fastcall16 DelSigPlus_1_iGetDataClearFlag
#pragma fastcall16 DelSigPlus_1_wGetDataClearFlag
#pragma fastcall16 DelSigPlus_1_ClearFlag

//-------------------------------------------------
// Prototypes of the DelSigPlus_1 API.
//-------------------------------------------------
extern void DelSigPlus_1_Start(BYTE bPower);
extern void DelSigPlus_1_SetPower(BYTE bPower);
extern void DelSigPlus_1_StartAD(void);
extern void DelSigPlus_1_StopAD(void);
extern void DelSigPlus_1_Stop(void);
extern BOOL DelSigPlus_1_fIsDataAvailable(void);
extern INT  DelSigPlus_1_iGetData(void);
extern WORD DelSigPlus_1_wGetData(void);
extern INT  DelSigPlus_1_iGetDataClearFlag(void);
extern WORD DelSigPlus_1_wGetDataClearFlag(void);
extern void DelSigPlus_1_ClearFlag(void);

//-------------------------------------------------
// Defines for DelSigPlus_1 API's.
//-------------------------------------------------
#define DelSigPlus_1_OFF                   (0)
#define DelSigPlus_1_LOWPOWER              (1)
#define DelSigPlus_1_MEDPOWER              (2)
#define DelSigPlus_1_HIGHPOWER             (3)

#define DelSigPlus_1_DATA_READY_BIT        (0x10)
#define DelSigPlus_1_2S_COMPLEMENT         (0)

//-------------------------------------------------
// Hardware Register Definitions
//-------------------------------------------------
#pragma ioport  DelSigPlus_1_AtoD1cr0:  0x080              //Analog block 1 control register 0
BYTE            DelSigPlus_1_AtoD1cr0;
#pragma ioport  DelSigPlus_1_AtoD1cr1:  0x081              //Analog block 1 control register 1
BYTE            DelSigPlus_1_AtoD1cr1;
#pragma ioport  DelSigPlus_1_AtoD1cr2:  0x082              //Analog block 1 control register 2
BYTE            DelSigPlus_1_AtoD1cr2;
#pragma ioport  DelSigPlus_1_AtoD1cr3:  0x083              //Analog block 1 control register 3
BYTE            DelSigPlus_1_AtoD1cr3;
#pragma ioport  DelSigPlus_1_AtoD2cr0:  0x090              //Analog block 2 control register 0
BYTE            DelSigPlus_1_AtoD2cr0;
#pragma ioport  DelSigPlus_1_AtoD2cr1:  0x091              //Analog block 2 control register 1
BYTE            DelSigPlus_1_AtoD2cr1;
#pragma ioport  DelSigPlus_1_AtoD2cr2:  0x092              //Analog block 2 control register 2
BYTE            DelSigPlus_1_AtoD2cr2;
#pragma ioport  DelSigPlus_1_AtoD2cr3:  0x093              //Analog block 2 control register 3
BYTE            DelSigPlus_1_AtoD2cr3;

#endif
// end of file DelSigPlus_1.h
