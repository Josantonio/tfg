/* --------------------------------------------------------------------------
* Copyright 2010, Cypress Semiconductor Corporation.
*
* This software is owned by Cypress Semiconductor Corporation (Cypress)
* and is protected by and subject to worldwide patent protection (United
* States and foreign), United States copyright laws and international
* treaty provisions. Cypress hereby grants to licensee a personal,
* non-exclusive, non-transferable license to copy, use, modify, create
* derivative works of, and compile the Cypress Source Code and derivative
* works for the sole purpose of creating custom software in support of
* licensee product to be used only in conjunction with a Cypress integrated
* circuit as specified in the applicable agreement. Any reproduction,
* modification, translation, compilation, or representation of this
* software except as specified above is prohibited without the express
* written permission of Cypress.
* 
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND,EXPRESS OR IMPLIED,
* WITH REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising
* out of the application or use of any product or circuit described herein.
* Cypress does not authorize its products for use as critical components in
* life-support systems where a malfunction or failure may reasonably be
* expected to result in significant injury to the user. The inclusion of
* Cypress' product in a life-support systems application implies that the
* manufacturer assumes all risk of such use and in doing so indemnifies
* Cypress against all charges.
* 
* Use may be limited by and subject to the applicable Cypress software
* license agreement.
*****************************************************************************
*  Project Name: AN2099_C
*  Device Tested: CY8C28545
*  Software Version: PSoC Designer 5.3
*  Complier tested: ImageCraft
*  Related Hardware: CY3210, CY8CKIT-001
******************************************************************************
* Project Description:
* This example project demonstrates a single pole low pass IIR filter. The ADC 
* data is passed to the single pole low pass IIR filter. The low pass filter 
* returns a filterd output. The filtered output and the ADC data are displayed 
* in an LCD for comparison.
*******************************************************************************/


#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "IIRFilters.h"


void main(void)
{
   int  ADC_data, filt_data;
   M8C_EnableGInt;
   
   LCD_Start();
   LCD_Position(0,0);
   LCD_PrCString("ADC Data");
   LCD_Position(1,0);
   LCD_PrCString("Fil Data");
   
   PGA_Start(PGA_HIGHPOWER);
   ADC_Start(ADC_HIGHPOWER);
   ADC_StartAD();
   
   if (ADC_fIsDataAvailable())
   	    ADC_data = ADC_iGetDataClearFlag();
    SimpleLowPassInit(ADC_data);
	
   while(1)
   {    
 		if (ADC_fIsDataAvailable())
   			ADC_data = ADC_iGetDataClearFlag();
     	
		filt_data=iSimpleLowPassFilter(ADC_data);
		
	 	LCD_Position(0,9);
	 	LCD_PrHexInt(ADC_data);
		LCD_Position(1,9);
	 	LCD_PrHexInt(filt_data);
   }	
}

