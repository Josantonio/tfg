;-------------------------------------------------------------------------------------------------
;(C) Cypress Semiconductor Corporation, 2002-2009. The information contained herein is subject to change 
; without notice. Cypress Semiconductor Corporation assumes no responsibility for the use of any 
; circuitry other than circuitry embodied in a Cypress product. Nor does it convey or imply any 
; license under patent or other rights. Cypress products are not warranted nor intended to be used 
; for medical, life support, life saving, critical control or safety applications, unless pursuant 
; to an express written agreement with Cypress. Furthermore, Cypress does not authorize its 
; products for use as critical components in life-support systems where a malfunction or failure 
; may reasonably be expected to result in significant injury to the user. The inclusion of Cypress 
; products in life-support systems application implies that the manufacturer assumes all risk of 
; such use and in doing so indemnifies Cypress against all charges.
;
; This Source Code (software and/or firmware) is owned by Cypress Semiconductor Corporation 
; (Cypress) and is protected by and subject to worldwide patent protection (United States and 
; foreign), United States copyright laws and international treaty provisions. Cypress hereby grants 
; to licensee a personal, non-exclusive, non-transferable license to copy, use, modify, create 
; derivative works of, and compile the Cypress Source Code and derivative works for the sole 
; purpose of creating custom software and or firmware in support of licensee product to be used 
; only in conjunction with a Cypress integrated circuit as specified in the applicable agreement. 
; Any reproduction, modification, translation, compilation, or representation of this Source Code 
; except as specified above is prohibited without the express written permission of Cypress.
;
; Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS 
; MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
; FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes without further notice 
; to the materials described herein. Cypress does not assume any liability arising out of the 
; application or use of any product or circuit described herein. Cypress does not authorize its 
; products for use as critical components in life-support systems where a malfunction or failure 
; may reasonably be expected to result in significant injury to the user. The inclusion of 
; Cypress’ product in a life-support systems application implies that the manufacturer assumes all 
; risk of such use and in doing so indemnifies Cypress against all charges.
;
; Use may be limited by and subject to the applicable Cypress software license agreement.
;-------------------------------------------------------------------------------------------------;;***********************************
;;  IIRFilters.asm
;;
;;  Assembler source for IIR Filters
;;***********************************
include "m8c.inc"
area bss(RAM) 
   iVlp:     BLK  3
   iVlp1::    BLK  3
   TempReg::  BLK  3   
area text(ROM,REL)

export  SimpleHighPassInit
export _SimpleHighPassInit
;;----------------------------------
;;  SimpleHighPassInit:
;;  
;;  Initializes the Vlp value
;;  INPUTS:  X,A Init Value
;;  OUTPUTS: None.  
;;----------------------------------
 SimpleHighPassInit:
_SimpleHighPassInit:
   mov [iVlp],X   
   mov [iVlp + 1],A
   mov [iVlp + 2],0
ret
;;----------------------------------
export  iSimpleHighPassFilter
export _iSimpleHighPassFilter
;;----------------------------------
;;  iSimpleHighPassFilter:
;;  
;;  Take input and output new
;;  higpassvalue
;;  INPUTS:  X,A Vin
;;  OUTPUTS: X,A Vhp  
;;----------------------------------
 iSimpleHighPassFilter:
_iSimpleHighPassFilter:
   sub  A,[iVlp+1]
   swap A,X
   sbb  A,[iVlp]
   ;Vhp now in A,X
   cmp A,128
   swap A,X
   if1: jc elseif1 ;(if neg)
       add [iVlp+2],A
       swap X,A
       adc [iVlp+1],A
       swap A,X
       adc [iVlp],ffh
       ret
   elseif1:;(pos)
       add [iVlp+2],A
       swap X,A 
       adc [iVlp+1],A
       swap A,X
       adc [iVlp],0 
       ret  
   endif1:
;----------------------------------- 


export  SimpleLowPassInit
export _SimpleLowPassInit
;;----------------------------------
;;  SimpleLowPassInit:
;;  
;;  Initializes the Vlp value
;;  INPUTS:  X,A Init Value
;;  OUTPUTS: None.  
;;----------------------------------
 SimpleLowPassInit:
_SimpleLowPassInit:
   mov [iVlp1],X   
   mov [iVlp1 + 1],A
   mov [iVlp1 + 2],0
ret
;;----------------------------------

macro  SHIFT
   asr [TempReg]
   rrc [TempReg + 1]
   rrc [TempReg + 2]   
endm

macro  ACCUM
   mov A,[TempReg + 2]
   add [iVlp1 + 2],A
   mov A,[TempReg + 1]
   adc [iVlp1 + 1],A
   mov A,[TempReg]
   adc [iVlp1],A
endm

export  iSimpleLowPassFilter
export _iSimpleLowPassFilter
;;----------------------------------
;;  iSimpleLowPassFilter:
;;  
;;  Take input and output new
;;  higpassvalue
;;  INPUTS:  X,A Vin
;;  OUTPUTS: X,A Vhp  
;;----------------------------------
 iSimpleLowPassFilter:
_iSimpleLowPassFilter:
   push A
   mov A,0
   sub A,[iVlp1+2] 
   mov  [TempReg + 2],A
   pop  A
   sbb  A,[iVlp1+1]
   mov  [TempReg + 1],A
   mov  A,X
   sbb  A,[iVlp1]
   mov  [TempReg],A
   
   SHIFT
   SHIFT
   SHIFT
   SHIFT
   SHIFT
   ACCUM
   SHIFT
   ACCUM
  
   mov  A,[iVlp1 + 2]
   add  A,128
   mov  X,[iVlp1]
   mov  A,[iVlp1 + 1]
   adc  A,0
   swap X,A
   adc  A,0
   swap X,A
ret  
;----------------------------------- 


 