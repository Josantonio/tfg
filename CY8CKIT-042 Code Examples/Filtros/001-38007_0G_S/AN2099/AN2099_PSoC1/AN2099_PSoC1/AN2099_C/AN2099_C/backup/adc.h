//*****************************************************************************
//*****************************************************************************
//  FILENAME: ADC.h
//   Version: 1.0, Updated on 2012/3/2 at 9:14:17
//
//  DESCRIPTION:  C declarations for the DelSig User Module with a 2nd-order
//                modulator based on a type-2 dcecimator
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2012. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef ADC_INCLUDE
#define ADC_INCLUDE

#include <m8c.h>

#pragma fastcall16 ADC_Start
#pragma fastcall16 ADC_SetPower
#pragma fastcall16 ADC_StartAD
#pragma fastcall16 ADC_StopAD
#pragma fastcall16 ADC_Stop
#pragma fastcall16 ADC_fIsDataAvailable
#pragma fastcall16 ADC_iGetData
#pragma fastcall16 ADC_wGetData
#pragma fastcall16 ADC_iGetDataClearFlag
#pragma fastcall16 ADC_wGetDataClearFlag
#pragma fastcall16 ADC_ClearFlag

//-------------------------------------------------
// Prototypes of the ADC API.
//-------------------------------------------------
extern void ADC_Start(BYTE bPower);
extern void ADC_SetPower(BYTE bPower);
extern void ADC_StartAD(void);
extern void ADC_StopAD(void);
extern void ADC_Stop(void);
extern BOOL ADC_fIsDataAvailable(void);
extern INT  ADC_iGetData(void);
extern WORD ADC_wGetData(void);
extern INT  ADC_iGetDataClearFlag(void);
extern WORD ADC_wGetDataClearFlag(void);
extern void ADC_ClearFlag(void);

//-------------------------------------------------
// Defines for ADC API's.
//-------------------------------------------------
#define ADC_OFF                            (0)
#define ADC_LOWPOWER                       (1)
#define ADC_MEDPOWER                       (2)
#define ADC_HIGHPOWER                      (3)

#define ADC_DATA_READY_BIT                 (0x10)
#define ADC_2S_COMPLEMENT                  (0)

//-------------------------------------------------
// Hardware Register Definitions
//-------------------------------------------------
#pragma ioport  ADC_AtoD1cr0:   0x080              //Analog block 1 control register 0
BYTE            ADC_AtoD1cr0;
#pragma ioport  ADC_AtoD1cr1:   0x081              //Analog block 1 control register 1
BYTE            ADC_AtoD1cr1;
#pragma ioport  ADC_AtoD1cr2:   0x082              //Analog block 1 control register 2
BYTE            ADC_AtoD1cr2;
#pragma ioport  ADC_AtoD1cr3:   0x083              //Analog block 1 control register 3
BYTE            ADC_AtoD1cr3;
#pragma ioport  ADC_AtoD2cr0:   0x090              //Analog block 2 control register 0
BYTE            ADC_AtoD2cr0;
#pragma ioport  ADC_AtoD2cr1:   0x091              //Analog block 2 control register 1
BYTE            ADC_AtoD2cr1;
#pragma ioport  ADC_AtoD2cr2:   0x092              //Analog block 2 control register 2
BYTE            ADC_AtoD2cr2;
#pragma ioport  ADC_AtoD2cr3:   0x093              //Analog block 2 control register 3
BYTE            ADC_AtoD2cr3;

#endif
// end of file ADC.h
