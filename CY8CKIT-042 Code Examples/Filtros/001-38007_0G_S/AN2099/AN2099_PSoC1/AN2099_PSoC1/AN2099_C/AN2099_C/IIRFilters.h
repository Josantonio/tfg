//**********************************
//
//  IIRFilters.h
//
//  C declarations for IIR Filters.
//
//  Copyright: Cypress Micro Systems
//  2002 All Rights Reserved
//
//**********************************

#pragma fastcall16 SimpleHighPassInit
#pragma fastcall16 iSimpleHighPassFilter
#pragma fastcall16 SimpleLowPassInit
#pragma fastcall16 iSimpleLowPassFilter
extern void SimpleHighPassInit(int iData);
extern  int iSimpleHighPassFilter(int iData);
extern void SimpleLowPassInit(int iData);
extern  int iSimpleLowPassFilter(int iData);
