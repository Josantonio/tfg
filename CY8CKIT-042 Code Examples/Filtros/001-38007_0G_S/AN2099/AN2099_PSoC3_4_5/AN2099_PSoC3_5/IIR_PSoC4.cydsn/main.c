/*  --------------------------------------------------------------------------
* Copyright 2013, Cypress Semiconductor Corporation.
*
* This software is owned by Cypress Semiconductor Corporation (Cypress)
* and is protected by and subject to worldwide patent protection (United
* States and foreign), United States copyright laws and international
* treaty provisions. Cypress hereby grants to licensee a personal,
* non-exclusive, non-transferable license to copy, use, modify, create
* derivative works of, and compile the Cypress Source Code and derivative
* works for the sole purpose of creating custom software in support of
* licensee product to be used only in conjunction with a Cypress integrated
* circuit as specified in the applicable agreement. Any reproduction,
* modification, translation, compilation, or representation of this
* software except as specified above is prohibited without the express
* written permission of Cypress.
* 
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND,EXPRESS OR IMPLIED,
* WITH REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising
* out of the application or use of any product or circuit described herein.
* Cypress does not authorize its products for use as critical components in
* life-support systems where a malfunction or failure may reasonably be
* expected to result in significant injury to the user. The inclusion of
* Cypress' product in a life-support systems application implies that the
* manufacturer assumes all risk of such use and in doing so indemnifies
* Cypress against all charges.
* 
* Use may be limited by and subject to the applicable Cypress software
* license agreement.
* -----------------------------------------------------------------------------
* Copyright (c) Cypress MicroSystems 2000-2013. All Rights Reserved.
*
*****************************************************************************
*  Project Name: IIR_PSoC4
*  Device Tested: 4245AXI-483
*  Software Version: PSoC Creator 3.0
*  Compilers Tested: ARM GCC
*  Related Hardware: CY8CKIT-042
*****************************************************************************
***************************************************************************** */

/* Project Description:
* This example project demonstrates a single pole low pass IIR filter. The ADC 
* data is passed to the single pole low pass IIR filter. The low pass filter 
* returns a filterd output. The filtered output and the ADC data are printed 
* out over UART. The result can be read using the Cypress Bridge Control Panel.
***************************************************************************** */
#include <project.h>

/*Function prototype for filter routine*/
int32 LowPassFilter(int32 input);

int main()
{
 
	/*Variables to hold raw ADC data, and Low Pass Filtered Data*/
    int32 ADC_data, filt_data; 	
   
	/*Start the UART to communicate out results*/
	/*UART is configured as follows:
	*Baud - 19200
	*Data - 8 Bits
	*Parity - Even
	*Stop - 2 Bits */
	UART_Start();
   
	/*Start the ADC and start converting*/
   	ADC_Start();
   	ADC_StartConvert();

 
   for(;;)
   {
		/*Wait for new data from the ADC*/
        ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);
		/*Grab the Data out of the ADC*/
   		ADC_data = ADC_GetResult16(0);
		
		/*Print a UART header to synchronize data packets*/
		UART_UartPutChar((int8)(0xAA));	
		
		UART_UartPutChar((int8)(ADC_data >> 16));				/* Print the high byte of the 24-bit ADC result */
		UART_UartPutChar((int8)(ADC_data >> 8));				/* Print the middle byte of the 24-bit ADC result */
		UART_UartPutChar((int8)(ADC_data >> 0));				/* Print the low byte of the 24-bit ADC result */
		
		/*Filter Raw ADC Data*/
   		filt_data = LowPassFilter(ADC_data);
		
		
		UART_UartPutChar((int8)(filt_data >> 16));				/* Print the high byte of the 24-bit Filtered result */
		UART_UartPutChar((int8)(filt_data >> 8));				/* Print the middle byte of the 24-bit Filtered result */
		UART_UartPutChar((int8)(filt_data >> 0));				/* Print the low byte of the 24-bit Filtered result */
		
		/*Print UART tail to synchronize data packets*/
		UART_UartPutChar((int8)(0x55));	
    }
}

int32 LowPassFilter(int32 input)
{
	/*Variable to hold hold value of filter*/
	static int32 filt;
	
	/*Variable to hold return value of fucntion*/
    int32 k;
	
	 /* Feedforward to cope with fast changes in Input */
    int32 feedforward = (int32)100 * 256; 
	
	 /* Input is left shifted by 8 to right shifts up to 8 without losing precision */
    input <<= 8;  
	
	/* Check to see if the input is fast changing*/
    if ((input > (filt + feedforward)) || (input <  (filt - feedforward)))
    {
	    filt = input;
    }
    else
    {
	    filt = filt + (((input-filt) >> 8) * 11);           /* The actual filtering, for this filter a = 256/11 */
    }
    k = (filt>>8) + ((filt & 0x00000080) >> 7);             /* Right shift the result by 8 to compensate for the input left shift. Round off instead of truncation. 
                                                               The additive term takes care of rounding off */	
    return k;               	
}

/* [] END OF FILE */
