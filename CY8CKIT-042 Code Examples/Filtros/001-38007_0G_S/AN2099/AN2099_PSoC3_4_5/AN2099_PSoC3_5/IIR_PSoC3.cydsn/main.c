/*  --------------------------------------------------------------------------
* Copyright 2013, Cypress Semiconductor Corporation.
*
* This software is owned by Cypress Semiconductor Corporation (Cypress)
* and is protected by and subject to worldwide patent protection (United
* States and foreign), United States copyright laws and international
* treaty provisions. Cypress hereby grants to licensee a personal,
* non-exclusive, non-transferable license to copy, use, modify, create
* derivative works of, and compile the Cypress Source Code and derivative
* works for the sole purpose of creating custom software in support of
* licensee product to be used only in conjunction with a Cypress integrated
* circuit as specified in the applicable agreement. Any reproduction,
* modification, translation, compilation, or representation of this
* software except as specified above is prohibited without the express
* written permission of Cypress.
* 
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND,EXPRESS OR IMPLIED,
* WITH REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising
* out of the application or use of any product or circuit described herein.
* Cypress does not authorize its products for use as critical components in
* life-support systems where a malfunction or failure may reasonably be
* expected to result in significant injury to the user. The inclusion of
* Cypress' product in a life-support systems application implies that the
* manufacturer assumes all risk of such use and in doing so indemnifies
* Cypress against all charges.
* 
* Use may be limited by and subject to the applicable Cypress software
* license agreement.
* -----------------------------------------------------------------------------
* Copyright (c) Cypress MicroSystems 2000-2013. All Rights Reserved.
*
*****************************************************************************
*  Project Name: IIR_PSoC3
*  Device Tested: CY8C3866AXI
*  Software Version: PSoC Creator 2.1 SP1
*  Compilers Tested: Keil(C51)
*  Related Hardware: CY8CKIT-001, CY8CKIT-030
*****************************************************************************
***************************************************************************** */

/* Project Description:
* This example project demonstrates a single pole low pass IIR filter. The ADC 
* data is passed to the single pole low pass IIR filter. The low pass filter 
* returns a filterd output. The filtered output and the ADC data are displayed 
* in an LCD for comparison.
***************************************************************************** */

#include <device.h>

int32 LowPassFilter(int32);

int32 filt;

int main()
{
   int32 ADC_data, filt_data; 	
   
   LCD_Start();
   LCD_Position(0,0);
   LCD_PrintString("ADC Data");
   LCD_Position(1,0);
   LCD_PrintString("Fil Data");
   
   ADC_Start();
   ADC_StartConvert();
   ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);
   ADC_data = ADC_GetResult32();
   
   for(;;)
   {
		ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);
   		ADC_data = ADC_GetResult32();
		
		LCD_Position(0,9);
		LCD_PrintHexUint8((int8)(ADC_data >> 16));		/* Print the high byte of the 24-bit ADC result */
		LCD_Position(0,11);
		LCD_PrintHexUint16((int16)ADC_data);			/* Print the middle and low bytes of the 24-bit ADC result */ 	
		
	 	filt_data = LowPassFilter(ADC_data);
		
		LCD_Position(1,9);
		LCD_PrintHexUint8((int8)(filt_data >> 16));		/* Print the high byte of the 24-bit filter result */
		LCD_Position(1,11);
		LCD_PrintHexUint16((int16)filt_data);			/* Print the middle and low bytes of the 24-bit filter result */
    }
}

int32 LowPassFilter(int32 input)
{
    int32 k;
    int32 feedforward = (int32)100 * 256;               /* Feedforward to cope with fast changes in Input */
    input <<= 8;                                        /* Input is left shifted by 256 to allow divisions up to 256 */
    if ((input > (filt + feedforward)) || (input <  (filt - feedforward)))
    {
	    filt = input;
    }
    else
    {   
	    filt = filt + ((input - filt) >> 7);             /* The actual Filtering */
    }
    k = (filt >> 8) + ((filt & 0x00000080) >> 7);        /* Right shift the result by 8 to compensate for the input left shift. Round off instead of truncation. 
														    The additive term takes care of rounding off */
    return k;               	
}

/* [] END OF FILE */
