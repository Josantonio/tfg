/*******************************************************************************
* File Name: ADC_DelSig_1.c  
* Version 2.10
*
* Description:
*  This file provides the source code to the API for the Delta-Sigma ADC
*  Component.
*
* Note:
*
*******************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "ADC_DelSig_1.h"

#if(ADC_DelSig_1_DEFAULT_INTERNAL_CLK)
    #include "ADC_DelSig_1_theACLK.h"
#endif /* ADC_DelSig_1_DEFAULT_INTERNAL_CLK */

#if(ADC_DelSig_1_DEFAULT_CHARGE_PUMP_CLOCK)
    #include "ADC_DelSig_1_Ext_CP_Clk.h"
#endif /* ADC_DelSig_1_DEFAULT_CHARGE_PUMP_CLOCK */

#if(ADC_DelSig_1_DEFAULT_INPUT_MODE)
    #include "ADC_DelSig_1_AMux.h"
#endif /* ADC_DelSig_1_DEFAULT_INPUT_MODE */

/* Software flag for checking conversion completed or not */
volatile uint8 ADC_DelSig_1_convDone = 0u;

/* Software flag to stop conversion for single sample conversion mode 
   with resolution above 16 bits */
volatile uint8 ADC_DelSig_1_stopConversion = 0;

/* To run the initialization block only at the start up */
uint8 ADC_DelSig_1_initVar = 0u;

volatile int32 ADC_DelSig_1_Offset = 0;
volatile int32 ADC_DelSig_1_CountsPerVolt = (uint32)ADC_DelSig_1_DFLT_COUNTS_PER_VOLT;


/******************************************************************************* 
* Function Name: ADC_DelSig_1_Init
********************************************************************************
*
* Summary:
*  Initialize component's parameters to the parameters set by user in the 
*  customizer of the component placed onto schematic. Usually called in 
* ADC_DelSig_1_Start().
*  
*
* Parameters:  
*  void
*
* Return: 
*  void 
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
void ADC_DelSig_1_Init(void) 
{
    /* Initialize the registers with default customizer settings for config 1 */
    ADC_DelSig_1_InitConfig(1);

    /* This is only valid if there is an internal clock */
    #if(ADC_DelSig_1_DEFAULT_INTERNAL_CLK)
        ADC_DelSig_1_theACLK_SetMode(CYCLK_DUTY);
    #endif /* ADC_DelSig_1_DEFAULT_INTERNAL_CLK */
    
    /* This is only valid if there is an external charge pump clock */
    #if(ADC_DelSig_1_DEFAULT_CHARGE_PUMP_CLOCK)
        ADC_DelSig_1_Ext_CP_Clk_SetMode(CYCLK_DUTY);
    #endif /* ADC_DelSig_1_DEFAULT_CHARGE_PUMP_CLOCK */

    /* To perform ADC calibration */
    ADC_DelSig_1_GainCompensation(ADC_DelSig_1_DEFAULT_RANGE, 
                                      ADC_DelSig_1_DEFAULT_IDEAL_DEC_GAIN, 
                                      ADC_DelSig_1_DEFAULT_IDEAL_ODDDEC_GAIN, 
                                      ADC_DelSig_1_DEFAULT_RESOLUTION);        
}


/******************************************************************************* 
* Function Name: ADC_DelSig_1_Enable
********************************************************************************
*
* Summary: 
*  Enables the ADC DelSig block operation.
*  
*
* Parameters:  
*  void
*
* Return: 
*  void 
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
void ADC_DelSig_1_Enable(void) 
{
    /* Enable active mode power for ADC */
    ADC_DelSig_1_PWRMGR_DEC_REG |= ADC_DelSig_1_ACT_PWR_DEC_EN;
    ADC_DelSig_1_PWRMGR_DSM_REG |= ADC_DelSig_1_ACT_PWR_DSM_EN;
    
     /* Enable alternative active mode power for ADC */
    ADC_DelSig_1_STBY_PWRMGR_DEC_REG |= ADC_DelSig_1_STBY_PWR_DEC_EN;
    #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
    ADC_DelSig_1_STBY_PWRMGR_DSM_REG |= ADC_DelSig_1_STBY_PWR_DSM_EN;
    #endif /* ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2 */

    /* Disable PRES, Enable power to VCMBUF0, REFBUF0 and REFBUF1, enable PRES */
    #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
        ADC_DelSig_1_RESET_CR4_REG |= ADC_DelSig_1_IGNORE_PRESA1;
        ADC_DelSig_1_RESET_CR5_REG |= ADC_DelSig_1_IGNORE_PRESA2;
    #elif (ADC_DelSig_1_PSOC5_ES1 || ADC_DelSig_1_PSOC3_ES2)
        ADC_DelSig_1_RESET_CR1_REG |= ADC_DelSig_1_DIS_PRES1;
        ADC_DelSig_1_RESET_CR3_REG |= ADC_DelSig_1_DIS_PRES2;
    #endif /* ADC_DelSig_1_PSOC5_ES1 */
    
    ADC_DelSig_1_DSM_CR17_REG |= (ADC_DelSig_1_DSM_EN_BUF_VREF | ADC_DelSig_1_DSM_EN_BUF_VCM);
    ADC_DelSig_1_DSM_REF0_REG |= ADC_DelSig_1_DSM_EN_BUF_VREF_INN;
    
        /* Wait for 3 microseconds */
    CyDelayUs(3);
    
    #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
        ADC_DelSig_1_RESET_CR4_REG &= ~ADC_DelSig_1_IGNORE_PRESA1;
        ADC_DelSig_1_RESET_CR5_REG &= ~ADC_DelSig_1_IGNORE_PRESA2;
        
        ADC_DelSig_1_RESET_CR3_REG = ADC_DelSig_1_EN_PRESA;
    #elif (ADC_DelSig_1_PSOC5_ES1 || ADC_DelSig_1_PSOC3_ES2)
        ADC_DelSig_1_RESET_CR1_REG &= ~ADC_DelSig_1_DIS_PRES1;
        ADC_DelSig_1_RESET_CR3_REG &= ~ADC_DelSig_1_DIS_PRES2;
    #endif /* ADC_DelSig_1_PSOC5_ES1 */
    
    /* Enable negative pumps for DSM  */
    ADC_DelSig_1_PUMP_CR1_REG  |= ( ADC_DelSig_1_PUMP_CR1_CLKSEL | ADC_DelSig_1_PUMP_CR1_FORCE );
 
    /* This is only valid if there is an internal clock */
    #if(ADC_DelSig_1_DEFAULT_INTERNAL_CLK)
        ADC_DelSig_1_PWRMGR_CLK_REG |= ADC_DelSig_1_ACT_PWR_CLK_EN;        
        ADC_DelSig_1_STBY_PWRMGR_CLK_REG |= ADC_DelSig_1_STBY_PWR_CLK_EN;
        
        ADC_DelSig_1_theACLK_Enable();
    #endif /* ADC_DelSig_1_DEFAULT_INTERNAL_CLK */
    
    /* This is only valid if there is an external charge pump clock */
    #if(ADC_DelSig_1_DEFAULT_CHARGE_PUMP_CLOCK)
        ADC_DelSig_1_PWRMGR_CHARGE_PUMP_CLK_REG |= ADC_DelSig_1_ACT_PWR_CHARGE_PUMP_CLK_EN;
        ADC_DelSig_1_STBY_PWRMGR_CHARGE_PUMP_CLK_REG |= ADC_DelSig_1_STBY_PWR_CHARGE_PUMP_CLK_EN;
        
        ADC_DelSig_1_Ext_CP_Clk_Enable();
    #endif /* ADC_DelSig_1_DEFAULT_CHARGE_PUMP_CLOCK */

}


/******************************************************************************* 
* Function Name: ADC_DelSig_1_Start
********************************************************************************
*
* Summary:
*  The start function initializes the Delta Sigma Modulator with the default values, 
*  and sets the power to the given level.  A power level of 0, is the same as executing
*  the stop function.
*
* Parameters:  
*  None
*
* Return: 
*  void 
*
* Global variables:
*  ADC_DelSig_1_initVar:  Used to check the initial configuration,
*  modified when this function is called for the first time.

*
* Reetrance: 
*  No
*
*******************************************************************************/
void ADC_DelSig_1_Start() 
{
    if(ADC_DelSig_1_initVar == 0u)
    {
        ADC_DelSig_1_Init();
        ADC_DelSig_1_initVar = 1u;
    }

    ADC_DelSig_1_Enable();
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_Stop
********************************************************************************
*
* Summary:
*  Stops and powers down ADC to lowest power state.
*
* Parameters:  
*  void
*
* Return: 
*  void 
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
void ADC_DelSig_1_Stop(void) 
{
    /* Stop conversions */
    ADC_DelSig_1_DEC_CR_REG &= ~ADC_DelSig_1_DEC_START_CONV;
    ADC_DelSig_1_DEC_SR_REG |=  ADC_DelSig_1_DEC_INTR_CLEAR;
    
    /* Disable PRES, Disable power to VCMBUF0, REFBUF0 and REFBUF1, enable PRES */
    #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
        ADC_DelSig_1_RESET_CR4_REG |= ADC_DelSig_1_IGNORE_PRESA1;
        ADC_DelSig_1_RESET_CR5_REG |= ADC_DelSig_1_IGNORE_PRESA2;
    #elif (ADC_DelSig_1_PSOC5_ES1 || ADC_DelSig_1_PSOC3_ES2)
        ADC_DelSig_1_RESET_CR1_REG |= ADC_DelSig_1_DIS_PRES1;
        ADC_DelSig_1_RESET_CR3_REG |= ADC_DelSig_1_DIS_PRES2;
    #endif /* ADC_DelSig_1_PSOC5_ES1 */
    
    ADC_DelSig_1_DSM_CR17_REG &= ~(ADC_DelSig_1_DSM_EN_BUF_VREF | ADC_DelSig_1_DSM_EN_BUF_VCM);
    ADC_DelSig_1_DSM_REF0_REG &= ~ADC_DelSig_1_DSM_EN_BUF_VREF_INN;
    
    /* Wait for 3 microseconds. */
    CyDelayUs(3);
    
    #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
        ADC_DelSig_1_RESET_CR4_REG &= ~ADC_DelSig_1_IGNORE_PRESA1;
        ADC_DelSig_1_RESET_CR5_REG &= ~ADC_DelSig_1_IGNORE_PRESA2;
        
        /* Enable the press circuit */
        ADC_DelSig_1_RESET_CR3_REG = ADC_DelSig_1_EN_PRESA;
    #elif (ADC_DelSig_1_PSOC5_ES1 || ADC_DelSig_1_PSOC3_ES2)
        ADC_DelSig_1_RESET_CR1_REG &= ~ADC_DelSig_1_DIS_PRES1;
        ADC_DelSig_1_RESET_CR3_REG &= ~ADC_DelSig_1_DIS_PRES2;
    #endif /* ADC_DelSig_1_PSOC5_ES1 */
    
    /* Disable power to the ADC */
    ADC_DelSig_1_PWRMGR_DSM_REG &= ~ADC_DelSig_1_ACT_PWR_DSM_EN;
    ADC_DelSig_1_PWRMGR_DEC_REG &= ~ADC_DelSig_1_ACT_PWR_DEC_EN;
    
    /* Disable alternative active power to the ADC */
    ADC_DelSig_1_STBY_PWRMGR_DEC_REG &= ~ADC_DelSig_1_STBY_PWR_DEC_EN;
    #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
    ADC_DelSig_1_STBY_PWRMGR_DSM_REG &= ~ADC_DelSig_1_STBY_PWR_DSM_EN;
    #endif /* ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2 */

   /* Disable negative pumps for DSM  */
    ADC_DelSig_1_PUMP_CR1_REG &= ~(ADC_DelSig_1_PUMP_CR1_CLKSEL | ADC_DelSig_1_PUMP_CR1_FORCE );
    
    /* This is only valid if there is an internal clock */
    #if(ADC_DelSig_1_DEFAULT_INTERNAL_CLK)
        ADC_DelSig_1_PWRMGR_CLK_REG &= ~ADC_DelSig_1_ACT_PWR_CLK_EN;
        ADC_DelSig_1_STBY_PWRMGR_CLK_REG &= ~ADC_DelSig_1_STBY_PWR_CLK_EN;
        
        ADC_DelSig_1_theACLK_Disable();
    #endif
    
    /* This is only valid if there is an external charge pump clock */
    #if(ADC_DelSig_1_DEFAULT_CHARGE_PUMP_CLOCK)
        ADC_DelSig_1_PWRMGR_CHARGE_PUMP_CLK_REG &= ~ADC_DelSig_1_ACT_PWR_CHARGE_PUMP_CLK_EN;
        ADC_DelSig_1_STBY_PWRMGR_CHARGE_PUMP_CLK_REG &= ~ADC_DelSig_1_STBY_PWR_CHARGE_PUMP_CLK_EN;
        
        ADC_DelSig_1_Ext_CP_Clk_Disable();
    #endif /* ADC_DelSig_1_DEFAULT_CHARGE_PUMP_CLOCK */    
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_SetBufferGain
********************************************************************************
*
* Summary:
*  Set input buffer range.
*
* Parameters:  
*  gain:  Two bit value to select a gain of 1, 2, 4, or 8.
*
* Return: 
*  void
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
void ADC_DelSig_1_SetBufferGain(uint8 gain) 
{
    uint8 tmpReg;
    tmpReg = ADC_DelSig_1_DSM_BUF1_REG & ~ADC_DelSig_1_DSM_GAIN_MASK;
    tmpReg |= (gain << 2);
    ADC_DelSig_1_DSM_BUF1_REG = tmpReg;
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_SetBufferChop
********************************************************************************
*
* Summary:
*  Sets the Delta Sigma Modulator Buffer chopper mode.
*
* Parameters:  
*  chopen:  If non-zero set the chopper mode.
*  chopFreq:  Chop frequency value.
*
* Return: 
*  void
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
void ADC_DelSig_1_SetBufferChop(uint8 chopen, uint8 chopFreq) 
{
    if(chopen != 0)
    {
        ADC_DelSig_1_DSM_BUF2_REG = (ADC_DelSig_1_DSM_BUF_FCHOP_MASK & chopFreq) | ADC_DelSig_1_DSM_BUF_CHOP_EN;
    }
    else
    {
        ADC_DelSig_1_DSM_BUF2_REG = 0x00;
    }
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_StartConvert
********************************************************************************
*
* Summary:
*  Starts ADC conversion using the given mode.
*
* Parameters:  
*  void
*
* Return: 
*  void 
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
void ADC_DelSig_1_StartConvert(void) 
{
    /* Start the conversion */
    ADC_DelSig_1_DEC_CR_REG |= ADC_DelSig_1_DEC_START_CONV;  
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_StopConvert
********************************************************************************
*
* Summary:
*  Starts ADC conversion using the given mode.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  ADC_DelSig_1_convDone:  Modified when conversion is complete for single
*  sample mode with resolution is above 16.
*
* Reentrance: 
*  No
*
*******************************************************************************/
void ADC_DelSig_1_StopConvert(void)
{
    /* Stop all conversions */
    ADC_DelSig_1_DEC_CR_REG &= ~ADC_DelSig_1_DEC_START_CONV; 
    
    /* Software flag for checking conversion complete or not. Will be used when
       resolution is above 16 bits and conversion mode is single sample */
    ADC_DelSig_1_convDone = 1u;
}

/*******************************************************************************
* Function Name: ADC_DelSig_1_IsEndConversion
********************************************************************************
*
* Summary:
*  Queries the ADC to see if conversion is complete
*
* Parameters:  
*  wMode:  Wait mode, 0 if return with answer immediately.
*                     1 if wait until conversion is complete, then return.
*
* Return: 
*  uint8 status:  0 =>  Conversion not complete.
*                 1 =>  Conversion complete.
*
* Global variables:
*  ADC_DelSig_1_convDone:  Used to check whether conversion is complete
*  or not for single sample mode with resolution is above 16
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
uint8 ADC_DelSig_1_IsEndConversion(uint8 wMode) 
{
    uint8 status;
        
    /* Check for stop convert if conversion mode is Single Sample with resolution above 16 bit */
    if(ADC_DelSig_1_stopConversion == 1)
    {
        do
        {
            status = ADC_DelSig_1_convDone;
        } while((status != ADC_DelSig_1_DEC_CONV_DONE) && (wMode == ADC_DelSig_1_WAIT_FOR_RESULT));
    }
    else
    {
        do 
        {
            status = ADC_DelSig_1_DEC_SR_REG & ADC_DelSig_1_DEC_CONV_DONE;
        } while((status != ADC_DelSig_1_DEC_CONV_DONE) && (wMode == ADC_DelSig_1_WAIT_FOR_RESULT));
    }
    return(status);
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_GetResult8
********************************************************************************
*
* Summary:
*  Returns an 8-bit result or the LSB of the last conversion.
*
* Parameters:  
*  void
*
* Return: 
*  int8:  ADC result.
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
int8 ADC_DelSig_1_GetResult8( void ) 
{
    return( ADC_DelSig_1_DEC_SAMP_REG );
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_GetResult16
********************************************************************************
*
* Summary:
*  Returns a 16-bit result from the last ADC conversion.
*
* Parameters:  
*   void
*
* Return: 
*  int16:  ADC result.
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
int16 ADC_DelSig_1_GetResult16(void) 
{
    uint16 result;
    result = ADC_DelSig_1_DEC_SAMPM_REG ;
    result = (result << 8 ) | ADC_DelSig_1_DEC_SAMP_REG;
    return( result );
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_GetResult32
********************************************************************************
*
* Summary:
*  Returns an 32-bit result from ADC
*
* Parameters:  
*  void
*
* Return: 
*  int32:  ADC result.
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
int32 ADC_DelSig_1_GetResult32(void) 
{
    uint32 result;

    result = (int8) ADC_DelSig_1_DEC_SAMPH_REG;
    result = (result << 8) | ADC_DelSig_1_DEC_SAMPM_REG;
    result = (result << 8) | ADC_DelSig_1_DEC_SAMP_REG;
    return( result );
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_SetOffset
********************************************************************************
*
* Summary:
*  This function sets the offset for voltage readings.
*
* Parameters:  
*  int32:  offset  Offset in counts
*
* Return: 
*  void
*
* Global variables:
*  ADC_DelSig_1_Offset:  Modified to set the user provided offset. This 
*  variable is used for offset calibration purpose.
*  Affects the ADC_DelSig_1_CountsTo_Volts, 
*  ADC_DelSig_1_CountsTo_mVolts, ADC_DelSig_1_CountsTo_uVolts functions 
*  by subtracting the given offset. 
*
* Reentrance: 
*  No
*
*******************************************************************************/
void ADC_DelSig_1_SetOffset(int32 offset)
{
 
    ADC_DelSig_1_Offset = offset;
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_SetGain
********************************************************************************
*
* Summary:
*  This function sets the ADC gain in counts per volt.
*
* Parameters:  
*  int32:  offset  Offset in counts
*
* Return: 
*  void 
*
* Global variables:
*  ADC_DelSig_1_CountsPerVolt:  modified to set the ADC gain in counts per volt.
*
* Reentrance: 
*  No
*
*******************************************************************************/
void ADC_DelSig_1_SetGain(int32 adcGain)
{
 
    ADC_DelSig_1_CountsPerVolt = adcGain;
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_CountsTo_mVolts
********************************************************************************
*
* Summary:
*  This function converts ADC counts to mVolts.
*
* Parameters:  
*  int32:  adcCounts   Reading from ADC.
*
* Return: 
*  int32:  Result in mVolts
*
* Global variables:
*  ADC_DelSig_1_CountsPerVolt:  used to convert ADC counts to mVolts.
*  ADC_DelSig_1_Offset:  Used as the offset while converting ADC counts to mVolts.
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
int16 ADC_DelSig_1_CountsTo_mVolts( int32 adcCounts) 
{

    int32 mVolts = 0;
    int32 A, B;

    /* Subtract ADC offset */
    adcCounts -= ADC_DelSig_1_Offset;

    if(ADC_DelSig_1_DEFAULT_RESOLUTION < 17)
    {
        A = 1000;
        B = 1;
    }
    else
    {
        A = 100;
        B = 10;
    }

    mVolts = ((A * adcCounts) / ((int32)ADC_DelSig_1_CountsPerVolt/B)) ;   

    return( (int16)mVolts );
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_CountsTo_Volts
********************************************************************************
*
* Summary:
*  This function converts ADC counts to Volts
*
* Parameters:  
*  int32:  adcCounts   Reading from ADC.
*
* Return: 
*  float:  Result in Volts
*
* Global variables:
*  ADC_DelSig_1_CountsPerVolt:  used to convert to Volts.
*  ADC_DelSig_1_Offset:  Used as the offset while converting ADC counts to Volts.
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
float ADC_DelSig_1_CountsTo_Volts( int32 adcCounts) 
{

    float Volts = 0;

    /* Subtract ADC offset */
    adcCounts -= ADC_DelSig_1_Offset;

    Volts = (float)adcCounts / (float)ADC_DelSig_1_CountsPerVolt;   
    
    return( Volts );
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_CountsTo_uVolts
********************************************************************************
*
* Summary:
*  This function converts ADC counts to uVolts
*
* Parameters:  
*  int32:  adcCounts   Reading from ADC.
*
* Return: 
*  int32:  Result in uVolts
*
* Global variables:
*  ADC_DelSig_1_CountsPerVolt:  used to convert ADC counts to mVolts.
*  ADC_DelSig_1_Offset:  Used as the offset while converting ADC counts to mVolts.
*
* Theory: 
* Care must be taken to not exceed the maximum value for a 32 bit signed
* number in the conversion to uVolts and at the same time not loose 
* resolution.
*
* uVolts = ((A * adcCounts) / ((int32)ADC_DelSig_1_CountsPerVolt/B)) ;   
*
*  Resolution       A           B
*   8 - 11      1,000,000         1
*  12 - 14        100,000        10
*  15 - 17         10,000       100
*  18 - 20           1000      1000
*
* Reentrance:
*  Yes
*
*******************************************************************************/
int32 ADC_DelSig_1_CountsTo_uVolts( int32 adcCounts) 
{

    int32 uVolts = 0;
    int32 A, B;
    
    if(ADC_DelSig_1_DEFAULT_RESOLUTION < 12)
    {
        A = 1000000;
        B = 1;
    }
    else if(ADC_DelSig_1_DEFAULT_RESOLUTION < 15)
    {
        A = 100000;
        B = 10;
    }
    else if(ADC_DelSig_1_DEFAULT_RESOLUTION < 18)
    {
        A = 10000;
        B = 100;
    }
    else
    {
        A = 1000;
        B = 1000;
    }

    /* Subtract ADC offset */
    adcCounts -= ADC_DelSig_1_Offset;

    uVolts = ((A * adcCounts) / ((int32)ADC_DelSig_1_CountsPerVolt/B)) ;   
  
    return( uVolts );
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_IRQ_Start(void)
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it. The IRQ_Start() API is included to 
*  support legacy code. The routine has been replaced by the library source 
*  code for interrupts. IRQ_Start() should not be used in new designs.
* 
* Parameters:  
*   void.
*
* Return:
*   void.
*
* Reentrance:
*  No
*
*******************************************************************************/
void ADC_DelSig_1_IRQ_Start(void) 
{
    /* For all we know the interrupt is active. */
    CyIntDisable(ADC_DelSig_1_IRQ__INTC_NUMBER );

    /* Set the ISR to point to the ADC_DelSig_1_IRQ Interrupt. */
    CyIntSetVector(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_ISR1);

    /* Set the priority. */
    CyIntSetPriority(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_IRQ_INTC_PRIOR_NUMBER);

    /* Enable interrupt. */
    CyIntEnable(ADC_DelSig_1_IRQ__INTC_NUMBER);
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_InitConfig(uint8 config)
********************************************************************************
*
* Summary:
*  Initializes all registers based on customizer settings
*
* Parameters:  
*   void
*
* Return: 
*  void
*
* Global variables:
*  ADC_DelSig_1_initVar:  used to set the common registers for the first time.
*  ADC_DelSig_1_CountsPerVolt:  Used to set the default counts per volt value.
*
* Reentrance: 
*  No
*
*******************************************************************************/
void ADC_DelSig_1_InitConfig(uint8 config)
{
    ADC_DelSig_1_stopConversion = 0;
    switch (config)
    {
        case 1:
            ADC_DelSig_1_DEC_CR_REG      = ADC_DelSig_1_DFLT_DEC_CR;      
            ADC_DelSig_1_DEC_SR_REG      = ADC_DelSig_1_DFLT_DEC_SR;      
            ADC_DelSig_1_DEC_SHIFT1_REG  = ADC_DelSig_1_DFLT_DEC_SHIFT1;  
            ADC_DelSig_1_DEC_SHIFT2_REG  = ADC_DelSig_1_DFLT_DEC_SHIFT2;  
            ADC_DelSig_1_DEC_DR2_REG     = ADC_DelSig_1_DFLT_DEC_DR2;     
            ADC_DelSig_1_DEC_DR2H_REG    = ADC_DelSig_1_DFLT_DEC_DR2H;    
            ADC_DelSig_1_DEC_DR1_REG     = ADC_DelSig_1_DFLT_DEC_DR1;     
            ADC_DelSig_1_DEC_OCOR_REG    = ADC_DelSig_1_DFLT_DEC_OCOR;    
            ADC_DelSig_1_DEC_OCORM_REG   = ADC_DelSig_1_DFLT_DEC_OCORM;   
            ADC_DelSig_1_DEC_OCORH_REG   = ADC_DelSig_1_DFLT_DEC_OCORH;   
            ADC_DelSig_1_DEC_COHER_REG   = ADC_DelSig_1_DFLT_DEC_COHER;   
         
            ADC_DelSig_1_DSM_CR0_REG     = ADC_DelSig_1_DFLT_DSM_CR0;     
            ADC_DelSig_1_DSM_CR1_REG     = ADC_DelSig_1_DFLT_DSM_CR1;     
            ADC_DelSig_1_DSM_CR2_REG     = ADC_DelSig_1_DFLT_DSM_CR2;     
            ADC_DelSig_1_DSM_CR3_REG     = ADC_DelSig_1_DFLT_DSM_CR3;     
            ADC_DelSig_1_DSM_CR4_REG     = ADC_DelSig_1_DFLT_DSM_CR4;     
            ADC_DelSig_1_DSM_CR5_REG     = ADC_DelSig_1_DFLT_DSM_CR5;     
            ADC_DelSig_1_DSM_CR6_REG     = ADC_DelSig_1_DFLT_DSM_CR6;     
            ADC_DelSig_1_DSM_CR7_REG     = ADC_DelSig_1_DFLT_DSM_CR7;     
            ADC_DelSig_1_DSM_CR8_REG     = ADC_DelSig_1_DFLT_DSM_CR8;     
            ADC_DelSig_1_DSM_CR9_REG     = ADC_DelSig_1_DFLT_DSM_CR9;     
            ADC_DelSig_1_DSM_CR10_REG    = ADC_DelSig_1_DFLT_DSM_CR10;    
            ADC_DelSig_1_DSM_CR11_REG    = ADC_DelSig_1_DFLT_DSM_CR11;    
            ADC_DelSig_1_DSM_CR12_REG    = ADC_DelSig_1_DFLT_DSM_CR12;    
            ADC_DelSig_1_DSM_CR13_REG    = ADC_DelSig_1_DFLT_DSM_CR13;    
            ADC_DelSig_1_DSM_CR14_REG    = ADC_DelSig_1_DFLT_DSM_CR14;    
            ADC_DelSig_1_DSM_CR15_REG    = ADC_DelSig_1_DFLT_DSM_CR15;    
            ADC_DelSig_1_DSM_CR16_REG    = ADC_DelSig_1_DFLT_DSM_CR16;    
            ADC_DelSig_1_DSM_CR17_REG    = ADC_DelSig_1_DFLT_DSM_CR17;    
            ADC_DelSig_1_DSM_REF0_REG    = ADC_DelSig_1_DFLT_DSM_REF0;    
            ADC_DelSig_1_DSM_REF2_REG    = ADC_DelSig_1_DFLT_DSM_REF2;    
            ADC_DelSig_1_DSM_REF3_REG    = ADC_DelSig_1_DFLT_DSM_REF3;    
        
            ADC_DelSig_1_DSM_BUF0_REG    = ADC_DelSig_1_DFLT_DSM_BUF0;    
            ADC_DelSig_1_DSM_BUF1_REG    = ADC_DelSig_1_DFLT_DSM_BUF1;    
            ADC_DelSig_1_DSM_BUF2_REG    = ADC_DelSig_1_DFLT_DSM_BUF2;    
            ADC_DelSig_1_DSM_BUF3_REG    = ADC_DelSig_1_DFLT_DSM_BUF3;   
            
            /* To select either Vssa or Vref to -ve input of DSM depending on the input
               range selected.
            */
        
            #if(ADC_DelSig_1_DEFAULT_INPUT_MODE)
                #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
                    #if (ADC_DelSig_1_DEFAULT_RANGE == ADC_DelSig_1_IR_VSSA_TO_2VREF)
                        ADC_DelSig_1_AMux_Select(1);
                    #else
                        ADC_DelSig_1_AMux_Select(0);
                    #endif /* ADC_DelSig_1_DEFAULT_RANGE == ADC_DelSig_1_IR_VSSA_TO_2VREF) */
                #elif (ADC_DelSig_1_PSOC3_ES2 || ADC_DelSig_1_PSOC5_ES1)
                    ADC_DelSig_1_AMux_Select(0);
                #endif /* ADC_DelSig_1_DEFAULT_RANGE == ADC_DelSig_1_IR_VSSA_TO_2VREF) */
            #endif /* ADC_DelSig_1_DEFAULT_INPUT_MODE */
                        
            if(ADC_DelSig_1_initVar == 0u)
            {
                ADC_DelSig_1_DSM_DEM0_REG    = ADC_DelSig_1_DFLT_DSM_DEM0;    
                ADC_DelSig_1_DSM_DEM1_REG    = ADC_DelSig_1_DFLT_DSM_DEM1;    
                ADC_DelSig_1_DSM_MISC_REG    = ADC_DelSig_1_DFLT_DSM_MISC;    
                ADC_DelSig_1_DSM_CLK_REG    |= ADC_DelSig_1_DFLT_DSM_CLK; 
                ADC_DelSig_1_DSM_REF1_REG    = ADC_DelSig_1_DFLT_DSM_REF1;    
             
                ADC_DelSig_1_DSM_OUT0_REG    = ADC_DelSig_1_DFLT_DSM_OUT0;    
                ADC_DelSig_1_DSM_OUT1_REG    = ADC_DelSig_1_DFLT_DSM_OUT1;   
            }
             
             /* Set the Conversion stop if resolution is above 16 bit and conversion 
               mode is Single sample */
            #if(ADC_DelSig_1_DEFAULT_RESOLUTION > 16 && \
                ADC_DelSig_1_DEFAULT_CONV_MODE == ADC_DelSig_1_MODE_SINGLE_SAMPLE) 
                ADC_DelSig_1_stopConversion = 1;
            #endif
            ADC_DelSig_1_CountsPerVolt = (uint32)ADC_DelSig_1_DFLT_COUNTS_PER_VOLT;
              
            /* Start and set interrupt vector */
            CyIntSetPriority(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_IRQ_INTC_PRIOR_NUMBER);
            CyIntSetVector(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_ISR1 );
            CyIntEnable(ADC_DelSig_1_IRQ__INTC_NUMBER);

            break;
          
        case 2:
            ADC_DelSig_1_DEC_CR_REG      = ADC_DelSig_1_DFLT_DEC_CR_CFG2;      
            ADC_DelSig_1_DEC_SR_REG      = ADC_DelSig_1_DFLT_DEC_SR_CFG2;      
            ADC_DelSig_1_DEC_SHIFT1_REG  = ADC_DelSig_1_DFLT_DEC_SHIFT1_CFG2;  
            ADC_DelSig_1_DEC_SHIFT2_REG  = ADC_DelSig_1_DFLT_DEC_SHIFT2_CFG2;  
            ADC_DelSig_1_DEC_DR2_REG     = ADC_DelSig_1_DFLT_DEC_DR2_CFG2;     
            ADC_DelSig_1_DEC_DR2H_REG    = ADC_DelSig_1_DFLT_DEC_DR2H_CFG2;    
            ADC_DelSig_1_DEC_DR1_REG     = ADC_DelSig_1_DFLT_DEC_DR1_CFG2;     
            ADC_DelSig_1_DEC_OCOR_REG    = ADC_DelSig_1_DFLT_DEC_OCOR_CFG2;    
            ADC_DelSig_1_DEC_OCORM_REG   = ADC_DelSig_1_DFLT_DEC_OCORM_CFG2;   
            ADC_DelSig_1_DEC_OCORH_REG   = ADC_DelSig_1_DFLT_DEC_OCORH_CFG2;   
            ADC_DelSig_1_DEC_COHER_REG   = ADC_DelSig_1_DFLT_DEC_COHER_CFG2;   
        
            ADC_DelSig_1_DSM_CR0_REG     = ADC_DelSig_1_DFLT_DSM_CR0_CFG2;     
            ADC_DelSig_1_DSM_CR1_REG     = ADC_DelSig_1_DFLT_DSM_CR1_CFG2;     
            ADC_DelSig_1_DSM_CR2_REG     = ADC_DelSig_1_DFLT_DSM_CR2_CFG2;     
            ADC_DelSig_1_DSM_CR3_REG     = ADC_DelSig_1_DFLT_DSM_CR3_CFG2;     
            ADC_DelSig_1_DSM_CR4_REG     = ADC_DelSig_1_DFLT_DSM_CR4_CFG2;     
            ADC_DelSig_1_DSM_CR5_REG     = ADC_DelSig_1_DFLT_DSM_CR5_CFG2;     
            ADC_DelSig_1_DSM_CR6_REG     = ADC_DelSig_1_DFLT_DSM_CR6_CFG2;     
            ADC_DelSig_1_DSM_CR7_REG     = ADC_DelSig_1_DFLT_DSM_CR7_CFG2;     
            ADC_DelSig_1_DSM_CR8_REG     = ADC_DelSig_1_DFLT_DSM_CR8_CFG2;     
            ADC_DelSig_1_DSM_CR9_REG     = ADC_DelSig_1_DFLT_DSM_CR9_CFG2;     
            ADC_DelSig_1_DSM_CR10_REG    = ADC_DelSig_1_DFLT_DSM_CR10_CFG2;    
            ADC_DelSig_1_DSM_CR11_REG    = ADC_DelSig_1_DFLT_DSM_CR11_CFG2;    
            ADC_DelSig_1_DSM_CR12_REG    = ADC_DelSig_1_DFLT_DSM_CR12_CFG2;    
            ADC_DelSig_1_DSM_CR13_REG    = ADC_DelSig_1_DFLT_DSM_CR13_CFG2;    
            ADC_DelSig_1_DSM_CR14_REG    = ADC_DelSig_1_DFLT_DSM_CR14_CFG2;    
            ADC_DelSig_1_DSM_CR15_REG    = ADC_DelSig_1_DFLT_DSM_CR15_CFG2;    
            ADC_DelSig_1_DSM_CR16_REG    = ADC_DelSig_1_DFLT_DSM_CR16_CFG2;    
            ADC_DelSig_1_DSM_CR17_REG    = ADC_DelSig_1_DFLT_DSM_CR17_CFG2;    
            ADC_DelSig_1_DSM_REF0_REG    = ADC_DelSig_1_DFLT_DSM_REF0_CFG2;    
            ADC_DelSig_1_DSM_REF2_REG    = ADC_DelSig_1_DFLT_DSM_REF2_CFG2;    
            ADC_DelSig_1_DSM_REF3_REG    = ADC_DelSig_1_DFLT_DSM_REF3_CFG2;    
        
            ADC_DelSig_1_DSM_BUF0_REG    = ADC_DelSig_1_DFLT_DSM_BUF0_CFG2;    
            ADC_DelSig_1_DSM_BUF1_REG    = ADC_DelSig_1_DFLT_DSM_BUF1_CFG2;    
            ADC_DelSig_1_DSM_BUF2_REG    = ADC_DelSig_1_DFLT_DSM_BUF2_CFG2;    
            ADC_DelSig_1_DSM_BUF3_REG    = ADC_DelSig_1_DFLT_DSM_BUF3_CFG2; 
            
            /* To select either Vssa or Vref to -ve input of DSM depending on the input
               range selected.
            */
            
            #if(ADC_DelSig_1_DEFAULT_INPUT_MODE)
                #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
                    #if (ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG2 == ADC_DelSig_1_IR_VSSA_TO_2VREF)
                        ADC_DelSig_1_AMux_Select(1);
                    #else
                        ADC_DelSig_1_AMux_Select(0);
                    #endif /* ADC_DelSig_1_DEFAULT_RANGE == ADC_DelSig_1_IR_VSSA_TO_2VREF) */
                #elif (ADC_DelSig_1_PSOC3_ES2 || ADC_DelSig_1_PSOC5_ES1)
                    ADC_DelSig_1_AMux_Select(0);
                #endif /* ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG2 == ADC_DelSig_1_IR_VSSA_TO_2VREF) */
            #endif /* ADC_DelSig_1_DEFAULT_INPUT_MODE */
            
            /* Set the Conversion stop if resolution is above 16 bit and conversion 
               mode is Single sample */
            #if(ADC_DelSig_1_DEFAULT_RESOLUTION_CFG2 > 16 && \
                ADC_DelSig_1_CONVMODE_CFG2 == ADC_DelSig_1_MODE_SINGLE_SAMPLE) 
                ADC_DelSig_1_stopConversion = 1;
            #endif
            
            ADC_DelSig_1_CountsPerVolt = (uint32)ADC_DelSig_1_DFLT_COUNTS_PER_VOLT_CFG2;
            
            /* Start and set interrupt vector */
            CyIntSetPriority(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_IRQ_INTC_PRIOR_NUMBER);
            CyIntSetVector(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_ISR2 );
            CyIntEnable(ADC_DelSig_1_IRQ__INTC_NUMBER);

            break;
          
        case 3:
            ADC_DelSig_1_DEC_CR_REG      = ADC_DelSig_1_DFLT_DEC_CR_CFG3;      
            ADC_DelSig_1_DEC_SR_REG      = ADC_DelSig_1_DFLT_DEC_SR_CFG3;      
            ADC_DelSig_1_DEC_SHIFT1_REG  = ADC_DelSig_1_DFLT_DEC_SHIFT1_CFG3;  
            ADC_DelSig_1_DEC_SHIFT2_REG  = ADC_DelSig_1_DFLT_DEC_SHIFT2_CFG3;  
            ADC_DelSig_1_DEC_DR2_REG     = ADC_DelSig_1_DFLT_DEC_DR2_CFG3;     
            ADC_DelSig_1_DEC_DR2H_REG    = ADC_DelSig_1_DFLT_DEC_DR2H_CFG3;    
            ADC_DelSig_1_DEC_DR1_REG     = ADC_DelSig_1_DFLT_DEC_DR1_CFG3;     
            ADC_DelSig_1_DEC_OCOR_REG    = ADC_DelSig_1_DFLT_DEC_OCOR_CFG3;    
            ADC_DelSig_1_DEC_OCORM_REG   = ADC_DelSig_1_DFLT_DEC_OCORM_CFG3;   
            ADC_DelSig_1_DEC_OCORH_REG   = ADC_DelSig_1_DFLT_DEC_OCORH_CFG3;   
            ADC_DelSig_1_DEC_COHER_REG   = ADC_DelSig_1_DFLT_DEC_COHER_CFG3;   
         
            ADC_DelSig_1_DSM_CR0_REG     = ADC_DelSig_1_DFLT_DSM_CR0_CFG3;     
            ADC_DelSig_1_DSM_CR1_REG     = ADC_DelSig_1_DFLT_DSM_CR1_CFG3;     
            ADC_DelSig_1_DSM_CR2_REG     = ADC_DelSig_1_DFLT_DSM_CR2_CFG3;     
            ADC_DelSig_1_DSM_CR3_REG     = ADC_DelSig_1_DFLT_DSM_CR3_CFG3;     
            ADC_DelSig_1_DSM_CR4_REG     = ADC_DelSig_1_DFLT_DSM_CR4_CFG3;     
            ADC_DelSig_1_DSM_CR5_REG     = ADC_DelSig_1_DFLT_DSM_CR5_CFG3;     
            ADC_DelSig_1_DSM_CR6_REG     = ADC_DelSig_1_DFLT_DSM_CR6_CFG3;     
            ADC_DelSig_1_DSM_CR7_REG     = ADC_DelSig_1_DFLT_DSM_CR7_CFG3;     
            ADC_DelSig_1_DSM_CR8_REG     = ADC_DelSig_1_DFLT_DSM_CR8_CFG3;     
            ADC_DelSig_1_DSM_CR9_REG     = ADC_DelSig_1_DFLT_DSM_CR9_CFG3;     
            ADC_DelSig_1_DSM_CR10_REG    = ADC_DelSig_1_DFLT_DSM_CR10_CFG3;    
            ADC_DelSig_1_DSM_CR11_REG    = ADC_DelSig_1_DFLT_DSM_CR11_CFG3;    
            ADC_DelSig_1_DSM_CR12_REG    = ADC_DelSig_1_DFLT_DSM_CR12_CFG3;    
            ADC_DelSig_1_DSM_CR13_REG    = ADC_DelSig_1_DFLT_DSM_CR13_CFG3;    
            ADC_DelSig_1_DSM_CR14_REG    = ADC_DelSig_1_DFLT_DSM_CR14_CFG3;    
            ADC_DelSig_1_DSM_CR15_REG    = ADC_DelSig_1_DFLT_DSM_CR15_CFG3;    
            ADC_DelSig_1_DSM_CR16_REG    = ADC_DelSig_1_DFLT_DSM_CR16_CFG3;    
            ADC_DelSig_1_DSM_CR17_REG    = ADC_DelSig_1_DFLT_DSM_CR17_CFG3;    
            ADC_DelSig_1_DSM_REF0_REG    = ADC_DelSig_1_DFLT_DSM_REF0_CFG3;    
            ADC_DelSig_1_DSM_REF2_REG    = ADC_DelSig_1_DFLT_DSM_REF2_CFG3;    
            ADC_DelSig_1_DSM_REF3_REG    = ADC_DelSig_1_DFLT_DSM_REF3_CFG3;    
        
            ADC_DelSig_1_DSM_BUF0_REG    = ADC_DelSig_1_DFLT_DSM_BUF0_CFG3;    
            ADC_DelSig_1_DSM_BUF1_REG    = ADC_DelSig_1_DFLT_DSM_BUF1_CFG3;    
            ADC_DelSig_1_DSM_BUF2_REG    = ADC_DelSig_1_DFLT_DSM_BUF2_CFG3;    
            ADC_DelSig_1_DSM_BUF3_REG    = ADC_DelSig_1_DFLT_DSM_BUF3_CFG3;   
            
            /* To select either Vssa or Vref to -ve input of DSM depending on the input
               range selected.
            */
            
            #if(ADC_DelSig_1_DEFAULT_INPUT_MODE)
                #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
                    #if (ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG3 == ADC_DelSig_1_IR_VSSA_TO_2VREF)
                        ADC_DelSig_1_AMux_Select(1);
                    #else
                        ADC_DelSig_1_AMux_Select(0);
                    #endif /* ADC_DelSig_1_DEFAULT_RANGE == ADC_DelSig_1_IR_VSSA_TO_2VREF) */
                #elif (ADC_DelSig_1_PSOC3_ES2 || ADC_DelSig_1_PSOC5_ES1)
                    ADC_DelSig_1_AMux_Select(0);
                #endif /* ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG3 == ADC_DelSig_1_IR_VSSA_TO_2VREF) */
            #endif /* ADC_DelSig_1_DEFAULT_INPUT_MODE */
                       
            /* Set the Conversion stop if resolution is above 16 bit and conversion 
               mode is Single sample */
            #if(ADC_DelSig_1_DEFAULT_RESOLUTION_CFG3 > 16 && \
                ADC_DelSig_1_CONVMODE_CFG3 == ADC_DelSig_1_MODE_SINGLE_SAMPLE) 
                ADC_DelSig_1_stopConversion = 1;
            #endif
            
            ADC_DelSig_1_CountsPerVolt = (uint32)ADC_DelSig_1_DFLT_COUNTS_PER_VOLT_CFG3;
            
            /* Start and set interrupt vector */
            CyIntSetPriority(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_IRQ_INTC_PRIOR_NUMBER);
            CyIntSetVector(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_ISR3 );
            CyIntEnable(ADC_DelSig_1_IRQ__INTC_NUMBER);
            
            break;
          
        case 4:
            ADC_DelSig_1_DEC_CR_REG      = ADC_DelSig_1_DFLT_DEC_CR_CFG4;      
            ADC_DelSig_1_DEC_SR_REG      = ADC_DelSig_1_DFLT_DEC_SR_CFG4;      
            ADC_DelSig_1_DEC_SHIFT1_REG  = ADC_DelSig_1_DFLT_DEC_SHIFT1_CFG4;  
            ADC_DelSig_1_DEC_SHIFT2_REG  = ADC_DelSig_1_DFLT_DEC_SHIFT2_CFG4;  
            ADC_DelSig_1_DEC_DR2_REG     = ADC_DelSig_1_DFLT_DEC_DR2_CFG4;     
            ADC_DelSig_1_DEC_DR2H_REG    = ADC_DelSig_1_DFLT_DEC_DR2H_CFG4;    
            ADC_DelSig_1_DEC_DR1_REG     = ADC_DelSig_1_DFLT_DEC_DR1_CFG4;     
            ADC_DelSig_1_DEC_OCOR_REG    = ADC_DelSig_1_DFLT_DEC_OCOR_CFG4;    
            ADC_DelSig_1_DEC_OCORM_REG   = ADC_DelSig_1_DFLT_DEC_OCORM_CFG4;   
            ADC_DelSig_1_DEC_OCORH_REG   = ADC_DelSig_1_DFLT_DEC_OCORH_CFG4;   
            ADC_DelSig_1_DEC_COHER_REG   = ADC_DelSig_1_DFLT_DEC_COHER_CFG4;   
         
            ADC_DelSig_1_DSM_CR0_REG     = ADC_DelSig_1_DFLT_DSM_CR0_CFG4;     
            ADC_DelSig_1_DSM_CR1_REG     = ADC_DelSig_1_DFLT_DSM_CR1_CFG4;     
            ADC_DelSig_1_DSM_CR2_REG     = ADC_DelSig_1_DFLT_DSM_CR2_CFG4;     
            ADC_DelSig_1_DSM_CR3_REG     = ADC_DelSig_1_DFLT_DSM_CR3_CFG4;     
            ADC_DelSig_1_DSM_CR4_REG     = ADC_DelSig_1_DFLT_DSM_CR4_CFG4;     
            ADC_DelSig_1_DSM_CR5_REG     = ADC_DelSig_1_DFLT_DSM_CR5_CFG4;     
            ADC_DelSig_1_DSM_CR6_REG     = ADC_DelSig_1_DFLT_DSM_CR6_CFG4;     
            ADC_DelSig_1_DSM_CR7_REG     = ADC_DelSig_1_DFLT_DSM_CR7_CFG4;     
            ADC_DelSig_1_DSM_CR8_REG     = ADC_DelSig_1_DFLT_DSM_CR8_CFG4;     
            ADC_DelSig_1_DSM_CR9_REG     = ADC_DelSig_1_DFLT_DSM_CR9_CFG4;     
            ADC_DelSig_1_DSM_CR10_REG    = ADC_DelSig_1_DFLT_DSM_CR10_CFG4;    
            ADC_DelSig_1_DSM_CR11_REG    = ADC_DelSig_1_DFLT_DSM_CR11_CFG4;    
            ADC_DelSig_1_DSM_CR12_REG    = ADC_DelSig_1_DFLT_DSM_CR12_CFG4;    
            ADC_DelSig_1_DSM_CR13_REG    = ADC_DelSig_1_DFLT_DSM_CR13_CFG4;    
            ADC_DelSig_1_DSM_CR14_REG    = ADC_DelSig_1_DFLT_DSM_CR14_CFG4;    
            ADC_DelSig_1_DSM_CR15_REG    = ADC_DelSig_1_DFLT_DSM_CR15_CFG4;    
            ADC_DelSig_1_DSM_CR16_REG    = ADC_DelSig_1_DFLT_DSM_CR16_CFG4;    
            ADC_DelSig_1_DSM_CR17_REG    = ADC_DelSig_1_DFLT_DSM_CR17_CFG4;    
            ADC_DelSig_1_DSM_REF0_REG    = ADC_DelSig_1_DFLT_DSM_REF0_CFG4;    
            ADC_DelSig_1_DSM_REF2_REG    = ADC_DelSig_1_DFLT_DSM_REF2_CFG4;    
            ADC_DelSig_1_DSM_REF3_REG    = ADC_DelSig_1_DFLT_DSM_REF3_CFG4;    
        
            ADC_DelSig_1_DSM_BUF0_REG    = ADC_DelSig_1_DFLT_DSM_BUF0_CFG4;    
            ADC_DelSig_1_DSM_BUF1_REG    = ADC_DelSig_1_DFLT_DSM_BUF1_CFG4;    
            ADC_DelSig_1_DSM_BUF2_REG    = ADC_DelSig_1_DFLT_DSM_BUF2_CFG4;    
            ADC_DelSig_1_DSM_BUF3_REG    = ADC_DelSig_1_DFLT_DSM_BUF3_CFG4;   
            
            /* To select either Vssa or Vref to -ve input of DSM depending on the input
               range selected.
            */
            
            #if(ADC_DelSig_1_DEFAULT_INPUT_MODE)
                #if (ADC_DelSig_1_PSOC3_ES3 || ADC_DelSig_1_PSOC5_ES2)
                    #if (ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG4 == ADC_DelSig_1_IR_VSSA_TO_2VREF)
                        ADC_DelSig_1_AMux_Select(1);
                    #else
                        ADC_DelSig_1_AMux_Select(0);
                    #endif /* ADC_DelSig_1_DEFAULT_RANGE == ADC_DelSig_1_IR_VSSA_TO_2VREF) */
                #elif (ADC_DelSig_1_PSOC3_ES2 || ADC_DelSig_1_PSOC5_ES1)
                    ADC_DelSig_1_AMux_Select(0);
                #endif /* ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG4 == ADC_DelSig_1_IR_VSSA_TO_2VREF) */
            #endif /* ADC_DelSig_1_DEFAULT_INPUT_MODE */
                       
            /* Set the Conversion stop if resolution is above 16 bit and conversion 
               mode is Single sample */
            #if(ADC_DelSig_1_DEFAULT_RESOLUTION_CFG4 > 16 && \
                ADC_DelSig_1_CONVMODE_CFG4 == ADC_DelSig_1_MODE_SINGLE_SAMPLE) 
                ADC_DelSig_1_stopConversion = 1;
            #endif
            
            ADC_DelSig_1_CountsPerVolt = (uint32)ADC_DelSig_1_DFLT_COUNTS_PER_VOLT_CFG4;

            /* Start and set interrupt vector */
            CyIntSetPriority(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_IRQ_INTC_PRIOR_NUMBER);
            CyIntSetVector(ADC_DelSig_1_IRQ__INTC_NUMBER, ADC_DelSig_1_ISR4 );
            CyIntEnable(ADC_DelSig_1_IRQ__INTC_NUMBER);
            
            break;
    }
}


/*******************************************************************************
* Function Name: ADC_DelSig_1_RoundValue(double busClockFreq, double clockFreq)
********************************************************************************
*
* Summary:
*  Takes the float value and rounds it to an integer value.
*
* Parameters:  
*  value:  float value which is to be converted to integer.
*
* Return: 
*  uint16: rounded integer value.
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
uint16 ADC_DelSig_1_RoundValue(double busClockFreq, double clockFreq) \
                                  
{
     uint16 x;
     double divider;
     
     divider = busClockFreq / clockFreq;
     if ((divider - (uint16)divider) >= 0.5)
     {
         x = (uint16)divider + 1;
     }
     else
     {
         x = (uint16)divider;
     }
     return x;
}
         

/*******************************************************************************
* Function Name: ADC_DelSig_1_SelectCofiguration(uint8 config, uint8 restart)
********************************************************************************
*
* Summary:
*  Selects the user defined configuration. This API first stops the current ADC
*  and then initializes the registers with the default values for that config. 
*  This also performs calibration by writing GCOR registers with trim values 
*  stored in the flash.
*
* Parameters:  
*  config:  configuration user wants to select.
*
* Return: 
*  void
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
void ADC_DelSig_1_SelectConfiguration(uint8 config, uint8 restart) \
                                              
{
    uint8 inputRange, resolution;
    uint16 idealGain, clockDivider;    
    uint16 idealOddGain;
    
    /* Stop the ADC  */
    ADC_DelSig_1_Stop();
    
    /* Check whether the config is valid or not */
    if( config <= ADC_DelSig_1_DEFAULT_NUM_CONFIGS)
    {   
        /* Set the  ADC registers based on the configuration */
        ADC_DelSig_1_InitConfig(config);
        
        /* Trim value calculation */
        switch(config)
        {
            case 1:
                inputRange = ADC_DelSig_1_DEFAULT_RANGE;
                resolution = ADC_DelSig_1_DEFAULT_RESOLUTION;
                idealGain = ADC_DelSig_1_DEFAULT_IDEAL_DEC_GAIN;
                idealOddGain = ADC_DelSig_1_DEFAULT_IDEAL_ODDDEC_GAIN;
                clockDivider = ADC_DelSig_1_RoundValue((double)BCLK__BUS_CLK__HZ,
                                                            (double)ADC_DelSig_1_DFLT_CLOCK_FREQ);
            break;
            
            case 2:
                inputRange = ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG2;
                resolution = ADC_DelSig_1_DEFAULT_RESOLUTION_CFG2;
                idealGain = ADC_DelSig_1_DEFAULT_IDEAL_DEC_GAIN_CFG2;
                idealOddGain = ADC_DelSig_1_DEFAULT_IDEAL_ODDDEC_GAIN_CFG2;
                clockDivider = ADC_DelSig_1_RoundValue((double)BCLK__BUS_CLK__HZ,
                                                            (double)ADC_DelSig_1_DFLT_CLOCK_FREQ_CFG2);
            break;
            
            case 3:
                inputRange = ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG3;
                resolution = ADC_DelSig_1_DEFAULT_RESOLUTION_CFG3;
                idealGain = ADC_DelSig_1_DEFAULT_IDEAL_DEC_GAIN_CFG3;
                idealOddGain = ADC_DelSig_1_DEFAULT_IDEAL_ODDDEC_GAIN_CFG3;
                clockDivider = ADC_DelSig_1_RoundValue((double)BCLK__BUS_CLK__HZ,
                                                            (double)ADC_DelSig_1_DFLT_CLOCK_FREQ_CFG3);
            break;
            
            case 4:
                inputRange = ADC_DelSig_1_DEFAULT_INPUT_RANGE_CFG4;
                resolution = ADC_DelSig_1_DEFAULT_RESOLUTION_CFG4;
                idealGain = ADC_DelSig_1_DEFAULT_IDEAL_DEC_GAIN_CFG4;
                idealOddGain = ADC_DelSig_1_DEFAULT_IDEAL_ODDDEC_GAIN_CFG4;
                clockDivider = ADC_DelSig_1_RoundValue((double)BCLK__BUS_CLK__HZ,  
                                                            (double)ADC_DelSig_1_DFLT_CLOCK_FREQ_CFG4);
            break;
            
            default:
                inputRange = ADC_DelSig_1_DEFAULT_RANGE;
                resolution = ADC_DelSig_1_DEFAULT_RESOLUTION;
                idealGain = ADC_DelSig_1_DEFAULT_IDEAL_DEC_GAIN;
                idealOddGain = ADC_DelSig_1_DEFAULT_IDEAL_ODDDEC_GAIN;
                clockDivider = ADC_DelSig_1_RoundValue((double)BCLK__BUS_CLK__HZ,
                                                            (double)ADC_DelSig_1_DFLT_CLOCK_FREQ);
            break;
        }
        
        clockDivider = clockDivider - 1;
        /* Set the proper divider for the internal clock */
        #if(ADC_DelSig_1_DEFAULT_INTERNAL_CLK)
            ADC_DelSig_1_theACLK_SetDividerRegister(clockDivider, 1);
        #endif
        
        /* Compensate the gain */
        ADC_DelSig_1_GainCompensation(inputRange, idealGain, idealOddGain, resolution);
        
        if(restart == 1)
        {        
            /* Restart the ADC */
            ADC_DelSig_1_Start();
        
            /* Restart the ADC conversion */
            ADC_DelSig_1_StartConvert();
        }
    }
}     


/*******************************************************************************
* Function Name: ADC_DelSig_1_GainCompensation(uint8, uint16, uint16, uint8)
********************************************************************************
*
* Summary:
*  This API calculates the trim value and then loads this to GCOR register.
*
* Parameters:  
*  inputRange:  input range for which trim value is to be calculated.
*  IdealDecGain:  Ideal Decimator gain for the selected resolution and conversion 
*                 mode.
*  IdealOddDecGain:  Ideal odd decimation gain for the selected resolution and 
                     conversion mode.
*  resolution:  Resolution to select the proper flash location for trim value.
*
* Return: 
*  void
*
* Reentrance: 
*  Yes
*
*******************************************************************************/
void ADC_DelSig_1_GainCompensation(uint8 inputRange, uint16 IdealDecGain, uint16 IdealOddDecGain,  \
                                       uint8 resolution) 
{
    int8 flash;
    int16 Normalised;
    uint16 GcorValue;
    uint32 GcorTmp;
    
    switch(inputRange)
    {         
        case ADC_DelSig_1_IR_VNEG_VREF_DIFF:
        
        /* Normalize the flash Value */
        if(resolution > 15)
        {
            flash = ADC_DelSig_1_DEC_TRIM_VREF_DIFF_16_20; 
        }    
        else
        {
            flash = ADC_DelSig_1_DEC_TRIM_VREF_DIFF_8_15;
        }        
        break;
        
        case ADC_DelSig_1_IR_VNEG_VREF_2_DIFF:
        
        /* Normalize the flash Value */
        if(resolution > 15)
        {
            flash = ADC_DelSig_1_DEC_TRIM_VREF_2_DIFF_16_20;
        }    
        else
        {
            flash = ADC_DelSig_1_DEC_TRIM_VREF_2_DIFF_8_15;
        }    
        break;
        
        case ADC_DelSig_1_IR_VNEG_VREF_4_DIFF:
        
        /* Normalize the flash Value */
        if(resolution > 15)
        {
            flash = ADC_DelSig_1_DEC_TRIM_VREF_4_DIFF_16_20;
        }    
        else
        {
            flash = ADC_DelSig_1_DEC_TRIM_VREF_4_DIFF_8_15;
        }    
        break;
        
        case ADC_DelSig_1_IR_VNEG_VREF_16_DIFF:
        
        /* Normalize the flash Value */
        if(resolution > 15)
        {
            flash = ADC_DelSig_1_DEC_TRIM_VREF_16_DIFF_16_20;
        }    
        else
        {
            flash = ADC_DelSig_1_DEC_TRIM_VREF_16_DIFF_8_15;
        }    
        break;
        
        default:
            flash = 0;
        break;
    }    
    if(inputRange == ADC_DelSig_1_IR_VSSA_TO_VREF || inputRange == ADC_DelSig_1_IR_VSSA_TO_2VREF || 
       inputRange == ADC_DelSig_1_IR_VSSA_TO_VDDA || inputRange == ADC_DelSig_1_IR_VSSA_TO_6VREF || 
       inputRange == ADC_DelSig_1_IR_VNEG_2VREF_DIFF || inputRange == ADC_DelSig_1_IR_VNEG_6VREF_DIFF ||
       inputRange == ADC_DelSig_1_IR_VNEG_VREF_8_DIFF)
    {
        Normalised  = 0;
    }
    else
    {
        Normalised  = ((int16)flash) * 32;
    }
                    
    /* Add two values */
    GcorValue = IdealDecGain + Normalised;  
    GcorTmp = (uint32)GcorValue * (uint32)IdealOddDecGain;
    GcorValue = (uint16)(GcorTmp / ADC_DelSig_1_IDEAL_GAIN_CONST);
        
    if (resolution < 14)
    {
        GcorValue = (GcorValue >> (15 - (resolution + 1)));
        ADC_DelSig_1_DEC_GVAL_REG = (resolution + 1);
    }
    else
    {
        ADC_DelSig_1_DEC_GVAL_REG = 15;  // Use all 16 bits
    }
      
    /* Load the gain correction register */    
    ADC_DelSig_1_DEC_GCOR_REG  = (uint8)GcorValue;
    ADC_DelSig_1_DEC_GCORH_REG = (uint8)(GcorValue >> 8);    
    
    /* Workaround for 0 to 2*Vref mode with PSoC3 ES2 and PSoC5 ES1 siliocn */
    #if( ADC_DelSig_1_PSOC3_ES2 || ADC_DelSig_1_PSOC5_ES1) 
        if( inputRange == ADC_DelSig_1_IR_VSSA_TO_2VREF)
        {
            ADC_DelSig_1_DEC_GCOR_REG = 0x00;
            ADC_DelSig_1_DEC_GCORH_REG = 0x00;
            ADC_DelSig_1_DEC_GVAL_REG = 0x00;
        }
    #endif
    
}        


/* [] END OF FILE */
