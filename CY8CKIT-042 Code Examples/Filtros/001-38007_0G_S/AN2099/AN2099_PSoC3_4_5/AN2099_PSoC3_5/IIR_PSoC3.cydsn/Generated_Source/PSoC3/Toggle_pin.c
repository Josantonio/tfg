/*******************************************************************************
* File Name: Toggle_pin.c  
* Version 1.50
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "Toggle_pin.h"


/*******************************************************************************
* Function Name: Toggle_pin_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void Toggle_pin_Write(uint8 value) 
{
    uint8 staticBits = Toggle_pin_DR & ~Toggle_pin_MASK;
    Toggle_pin_DR = staticBits | ((value << Toggle_pin_SHIFT) & Toggle_pin_MASK);
}


/*******************************************************************************
* Function Name: Toggle_pin_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void Toggle_pin_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(Toggle_pin_0, mode);
}


/*******************************************************************************
* Function Name: Toggle_pin_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro Toggle_pin_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 Toggle_pin_Read(void) 
{
    return (Toggle_pin_PS & Toggle_pin_MASK) >> Toggle_pin_SHIFT;
}


/*******************************************************************************
* Function Name: Toggle_pin_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 Toggle_pin_ReadDataReg(void) 
{
    return (Toggle_pin_DR & Toggle_pin_MASK) >> Toggle_pin_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(Toggle_pin_INTSTAT) 

    /*******************************************************************************
    * Function Name: Toggle_pin_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 Toggle_pin_ClearInterrupt(void) 
    {
        return (Toggle_pin_INTSTAT & Toggle_pin_MASK) >> Toggle_pin_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 


/* [] END OF FILE */ 
