/******************************************************************************
* Project Name		: Deep Sleep
* File Name			: main.c
* Version 			: 1.0
* Device Used		: CY8C4245AXI-483
* Software Used		: PSoC Creator 4.2
* Compiler    		: ARM GCC 5.4.1
* Related Hardware	: CY8CKIT-042 PSoC 4 Pioneer Kit 
*
*******************************************************************************
* Copyright (2018), Cypress Semiconductor Corporation. All rights reserved.
*******************************************************************************
* This software, including source code, documentation and related materials
* (“Software”), is owned by Cypress Semiconductor Corporation or one of its
* subsidiaries (“Cypress”) and is protected by and subject to worldwide patent
* protection (United States and foreign), United States copyright laws and
* international treaty provisions. Therefore, you may use this Software only
* as provided in the license agreement accompanying the software package from
* which you obtained this Software (“EULA”).
*
* If no EULA applies, Cypress hereby grants you a personal, nonexclusive,
* non-transferable license to copy, modify, and compile the Software source
* code solely for use in connection with Cypress’s integrated circuit products.
* Any reproduction, modification, translation, compilation, or representation
* of this Software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, 
* EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress 
* reserves the right to make changes to the Software without notice. Cypress 
* does not assume any liability arising out of the application or use of the 
* Software or any product or circuit described in the Software. Cypress does 
* not authorize its products for use in any products where a malfunction or 
* failure of the Cypress product may reasonably be expected to result in 
* significant property damage, injury or death (“High Risk Product”). By 
* including Cypress’s product in a High Risk Product, the manufacturer of such 
* system or application assumes all risk of such use and in doing so agrees to 
* indemnify Cypress against all liability.
*******************************************************************************/

/******************************************************************************
*                           THEORY OF OPERATION
* This project demonstrates the Deep Sleep mode of the PSoC 4. The LED on the 
* kit is turned on for one second and then turned off. The device enters the 
* deep sleep mode after that. The device wakes up from the sleep mode when user
* presses the button on the kit. The device wakes up from sleep as a result of
* the PICU interrupt being triggered by the button press. This interrupt is 
* cleared in the ISR of the isr_WakeUp.c file.
*
* Hardware connection on the Kit
* LED_Red 		- P1[6]
* WakeUp_Switch - P0[7]
******************************************************************************/
#include <device.h>

/* Write '0' to Pin_RedLED to turn on the LED. 
 * See the connections on the TopDesign. */

#define LED_ON 0u
#define LED_OFF 1u
#define DELAY_1S 1000

/* Function declarations */
void Component_ShutDown(void);
void Component_Restore(void);
CY_ISR_PROTO(WakeUpSwitchISR);

int main()
{
	/* Enable Global Interrupts */
	CyGlobalIntEnable;
    
    /* Start the interrupt to allow wakeup from Deep Sleep */
    isr_WakeUp_StartEx(WakeUpSwitchISR);
	
    for(;;)
    {
		/* Wake Up for one second in active mode 
		 * Glow the LED to indicate that the device is in active mode */
		Pin_RedLED_Write(LED_ON);		
		CyDelay(DELAY_1S);
		Pin_RedLED_Write(LED_OFF);
		
		/* Prepare Device to Sleep */
		Component_ShutDown();
		
		/* Enter Deep Sleep Mode */
		CySysPmDeepSleep();

		/* Restore the device configuration after coming back from sleep */
		Component_Restore();
    }
}


/*******************************************************************************
* Function Name: Component_ShutDown
********************************************************************************
*
* Summary:
*  Shuts down all the components and puts all the pins to Hi-Z state to reduce
* power consumption.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Component_ShutDown(void)
{
	/* Call all component stop/sleep functions here . 
	 * This project doesn't have any components that needs shut down */ 
	 
	/* Set LED pin Drive Mode to High Impedence */
	Pin_RedLED_SetDriveMode(Pin_RedLED_DM_ALG_HIZ);
	
	/* Set the WakeUp Switch pin in resistive pull up mode for detecting 
	 * the falling edge interrupt when switch is pressed */
	Pin_WakeUpSwitch_SetDriveMode(Pin_WakeUpSwitch_DM_RES_UP);
}


/*******************************************************************************
* Function Name: Component_Restore
********************************************************************************
*
* Summary:
*  Restore all the components and pin state after coming back from sleep.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Component_Restore(void)
{
	/* Call all component restore functions here . 
	 * This project doesn't have any components that needs to be restored */ 
	 
	/* Restore all the pin states */
	
	/* Set the Red LED output pin in strong drive mode */
	Pin_RedLED_SetDriveMode(Pin_RedLED_DM_STRONG);
}


/*******************************************************************************
* Function Name: CY_ISR
********************************************************************************
* Summary: ISR routine for button interrupt. The system enters here after the CPU wakeup.
* It also Clears pending interrupt.
*
* Parameters:
*  WakeUpSwitchISR - Address of the ISR to set in the interrupt vector table
*
* Return:
*  void
*
*******************************************************************************/
CY_ISR(WakeUpSwitchISR)
{
    /* Clear the pending interrupts */
    isr_WakeUp_ClearPending();    
    Pin_WakeUpSwitch_ClearInterrupt();
}


/* [] END OF FILE */
