/*******************************************************************************
* File Name: clock_timer.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_clock_timer_H)
#define CY_CLOCK_clock_timer_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void clock_timer_StartEx(uint32 alignClkDiv);
#define clock_timer_Start() \
    clock_timer_StartEx(clock_timer__PA_DIV_ID)

#else

void clock_timer_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void clock_timer_Stop(void);

void clock_timer_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 clock_timer_GetDividerRegister(void);
uint8  clock_timer_GetFractionalDividerRegister(void);

#define clock_timer_Enable()                         clock_timer_Start()
#define clock_timer_Disable()                        clock_timer_Stop()
#define clock_timer_SetDividerRegister(clkDivider, reset)  \
    clock_timer_SetFractionalDividerRegister((clkDivider), 0u)
#define clock_timer_SetDivider(clkDivider)           clock_timer_SetDividerRegister((clkDivider), 1u)
#define clock_timer_SetDividerValue(clkDivider)      clock_timer_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define clock_timer_DIV_ID     clock_timer__DIV_ID

#define clock_timer_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define clock_timer_CTRL_REG   (*(reg32 *)clock_timer__CTRL_REGISTER)
#define clock_timer_DIV_REG    (*(reg32 *)clock_timer__DIV_REGISTER)

#define clock_timer_CMD_DIV_SHIFT          (0u)
#define clock_timer_CMD_PA_DIV_SHIFT       (8u)
#define clock_timer_CMD_DISABLE_SHIFT      (30u)
#define clock_timer_CMD_ENABLE_SHIFT       (31u)

#define clock_timer_CMD_DISABLE_MASK       ((uint32)((uint32)1u << clock_timer_CMD_DISABLE_SHIFT))
#define clock_timer_CMD_ENABLE_MASK        ((uint32)((uint32)1u << clock_timer_CMD_ENABLE_SHIFT))

#define clock_timer_DIV_FRAC_MASK  (0x000000F8u)
#define clock_timer_DIV_FRAC_SHIFT (3u)
#define clock_timer_DIV_INT_MASK   (0xFFFFFF00u)
#define clock_timer_DIV_INT_SHIFT  (8u)

#else 

#define clock_timer_DIV_REG        (*(reg32 *)clock_timer__REGISTER)
#define clock_timer_ENABLE_REG     clock_timer_DIV_REG
#define clock_timer_DIV_FRAC_MASK  clock_timer__FRAC_MASK
#define clock_timer_DIV_FRAC_SHIFT (16u)
#define clock_timer_DIV_INT_MASK   clock_timer__DIVIDER_MASK
#define clock_timer_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_clock_timer_H) */

/* [] END OF FILE */
