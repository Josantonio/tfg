/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

/* Project Defines */
#define FALSE  0
#define TRUE   1

/*******************************************************************************
*            Global variables
*******************************************************************************/

uint32 read_character;
uint32 received_character;
uint32 error_uart;
uint8 lights[3] = {49,50,51}; //Númeración de las luces ROJO, VERDE, NARANJA
uint8 i = 0u; //índice
uint8 connected = FALSE;

/***************************************************************************//**
* Function Name: handle_error
********************************************************************************
*
* Summary:
* This function processes unrecoverable errors such as any component
* initialization errors etc. In case of such error the system will switch on
* RED_LED_ERROR and stay in the infinite loop of this function.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void handle_error(void)
{
    /* Disable all interrupts */
    CyGlobalIntDisable;

    /* Switch on error LED */
    LED_pin_error_Write(0);
    while(1u)
    {
    }
}
/***************************************************************************//**
* Function Name: ISR_UART
********************************************************************************
*
* Summary:
*  This function is registered to be called when UART interrupt occurs.
*  Note that only RX fifo not empty interrupt is used.
*  Whenever there is data in UART RX fifo, get the data and put it into
*  UART TX fifo which will be transmitted to terminal.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void ISR_UART()
{
    /* Returns the status/identity of which enabled RX interrupt source caused interrupt event */
    uint32 source = UART_GetRxInterruptSource();

	/* Checks for "RX FIFO AND not empty" interrupt */
    if(0ul != (UART_INTR_RX_NOT_EMPTY & source))
    {
        /* Get the character from terminal */
        read_character = UART_UartGetChar();

        /* Clear UART "RX FIFO not empty interrupt" */
        UART_ClearRxInterruptSource(UART_INTR_RX_NOT_EMPTY);

        /* Sets received_character flag */
        received_character = 1;
    }
    else
    {
        error_uart = 1;
    }
    UART_ClearPendingInt();
}
/*******************************************************************************
* Function Name: main
********************************************************************************
*
* Summary:
*  main() performs following functions:
*  1. Initializes UART component.    
*  2. UART sends text header into the serial terminal.
*  3. Unmask UART RX fifo not empty interrupt.
*  4. Enable UART interrupts.
*  5. Wait for UART RX fifo not empty interrupt.
*  2: Established connection with LabView
*       LabView sends 'K'
*       Psoc sends 'OK'
*  3: Send a sequence which emulates a traffic light
*     '1' -> red
*     '2' -> green
*     '3' -> orange

* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
CY_ISR(ISR_TIMER_HANDLE){ //lights[0,1,2] posiciones ROJO, VERDE, NARANJA
    if(connected){
        UART_UartPutChar(lights[i]); //Nos vamos moviendo en orden por los valores
        i++;
        if(i==2){
            Timer_WritePeriod(10000);
        }
        if(i==3){
            i=0;
            Timer_WritePeriod(50000);
        } //Si hemos llegado a la posición 3 volvemos a la 0
    }
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    
    /* Flag initialization */
    received_character = 0;
    error_uart = 0;

    /* Start UART operation */
    UART_Start();
    clock_timer_Start();
    Timer_Start();

    isr_timer_StartEx(ISR_TIMER_HANDLE);
    /* Enable interrupt for UART */
    UART_EnableInt();

    /* Sets interrupt handler for UART */
    UART_SetCustomInterruptHandler(*ISR_UART);

    UART_UartPutString("COM connected");

    /* Wait for RX fifo not empty interrupt */
    for(;;)
    {
        if(received_character == 1)
        {
            received_character = 0;
            /* Characters typed on console are transmitted via UART*/
            switch (read_character)
            {
                case 'k':
                        UART_UartPutString("OK"); 
                        connected = TRUE;
                        LED_pin_connected_Write(0);
                    break;
                        
                case 'q':
                        UART_UartPutString("DISCONNECTED");
                        connected = FALSE;
                        LED_pin_connected_Write(1);
                    break;
                        
                default:
                    break;
            }
        }
        if(error_uart == 1)
        {
            handle_error();
        }
    }
}

/* [] END OF FILE */
