/******************************************************************************
* Project Name		: PWM
* File Name			: main.c
* Version 			: 1.0
* Device Used		: CY8C4245AXI-483
* Software Used		: PSoC Creator 4.2
* Compiler    		: ARM GCC 5.4.1
* Related Hardware	: CY8CKIT-042 PSoC 4 Pioneer Kit 
*
*******************************************************************************
* Copyright (2018), Cypress Semiconductor Corporation. All rights reserved.
*******************************************************************************
* This software, including source code, documentation and related materials
* (“Software”), is owned by Cypress Semiconductor Corporation or one of its
* subsidiaries (“Cypress”) and is protected by and subject to worldwide patent
* protection (United States and foreign), United States copyright laws and
* international treaty provisions. Therefore, you may use this Software only
* as provided in the license agreement accompanying the software package from
* which you obtained this Software (“EULA”).
*
* If no EULA applies, Cypress hereby grants you a personal, nonexclusive,
* non-transferable license to copy, modify, and compile the Software source
* code solely for use in connection with Cypress’s integrated circuit products.
* Any reproduction, modification, translation, compilation, or representation
* of this Software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND, 
* EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED 
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress 
* reserves the right to make changes to the Software without notice. Cypress 
* does not assume any liability arising out of the application or use of the 
* Software or any product or circuit described in the Software. Cypress does 
* not authorize its products for use in any products where a malfunction or 
* failure of the Cypress product may reasonably be expected to result in 
* significant property damage, injury or death (“High Risk Product”). By 
* including Cypress’s product in a High Risk Product, the manufacturer of such 
* system or application assumes all risk of such use and in doing so agrees to 
* indemnify Cypress against all liability.
*******************************************************************************/

/******************************************************************************
*                           THEORY OF OPERATION
* This project demonstrates the use of the PWM component. The project uses
* three PWM outputs to set the color of the tricolor LED on the CY8CKIT-042. 
* The color changes  every one second from Violet to Red in the order Violet - 
* Indigo - Blue - Green - Yellow - Orange - Red. This is achieved by changing
* the pulse width of the PWMs.
*
* Hardware connection on the Kit
* LED_Red   - P1[6]
* LED_Green - P0[2]
* LED_Blue  - P0[3]
******************************************************************************/
#include <device.h>

/* Se definen los índices que ocupa cada color del RGB dentro de la matriz */
#define RED 0u // u -> hexadecimal unsigned
#define GREEN 1u
#define BLUE 2u
#define NUM_OF_COLORS 7u

/* Mask for BYTE shifting */

#define BLUE_POSITION 0
#define GREEN_POSITION 8
#define RED_POSITION 16
#define ALPHA_POSITION 24

/* Color code for VIBGYOR */       // R     G    B
//CYCODE, ni idea, creo que para
//Almacenar en una memoria específica
CYCODE const uint8 colorMap[][3] = {{0x99,0x00,0x99},  /* Violet */ //Hexadecimal numbers, unsigned int 8 bits
									{0x4B,0x00,0x82},  /* Indigo */
									{0x00,0x00,0xFF},  /* Blue   */
									{0x00,0xFF,0x00},  /* Green  */
									{0xFF,0xD3,0x00},  /* Yellow */
									{0xFF,0x45,0x00},  /* Orange */
									{0xFF,0x00,0x00}}; /* Red    */


uint8 ucrCompare, ucgCompare, ucbCompare;
uint32 ucARGB;

int main()
{
    uint8 colorIndex = 0u;
	
	/* Start all components */
	PWM_Red_Start();
	PWM_Green_Start();
	PWM_Blue_Start();
	
	/* Clock can be started automatically after reset by enabling “Start on 
	Reset” in Clocks tab of PWM.cydwr. We are doing this manually for instructive 
	purpose */
	Clock_PWM_Start();

	for(;;)
    {
		/* Loop to change the LED color. */
        for(colorIndex = 0; colorIndex < NUM_OF_COLORS; colorIndex++)
		{
			/* Set the compare value of the PWM components to change 
			the PWM duty-cycle. This wll set the tricolor LED color
			to one of the pre-defined colors as given in the colormap */
            
			/* Modificamos el ciclo de trabajo de cada PWM leyendo de la tabla de colores */
            PWM_Red_WriteCompare(colorMap[colorIndex][RED]);
			PWM_Green_WriteCompare(colorMap[colorIndex][GREEN]);
			PWM_Blue_WriteCompare(colorMap[colorIndex][BLUE]);
			
			ucrCompare = colorMap[colorIndex][RED];
		    ucgCompare = colorMap[colorIndex][GREEN];
			ucbCompare = colorMap[colorIndex][BLUE];			
			
			ucARGB = (ucrCompare << RED_POSITION) | (ucgCompare << GREEN_POSITION) | (ucbCompare << BLUE_POSITION);
			/* Wait for one second before changing the color */
			CyDelay(1000);
		}
    }
}

/* [] END OF FILE */
