/* programa que simula el comportamiento de un semáforo, con la siguiente secuencia rojo > verde > ambar
                                                                                     3s      3s     1s 
si se pulsa un botón se activa el estado de emergencia, que corresponde a parpadeo en ambar de 0.5s */
                                                                                    
#include "project.h"
#include "string.h"

/* Se definen los índices que ocupa cada color del RGB dentro de la matriz, que se corresponden a 
la posición en las columnas */
#define RED 0u
#define GREEN 1u
#define BLUE 2u

#define NUM_COLORS 4u
//Ven ordenados por columnas, mientras que el color que se selecciona, el que vemos al final, va por filas

/* Variables globales */
CYBIT emergency = 0; //Declaramo una variable de 1 bit que nos servirá para saber si estamos en emergencia
uint8 index_color = 0u; //Variable global que nos servirá para movernos por los colores

CY_ISR (isr_timer_Handle){ //Cada vez que el contador cuente un periodo se producirá una interrupción
    
    if ((index_color != 2) & (emergency == 0)){ //Cambiamos de color
        index_color++;
        if(index_color == 2){ //Justo cuando se cambia a ambar activamos el periodo a 1 seg
            Timer_WritePeriod(10000); //Cambiamos el período de reloj a 1 segundo 
        }
    }else if ((index_color == 2) & (emergency == 0)){ //Caso en el que llegamos al ambar, tenemos que pasar a rojo y cambiar el periodo del timer
        index_color = 0;
        Timer_WritePeriod(30000); //Volvemos a ponerlo a 3 segundos
    } 
    if (emergency){
        if (index_color == 3){
            index_color = 2;
        }else if (index_color == 2){
            index_color = 3;
        }
        else {
            index_color = 2;
        }
    }
}

CY_ISR (isr_button_Handle){ //Interrupción que se activa al pulsar el botón
    emergency = !emergency; //Cambiamos el estado de emergencia
    Timer_WritePeriod(5000); //Cambiamos el período de reloj a 0.5 segundos 
}

int main(void)
{
    /*Color code para la secuencia rojo > verde > ambar */
                                             //  R      G     B
    CYCODE const uint8 colormap [NUM_COLORS][4] = {{0xFF, 0x00, 0x00}, //Red FF = 255 en hexadecimal
                                               {0x00, 0xFF, 0x00}, //Green
                                               {0xFF, 0xA5, 0x00}, //Orange, created RGB = (255,165,0)
                                               {0x00, 0x00, 0x00}}; //Apagado

    CyGlobalIntEnable; 
    /* Start all components */
    Clock_Start();
    Clock_PWM_Start();
    PWM_Blue_Start();
    PWM_Red_Start();
    PWM_Green_Start();
    Timer_Start(); //Iniciamos y activamos


    isr_timer_StartEx(isr_timer_Handle); //Relación a cabecera
    isr_button_StartEx(isr_button_Handle);

    
    for(;;)
    {   
     PWM_Red_WriteCompare(colormap[index_color][RED]); //Escribimos el duty cyle para el LED red
     PWM_Green_WriteCompare(colormap[index_color][GREEN]); //Escribimos el duty cycle para el LED green
     PWM_Blue_WriteCompare(colormap[index_color][BLUE]); //Escribimos el duty cycle para el LED azul 
    }
    
}
