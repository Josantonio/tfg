/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* programa que simula el comportamiento de un semáforo, con la siguiente secuencia rojo > verde > ambar
                                                                                     3s      3s     1s 
si se pulsa un botón se activa el estado de emergencia, que corresponde a parpadeo en ambar de 0.5s */
                                                                                    
#include "project.h"

/* Se definen los índices que ocupa cada color del RGB dentro de la matriz, que se corresponden a 
la posición en las columnas */
#define RED 0u
#define GREEN 1u
#define BLUE 2u

#define NUM_COLORS 4u
//Ven ordenados por columnas, mientras que el color que se selecciona, el que vemos al final, va por filas

CYBIT emergency = 0; //Declaramo una variable de 1 bit que nos servirá para saber si estamos en emergencia

/*Color code para la secuencia rojo > verde > ambar */
                                             //  R      G     B
CYCODE const uint8 colormap [NUM_COLORS][4] = {{0xFF, 0x00, 0x00}, //Red FF = 255 en hexadecimal
                                               {0x00, 0xFF, 0x00}, //Green
                                               {0xFF, 0xA5, 0x00}, //Orange, created RGB = (255,165,0)
                                               {0x00, 0x00, 0x00}}; //Apagado

int main(void)
{
    /* Start all components */
    PWM_Blue_Start();
    PWM_Red_Start();
    PWM_Green_Start();
    
    Clock_1_Start();
    
    uint8 i=0u; //índice para moverse por la matriz
    
    for(;;)
    {
            for (i=0; i<3; i++){
            
                PWM_Red_WriteCompare(colormap[i][RED]); //Escribimos el duty cyle para el LED red
                PWM_Green_WriteCompare(colormap[i][GREEN]); //Escribimos el duty cycle para el LED green
                PWM_Blue_WriteCompare(colormap[i][BLUE]); //Escribimos el duty cycle para el LED azul
        
                if (i!=2 && emergency != 1){ //No estamos en el caso de ambar y además no hay emergencia
                    CyDelay(3000); //Esperamos 3 segundos
                }else{
                    CyDelay(1000); //En el caso ambar esperamos sólo 1s
                } 
            }
    }
}

/* [] END OF FILE */
