/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "math.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define ADC_RANGE (2048)
#define V_MAX (ADC_RANGE*(330+10000)/(10000+2*330)) //Lectura máxima posible en el potenciómetro
#define V_MIN (ADC_RANGE*330/(10000+2*330)) //Lectura mínima en el potnciómetro

uint8 State_Button=0; //OFF
uint8 arranque = 1; //El motor debe realizar un arranque progresivo antes de alcanzar la velocidad estacionaria
float duty;
uint16 Vel_real = 0; //Velocidad leída en rpm
uint16 vueltas_cont = 0; //Pasos por Tc del contador de velocidad
uint8 first_turn = 1; //Variable para saber si ha dado la primera vuelta
uint32 delay = 0; //Tiempo para un giro completo
uint32 T0 = 0; //Lectura del timer al pasar por el sensor hall
uint8 T_clock = 10; //Período de reloj del speed_counter [us]

CY_ISR (isr_button_Handler){
    State_Button = ~State_Button; //Cambiamos a ON o a OFF
}

CY_ISR (isr_spd_Handler){
    //Si la interrupción es porque ha llegado al final de la cuenta, y además el motor está en marcha
    if ((Speed_Counter_GetInterruptSource() == Speed_Counter_INTR_MASK_TC) && State_Button && !first_turn){ 
        vueltas_cont++;
        Speed_Counter_ClearInterrupt(Speed_Counter_INTR_MASK_TC);
    }
    //Si es porque ha recibido señal de los sensores hall de una vuelta completa
    if (Speed_Counter_GetInterruptSource() == Speed_Counter_INTR_MASK_CC_MATCH){
        if (first_turn){ //Es la primera vez
            first_turn = ~first_turn;
            T0 = Speed_Counter_ReadCapture();
        }else{
            delay = (Speed_Counter_ReadCapture()-T0+vueltas_cont)*T_clock;
            T0 = Speed_Counter_ReadCapture();
            vueltas_cont = 0;
            Vel_real = 1/(delay*60e-6); //Vel obtenida [rpm] 
             
        }
        Speed_Counter_ClearInterrupt(Speed_Counter_INTR_MASK_CC_MATCH);
    }
    
    
}

struct motor {
    uint8 Potencia;     //Potencia [w]
    uint16 SP_MAX;       //Vel máxima [rpm]
    uint16 SP_MIN;      //Vel minim [rpm]
    uint8 p_polos;        //Nº pares de polos
    float K_tau;        //Constante mecánica [Nm/A]
    float Ke;           //Constante eléctrica [V/rpm]
    float R_line;      //Resistencia fase-fase [Ohm]
    float L_line;      //Impedancia fase-fase [mH]
    uint16 I_max;       //Corriente máxima [mA] 
};

struct motor BLY172S = { 53, 4000, 4000/8, 4, 0.0410276162, 0.00335, 0.8, 1.2, 2208};


struct paramPID {
    float Kp; //Constante proporcional Kp = 1.2*T/L
    float Ti; //Parámetro integral ki = 2*L
    float Td; //Parámetro derivativo Kd = 0.5*L
    uint8 Tn; //Tiempo de muestreo del controlador en ms
    
};
struct paramPID PID = {15.61, 4.84e-3, 1.21e-3, 10};
    
int crear_tabla (void){
    
    return 0;
}
int Vel_ref(void){ //Función que lee el valor del potenciómetro y lo transforma en un valor 
    //de velocidad deseada (rpm)
    uint16 SP_ref = 0; //Velocidad deseada, a través del potenciómetro
    
    ADC_SAR_StartConvert();
    ADC_SAR_IsEndConversion(ADC_SAR_WAIT_FOR_RESULT);
    SP_ref = (ADC_SAR_GetResult16(0)-V_MIN)*(BLY172S.SP_MAX-BLY172S.SP_MIN)/(V_MAX-V_MIN)+BLY172S.SP_MIN;
    
    if (SP_ref < BLY172S.SP_MIN){ SP_ref = BLY172S.SP_MIN;}
    if (SP_ref > BLY172S.SP_MAX){ SP_ref = BLY172S.SP_MAX;}
    
    return SP_ref;
}


int controlPID(void){
    float eT; //Señal de error eT = Vel_ref - Vel_read
    float iT, iT0 = 0.0; //Señal integrativa iT = b*eT+iT0
    float dT, dT0 = 0.0; //Señal derivativa dT = c*(eT-eT0)
    float b = (PID.Kp*PID.Tn)/PID.Ti;
    float c = (PID.Kp*PID.Td)/PID.Tn;
    
    //Señal de error
    eT = Vel_ref() - Vel_real;
    //Señal integrativa
    iT = b*eT+iT0;
    iT0 = iT;
    //Señal derivativa
    dT = c*(eT-dT0);
    dT0 = dT;
    //Señal de salida de control
    duty = PID.Kp*eT+iT+dT;
    return duty;
}


int main(void)
{
    PWM_Drive_Start();
    ADC_SAR_Start();
    Speed_Counter_Start();
    UART_Start();
    
    isr_button_StartEx(isr_button_Handler);
    isr_spd_StartEx(isr_spd_Handler);
    
    CyGlobalIntEnable; /* Enable global interrupts. */

    for(;;)
    {
        if(State_Button){ //Motor run
                Control_Reg_Write(1); //Se enciende el motor
                PWM_Drive_WriteCompare(round(controlPID()*100));
                CyDelay(PID.Tn);
        }else{ //Motor Stop
            Control_Reg_Write(0); //Se para el motor
            arranque = 1; //Se prepara para arrancar la siguiente vez
            first_turn = 1; //Contabilizar la primera vuelta
        }
    }
}

