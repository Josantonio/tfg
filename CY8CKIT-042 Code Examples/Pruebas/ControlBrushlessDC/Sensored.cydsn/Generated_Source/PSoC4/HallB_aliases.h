/*******************************************************************************
* File Name: HallB.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_HallB_ALIASES_H) /* Pins HallB_ALIASES_H */
#define CY_PINS_HallB_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define HallB_0			(HallB__0__PC)
#define HallB_0_PS		(HallB__0__PS)
#define HallB_0_PC		(HallB__0__PC)
#define HallB_0_DR		(HallB__0__DR)
#define HallB_0_SHIFT	(HallB__0__SHIFT)
#define HallB_0_INTR	((uint16)((uint16)0x0003u << (HallB__0__SHIFT*2u)))

#define HallB_INTR_ALL	 ((uint16)(HallB_0_INTR))


#endif /* End Pins HallB_ALIASES_H */


/* [] END OF FILE */
