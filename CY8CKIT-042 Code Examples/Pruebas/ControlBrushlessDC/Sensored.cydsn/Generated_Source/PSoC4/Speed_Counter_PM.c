/*******************************************************************************
* File Name: Speed_Counter_PM.c
* Version 2.10
*
* Description:
*  This file contains the setup, control, and status commands to support
*  the component operations in the low power mode.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "Speed_Counter.h"

static Speed_Counter_BACKUP_STRUCT Speed_Counter_backup;


/*******************************************************************************
* Function Name: Speed_Counter_SaveConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to save here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Speed_Counter_SaveConfig(void)
{

}


/*******************************************************************************
* Function Name: Speed_Counter_Sleep
********************************************************************************
*
* Summary:
*  Stops the component operation and saves the user configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Speed_Counter_Sleep(void)
{
    if(0u != (Speed_Counter_BLOCK_CONTROL_REG & Speed_Counter_MASK))
    {
        Speed_Counter_backup.enableState = 1u;
    }
    else
    {
        Speed_Counter_backup.enableState = 0u;
    }

    Speed_Counter_Stop();
    Speed_Counter_SaveConfig();
}


/*******************************************************************************
* Function Name: Speed_Counter_RestoreConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to restore here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Speed_Counter_RestoreConfig(void)
{

}


/*******************************************************************************
* Function Name: Speed_Counter_Wakeup
********************************************************************************
*
* Summary:
*  Restores the user configuration and restores the enable state.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Speed_Counter_Wakeup(void)
{
    Speed_Counter_RestoreConfig();

    if(0u != Speed_Counter_backup.enableState)
    {
        Speed_Counter_Enable();
    }
}


/* [] END OF FILE */
