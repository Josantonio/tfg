/*******************************************************************************
* File Name: HallC.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_HallC_H) /* Pins HallC_H */
#define CY_PINS_HallC_H

#include "cytypes.h"
#include "cyfitter.h"
#include "HallC_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} HallC_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   HallC_Read(void);
void    HallC_Write(uint8 value);
uint8   HallC_ReadDataReg(void);
#if defined(HallC__PC) || (CY_PSOC4_4200L) 
    void    HallC_SetDriveMode(uint8 mode);
#endif
void    HallC_SetInterruptMode(uint16 position, uint16 mode);
uint8   HallC_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void HallC_Sleep(void); 
void HallC_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(HallC__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define HallC_DRIVE_MODE_BITS        (3)
    #define HallC_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - HallC_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the HallC_SetDriveMode() function.
         *  @{
         */
        #define HallC_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define HallC_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define HallC_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define HallC_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define HallC_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define HallC_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define HallC_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define HallC_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define HallC_MASK               HallC__MASK
#define HallC_SHIFT              HallC__SHIFT
#define HallC_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in HallC_SetInterruptMode() function.
     *  @{
     */
        #define HallC_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define HallC_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define HallC_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define HallC_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(HallC__SIO)
    #define HallC_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(HallC__PC) && (CY_PSOC4_4200L)
    #define HallC_USBIO_ENABLE               ((uint32)0x80000000u)
    #define HallC_USBIO_DISABLE              ((uint32)(~HallC_USBIO_ENABLE))
    #define HallC_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define HallC_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define HallC_USBIO_ENTER_SLEEP          ((uint32)((1u << HallC_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << HallC_USBIO_SUSPEND_DEL_SHIFT)))
    #define HallC_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << HallC_USBIO_SUSPEND_SHIFT)))
    #define HallC_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << HallC_USBIO_SUSPEND_DEL_SHIFT)))
    #define HallC_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(HallC__PC)
    /* Port Configuration */
    #define HallC_PC                 (* (reg32 *) HallC__PC)
#endif
/* Pin State */
#define HallC_PS                     (* (reg32 *) HallC__PS)
/* Data Register */
#define HallC_DR                     (* (reg32 *) HallC__DR)
/* Input Buffer Disable Override */
#define HallC_INP_DIS                (* (reg32 *) HallC__PC2)

/* Interrupt configuration Registers */
#define HallC_INTCFG                 (* (reg32 *) HallC__INTCFG)
#define HallC_INTSTAT                (* (reg32 *) HallC__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define HallC_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(HallC__SIO)
    #define HallC_SIO_REG            (* (reg32 *) HallC__SIO)
#endif /* (HallC__SIO_CFG) */

/* USBIO registers */
#if !defined(HallC__PC) && (CY_PSOC4_4200L)
    #define HallC_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define HallC_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define HallC_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define HallC_DRIVE_MODE_SHIFT       (0x00u)
#define HallC_DRIVE_MODE_MASK        (0x07u << HallC_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins HallC_H */


/* [] END OF FILE */
