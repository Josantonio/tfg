/*******************************************************************************
* File Name: Run_Stop.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Run_Stop_ALIASES_H) /* Pins Run_Stop_ALIASES_H */
#define CY_PINS_Run_Stop_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define Run_Stop_0			(Run_Stop__0__PC)
#define Run_Stop_0_PS		(Run_Stop__0__PS)
#define Run_Stop_0_PC		(Run_Stop__0__PC)
#define Run_Stop_0_DR		(Run_Stop__0__DR)
#define Run_Stop_0_SHIFT	(Run_Stop__0__SHIFT)
#define Run_Stop_0_INTR	((uint16)((uint16)0x0003u << (Run_Stop__0__SHIFT*2u)))

#define Run_Stop_INTR_ALL	 ((uint16)(Run_Stop_0_INTR))


#endif /* End Pins Run_Stop_ALIASES_H */


/* [] END OF FILE */
