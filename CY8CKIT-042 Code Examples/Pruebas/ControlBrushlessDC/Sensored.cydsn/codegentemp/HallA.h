/*******************************************************************************
* File Name: HallA.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_HallA_H) /* Pins HallA_H */
#define CY_PINS_HallA_H

#include "cytypes.h"
#include "cyfitter.h"
#include "HallA_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} HallA_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   HallA_Read(void);
void    HallA_Write(uint8 value);
uint8   HallA_ReadDataReg(void);
#if defined(HallA__PC) || (CY_PSOC4_4200L) 
    void    HallA_SetDriveMode(uint8 mode);
#endif
void    HallA_SetInterruptMode(uint16 position, uint16 mode);
uint8   HallA_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void HallA_Sleep(void); 
void HallA_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(HallA__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define HallA_DRIVE_MODE_BITS        (3)
    #define HallA_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - HallA_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the HallA_SetDriveMode() function.
         *  @{
         */
        #define HallA_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define HallA_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define HallA_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define HallA_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define HallA_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define HallA_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define HallA_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define HallA_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define HallA_MASK               HallA__MASK
#define HallA_SHIFT              HallA__SHIFT
#define HallA_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in HallA_SetInterruptMode() function.
     *  @{
     */
        #define HallA_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define HallA_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define HallA_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define HallA_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(HallA__SIO)
    #define HallA_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(HallA__PC) && (CY_PSOC4_4200L)
    #define HallA_USBIO_ENABLE               ((uint32)0x80000000u)
    #define HallA_USBIO_DISABLE              ((uint32)(~HallA_USBIO_ENABLE))
    #define HallA_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define HallA_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define HallA_USBIO_ENTER_SLEEP          ((uint32)((1u << HallA_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << HallA_USBIO_SUSPEND_DEL_SHIFT)))
    #define HallA_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << HallA_USBIO_SUSPEND_SHIFT)))
    #define HallA_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << HallA_USBIO_SUSPEND_DEL_SHIFT)))
    #define HallA_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(HallA__PC)
    /* Port Configuration */
    #define HallA_PC                 (* (reg32 *) HallA__PC)
#endif
/* Pin State */
#define HallA_PS                     (* (reg32 *) HallA__PS)
/* Data Register */
#define HallA_DR                     (* (reg32 *) HallA__DR)
/* Input Buffer Disable Override */
#define HallA_INP_DIS                (* (reg32 *) HallA__PC2)

/* Interrupt configuration Registers */
#define HallA_INTCFG                 (* (reg32 *) HallA__INTCFG)
#define HallA_INTSTAT                (* (reg32 *) HallA__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define HallA_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(HallA__SIO)
    #define HallA_SIO_REG            (* (reg32 *) HallA__SIO)
#endif /* (HallA__SIO_CFG) */

/* USBIO registers */
#if !defined(HallA__PC) && (CY_PSOC4_4200L)
    #define HallA_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define HallA_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define HallA_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define HallA_DRIVE_MODE_SHIFT       (0x00u)
#define HallA_DRIVE_MODE_MASK        (0x07u << HallA_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins HallA_H */


/* [] END OF FILE */
