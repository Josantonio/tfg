/*******************************************************************************
* File Name: HallC.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_HallC_ALIASES_H) /* Pins HallC_ALIASES_H */
#define CY_PINS_HallC_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define HallC_0			(HallC__0__PC)
#define HallC_0_PS		(HallC__0__PS)
#define HallC_0_PC		(HallC__0__PC)
#define HallC_0_DR		(HallC__0__DR)
#define HallC_0_SHIFT	(HallC__0__SHIFT)
#define HallC_0_INTR	((uint16)((uint16)0x0003u << (HallC__0__SHIFT*2u)))

#define HallC_INTR_ALL	 ((uint16)(HallC_0_INTR))


#endif /* End Pins HallC_ALIASES_H */


/* [] END OF FILE */
