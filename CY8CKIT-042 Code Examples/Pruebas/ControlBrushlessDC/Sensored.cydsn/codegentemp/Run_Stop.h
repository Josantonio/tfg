/*******************************************************************************
* File Name: Run_Stop.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Run_Stop_H) /* Pins Run_Stop_H */
#define CY_PINS_Run_Stop_H

#include "cytypes.h"
#include "cyfitter.h"
#include "Run_Stop_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} Run_Stop_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   Run_Stop_Read(void);
void    Run_Stop_Write(uint8 value);
uint8   Run_Stop_ReadDataReg(void);
#if defined(Run_Stop__PC) || (CY_PSOC4_4200L) 
    void    Run_Stop_SetDriveMode(uint8 mode);
#endif
void    Run_Stop_SetInterruptMode(uint16 position, uint16 mode);
uint8   Run_Stop_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void Run_Stop_Sleep(void); 
void Run_Stop_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(Run_Stop__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define Run_Stop_DRIVE_MODE_BITS        (3)
    #define Run_Stop_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - Run_Stop_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the Run_Stop_SetDriveMode() function.
         *  @{
         */
        #define Run_Stop_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define Run_Stop_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define Run_Stop_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define Run_Stop_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define Run_Stop_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define Run_Stop_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define Run_Stop_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define Run_Stop_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define Run_Stop_MASK               Run_Stop__MASK
#define Run_Stop_SHIFT              Run_Stop__SHIFT
#define Run_Stop_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Run_Stop_SetInterruptMode() function.
     *  @{
     */
        #define Run_Stop_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define Run_Stop_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define Run_Stop_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define Run_Stop_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(Run_Stop__SIO)
    #define Run_Stop_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(Run_Stop__PC) && (CY_PSOC4_4200L)
    #define Run_Stop_USBIO_ENABLE               ((uint32)0x80000000u)
    #define Run_Stop_USBIO_DISABLE              ((uint32)(~Run_Stop_USBIO_ENABLE))
    #define Run_Stop_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define Run_Stop_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define Run_Stop_USBIO_ENTER_SLEEP          ((uint32)((1u << Run_Stop_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << Run_Stop_USBIO_SUSPEND_DEL_SHIFT)))
    #define Run_Stop_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << Run_Stop_USBIO_SUSPEND_SHIFT)))
    #define Run_Stop_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << Run_Stop_USBIO_SUSPEND_DEL_SHIFT)))
    #define Run_Stop_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(Run_Stop__PC)
    /* Port Configuration */
    #define Run_Stop_PC                 (* (reg32 *) Run_Stop__PC)
#endif
/* Pin State */
#define Run_Stop_PS                     (* (reg32 *) Run_Stop__PS)
/* Data Register */
#define Run_Stop_DR                     (* (reg32 *) Run_Stop__DR)
/* Input Buffer Disable Override */
#define Run_Stop_INP_DIS                (* (reg32 *) Run_Stop__PC2)

/* Interrupt configuration Registers */
#define Run_Stop_INTCFG                 (* (reg32 *) Run_Stop__INTCFG)
#define Run_Stop_INTSTAT                (* (reg32 *) Run_Stop__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define Run_Stop_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(Run_Stop__SIO)
    #define Run_Stop_SIO_REG            (* (reg32 *) Run_Stop__SIO)
#endif /* (Run_Stop__SIO_CFG) */

/* USBIO registers */
#if !defined(Run_Stop__PC) && (CY_PSOC4_4200L)
    #define Run_Stop_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define Run_Stop_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define Run_Stop_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define Run_Stop_DRIVE_MODE_SHIFT       (0x00u)
#define Run_Stop_DRIVE_MODE_MASK        (0x07u << Run_Stop_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins Run_Stop_H */


/* [] END OF FILE */
