/*******************************************************************************
* File Name: HallA.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_HallA_ALIASES_H) /* Pins HallA_ALIASES_H */
#define CY_PINS_HallA_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define HallA_0			(HallA__0__PC)
#define HallA_0_PS		(HallA__0__PS)
#define HallA_0_PC		(HallA__0__PC)
#define HallA_0_DR		(HallA__0__DR)
#define HallA_0_SHIFT	(HallA__0__SHIFT)
#define HallA_0_INTR	((uint16)((uint16)0x0003u << (HallA__0__SHIFT*2u)))

#define HallA_INTR_ALL	 ((uint16)(HallA_0_INTR))


#endif /* End Pins HallA_ALIASES_H */


/* [] END OF FILE */
