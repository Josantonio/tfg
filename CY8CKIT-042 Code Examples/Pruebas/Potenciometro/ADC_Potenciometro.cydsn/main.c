/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char string [100]; 
#define ADC_RANGE (2048)
#define V_MAX (ADC_RANGE*(10000+330)/(10000+2*330)) //Tensión máxima que se lee en el pin del pot
#define V_MIN (ADC_RANGE*330/(10000+2*330)) //Tensión mínima que se lee en el pin del pot
//#define RESOLUCION (2048/VDD) // nºbit/voltio
#define SP_MAX (4000) //Vel máxima [rpm]
#define SP_MIN (500) //Vel mínima [rpm]

int main(void)
{
    UART_Start();
    ADC_SAR_Start();
    
    uint16 SP;
    uint16 pot;
    
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        ADC_SAR_StartConvert();
        ADC_SAR_IsEndConversion(ADC_SAR_WAIT_FOR_RESULT);
        pot = ADC_SAR_GetResult16(0);
        SP = (pot-V_MIN)*(SP_MAX-SP_MIN)/(V_MAX-V_MIN)+SP_MIN;
        if(SP < SP_MIN){SP = SP_MIN;}
        if(SP > SP_MAX){SP = SP_MAX;}
        sprintf(string, "Potenciometro: %d\n",pot);        
        UART_UartPutString(string); 
        sprintf(string, "Velocidad: %d\n",SP);        
        UART_UartPutString(string); 
        CyDelay(1000);

    }
}

/* [] END OF FILE */
